# TbGaudi - A modular software framework for test-beam data analysis.

## Introduction
The package allows to handle the test beam data for a set of runs, as well as the set of different devices under test in one-go, and finally obtain an integrated workflow to present the results.
All code is written in C++, which is a general-purpose objective programming language.
A class based design makes it flexible to add any new features of the device under investigation following a plug-in scheme.

## General principles
The framework is being developed under the gcc 6.3.1 compiler with c++14 standard features used. The package is primarily based on ROOT6 libraries.
The main goal of this analysis framework is to perform the off-line analysis of the test beam data which are needed to be initially produced with with external software.
The baseline are NTuples containing track and cluster data information.

## Working at the lxplus s/w environment
It is very comfortable to utilize the CERN LXPLUS resources to handle the analysis (e.g. it's easy to shared test-beam data to be analysed using LXPLUS EOS area).
The dedicated automatic configuration has been prepared (experiment independent) in order to load necessary libraries and packages.
Once the repository is cloned to the User's area, the environment setup can be performed simply by running:
```
source {path_to_source}/Tb/setup-slc6-cvmfs.sh
```

## Available implementations and configuration
All packages compilation process is being handled with the CMake tool, hence there is a top level CMakeLists.txt which handle the general configuration and options.

### TbVeloPix analysis
* configuration
```
cmake -DBUILD_TBVELOPIX=ON {path_to_source}/Tb
```
* compilation
```
make -j4
```
* run
```
./TbVeloPix/TbVeloPixAnalysis
```

### TbUt analysis
* configuration
```
cmake -DBUILD_TBUT=ON {path_to_source}/Tb
```
* compilation
```
make -j4
```
* run
```
./TbVeloPix/TbUtAnalysis
```
