// std libriaries
#include <iostream>
#include <memory>

// ROOT libriaries
#include <TROOT.h>
#include <TApplication.h>


// TbGaudi libriaries
#include "TbGaudi.h"
#include "TbStyle.h"
#include "cxxopts.h"

// Project libriaries
#include "TbVeloPixAnalysis.h"

int main(int argc, char **argv)
{
    TbGaudi::Severity("Debug");

    if (argc > 1) {
        cxxopts::Options options(argv[0], "[INFO]:: Text UI mode - command line options");
        try {
            options.positional_help("[optional args]");
            options.show_positional_help();
            options.add_options()("help", "Print help");
            options.add_options("Analysis modes")
                    ("CCE", "Charge Collection Efficiency studies")
                    ("Calib_IRRAD", "Calibration study of IRRAD sensors")
                    ("iCCE_Bartek", "Charge Collection Efficiency studies for intrapixel")
                    ("iCCE_Maciek", "Charge Collection Efficiency studies for intrapixel")
                    ("Ecs", "Getting started analysis with ECS data")
                    ("TomlDev", "TOML library implementation development")
                    ("CsvDev", "Export to csv development");
            options.add_options("Job processing")
                    ("NEvt", "Number of events to be pocessed in TTree event loop (default -1, all events)",cxxopts::value<int>(),"NUMBER")
                    ("Input", "Specify input data file", cxxopts::value<std::string>(), "FILE");

            auto results = options.parse(argc, argv);
            if (results.count("help")) {
                std::cout << options.help({"Analysis modes"
                                          ,"Job processing"}) << std::endl;
                std::exit(EXIT_SUCCESS);
            }

            // --------------------------------------------------------------------
            auto cmdopts = std::move(results);

            // --------------------------------------------------------------------
            auto rootapp = std::make_unique<TApplication>("App", &argc, argv);
            auto MyAnalysis = std::make_unique<TbVeloPixAnalysis>();

            // --------------------------------------------------------------------
            // ** Job processing configuration ***
            if (cmdopts.count("NEvt"))
                RootData::UsrNEntries(cmdopts["NEvt"].as<int>());
            if (cmdopts.count("Input"))
                TbData::TbDBFile(cmdopts["Input"].as<std::string>());

            // --------------------------------------------------------------------
            // ** Analysis modes ***
            if (cmdopts.count("CCE"))           MyAnalysis->Ana_CCE_IRRAD();
            if (cmdopts.count("Calib_IRRAD"))   MyAnalysis->Ana_Calib_IRRAD();
            if (cmdopts.count("iCCE_Bartek"))   MyAnalysis->Ana_iCCE_Bartek();
            if (cmdopts.count("iCCE_Maciek"))   MyAnalysis->Ana_iCCE_Maciek();
            if (cmdopts.count("Ecs"))           MyAnalysis->Ana_ECS_General();
            if (cmdopts.count("TomlDev"))       MyAnalysis->DevToml();
            if (cmdopts.count("CsvDev"))        MyAnalysis->DevExportToCsv();

            // --------------------------------------------------------------------
            //PressEnterToQuit();
            rootapp->Run(!gROOT->IsBatch());

        }
        catch (const cxxopts::OptionException& e) {
            std::cout << "error parsing options: " << e.what() << std::endl;
            std::exit(EXIT_FAILURE);
        }

    }
    else
        std::cout << "[ERROR]:: Command line options missing (use '" << argv[0] << " --help' if needed)" << std::endl;

    std::exit(EXIT_SUCCESS);
}

