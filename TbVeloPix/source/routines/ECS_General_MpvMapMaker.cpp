#include "TbVeloPixAnalysis.h"
#include "TbJobPlotter.h"
#include "TbGaudi.h"
#include "DutSvc.h"
#include "memory.h"
#include <functional>

#include "TH1D.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::ECS_General_MpvMapMaker()
{

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::ECS_General_MpvMapMaker is executed.");

    for (auto& iTbJob : GetTbJobsCollection() ) {
        //_______________________________________________________________________________________
        // SET GLOBALLY AVAILABLE TBRUN CURRENTLY BEING PROCESSED!
        TbJob::CurrentTbJob(iTbJob);

        //_______________________________________________________________________________________
        // List general info about the test-beam run being processed
        std::cout<<"[INFO]:: TbVeloPixAnalysis:: processing: "<< std::endl;
        iTbJob->PrintAllInfo();

        //_______________________________________________________________________________________
        // Define data containers, ROOT hists, RooDataSets, RooCategories etc.
        iTbJob->DefineDutHist("ChargeClSizeAll",79,1.,80.);
        iTbJob->DefineDutHist("ChargeClSize1",79,1.,80.);
        iTbJob->DefineDutHist("ChargeClSize2",79,1.,80.);

        //_______________________________________________________________________________________
        // TTree variables definition
        auto tree = iTbJob->RootDataPtr()->GetTTree("Clusters");

        Int_t var_row[200];   // max from KEPLER::m_maxclustersize
        Int_t var_col[200];
        UInt_t var_clSize;
        Double_t var_clToT;

        tree->SetBranchAddress("hRow", var_row);
        tree->SetBranchAddress("hCol", var_col);
        tree->SetBranchAddress("clSize", &var_clSize);
        tree->SetBranchAddress("clToT", &var_clToT);


        //_______________________________________________________________________________________
        // EVENT LOOP AND EVENT SELECTION
        auto Nentries = tree->GetEntriesFast();
        std::cout<<"[INFO]:: TbVeloPixAnalysis::EventLoop:: NEntries "<< Nentries << std::endl;
        auto runNumber = iTbJob->GetParam("RunNumber");

        for (int it = 0; it < Nentries; it++) {
            tree->GetEntry(it);

            if(it%1000==0 && TbGaudi::Verbose()) {
                std::cout << "[INFO]:: TbVeloPixAnalysis:: Run::"
                          << runNumber << " EventLoop:: Processed event "<< it << " ("
                          << std::setprecision(2)<< (static_cast<double>(it)/Nentries) * 100 << "%)" << std::endl;
            }

            // Preselection:
            if (var_clToT < 10)   continue;    // unphysical entries
            if (var_clSize > 20)  continue;    // max clusters size
            if (!iTbJob->IsGoodClusterSize(var_clSize)) continue;   // check and count
            // Take into account each pixel from the cluster, but keep events where all pixes belongs
            // to single DUT bin (cluster is laying in the single DUT bin)

            if (!iTbJob->IsClusterInsideDutBin(var_col, var_row, var_clSize)) continue; // check and count

            iTbJob->CountDutHit(var_row[0],var_col[0]); // count a dut bin/cluster statistic hit map

            // Fill histograms, RooDataSets, etc.
            iTbJob->DutHistPtr("ChargeClSizeAll")->Fill(var_clToT);
            if(var_clSize==1)
                iTbJob->DutHistPtr("ChargeClSize1")->Fill(var_clToT);
            if(var_clSize==2)
                iTbJob->DutHistPtr("ChargeClSize2")->Fill(var_clToT);

        }

        iTbJob->PrintCountersStatistic();
        //iTbJob->DutBinningPtr()->PrintStatistic();
        iTbJob->PlotterPtr()->DrawDutStatistic();       // hists are automatically exported to TbResults

        //_______________________________________________________________________________________
        // write histograms into .root file, if requested by the User.
//        if(RootSvc::WriteHistToNTuple()) {
//            // ... all types, TODO:: includ TH2D of statistic if it was being created
//            iTbJob->WriteEventLoopHistToNTuple("ChargeClSizeAll");
//            iTbJob->WriteEventLoopHistToNTuple("ChargeClSize1");
//            iTbJob->WriteEventLoopHistToNTuple("ChargeClSize2");
//        }

        //_______________________________________________________________________________________
        // Perform data fitting
        auto chargeFitter =  std::make_unique<ChargeFitter>(iTbJob);
        //chargeFitter->SetData(iTbJob, "ChargeClSizeAll", FitDataType::TH1);
        //chargeFitter->FC(-1); // run fitting

        chargeFitter->SetData(iTbJob, "ChargeClSize1", FitDataType::TH1);
        chargeFitter->FC(1); // run fitting

        //chargeFitter->SetData(iTbJob, "ChargeClSize2", FitDataType::TH1);
        //chargeFitter->FC(2); // run fitting

        // fitting of other data and routines can be called here, simply:
        // 1. SetData(...)
        // 2. Run fitting method, e.g. Roofit() or newly implemented...

        //_______________________________________________________________________________________
        // At the end of each iteration release memory, loop related data are no needed anymore!
        iTbJob->ClearEventLoopCollectedData(); // one method to clear all!

    } // TbJobCollection() loop

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::EcsDataMpvMapMaker THE END!");
}

////////////////////////////////////////////////////////////////////////////////////
///