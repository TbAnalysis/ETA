#include "TbVeloPixAnalysis.h"
#include "TbJobPlotter.h"
#include "IRRAD.h"
#include "TbGaudi.h"
#include "DutSvc.h"
#include "memory.h"
#include <functional>

#include "TH1D.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"

////////////////////////////////////////////////////////////////////////////////////
///
#include "TTreeReader.h"
#include "TTreeReaderArray.h"
#include "ROOT/TTreeProcessorMT.hxx"
#include "TThread.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::iCCE_Bartek_MTMpvMapMaker()
{
    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: iCCE MT MPVMapMaker is executed (Bartek)...");

    for (auto& iTbJob : GetTbJobsCollection() ) {
        //_______________________________________________________________________________________
        // SET GLOBALLY AVAILABLE TBRUN CURRENTLY BEING PROCESSED!
        TbJob::CurrentTbJob(iTbJob);

        //_______________________________________________________________________________________
        // List general info about the test-beam run being processed
        std::cout<<"[INFO]:: TbVeloPixAnalysis:: processing: "<< std::endl;
        iTbJob->PrintAllInfo();

        //_______________________________________________________________________________________
        // Define data containers, ROOT hists, RooDataSets, RooCategories etc.
        // Note: This is done with TThreadedObjects<>
        iTbJob->MTDefineDutBinning2DHist("PixelHits",DutPix::NBinsX(),0.,Dut::PitchSize(),DutPix::NBinsY(),0.,Dut::PitchSize());
        iTbJob->MTDefineDutBinningHist("Charge",200,0.,30000.);               // to dump to files
        iTbJob->MTDefineDutBinningPixHist("PixCharge",200,0.,30000.);         // to dump to file
        // TODO:: iTbJob->MTDefineDutPixHist("PixCharge",200,0.,30000.);         // to dump to file

        // Histograms with internal uniform binning of pixel area:
        // Note: pixel binning:
        // There is no sense to put 55 x 55 (1 um) binning. Telescope resolution is 3 um in best.
        // Binning of pixel is taken from DutPix::NBinsX(Y) static variable
        // NOTE: - the following will create only basic objects, like RooRealVar as being TThreadedObject
        //       - the composite objects, like RooDataSet, is being create on demand for a given thread.
        //iTbJob->MTDefineDutBinningPixRRealVar("PixCharge",0.,30000.,"electrons"); // for fitting
        iTbJob->DefineRooRealVar("Charge",0.,30000.,"electrons"); // template variable to construct RDS
        //iTbJob->MTDefineDutPixDataSet(); // construct from the all variables defined above;

        //_______________________________________________________________________________________
        ROOT::TTreeProcessorMT tp(iTbJob->RootDataFile(),"TbTupleWriter/Clusters");


        //_______________________________________________________________________________________
        // EVENT LOOP AND EVENT SELECTION
        auto Nentries = iTbJob->RootDataPtr()->GetNEntries("Clusters");
        std::cout<<"[INFO]:: TbVeloPixAnalysis::EventLoop:: NEntries "<< Nentries << std::endl;

        auto runNumber = iTbJob->GetParam("RunNumber");

        //_______________________________________________________________________________________
        auto readTTree = [&](TTreeReader &myReader) {
            TTreeReaderArray<Int_t> rowRA(myReader, "hRow");
            TTreeReaderArray<Int_t> colRA(myReader, "hCol");
            TTreeReaderValue<UInt_t> clSizeRV(myReader, "clSize");
            TTreeReaderValue<Bool_t> clIsTrkRV(myReader, "clIsTracked");
            TTreeReaderValue<Double_t> clChargeRV(myReader, "clCharge");
            TTreeReaderValue<Double_t> clGxRV(myReader, "clGx");
            TTreeReaderValue<Double_t> clGyRV(myReader, "clGy");

//            auto tID = std::this_thread::get_id();
//            RooRealVar rrv(("PixCharge"+idtos(tID)).c_str(),"PixCharge",0.,30000.,"electrons");
//            RooArgSet RAS;
//            RAS.add(rrv);
            // For the performance reason, we have to omit calling the TThreadedObject<>::Get() method in the loop!
            auto histDutChargeCollection = iTbJob->MTThisSlotTH1DCollection("Charge");
            auto histPixHitMapCollection = iTbJob->MTThisSlotTH2DCollection("PixelHits");
            auto histPixChargeCollection = iTbJob->MTThisSlotTH1DCollection("PixCharge");

            //auto rrvPixChargeCollection  = iTbJob->MTThisSlotRRVCollection("PixCharge");
//            TThread::Lock();
//            std::cout<<"[DEBUG]::thread id " << std::this_thread::get_id()<<std::endl;
//            TThread::UnLock();
            auto dutPixTbRdsCollection  = iTbJob->ThisSlotRooDataSetCollection(); // there is only one TbRDS per job.
                                            // TODO: ThisSlotRooDataSetCollection

//            TThread::Lock();
//            std::cout<<"[DEBUG]:: rds coll.size: "<<rdsPixCollection.size()<<std::endl;
////            std::cout<<"[DEBUG]:: RDS COLLECTION:" <<std::endl;
////            for(auto& iRds : rdsPixCollection){
////                std::cout<<"[DEBUG]::RDS " << iRds->GetName() <<std::endl;
////            }
//            TThread::UnLock();

//            TThread::Lock();
//            std::cout<<"[DEBUG]:: rds coll.size: "<<rdsPixCollection.size()<<std::endl;
//            TThread::UnLock();

            //auto id = TThread::SelfId();
            //auto entriesRange = myReader.GetEntriesRange(); to be used with newer ROOT
            //auto nEntries = entriesRange.second - entriesRange.first;
            //unsigned long counter(0);
            while (myReader.Next()) {
                //++counter;
                //if(counter%20000==0) {
                //    TThread::Printf("INFO:: (Thread:%ld):: Processed %3.2lf percent", id,(float)counter/nEntries * 100);
                //}
                // Fill histograms, RooDataSets, etc.
                auto dutBinNumber = iTbJob->DutBinNumber(rowRA[0],colRA[0]); //all pixels lay in the same bin!
                if(dutBinNumber>=0) { // '-1' is returned for outsiders!
                    auto x = DutPix::IntraPixPosition(*clGxRV);
                    auto y = DutPix::IntraPixPosition(*clGyRV);
                   histDutChargeCollection.at(dutBinNumber)->Fill(*clChargeRV);
                   histPixHitMapCollection.at(dutBinNumber)->Fill(x,y);
                   auto globalPixBin = DutPix::GlobalBinNumber(dutBinNumber,DutPix::BinX(x),DutPix::BinY(y));
                   histPixChargeCollection.at(globalPixBin)->Fill(*clChargeRV);

                   dutPixTbRdsCollection.at(globalPixBin)->Fill("Charge",*clChargeRV);
                    // iTbJob->MTDut2DHistPtr("PixelHits", dutBinNumber)->Fill(x,y);
                    // iTbJob->MTDutHistPtr("Charge", dutBinNumber)->Fill(*clChargeRV);
                   // iTbJob->MTDutPixHistPtr("PixCharge", dutBinNumber,x,y)->Fill(*clChargeRV);

                    //auto rrvPixCharge = rrvPixChargeCollection.at(globalPixBin);
                    //rrvPixCharge->setVal(*clChargeRV);
                    //rrv.setVal(*clChargeRV);
//                    TThread::Lock();
                    //std::cout<<"[DEBUG]::thread id " << std::this_thread::get_id() <<"\n"
//                    std::cout << "Var addres: " << rrvPixCharge << " "
//                             << "Var name: " << rrvPixCharge->GetName() << " "
//                             << "RDS: " << rdsPixCollection.at(globalPixBin)->GetName() << std::endl;
//                    auto ras = rdsPixCollection.at(globalPixBin)->get();
//                    if(!ras->find(rrvPixCharge->GetName())){
//                        TThread::Lock();
//                        std::cout<<"[DEBUG]:: huston we got problem" << std::endl;
//                        TThread::UnLock();
//                    }
//                    ((RooRealVar*)ras->find(rrvPixCharge->GetName()))->setVal(*clChargeRV);
                    //ras->SetRealValue(rrvPixCharge->GetName(),0.0002);

//                    TThread::Lock();
//                    std::cout<<"[DEBUG]:: thread: " << std::this_thread::get_id() << std::endl;
//                    std::cout<<"[DEBUG]:: rrv name: " << rrvPixCharge->GetName() << std::endl;
//                    TThread::UnLock();


//                    const RooAbsArg& var = *rrvPixCharge;

                    //delete RAS;
                    //rdsPixCollection.at(globalPixBin)->addFast(*ras);

                    //rdsPixCollection.at(globalPixBin)->GetName();
//                    TThread::UnLock();
                    //
                    //iTbJob->MTDutPixDataSetRRVarPtr("PixCharge", dutBinNumber,x,y)->setVal(*clChargeRV);
                    //iTbJob->MTDutPixDataSetPtr("PixCharge", dutBinNumber,x,y);
                    //iTbJob->MTAddToDutPixDataSet(dutBinNumber,x,y); // TThreadedObject is being created
                                                                    // for all usr defined RooRealVar
                }
            }
//            TThread::Lock();
//            rdsPixCollection.at(0)->Print("v");
//            const RooArgSet* row = rdsPixCollection.at(0)->get() ;
//            row->Print("v") ;
//
//            TThread::UnLock();
        };
        //_______________________________________________________________________________________
        // Launch the parallel processing of the tree
        std::cout<<"[INFO]:: TbVeloPixAnalysis:: Enter parallel processing loop..." << std::endl;
        tp.Process(readTTree);
        // Merge all threads together
        iTbJob->MTHistMerge();
        iTbJob->MTRooDataSetMerge();

        auto c1 = new TCanvas("c1","c1",600,600);
        iTbJob->Dut2DHistPtr("PixelHits", 0)->DrawCopy("COLZ");
        auto c2 = new TCanvas("c2","c2",600,600);
        iTbJob->Dut2DHistPtr("PixelHits", 1)->DrawCopy("COLZ");
        auto c3 = new TCanvas("c3","c3",600,600);
        iTbJob->DutHistPtr("Charge", 1)->DrawCopy();
        auto c4 = new TCanvas("c4","c4",600,600);
        iTbJob->DutPixHistPtr("PixCharge", 1,5,5)->DrawCopy();



        /*
        for (int it = 0; it < Nentries; it++) {
            tree->GetEntry(it);

            // Preselection:
            if (var_clCharge < 1000) continue;    // unphysical entries
            if (var_clSize > 20)     continue;    // max clusters size
            if (!var_clIsTrk)        continue;    // process only associated clusters

            //Take into account the cluster size requirement
            if (!iTbJob->IsGoodClusterSize(var_clSize)) continue;

            // Take into account each pixel from the cluster, but keep events where all pixes belongs
            // to single DUT bin (cluster is laying in the single DUT bin)
            //if (!iTbJob->IsClusterInsideDutBin(var_col, var_row, var_clSize)) continue;
            // UWAGA: to generuje zanizona statystyke na liniach binowania,
            // widac ze dla elliptic jest odwrocony x z y ! todo: debug

            if(it%10000==0 && TbGaudi::Verbose()) {
                std::cout << "[INFO]:: TbVeloPixAnalysis:: Run::"
                          << runNumber << " EventLoop:: Processed event "<< it << " ("
                          << std::setprecision(2)<< (static_cast<double>(it)/Nentries) * 100 << "%)" << std::endl;
            }

            iTbJob->CountDutHit(var_row[0],var_col[0]); // count a dut bin/pixel statistic hit map


            // Fill histograms, RooDataSets, etc.
            auto dutBinNumber = iTbJob->DutBinNumber(var_row[0],var_col[0]); //all pixels lay in the same bin!

            auto dutPix = iTbJob->DutPixPtr();
            if(dutBinNumber>=0) { // '-1' is returned for outsiders!
                auto x = dutPix->IntraPixPosition(var_clGx);
                auto y = dutPix->IntraPixPosition(var_clGy);
                iTbJob->Dut2DHistPtr("PixelHits", dutBinNumber)->Fill(x,y);
                iTbJob->DutHistPtr("Charge", dutBinNumber)->Fill(var_clCharge);

                iTbJob->DutPixHistPtr("PixCharge", dutBinNumber,x,y)->Fill(var_clCharge);

                iTbJob->DutPixDataSetRCatPtr("PixCharge", dutBinNumber,x,y)->setIndex(1);   //temporary, set everything as signal
                iTbJob->DutPixDataSetRRVarPtr("PixCharge", dutBinNumber,x,y, catIndex=1(default))->setVal(var_clCharge);
                iTbJob->AddToDutPixDataSet("PixCharge", dutBinNumber,x,y);
            }
        }

        iTbJob->PrintCountersStatistic();
        //iTbJob->DutBinningPtr()->PrintStatistic();
        iTbJob->PlotterPtr()->DrawDutStatistic(true); // true - draw interactively

        //_______________________________________________________________________________________
        // Perform data fitting
        auto chargeFitter =  std::make_unique<ChargeFitter>(iTbJob);
        chargeFitter->SetData(iTbJob, "PixCharge", FitDataType::RooDataSet);
        chargeFitter->Roofit("LandauGaussConv"); // run fitting


        // fitting of other data and routines can be called here, simply:
        // 1. SetData
        // 2. Run fitting method, e.g. Roofit() or other...

         */
        //_______________________________________________________________________________________
        // At the end of each iteration release memory, loop related data are no needed anymore!
        iTbJob->ClearEventLoopCollectedData(); // one method to clear all!

    } // TbJobCollection() loop

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: iCCE MT MPVMapMaker (Bartek): THE END!");
}

////////////////////////////////////////////////////////////////////////////////////
///