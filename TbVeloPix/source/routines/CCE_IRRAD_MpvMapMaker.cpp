#include "TbVeloPixAnalysis.h"
#include "TbJobPlotter.h"
#include "TbGaudi.h"
#include "DutSvc.h"
#include "memory.h"
#include <functional>

#include "TH1D.h"
#include "TLegend.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "RooPlot.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::CCE_IRRAD_MpvMapMaker()
{

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::MPVMapMaker is executed.");
    auto jobs = GetTbJobsCollection();
    for (auto& iTbJob : jobs ) {
        //_______________________________________________________________________________________
        // SET GLOBALLY AVAILABLE TBRUN CURRENTLY BEING PROCESSED!
        TbJob::CurrentTbJob(iTbJob);

        //_______________________________________________________________________________________
        // List general info about the test-beam run being processed
        std::cout<<"[INFO]:: TbVeloPixAnalysis:: processing: "<< std::endl;
        iTbJob->PrintAllInfo();

        //_______________________________________________________________________________________
        // Define data containers, ROOT hists, RooDataSets, RooCategories etc.
        iTbJob->DefineDutBinningHist("Charge",200,0.,20000.);

        iTbJob->DefineRooRealVar("Charge",0.,30000.,"electrons");       // template variable to construct RooDataSet instances
        iTbJob->DefineRooRealVar("clSize",0.,20.);                      // template variable to construct RooDataSet instances
        
        iTbJob->DefineRooCategory("Signal",1); // for all variables - it's being set per RDS
        iTbJob->DefineRooCategory("Noise",0); // for all variables - it's being set per RDS
        
        auto jobDutTbRds = iTbJob->CreateDutTbRooDataSet();                     // creates single RooDataSet instance 
        auto jobDutBinnedTbRds = iTbJob->CreateDutBinningTbRooDataSet();  // creates collection of RooDataSets according to DUT binning scheme
        //_______________________________________________________________________________________
        // Define loop counters
        //iTbJob->DefineEventCounter("Spill untagged",0);
        //iTbJob->DefineEventCounter("Out of spill",1);
        //_______________________________________________________________________________________

        // TTree variables definition
        auto tree = iTbJob->RootDataPtr()->GetTTree("Clusters");
        Int_t var_row[200];   // max from KEPLER::m_maxclustersize
        Int_t var_col[200];
        UInt_t var_clSize;
        Int_t var_clisInSpill;
        Bool_t var_clIsTrk;
        Double_t var_clCharge;

        tree->SetBranchAddress("hRow", var_row);
        tree->SetBranchAddress("hCol", var_col);
        tree->SetBranchAddress("clSize", &var_clSize);
        tree->SetBranchAddress("clCharge", &var_clCharge);
        tree->SetBranchAddress("clIsTracked", &var_clIsTrk);
        //tree->SetBranchAddress("clisInSpill", &var_clisInSpill);


        //_______________________________________________________________________________________
        // EVENT LOOP AND EVENT SELECTION
        auto Nentries = iTbJob->RootDataPtr()->GetNEntries("Clusters");
        std::cout<<"[INFO]:: TbVeloPixAnalysis::EventLoop:: NEntries "<< Nentries << std::endl;
        auto runNumber = iTbJob->GetParam("RunNumber");
        for (int it = 0; it < Nentries; it++) {
            tree->GetEntry(it);

            // Preselection:
            if (var_clCharge < 1000) continue;    // unphysical entries
            if (var_clSize > 20)     continue;    // max clusters size
            if (!var_clIsTrk)        continue;    // process only associated clusters

            //Take into account the cluster size requirement
            //if (!iTbJob->IsGoodClusterSize(var_clSize)) continue;

            // Take into account each pixel from the cluster, but keep events where all pixes belongs
            // to single DUT bin (cluster is laying in the single DUT bin)
            //if (!iTbJob->IsClusterInsideDutBin(var_col, var_row, var_clSize)) continue;
            // UWAGA: to generuje zanizona statystyke na liniach binowania,
            // widac ze dla elliptic jest odwrocony x z y ! todo: debug

            if(it%10000==0 && TbGaudi::Verbose()) {
                std::cout << "[INFO]:: TbVeloPixAnalysis:: Run::"
                          << runNumber << " EventLoop:: Processed event "<< it << " ("
                          << std::setprecision(2)<< (static_cast<double>(it)/Nentries) * 100 << "%)" << std::endl;
            }
            //std::cout<<"[INFO]:: TbVeloPixAnalysis::EventLoop:: DEBUG 1 " << std::endl;
            iTbJob->CountDutHit(var_row[0],var_col[0]); // count a dut bin/pixel statistic hit map
            //std::cout<<"[INFO]:: TbVeloPixAnalysis::EventLoop:: DEBUG 2 " << std::endl;

            // Fill histograms, RooDataSets, etc.
            auto dutBinNumber = iTbJob->DutBinNumber(var_row[0],var_col[0]); //all pixels lay in the same bin!
            //std::cout<<"[INFO]:: TbVeloPixAnalysis::EventLoop:: DEBUG 3 " << std::endl;

            if(dutBinNumber>=0) { // '-1' is returned for outsiders!
                //std::cout<<"[DEBUG]:: TbVeloPixAnalysis::EventLoop:: dutBinNumber " << dutBinNumber << std::endl;
                iTbJob->DutHistPtr("Charge", dutBinNumber)->Fill(var_clCharge);
                //std::cout<<"[INFO]:: TbVeloPixAnalysis::EventLoop:: DEBUG 5 " << std::endl;
                
                // -----------------------------------------------------------------------------
                // The main interface to filling RDS with event loop data! 
                int evtFlag = 1; // set each event as a signal
                jobDutTbRds->Fill("Charge",var_clCharge,evtFlag);
                jobDutTbRds->Fill("clSize",var_clSize,evtFlag);
                jobDutBinnedTbRds->At(dutBinNumber)->Fill("Charge",var_clCharge,evtFlag);
                jobDutBinnedTbRds->At(dutBinNumber)->Fill("clSize",var_clSize,evtFlag);
            }
        }
        jobDutTbRds->GetRooDataSetPtr()->Print();
        auto chargeFrame = iTbJob->GetRooRealVar("clSize")->frame(RooFit::Bins(10),RooFit::Title("DUT cluster size data")) ;
        jobDutTbRds->GetRooDataSetPtr()->plotOn(chargeFrame, RooFit::MarkerColor(1), RooFit::Name("AllData"));
        jobDutBinnedTbRds->At(0)->GetRooDataSetPtr()->plotOn(chargeFrame, RooFit::MarkerColor(2), RooFit::Name("DutBin0"));
        jobDutBinnedTbRds->At(1)->GetRooDataSetPtr()->plotOn(chargeFrame, RooFit::MarkerColor(3), RooFit::Name("DutBin1"));

        new TCanvas("dev","dev",600,600) ;
        gPad->SetLeftMargin(0.15);
        chargeFrame->GetYaxis()->SetTitleOffset(1.4) ;
        chargeFrame->Draw();

        auto leg = new TLegend(0.65,0.73,0.86,0.87);
        leg->AddEntry("AllData","AllData", "P");
        leg->AddEntry("DutBin0","DutBin0", "P");
        leg->AddEntry("DutBin1","DutBin1", "P");
        leg->Draw();


        iTbJob->PrintCountersStatistic();
        //iTbJob->DutBinningPtr()->PrintStatistic();
        //iTbJob->PlotterPtr()->DrawDutStatistic(true); // true - print them interactively

        //_______________________________________________________________________________________
        // Perform data fitting
        auto chargeFitter =  std::make_unique<ChargeFitter>(iTbJob);
        chargeFitter->SetData(iTbJob, "Charge", FitDataType::RooDataSet);
        //chargeFitter->SetData(iTbJob, "Charge", FitDataType::TH1);

        chargeFitter->Roofit("LandauGaussConv"); // run fitting
        // chargeFitter->Roofit("TwoLandauGaussConv"); // run fitting
        //chargeFitter->FC();                    // run fitting

        // fitting of other data and routines can be called here, simply:
        // 1. SetData
        // 2. Run fitting method, e.g. Roofit() or other...

        //_______________________________________________________________________________________
        // At the end of each iteration release memory, loop related data are no needed anymore!
        iTbJob->ClearEventLoopCollectedData(); // one method to clear all!

    } // TbJobCollection() loop

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::MPVMapMaker THE END!");
}

////////////////////////////////////////////////////////////////////////////////////
///