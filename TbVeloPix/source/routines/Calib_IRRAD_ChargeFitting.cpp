#include "TbVeloPixAnalysis.h"
#include "TbJobPlotter.h"
#include "IRRAD.h"
#include "TbGaudi.h"
#include "DutSvc.h"
#include "memory.h"
#include <functional>

#include "TH1D.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::Calib_IRRAD_ChargeFitting()
{
    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::Calib_IRRAD_ChargeFitting is executed...");
    auto jobs = GetTbJobsCollection();
    for (auto& iTbJob : jobs ) {
        //_______________________________________________________________________________________
        // SET GLOBALLY AVAILABLE TBRUN CURRENTLY BEING PROCESSED!
        TbJob::CurrentTbJob(iTbJob);

        //_______________________________________________________________________________________
        // List general info about the test-beam run being processed
        std::cout<<"[INFO]:: TbVeloPixAnalysis:: processing: "<< std::endl;
        iTbJob->PrintAllInfo();

        //_______________________________________________________________________________________
        // Define data containers, ROOT hists, RooDataSets, RooCategories etc.
        iTbJob->DefineDutBinningHist("Charge",100,0.,20000.);

        // Template variables to construct RooDataSet instances
        iTbJob->DefineRooRealVar("Charge",0.,20000.,"electrons"); 

        // Create collection of RooDataSets according to DUT binning scheme          
        auto jobDutBinnedTbRds = iTbJob->CreateDutBinningTbRooDataSet();  

        //_______________________________________________________________________________________

        // TTree variables definition
        auto tree = iTbJob->RootDataPtr()->GetTTree("Clusters");
        Int_t var_row[200];   // max from KEPLER::m_maxclustersize
        Int_t var_col[200];
        UInt_t var_clSize;
        Int_t var_clisInSpill;
        Bool_t var_clIsTrk;
        Double_t var_clCharge;

        tree->SetBranchAddress("hRow", var_row);
        tree->SetBranchAddress("hCol", var_col);
        tree->SetBranchAddress("clSize", &var_clSize);
        tree->SetBranchAddress("clCharge", &var_clCharge);
        tree->SetBranchAddress("clIsTracked", &var_clIsTrk);

        //_______________________________________________________________________________________
        // EVENT LOOP AND EVENT SELECTION
        auto Nentries = iTbJob->RootDataPtr()->GetNEntries("Clusters");
        std::cout<<"[INFO]:: TbVeloPixAnalysis::EventLoop:: NEntries "<< Nentries << std::endl;
        auto runNumber = iTbJob->GetParam("RunNumber");
        for (int it = 0; it < Nentries; it++) {
            tree->GetEntry(it);

            // Preselection:
            if (var_clCharge < 1000) continue;    // unphysical entries
            if (var_clSize > 20)     continue;    // max clusters size
            if (!var_clIsTrk)        continue;    // process only associated clusters

            //Take into account the cluster size requirement
            if (!iTbJob->IsGoodClusterSize(var_clSize)) continue;

            // TODO: TbGaudi::PrintEvtLoopStatus(iTbJob,"TbVeloPixAnalysis");

            if(it%10000==0 && TbGaudi::Verbose()) {
                std::cout << "[INFO]:: TbVeloPixAnalysis:: Run::"
                          << runNumber << " EventLoop:: Processed event "<< it << " ("
                          << std::setprecision(2)<< (static_cast<double>(it)/Nentries) * 100 << "%)" << std::endl;
            }

            iTbJob->CountDutHit(var_row[0],var_col[0]); // count a dut bin/pixel statistic hit map

            // Fill histograms, RooDataSets, etc.
            // ________________________________________________________________________________
            // Get global bin number 
            // clSize = 1 // it's obvious to take first pixel
            // clSize > 1 // all pixels lay in the same bin, hence we can take one first pixel for this evaluation
            auto dutBinNumber = iTbJob->DutBinNumber(var_row[0],var_col[0]); 

            if(dutBinNumber>=0) { // '-1' is returned for outsiders!
                iTbJob->DutHistPtr("Charge", dutBinNumber)->Fill(var_clCharge);
                jobDutBinnedTbRds->At(dutBinNumber)->Fill("Charge",var_clCharge);

            }
        }

        iTbJob->PrintCountersStatistic();
        iTbJob->PlotterPtr()->DrawDutStatistic(true); // true - print them interactively

        //_______________________________________________________________________________________
        // Perform data fitting
        auto chargeFitter =  std::make_unique<ChargeFitter>(iTbJob);
        chargeFitter->SetData(iTbJob, "Charge", FitDataType::RooDataSet);
        chargeFitter->Roofit("LandauGaussConv"); // run fitting

        //_______________________________________________________________________________________
        // At the end of each iteration release memory, loop related data are no needed anymore!
        iTbJob->ClearEventLoopCollectedData(); // one method to clear all!

    } // End of TbJobCollection() loop

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::MPVMapMaker THE END!");
}

////////////////////////////////////////////////////////////////////////////////////
///