#include "TbVeloPixAnalysis.h"
#include "TbJobPlotter.h"
#include "TbGaudi.h"
#include "DutSvc.h"
#include "memory.h"
#include <functional>

#include "TH1D.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::iCCE_Maciek_MpvMapMaker()
{

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: iCCE MPVMapMaker is executed (Maciek)...");

    for (auto& iTbJob : GetTbJobsCollection() ) {
        //_______________________________________________________________________________________
        // SET GLOBALLY AVAILABLE TBRUN CURRENTLY BEING PROCESSED!
        TbJob::CurrentTbJob(iTbJob);

        //_______________________________________________________________________________________
        // List general info about the test-beam run being processed
        std::cout<<"[INFO]:: TbVeloPixAnalysis:: processing: "<< std::endl;
        iTbJob->PrintAllInfo();

        //_______________________________________________________________________________________
        // Define data containers, ROOT hists, RooDataSets, RooCategories etc.
        iTbJob->DefineDutBinning2DHist("PixelHits",DutPix::NBinsX(),0.,Dut::PitchSize(),DutPix::NBinsY(),0.,Dut::PitchSize());

        // Histograms with internal uniform binning of pixel area:
        // Note: pixel binning:
        // There is no sense to put 55 x 55 (1 um) binning. Telescope resolution is 3 um in best.
        // Binning of pixel is taken from DutPix::NBinsX(Y) static variable
        iTbJob->DefineDutBinningHist("Charge",200,0.,30000.);               // to dump to files
        iTbJob->DefineDutBinningPixHist("PixCharge",200,0.,30000.);         // to dump to file
        // OBSOLETE iTbJob->DefineDutBinningPixDataSet("PixCharge",0.,30000.,"electrons"); // for fitting
        // OBSOLETE iTbJob->DefineDutBinningPixDataSetType("PixCharge","Signal",1);        // RooCategory::defineType(...)


        //_______________________________________________________________________________________
        // TTree variables definition
        auto tree = iTbJob->RootDataPtr()->GetTTree("Clusters");
        Int_t var_row[200];   // max from KEPLER::m_maxclustersize
        Int_t var_col[200];
        UInt_t var_clSize;
        Int_t var_clisInSpill;
        Bool_t var_clIsTrk;
        Double_t var_clCharge;
        Double_t var_clGx;
        Double_t var_clGy;

        tree->SetBranchAddress("hRow", var_row);
        tree->SetBranchAddress("hCol", var_col);
        tree->SetBranchAddress("clSize", &var_clSize);
        tree->SetBranchAddress("clCharge", &var_clCharge);
        tree->SetBranchAddress("clIsTracked", &var_clIsTrk);
        tree->SetBranchAddress("clGx", &var_clGx);
        tree->SetBranchAddress("clGy", &var_clGy);

        //_______________________________________________________________________________________
        // EVENT LOOP AND EVENT SELECTION
        auto Nentries = iTbJob->RootDataPtr()->GetNEntries("Clusters");
        std::cout<<"[INFO]:: TbVeloPixAnalysis::EventLoop:: NEntries "<< Nentries << std::endl;
        auto runNumber = iTbJob->GetParam("RunNumber");
        for (int it = 0; it < Nentries; it++) {
            tree->GetEntry(it);

            // Preselection:
            if (var_clCharge < 1000) continue;    // unphysical entries
            if (var_clSize > 20)     continue;    // max clusters size
            if (!var_clIsTrk)        continue;    // process only associated clusters

            //Take into account the cluster size requirement
            if (!iTbJob->IsGoodClusterSize(var_clSize)) continue;

            // Take into account each pixel from the cluster, but keep events where all pixes belongs
            // to single DUT bin (cluster is laying in the single DUT bin)
            //if (!iTbJob->IsClusterInsideDutBin(var_col, var_row, var_clSize)) continue;
            // UWAGA: to generuje zanizona statystyke na liniach binowania,
            // widac ze dla elliptic jest odwrocony x z y ! todo: debug

            if(it%10000==0 && TbGaudi::Verbose()) {
                std::cout << "[INFO]:: TbVeloPixAnalysis:: Run::"
                          << runNumber << " EventLoop:: Processed event "<< it << " ("
                          << std::setprecision(2)<< (static_cast<double>(it)/Nentries) * 100 << "%)" << std::endl;
            }

            iTbJob->CountDutHit(var_row[0],var_col[0]); // count a dut bin/pixel statistic hit map


            // Fill histograms, RooDataSets, etc.
            auto dutBinNumber = iTbJob->DutBinNumber(var_row[0],var_col[0]); //all pixels lay in the same bin!

            auto dutPix = iTbJob->DutPixPtr();
            if(dutBinNumber>=0) { // '-1' is returned for outsiders!
                auto x = dutPix->IntraPixPosition(var_clGx);
                auto y = dutPix->IntraPixPosition(var_clGy);
                iTbJob->Dut2DHistPtr("PixelHits", dutBinNumber)->Fill(x,y);
                iTbJob->DutHistPtr("Charge", dutBinNumber)->Fill(var_clCharge);

                iTbJob->DutPixHistPtr("PixCharge", dutBinNumber,x,y)->Fill(var_clCharge);

                // OBSOLETE iTbJob->DutPixDataSetRCatPtr("PixCharge", dutBinNumber,x,y)->setIndex(1);   //temporary, set everything as signal
                // OBSOLETE iTbJob->DutPixDataSetRRVarPtr("PixCharge", dutBinNumber,x,y)->setVal(var_clCharge);
                // OBSOLETE iTbJob->AddToDutPixDataSet("PixCharge", dutBinNumber,x,y);
            }
        }

        iTbJob->PrintCountersStatistic();
        //iTbJob->DutBinningPtr()->PrintStatistic();
        iTbJob->PlotterPtr()->DrawDutStatistic(true); // true - draw interactively

        //_______________________________________________________________________________________
        // Perform data fitting
        auto chargeFitter =  std::make_unique<ChargeFitter>(iTbJob);
        // chargeFitter->SetData(iTbJob, "PixCharge", FitDataType::RooDataSet);
        // chargeFitter->Roofit("LandauGaussConv"); // run fitting


        // fitting of other data and routines can be called here, simply:
        // 1. SetData
        // 2. Run fitting method, e.g. Roofit() or other...

        //_______________________________________________________________________________________
        // At the end of each iteration release memory, loop related data are no needed anymore!
        iTbJob->ClearEventLoopCollectedData(); // one method to clear all!

    } // TbJobCollection() loop

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: iCCE MPVMapMaker (Maciek): THE END!");
}

////////////////////////////////////////////////////////////////////////////////////
///