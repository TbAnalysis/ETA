#include "TbVeloPixAnalysis.h"
#include "DutSvc.h"
#include "memory.h"
#include <functional>

#include "TH1D.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"
#include "ROOT/RDataFrame.hxx"

using namespace ROOT; // RDataFrame's namespace

////////////////////////////////////////////////////////////////////////////////////
/// *** WORK IN PROGRESS ***
void TbVeloPixAnalysis::CCE_IRRAD_RDF_MpvMapMaker()
{
    std::cout<<std::endl<<"******************************************************"<<std::endl;
    std::cout<<"[INFO]:: TbVeloPixAnalysis::CCE_IRRAD_RDF_MpvMapMaker is executed... "<<std::endl;
    std::cout<<"******************************************************"<<std::endl<<std::endl;

    for (auto& iTbJob : GetTbJobsCollection() ) {
        //_______________________________________________________________________________________
        // List general info about the test-beam run being processed
        iTbJob->PrintInfo();

        //_______________________________________________________________________________________
        // SET GLOBALLY AVAILABLE TBRUN CURRENTLY BEING PROCESSED!
        TbJob::CurrentTbJob(iTbJob);

        //_______________________________________________________________________________________
        // Define data containers, ROOT hists, RooDataSets, RooCategories etc.
        iTbJob->DefineDutBinningHist("Charge",500,0.,1000.);
        // OBSOLETE iTbJob->DefineDutBinningDataSet("Charge",0.,1000.,"electrons");
        // OBSOLETE iTbJob->DefineDutBinningDataSetType("Charge","Signal",1);   // RooCategory::defineType(...)
        // OBSOLETE iTbJob->DefineDutBinningDataSetType("Charge","Noise",0);    // RooCategory::defineType(...)

        //_______________________________________________________________________________________
        // EVENT SELECTION

        // 0. Take into account globally predefined filters
        auto globalFilters = RdfData::Filters("Clusters");
        auto preselectedTbData = iTbJob->RdfDataPtr()->ApplyFilters("Clusters",globalFilters);

        // 1. Take into account the cluster size requirement
        // 2. Take into account each pixel from the cluster, but keep events where all pixes belongs
        //    single DUT bin (whole cluster is laying in the same DUT bin);


        /*auto selectedData = preselectedTbData
                ->Filter(iTbJob->IsGoodClusterSize,{"clSize"});
                .Filter(iTbJob->IsClusterInsideDutBin,{"hRow","hCol","clSize"});

        //_______________________________________________________________________________________
        // EVENT LOOP LAMBDA FUNCTION (called for each seleceted event)
        // Get global DUT bin number and fill corresponding DataSet and Histogram.
        auto FillWithTbData = [=](double charge, Int_t col[200],Int_t row[200]){
            auto dutBin = iTbJob->GlobalDutBin(col[0],row[0]);
            iTbJob->CountDutBinStatistics(dutBin);
            iTbJob->FillDutBinningHist(dutBin,charge);
            iTbJob->FillDutBinningDataSet(dutBin,charge);
        };

        //_______________________________________________________________________________________
        // EVENT LOOP
        selectedData->Foreach(FillWithTbData,{"clCharge","hRow","hCol"});*/

        //_______________________________________________________________________________________
        // write histograms into .root file, if requested by the User.
        // ...

        //_______________________________________________________________________________________
        // Perform data fitting (The Fitter handle the data trough the TbJob::CurrentTbJob() pointer)
        // ...

        //_______________________________________________________________________________________
        // At the end of each iteration release memory, these data are no needed anymore!
        iTbJob->ClearDutBinningHist("Charge");

    }

    std::cout<<std::endl<<"******************************************************"<<std::endl;
    std::cout<<"[INFO]:: TbVeloPixAnalysis::MPVMapMakerRDF THE END! "<<std::endl;
    std::cout<<"******************************************************"<<std::endl<<std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///