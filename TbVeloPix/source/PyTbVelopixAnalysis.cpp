#include <pybind11/pybind11.h>
#include "TbVeloPixAnalysis.h"


namespace py = pybind11;


void PyTbVeloPixAnalysis(py::module &m) {
    py::class_<TbVeloPixAnalysis, TbAnalysis>(m, "TbVeloPixAnalysis")
    .def(py::init<>())
    .def("Ana_CCE_IRRAD", &TbVeloPixAnalysis::Ana_CCE_IRRAD)
    .def("Ana_Calib_IRRAD", &TbVeloPixAnalysis::Ana_Calib_IRRAD)
    .def("Ana_iCCE_Bartek", &TbVeloPixAnalysis::Ana_iCCE_Bartek)
    .def("Ana_iCCE_Maciek", &TbVeloPixAnalysis::Ana_iCCE_Maciek)
    .def("Ana_CCE_IRRRAD_RDF", &TbVeloPixAnalysis::Ana_CCE_IRRRAD_RDF)
    .def("Ana_CCE_KIT", &TbVeloPixAnalysis::Ana_CCE_KIT)
    .def("Ana_ECS_General", &TbVeloPixAnalysis::Ana_ECS_General)
    .def("DevToml", &TbVeloPixAnalysis::DevToml)
    .def("DevExportToCsv", &TbVeloPixAnalysis::DevExportToCsv);
}