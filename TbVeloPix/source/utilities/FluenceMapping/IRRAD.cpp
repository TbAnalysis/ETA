#include "IRRAD.h"


////////////////////////////////////////////////////////////////////////////////////
///
IRRAD::IRRAD()
{
    std::cout<<"[INFO]:: IRRAD:: Building the LHCb VeloPix fluence profile... "<<std::endl;
}

void IRRAD::Initialize(){

    if(!m_isInitialized) {
        std::cout << "[INFO]:: IRRAD:: LHCb VeloPix parameterization is used." << std::endl;

        // ____________________________________________
        // Dosimetry measurement related values

        // allignment of dosimetry measurement binning from Elena's fit - general profile:
        m_dosimetry_sigma_col = 98.1;
        m_dosimetry_sigma_row = 165.5;

        m_dosimetry_mean_col = 126.1;
        m_dosimetry_mean_row = 141.6;

        // Integrated fluence - value from Elena's General Profile Integral:
        m_dosimetry_fluence = 5.217e15;

        // ____________________________________________
        // DUT related values
        auto name = LinkedDutName();
        auto isThisDut = [=](std::string dutName) -> bool {
            return name.find(dutName) != std::string::npos ? true : false;
        };

        if (isThisDut("S8")) {
            m_mean_col = 124.1;
            m_mean_row = 96.55;
            m_theta = 0.;
        } else if (isThisDut("S11")) {
            m_mean_col = 117.3;
            m_mean_row = 84.7;
            m_theta = 0.;
        } else if (isThisDut("S25")) {
            m_mean_col = 113.5;
            m_mean_row = 119.15;
            m_theta = 0.;
        } else if (isThisDut("S30")) {
            m_mean_col = 117.75;
            m_mean_row = 98.5;
            m_theta = 0.;
        }

        std::cout << "[INFO]:: IRRAD:: DUT(\"" << name << "\") parameterization..." << std::endl;
        std::cout << "[INFO]:: IRRAD:: mean col " << m_mean_col << std::endl;
        std::cout << "[INFO]:: IRRAD:: mean row " << m_mean_row << std::endl;
        //std::cout<<"[INFO]::IRRAD:: theta    "<< m_theta <<std::endl;

        m_isInitialized = true;
    }
    else{
        std::cout << "[INFO]:: IRRAD:: the LHCb VeloPix parameterization already initialized for this DUT." << std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///