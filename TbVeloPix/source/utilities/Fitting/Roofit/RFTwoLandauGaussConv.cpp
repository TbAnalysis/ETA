#include "ChargeFitter.h"
#include "TbJob.h"
#include "TbResults.h"
#include "TbGaudi.h"

// RooFIT libriaryies
#include "RooDataHist.h"
#include "RooArgList.h"
#include "RooFFTConvPdf.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooPlot.h"

using namespace RooFit;

////////////////////////////////////////////////////////////////////////////////////
//
//   Very first model implemented by Kazu
//
void ChargeFitter::RFTwoLandauGaussConv() {
    gROOT->SetBatch(true);

    TbGaudi::PrintBanner("INFO", "TbVeloPix:: Roofit is executed:: fit RFTwoLandauGaussConv method...");

    auto nEntriesThr = TbFitter::NEntriesFitThreshold(10); // set 10 events as a threshold which dataset will taken for fitting
    auto nFits = InitializeData(); // initialize the data and get the number of DataSets to be fitted
    auto nBins = TbFitter::RooFitBinning(200);
    std::cout << "[INFO]:: ChargeFitter:: Fit nEntries threshold: " << nEntriesThr << std::endl;
    std::cout << "[INFO]:: ChargeFitter:: Number of RooDataSets nEntries above threshold: " << nFits << std::endl;
    auto data = GetTbRooDataSetCollection();

    // prepare container for the plots
    auto plots = MTCanvas(); // TODEL ???

    for (auto& idata : data) {
        auto rds = idata->GetRooDataSetPtr();
        auto rrv = GetRooRealVarPtr(rds);
        std::cout << "[INFO]:: ChargeFitter: PROCESSING: " << rds->GetName() << std::endl;

        auto hist = GetTH1FUniquePtr(rds); // TODO: icnlude RooFit::CutRange("Signal") as GetSignalTH1FUniquePtr(rds, "Signal")
        std::cout << "[INFO]:: ChargeFitter: created TH1F: " << hist->GetName() << std::endl;
        double mpv = 0.8*hist->GetMean();
        double sigma = hist->GetRMS();
        double mean_low = mpv - 0.2*mpv;
        double mean_high = mpv + 0.4*sigma;
    	double factor = GetRunVoltage()/800.;
        std::cout << "[INFO]:: >>   bin statistics " <<  hist->GetEntries() << std::endl;
        std::cout << "[INFO]:: >>           factor " <<  factor << std::endl;
        std::cout << "[INFO]:: >>   mpv (estimate) " <<  mpv << std::endl;
        std::cout << "[INFO]:: >> sigma (estimate) " <<  sigma << std::endl;

        auto RRV_Gmean =  std::make_unique<RooRealVar>("Gmean", "mean",0);
        auto RRV_Lmean =  std::make_unique<RooRealVar>("MPV", "MPV",mpv, mean_low, mean_high);
        auto RRV_Lsigma = std::make_unique<RooRealVar>("Lsigma", "Lsigma", 250*factor*1.2, 50*factor*0.8,1075*factor*1.2 );
        auto RRV_Gsigma = std::make_unique<RooRealVar>("Gsigma", "Gsigma", 1000*factor*1.2, 500*factor*0.8, 3000*factor*1.2 );
        auto RRV_Lsigma2 =std::make_unique<RooRealVar>("Lsigma2", "Lsigma2", 150*factor*1.2, 80*factor*0.8, 1200*factor*1.2 );
        auto RRV_Gsigma2 =std::make_unique<RooRealVar>("Gsigma2", "Gsigma2", 50*factor*1.2, 40*factor*0.8, 600*factor*1.2 );

        auto frac    = std::make_unique<RooRealVar>("frac","frac",0.95,0.5,1);
        auto gauss   = std::make_unique<RooGaussian>("GaussPdf","gaussian PDF", *rrv,*RRV_Gmean,*RRV_Gsigma);
        auto landau  = std::make_unique<RooLandau>("LandauPdf","landau PDF", *rrv,*RRV_Lmean,*RRV_Lsigma);
        auto lxg     = std::make_unique<RooFFTConvPdf>("lxg","landau (X) gauss",*rrv, *landau, *gauss);
        auto gauss2  = std::make_unique<RooGaussian>("GaussPdf2","gaussian2 PDF", *rrv,*RRV_Gmean,*RRV_Gsigma2);
        auto landau2 = std::make_unique<RooLandau>("LandauPdf2","landau2 PDF", *rrv,*RRV_Lmean,*RRV_Lsigma2);
        auto lxg2    = std::make_unique<RooFFTConvPdf>("lxg2","landau (X) gauss",*rrv, *landau2, *gauss2);

        auto RRV_Gmean_bkg =  std::make_unique<RooRealVar>("bkg_shift", "bkg_shift", 120.*factor, -500*factor,2000*factor);
        auto RRV_Gsigma_bkg = std::make_unique<RooRealVar>("bkg_sigma", "bkg_sigma", 10*factor, 1.*factor, 600*factor );

        // The cut range (the RooCategory) is not implemented yet!
        /*
        auto rdsSignal = std::unique_ptr<RooDataSet>(static_cast<RooDataSet*>(rds->reduce(RooFit::CutRange("Signal"))));
        auto rdhSignal = std::unique_ptr<RooDataHist>(rdsSignal->binnedClone());
        */
        auto rdhSignal = std::unique_ptr<RooDataHist>(rds->binnedClone());
        auto modelPdf    = std::make_unique<RooAddPdf>("Signal", "double langaus", RooArgList(*lxg,*lxg2), RooArgList(*frac));

        modelPdf->fitTo(*rdhSignal, NumCPU(4), Minos(true), Extended(false));

        //________________________________________________________________________
        // Export results
        auto bin = GetDutBin(rds);
        // Export fitted model parameters
        double fit_mpv      = RRV_Lmean->getVal();
        double fit_mpvErr   = RRV_Lmean->getError();
        double fit_lSig     = RRV_Lsigma->getVal();
        double fit_lSigErr  = RRV_Lsigma->getError();

        if(bin<0){
            StoreFitResult(rds,"MPV",fit_mpv, fit_mpvErr);
            StoreFitResult(rds,"Sigma",fit_lSig, fit_lSigErr);
        } else { // we have to give unique name 
                 //- the results are being stored in different data type collection
                 //  when considering DUT, Binned DUT, Binned DUT and Pixel!
            StoreFitResult(rds,"MPV_BinnedDUT",fit_mpv, fit_mpvErr);
            StoreFitResult(rds,"Sigma_BinnedDUT",fit_lSig, fit_lSigErr);
        }

        // Create and store individual plot as a canvas
        StoreFitPlot(rds,"DATA",modelPdf.get(),"MODEL","Signal","lxg","lxg2");
    }  // TbRooDataSetCollection loop

    gROOT->SetBatch(false);
}