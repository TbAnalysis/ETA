#include "ChargeFitter.h"
#include "TbJob.h"
#include "TbResults.h"
#include "TbGaudi.h"

// RooFIT libriaryies
#include "RooDataHist.h"
#include "RooFFTConvPdf.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooPlot.h"

using namespace RooFit;

////////////////////////////////////////////////////////////////////////////////////
//
void ChargeFitter::RoofitWithOutOfSpillNoise(){
	gROOT->SetBatch(true);
    TbGaudi::PrintBanner("INFO", "TbVeloPix:: RoofitWithOutOfSpillNoise is executed...");

    auto nEntriesThr = TbFitter::NEntriesFitThreshold(10); // set 10 events as a threshold which dataset will taken for fitting
    auto nFits = InitializeData(); // initialize the data and get the number of DataSets to be fitted
    auto nBins = TbFitter::RooFitBinning(200);
    std::cout << "[INFO]:: ChargeFitter:: Fit nEntries threshold: " << nEntriesThr << std::endl;
    std::cout << "[INFO]:: ChargeFitter:: Number of RooDataSets nEntries above threshold: " << nFits << std::endl;
    auto data = GetTbRooDataSetCollection();

    for (auto& idata : data) {
        auto rds = idata->GetRooDataSetPtr();
        auto rrv = GetRooRealVarPtr(rds);
        std::cout << "[INFO]:: ChargeFitter: PROCESSING: " << rds->GetName() << std::endl;

		auto hist = GetTH1FUniquePtr(rds); // TODO: icnlude RooFit::CutRange("Signal") as GetSignalTH1FUniquePtr(rds, "Signal")
        std::cout << "[INFO]:: ChargeFitter: created TH1F: " << hist->GetName() << std::endl;
        double mpv = 0.8*hist->GetMean();
        double sigma = hist->GetRMS();
        double mean_low = mpv - 0.2*mpv;
        double mean_high = mpv + 0.4*sigma;
    	double factor = GetRunVoltage()/800.;
        std::cout << "[INFO]:: >>   bin statistics " <<  hist->GetEntries() << std::endl;
        std::cout << "[INFO]:: >>           factor " <<  factor << std::endl;
        std::cout << "[INFO]:: >>   mpv (estimate) " <<  mpv << std::endl;
        std::cout << "[INFO]:: >> sigma (estimate) " <<  sigma << std::endl;

    	auto RRV_Gmean = std::make_unique<RooRealVar>("Gmean", "Gmean",0);
    	auto RRV_Lmean = std::make_unique<RooRealVar>("MPV","MPV",mpv, mean_low, mean_high);
    	auto RRV_Lsigma = std::make_unique<RooRealVar>("Lsigma","Lsigma", 250*factor*1.2, 50*factor*0.8,1075*factor*1.2 );
    	auto RRV_Gsigma = std::make_unique<RooRealVar>("Gsigma","Gsigma", 1000*factor*1.2, 500*factor*0.8, 3000*factor*1.2 );
    	auto RRV_Lsigma2 = std::make_unique<RooRealVar>("Lsigma2","Lsigma2", 150*factor*1.2, 80*factor*0.8, 1200*factor*1.2 );
    	auto RRV_Gsigma2 = std::make_unique<RooRealVar>("Gsigma2","Gsigma2", 50*factor*1.2, 40*factor*0.8, 600*factor*1.2 );
    	auto frac = std::make_unique<RooRealVar>("frac","frac",0.95,0.5,1);
    	auto gauss =  std::make_unique<RooGaussian>("GaussPdf","Gaussian PDF", *rrv,*RRV_Gmean,*RRV_Gsigma);
    	auto landau =  std::make_unique<RooLandau>("LandauPdf","Landau PDF", *rrv,*RRV_Lmean,*RRV_Lsigma);
    	auto lxg =  std::make_unique<RooFFTConvPdf>("lxg","landau (X) gauss",*rrv, *landau, *gauss);
    	auto gauss2 =  std::make_unique<RooGaussian>("GaussPdf2","Gaussian2 PDF", *rrv,*RRV_Gmean,*RRV_Gsigma2);
    	auto landau2 =  std::make_unique<RooLandau>("LandauPdf2"," Landau2 PDF", *rrv,*RRV_Lmean,*RRV_Lsigma2);
    	auto lxg2 =  std::make_unique<RooFFTConvPdf>("lxg2","landau (X) gauss",*rrv, *landau2, *gauss2);
    	auto signalPdf =  std::make_unique<RooAddPdf>("Signal", "Double langaus", RooArgList(*lxg,*lxg2), RooArgList(*frac));


		//////////////////////////////////////////////////////////////////////////////////////
		//number of signal and background events to be fitted
		double nEntries = hist->GetEntries();
		double inisig = 0, maxsig=0, minsig=0;
		double inibkg = 0, maxbkg=0, minbkg=0;
		if (nEntries > 1100000){
			inisig = nEntries;
			minsig= 0.6*nEntries;
			maxsig= nEntries;
			inibkg =0.15*nEntries;
			minbkg=0.01*nEntries;
			maxbkg = 0.3*nEntries;
		}
		if (nEntries > 30000 && nEntries < 1100000){
			inisig = 0.7*nEntries;
			minsig= 0.5*nEntries;
			maxsig= nEntries;
			inibkg =0.2*nEntries;
			minbkg=0.01*nEntries;
			maxbkg = 0.3*nEntries;
		}
		if (nEntries < 30000 ){
			inisig = 0.5*nEntries;
			minsig= 0.5*nEntries;
			maxsig= nEntries;
			inibkg =0.2*nEntries;
			minbkg=0.005*nEntries;
			maxbkg = 0.3*nEntries;
		}
		auto N_Sig = std::make_unique<RooRealVar>("N_Sig","# of signal events ", inisig, minsig, maxsig);
		auto N_Bkg = std::make_unique<RooRealVar>("N_Bkg","# of background events ", inibkg, minbkg, maxbkg);

		///////////////////////////////////////////////////////////
		// The cut range (the RooCategory) is not implemented yet!
		///////////////////////////////////////////////////////////

		/*
		auto rdsSignal = std::unique_ptr<RooDataSet>(static_cast<RooDataSet*>(rds->reduce(RooFit::CutRange("signal") ) ));
		auto rdsNoise = std::unique_ptr<RooDataSet>(static_cast<RooDataSet*>(rds->reduce(RooFit::CutRange("noise") ) ));


		if (rdsNoise->sumEntries() < 100){
        	std::cout << "[WARNING]::ChargeFitter:: OOPS entries on bkg too low"  << rdsNoise->sumEntries() << std::endl;
        	continue; 
      	}
      	std::cout <<"[DEBUG]::ChargeFitter:: Number of out-of-spill noise entries "  << rdsNoise->sumEntries() << std::endl;
      	std::cout<< "[INFO]::ChargeFitter:: Making kernel estimation... "  << std::endl;
      	
      	auto kestBgd = std::make_unique<RooKeysPdf>("kestBgd","kestBgd",*rrv,*rdsNoise,RooKeysPdf::NoMirror);
      	auto RRV_Gmean_bkg = std::make_unique<RooRealVar>("Bkg_shift","Bkg_shift", 120.*factor, -500*factor,2000*factor);
      	auto RRV_Gsigma_bkg = std::make_unique<RooRealVar>("Bkg_sigma","Bkg_sigma", 10*factor, 1.*factor, 600*factor );
      	auto gauss_bkg = std::make_unique<RooGaussian>("Bkg_GaussPdf","Gaussian PDF", *rrv,*RRV_Gmean_bkg,*RRV_Gsigma_bkg);
      	auto bkg_adapt = std::make_unique<RooFFTConvPdf>("keysConvGausbkg","noise (x) gauss",*rrv, *kestBgd, *gauss_bkg);

      	auto rdhSignal = std::unique_ptr<RooDataHist>(rdsSignal->binnedClone());
      	
      	auto modelPdf = std::make_unique<RooAddPdf>("modelPdf","totalpdf", RooArgList(*signalPdf,*bkg_adapt), RooArgList(*N_Sig,*N_Bkg));
      	modelPdf->fitTo(*rdhSignal,NumCPU(4), Minos(true), Extended());
		*/
      	// TODO :: export fit results
      	// TODO :: export fit plots
      	
 	} // nFits loop
	gROOT->SetBatch(false);
}