#include "ChargeFitter.h"
#include "TbJob.h"
#include "TbResults.h"
#include "TbGaudi.h"

// RooFIT libriaryies
#include "RooFFTConvPdf.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooPlot.h"

using namespace RooFit;

////////////////////////////////////////////////////////////////////////////////////
//
void ChargeFitter::RFLandauGaussConv() {
    gROOT->SetBatch(true);
    TbGaudi::PrintBanner("INFO", "TbVeloPix:: Roofit is executed:: fit RFLandauGaussConv method...");

    auto nEntriesThr = TbFitter::NEntriesFitThreshold(10); // set 10 events as a threshold which dataset will taken for fitting
    auto nFits = InitializeData(); // initialize the data and get the number of DataSets to be fitted
    auto nBins = TbFitter::RooFitBinning(100);
    std::cout << "[INFO]:: ChargeFitter:: Fit nEntries threshold: " << nEntriesThr << std::endl;
    std::cout << "[INFO]:: ChargeFitter:: Number of RooDataSets nEntries above threshold: " << nFits << std::endl;
    auto data = GetTbRooDataSetCollection();
    for (auto& idata : data) {
        auto rds = idata->GetRooDataSetPtr();
        auto rrv = GetRooRealVarPtr(rds);
        std::cout << "[INFO]:: ChargeFitter: PROCESSING: " << rds->GetName() << std::endl;

        auto hist = GetTH1FUniquePtr(rds); // TODO: icnlude RooFit::CutRange("Signal") as GetSignalTH1FUniquePtr(rds, "Signal")
        std::cout << "[INFO]:: ChargeFitter: created TH1F: " << hist->GetName() << std::endl;
        double mpv = 0.8*hist->GetMean();
        double sigma = hist->GetRMS();
        double mean_low = mpv - 0.2*mpv;
        double mean_high = mpv + 0.4*sigma;
        std::cout << "[INFO]:: >>   mpv (estimate) " <<  mpv << std::endl;
        std::cout << "[INFO]:: >> sigma (estimate) " <<  sigma << std::endl;
        
        auto rrvGMean =  std::make_unique<RooRealVar>("Gmean", "mean",0);
        auto rrvGSigma = std::make_unique<RooRealVar>("Gsigma", "Gsigma", sigma, 0.001*sigma, 4.*sigma );

        auto rrvLMean =  std::make_unique<RooRealVar>("MPV", "MPV",mpv, mean_low, mean_high);
        auto rrvLSigma = std::make_unique<RooRealVar>("Lsigma", "Lsigma", sigma, 0.001*sigma, 2.*sigma  );

        auto gauss   = std::make_unique<RooGaussian>("GaussPdf","gaussian PDF", *rrv,*rrvGMean,*rrvGSigma);
        auto landau  = std::make_unique<RooLandau>("LandauPdf","landau PDF", *rrv,*rrvLMean,*rrvLSigma);
        auto lxgPdf     = std::make_unique<RooFFTConvPdf>("lxg","landau (X) gauss",*rrv, *landau, *gauss);

        /* If the User would define RooCategory by iTbJob->DefineRooCategory("Signal",1);
        auto rdsSignal = std::unique_ptr<RooDataSet>(static_cast<RooDataSet*>(rds->reduce(RooFit::CutRange("Signal"))));
        auto rdhSignal = std::unique_ptr<RooDataHist>(rdsSignal->binnedClone());
        lxgPdf->fitTo(*rdhSignal, NumCPU(4), Minos(true), Extended(false));
        */

        lxgPdf->fitTo(*rds, NumCPU(4), Minos(true), Extended(false));

        // TODO: define GetDutBin(idata) instead GetDutBin(rds)
        // in new proposal the bin info can be stored in TbRooDataSet instead of being extracted from rds name..
        auto bin = GetDutBin(rds); 
        // Export fitted model parameters
        double fit_mpv      = rrvLMean->getVal();
        double fit_mpvErr   = rrvLMean->getError();
        double fit_lSig     = rrvLSigma->getVal();
        double fit_lSigErr  = rrvLSigma->getError();

        std::cout << "[INFO]:: >>   mpv (fit) " <<  fit_mpv << std::endl;
        std::cout << "[INFO]:: >> sigma (fit) " <<  fit_lSig << std::endl;

        // Store the produced results
        // NOTE: we have to give a unique name 
        //- the results are being stored in the scope of the TbJob outcome
        // however we can consider different data type collections:
        // DUT, Binned DUT and Pixel!
        if(bin<0){ // DUT is not binned
            StoreFitResult(rds,"MPV",fit_mpv, fit_mpvErr);
            StoreFitResult(rds,"Sigma",fit_lSig, fit_lSigErr);
        } else { 
            StoreFitResult(rds,"MPV_BinnedDUT",fit_mpv, fit_mpvErr);
            StoreFitResult(rds,"Sigma_BinnedDUT",fit_lSig, fit_lSigErr);
        }
        
        // Create and store individual plot as a canvas
        StoreFitPlot(rds,"DATA",lxgPdf.get(),"MODEL");
        
    } // TbRooDataSetCollection loop
    
    gROOT->SetBatch(false);
}