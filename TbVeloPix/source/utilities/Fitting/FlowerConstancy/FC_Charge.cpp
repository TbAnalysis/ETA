#include "ChargeFitter.h"
#include "TbJob.h"
#include "TbResults.h"
#include "TbGaudi.h"

// ROOT libriaries
#include "TF2.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"

// RooFIT libriaries
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooConstVar.h"
#include "RooFFTConvPdf.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooPlot.h"
#include "TLegend.h"

#include <math.h>

using namespace RooFit;
using namespace std;

////////////////////////////////////////////////////////////////////////////////////
StdDoubleVec ChargeFitter::LoadLandauDataBase() {
    const std::string filePath = TbGaudi::ProjectLocation()+"/TbVeloPix/src/utilities/Fitting/FlowerConstancy/Landau2.txt";
    cout << "[INFO]::ChargeFitter:: Landau DB file " << filePath << endl;
    std::ifstream fin(filePath.c_str());
    int row=1980;
    int col=800;
    StdDoubleVec LandauDB (row,std::vector<double>(col));
    if (fin.is_open()) {
        double test;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                fin >> LandauDB[i][j];
            }
        }
        fin.close();
    }
    else
        std::cout << "[ERROR]:: ChargeFitter:: couldn't open file!" << std::endl;
    std::cout << "[INFO]:: ChargeFitter:: FC:: Landau parametrization readied!" << std::endl;
    return LandauDB;
}


////////////////////////////////////////////////////////////////////////////////////
void ChargeFitter::FC_Charge() {
    //gROOT->SetBatch(true);
    TbGaudi::PrintBanner("INFO", "TbVeloPix:: FC ( Charge data type ) is executed...");

    TbFitter::NEntriesFitThreshold(10); // set 10 events as a fit trigger threshold
    auto threshold = TbFitter::NEntriesFitThreshold(); // just to check
    auto nFits = NGoodDataSets();
    std::cout << "[INFO]:: ChargeFitter:: FC:: Fit nEntries threshold: " << threshold << std::endl;
    std::cout << "[INFO]:: ChargeFitter:: FC:: NR OF FITTING JOBS " << nFits << std::endl;

    // _____________________________________________________________
    auto Landau = LoadLandauDataBase();

    // _____________________________________________________________
    for (int iFit = 0; iFit < nFits; ++iFit) {
//    for (int iFit = 0; iFit < 1; ++iFit) {
        // ____________________________________________________________
        // Establishing canvas & histogram
        auto myHist = ITHistPtr(iFit);
        if (!myHist) TbGaudi::RunTimeError("Something went wong with input data TH1");
        int bin = 0; // TODO: GetDutBin(myHist);
        TbGaudi::PrintBanner("INFO"," ChargeFitter:: Start fitting:: "+std::string(myHist->GetName())+" (DUT bin " +
                             itos(bin) + ")");

        int Nbins = myHist->GetNbinsX();
        int NEntries = myHist->GetEntries();
        std::cout << "[INFO]:: ChargeFitter:: FC:: HIST (NR OF BINS) " << Nbins << std::endl;
        std::cout << "[INFO]:: ChargeFitter:: FC:: HIST (NR OF ENTRIES) " << NEntries << std::endl;
        // _____________________________________________________________
        // Algorithm's agents and variables definition
        int n = 35;            //number of fit agents
        double pop[50][4];
        double pop2[50][4];
        double koszt[n];
        double koszt2;
        double kosztEnd[n];



        double Skx = 400;
        double Sky = 1;   //Scaling if necessary
        double bins[Nbins];
        double values[Nbins];  //Root-histogram data will be put here
        double Xt[Nbins];
        double Yt[Nbins];

        cout << "Width of the bin:    " << myHist->GetBinWidth(0) << endl;
        cout << "Center of the 1st bin:    " << myHist->GetBinCenter(0) << endl;   //Binning control
        for (int i = 0; i < Nbins; i++) {
            bins[i] = myHist->GetBinCenter(i);
            values[i] = myHist->GetBinContent(i);
            Xt[i] = bins[i] / Skx;
            Yt[i] = values[i] / Sky;
        }
        int kk=1;
        int FirstEffective = 10;   //Left bin fit constraint. Change it if your data is contaminated by low-charge noise distribution.
        int P = 30;                //Number of data points taken for the fitting
        if(Nbins>100) kk=(int) (((double) Nbins) / 101)+1; //P = (int) (((double) Nbins) / 100 * (double) P);
        for (int i = 0; i < P; i++) {
            Xt[i] = Xt[FirstEffective + i*kk];
            Yt[i] = Yt[FirstEffective + i*kk];
        }


        for (int i = 0; i < n; i++) {                                      //Initialization of model parameters
            pop[i][0] = 3 + 2 * round(100 * ((double) rand() / RAND_MAX)) / 100;  //Landau c parameter
            pop[i][1] = 5000 + 5000 * ((double) rand() / RAND_MAX - 0.5);        //Amplitude
            pop[i][2] = 2 + 2 * ((double) rand() / RAND_MAX - 0.5);             //Sigma gauss
            pop[i][3] = 10 + 10 * ((double) rand() / RAND_MAX - 0.5);           //Mean gauss
        }

        double kosztT;
        for (int j = 0; j < n; j++) {               //Calculating the initial population quality
            kosztT = 0;
            for (int k = 0; k < P; k++) {
                double I = 0;
                for (int q = 0; q < 800; q++) {
                    auto expv = exp(-(Xt[k] + 10.2 - 0.2 * q - pop[j][3]) * (Xt[k] + 10.2 - 0.2 * q - pop[j][3]) /
                                    (2 * pop[j][2] * pop[j][2]));
                    I = I + pop[j][1] * expv * Landau[(int) (100 * pop[j][0] - 10)][q] * 0.2;
                }
                kosztT = kosztT + (I - Yt[k]) * (I - Yt[k]);
            }
            koszt[j] = kosztT;
        }
        // _____________________________________________________________
        // Fitting the data
        cout << "Fitting the data... " << endl;
        for (int i = 0; i < 310; i++) {              //Starting the algorithm loop
            for (int j = 0; j < n; j++) {
                double e = 2 * (double) rand() / RAND_MAX - 1;
                int z1 = round((n - 1) * (double) rand() / RAND_MAX);
                int z2 = round((n - 1) * (double) rand() / RAND_MAX);
                for (int p = 0; p < 4; p++)
                    pop2[j][p] = pop[j][p] + e * (pop[z1][p] - pop[z2][p]);   //Algorithm main equation
                pop2[j][0] = round(pop2[j][0] * 100) / 100;

                if (pop2[j][0] < 0.2)     //Landau c
                    pop2[j][0] = 0.2;
                if (pop2[j][0] > 12)
                    pop2[j][0] = 12;
                if (pop2[j][1] > 600000)    //Amplitude
                    pop2[j][1] = 600000;
                if (pop2[j][1] < 10)
                    pop2[j][1] = 10;
                if (pop2[j][2] > 11)     //Gauss sigma
                    pop2[j][2] = 11;
                if (pop2[j][2] < 0)
                    pop2[j][2] = 0;
                if (pop2[j][3] > 120)    //Gauss mean
                    pop2[j][3] = 120;
                if (pop2[j][3] < 5)
                    pop2[j][3] = 5;

                koszt2 = 0;
                for (int k = 0; k < P; k++) {      //Evaluation the solutions quality after the step of optimization
                    double I = 0;
                    for (int q = 0; q < 800; q++)
                        I = I + pop2[j][1] *
                                exp(-(Xt[k] + 10.2 - 0.2 * q - pop2[j][3]) * (Xt[k] + 10.2 - 0.2 * q - pop2[j][3]) /
                                    (2 * pop2[j][2] * pop2[j][2])) * Landau[(int) (100 * pop2[j][0] - 10)][q] * 0.2;
                    koszt2 = koszt2 + (I - Yt[k]) * (I - Yt[k]) * sqrt(Yt[k]);
                }

                if (koszt2 < koszt[j]) {        //Update inferior solution
                    for (int it = 0; it < 4; it++)
                        pop[j][it] = pop2[j][it];
                    koszt[j] = koszt2;
                }
            }
        }
        // _____________________________________________________________
        // Scanning the final population in order to get best solution
        cout << "Scanning the final population in order to get best solution... " << endl;
        double value = koszt[0];
        double wskaznik = 0;
        int no = 0;
        for (int j = 0; j < n; j++) {
            double kosztT = 0;
            for (int k = 0; k < P; k++) {
                double I = 0;
                for (int q = 0; q < 800; q++) {
                    auto expv = exp(-(Xt[k] + 10.2 - 0.2 * q - pop[j][3]) * (Xt[k] + 10.2 - 0.2 * q - pop[j][3]) /
                                    (2 * pop[j][2] * pop[j][2]));
                    I = I + pop[j][1] * expv * Landau[(int) (100 * pop[j][0] - 10)][q] * 0.2;
                }
                kosztT = kosztT + (I - Yt[k]) * (I - Yt[k]) * sqrt(Yt[k]);
            }
            kosztEnd[j] = kosztT;
            if (kosztEnd[j] < value) {
                value = kosztEnd[j];
                no = j;
            }
        }
        // _____________________________________________________________
        // Recreating the fit
        double c = pop[no][0];        //Landau c
        double B = pop[no][1];        //Amplitude
        double sigma = pop[no][2];    //Mean gauss
        double mi = pop[no][3];       //Sigma gauss

        cout << "Variance: " << value / P << endl;
        cout << c << " " << B << " " << sigma << " " << mi << endl;

        double xx[1000];
        double yy[1000];
        int range = (int) (0 + 1000 * (Xt[Nbins - 1] / 1000));

        double I = 0;
        for (int i = 0; i < 1000; i++) {
            I = 0;
            xx[i] = 0 + i * (double(Nbins) / 1000);
            for (int q = 0; q < 800; q++) {
                I = I + B * exp(-(xx[i] + 10.2 - 0.2 * q - mi) * (xx[i] + 10.2 - 0.2 * q - mi) / (2 * sigma * sigma)) *
                        Landau[(int) round(100 * c - 10)][q] * 0.2;
            }
            yy[i] = I * Sky;
            xx[i] = xx[i] * Skx;
        }

        // _____________________________________________________________
        // Calculating the MPV
        int wsk = 0;
        double dy[900];
        for (int i = 0; i < 900; i++) {
            dy[i] = (yy[49 + i] - yy[51 + i]) / (2 * (double) range / 1000);
            if (dy[i - 1] < 0 && dy[i] > 0) {
                wsk = i;
                break;
            }
        }
        double MPV = Skx * ((double) (wsk + 50) / 1000) * range - dy[wsk - 1] / (-dy[wsk - 1] + dy[wsk]);
        double bin_resolution = (Xt[2] - Xt[1]) / 2;
        double errMPV = bin_resolution / sqrt(12) / sqrt((double) P / 4.) * sqrt(value / P / MPV);
        cout << "MPV of fitted LG distribution: " << MPV << " Error +/- " << errMPV << endl;

        // Export fitted model parameters
        InsertTbJobResult("MPV",bin,MPV,errMPV);

        // _____________________________________________________________
        // Plotting the results and export individual plot as a canvas
        auto cname = ThisTbJobPtr()->Label("JOB")+"_bin"+itos(bin);
        auto can = std::make_shared<TCanvas>(TString("Can"+cname),TString(cname),660,600);
        can->cd();
        auto hist = dynamic_cast<TH1*>(myHist->Clone());
        hist->GetXaxis()->SetTitle("Charge value");
        hist->SetStats(0);
        //hist->GetYaxis()->SetTitle("Number of counts");
        hist->SetMarkerStyle(8);
        hist->Draw("PE1");

        TGraph *fit = new TGraph(1000, xx, yy);
        fit->SetName("fit");
        fit->SetMarkerColor(6);
        fit->SetLineColor(6);
        fit->SetLineWidth(3);
        fit->Draw("SAMEC");

        auto legend = new TLegend(0.60, 0.82, 0.9, 0.9);
        legend->AddEntry(hist, "Test beam data", "pe");
        legend->AddEntry("fit", "Main LG distribution", "l");
        legend->Draw("SAME");
        InsertTbJobPlot(cname,can);
    }
    gROOT->SetBatch(false);
}
