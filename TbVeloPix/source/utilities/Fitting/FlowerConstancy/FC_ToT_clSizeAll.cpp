#include "ChargeFitter.h"
#include "TbJob.h"
#include "TbResults.h"
#include "TbGaudi.h"

// ROOT libriaryies
#include "TF2.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"

// RooFIT libriaryies
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooConstVar.h"
#include "RooFFTConvPdf.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooPlot.h"
#include "TLegend.h"

using namespace RooFit;
using namespace std;

////////////////////////////////////////////////////////////////////////////////////
//
void ChargeFitter::FC_ToT_clSizeAll() {
    //gROOT->SetBatch(true);
    TbGaudi::PrintBanner("INFO", "TbVeloPix:: FC (ToT, all cluster sizes) is executed...");

	// _____________________________________________________________
	auto Landau = LoadLandauDataBase();

    // ____________________________________________________________
    // Establishing canvas & histogram
    auto myHist = ITHistPtr();
    auto canTest = new TCanvas(TString("c"+TString(myHist->GetName())), myHist->GetTitle(),700,600);

    TbFitter::NEntriesFitThreshold(10); // set 10 events as a fit trigger threshold
    std::cout << "[INFO]:: ChargeFitter:: FC:: Fit nEntries threshold: " << TbFitter::NEntriesFitThreshold() << std::endl;

    
    //
    // _____________________________________________________________
    // Algorithm's agents and variables definition
    int n=50;            //number of fit agents
    double pop[50][6]; double pop2[50][6]; 
    double koszt[n]; double koszt2; double kosztEnd[n];

    int Nbins=myHist->GetNbinsX();
    double Skx=1; double Sky=1;         //Scaling if necessary
    double bins[Nbins]; double values[Nbins];   //Root-histogram data will be put here
    double Xt[Nbins]; double Yt[Nbins];
    
    std::cout<<"Width of the bin:    "<<myHist->GetBinWidth(0)<<std::endl;
    std::cout<<"Center of the 1st bin:    "<<myHist->GetBinCenter(0)<<std::endl;
    
    for(int i=0; i<Nbins; i++){  //uzupelniamy tablice robocze + skalowanie
	    bins[i]=myHist->GetBinCenter(i);
    	values[i]=myHist->GetBinContent(i);
    	Xt[i]=bins[i]/Skx;
    	Yt[i]=values[i]/Sky;
    }

    int FirstEffective=0;   //Left bin fit constraint. Change it if your data is contaminated by low-charge noise distribution.
    int P=60;               //Number of data points taken for the fitting

    if(Nbins>100) P=(int)(((double)Nbins)/100 *(double)P);
    for(int i=0; i<P; i++){  
	    Xt[i]=Xt[FirstEffective+2*i];
	    Yt[i]=Yt[FirstEffective+2*i];
    }

    for(int i=0; i<n; i++){                                      //Initialization of model parameters
    	pop[i][0]=3+2*round(100*((double)rand()/RAND_MAX))/100;  //Landau c parameter
    	pop[i][1]=1000+500*((double)rand()/RAND_MAX-0.5);        //Amplitude
    	pop[i][2]=2+2*((double)rand()/RAND_MAX-0.5);             //Sigma gauss
    	pop[i][3]=10+10*((double)rand()/RAND_MAX-0.5);           //Mean gauss
	    pop[i][4]=100000+100000*((double)rand()/RAND_MAX-0.5);   //First conv LG gain
	    pop[i][5]=100000+100000*((double)rand()/RAND_MAX-0.5);   //Second conv LG gain
    }
    double Iy[P]; double It[P]; double kosztT;
    
    for(int j=0; j<n; j++){                  //Calculating the initial population quality   
	    kosztT=0;
	    for(int k=0; k<P; k++){           
		    double I=0;
		    for(int q=0; q<800; q++){
		        I=I+pop[j][1]*exp(-(Xt[k]+10.2-0.2*q-pop[j][3])*(Xt[k]+10.2-0.2*q-pop[j][3])/(2*pop[j][2]*pop[j][2]))* Landau[(int)(100*pop[j][0]-10)][q]*0.2;
		    }
		    Iy[k]=I;
        }
	    for(int k=0;k<P;k++){
		    double I=0;
		    for(int i=0;i<P;i++){
			    if(k-i+1>0)
				    I+=Iy[i]*Iy[k-i]/pop[j][4];
		    }
		    It[k]=I;              
		}
		for(int k=0;k<P;k++){
		    double I=0;
		    for(int i=0;i<P;i++){
			    if(k-i+1>0)
				    I+=It[i]*Iy[k-i]/pop[j][5];
		    }
		    kosztT=kosztT+(I+Iy[k]+It[k]-Yt[k])*(I+Iy[k]+It[k]-Yt[k]);//*sqrt(Yt[k]);                
		}
		for(int k=0;k<P;k++){
		    Iy[k]=0;
		    It[k]=0;}
	    koszt[j]=kosztT;
    }
    // _____________________________________________________________
    // Fitting the data
    cout<<"Fitting the data... "<<endl;
    for(int i=0; i<600; i++){              //Starting the algorithm loop

	    for(int j=0; j<n; j++){
            double e=2*(double)rand()/RAND_MAX-1;
            int z1=round((n-1)*(double)rand()/RAND_MAX); int z2=round((n-1)*(double)rand()/RAND_MAX);
                
		    for(int p=0; p<6; p++)          
                pop2[j][p]=pop[j][p]+e*(pop[z1][p]-pop[z2][p]);   //Algorithm main equation
                
		    pop2[j][0]=round(pop2[j][0]*100)/100;            
            if(pop2[j][0]<0.2)                          //Landau c
                pop2[j][0]=0.2;
            if(pop2[j][0]>12)
                pop2[j][0]=12;
            if(pop2[j][1]>6000)                         //Amplitude
                pop2[j][1]=6000;
            if(pop2[j][1]<10)
                pop2[j][1]=10;
            if(pop2[j][2]>11)                           //Gauss sigma
                pop2[j][2]=11;          
            if(pop2[j][2]<0)
                pop2[j][2]=0;
            if(pop2[j][3]>120)                          //Gauss mean
                pop2[j][3]=120;
            if(pop2[j][3]<5)
                pop2[j][3]=5;
		    if(pop2[j][4]>10000000)                     //First conv LG gain
                pop2[j][4]=10000000;
            if(pop2[j][4]<100)
                pop2[j][4]=100;
            if(pop2[j][5]>1000000)                      //Second conv LG gain
                pop2[j][5]=1000000;
            if(pop2[j][5]<10)
                pop2[j][5]=10;
            
            koszt2=0;
            for(int k=0; k<P; k++){                  //Evaluation the solutions quality after the step of optimization
                double I=0;
                for(int q=0; q<800; q++){
                    I=I+pop2[j][1]*exp(-(Xt[k]+10.2-0.2*q-pop2[j][3])*(Xt[k]+10.2-0.2*q-pop2[j][3]) /(2*pop2[j][2]*pop2[j][2]))*Landau[(int)(100*pop2[j]	[0]-10)][q]*0.2;
                }
		    Iy[k]=I;
            }

		    for(int k=0;k<P;k++){
		        double I=0;
		        for(int w=0;w<P;w++){
		    	    if(k-w+1>0)
		    		    I+=Iy[w]*Iy[k-w]/pop2[j][4];
		        }
		        It[k]=I;              
		    }
		    for(int k=0;k<P;k++){
		        double I=0;
		        for(int w=0;w<P;w++){
		    	    if(k-w+1>0)
		    		    I+=It[w]*Iy[k-w]/pop2[j][5];
		        }
		        koszt2=koszt2+(I+Iy[k]+It[k]-Yt[k])*(I+Iy[k]+It[k]-Yt[k]);//*sqrt(Yt[k]);                
		    }
		    for(int k=0;k<P;k++){
		        Iy[k]=0;
		        It[k]=0;
		    }
		    if(koszt2<koszt[j]){
                for(int it=0; it<5; it++)    //Update inferior solution
                    pop[j][it]=pop2[j][it];                
		        koszt[j]=koszt2;
            }
	    }
    }
    
    // _____________________________________________________________
    // Scanning the final population in order to get best solution

    double value= koszt[0];
    double wskaznik= 0;
    int no;
    for(int j=0; j<n; j++){
    	double kosztT=0;
        for(int k=0; k<P; k++){
            double I=0;
            for(int q=0; q<800; q++){
                I=I+pop[j][1]*exp(-(Xt[k]+10.2-0.2*q-pop[j][3])*(Xt[k]+10.2-0.2*q-pop[j][3])/(2*pop[j][2]*pop[j][2]))* Landau[(int)(100*pop[j][0]-10)][q]*0.2;
            }
		    Iy[k]=I;
        }
	    for(int k=0;k<P;k++){
		    double I=0;
		    for(int i=0;i<P;i++){
		    	if(k-i+1>0)
				    I+=Iy[i]*Iy[k-i]/pop[j][4];
		    }
		    It[k]=I;              
		}
		for(int k=0;k<P;k++){
		    double I=0;
		    for(int i=0;i<P;i++){
			    if(k-i+1>0)
				    I+=It[i]*Iy[k-i]/pop[j][5];
		    }
		kosztT=kosztT+(I+Iy[k]+It[k]-Yt[k])*(I+Iy[k]+It[k]-Yt[k]);//*sqrt(Yt[k]);                
		}
		
	    kosztEnd[j]=kosztT;
	    if(kosztEnd[j]< value){
            value= kosztEnd[j];
            no= j;
        }
    }
    // _____________________________________________________________
    // Recreating the fit
    double c=pop[no][0];        //Landau c
    double B=pop[no][1];        //Amplitude
    double sigma=pop[no][2];    //Mean gauss
    double mi=pop[no][3];       //Sigma gauss
    double A = pop[no][4];      //First LG conv gain
    double A2 = pop[no][5];     //Second LG conv gain

    std::cout<<"Variance: "<<value/P<<std::endl;

    double xx[1000]; double yy[1000]; double dy[900]; double yy2[1000]; double yy3[1000]; double yy4[1000]; double LG2[1000]; double LG3[1000]; double LG4[1000];
    for(int i=0;i<1000;i++)
	    LG2[i]=0;
    double max= Xt[Nbins-1];
    int range=(int)(0+1000*(max/1000));
    double I=0;
    for(int i=0;i<1000;i++){
    	I=0;
	    xx[i]=0+i*(double(Nbins)/1000);
	    for(int q=0;q<800;q++){
		    I=I+B*exp(-(xx[i]+10.2-0.2*q-mi)*(xx[i]+10.2-0.2*q-mi) /(2*sigma*sigma))*Landau[(int)round(100*c-10)][q]*0.2;
	    }
	    yy3[i]=I*Sky;
	    yy[i]=I*Sky;
	    xx[i]=xx[i]*Skx;
	    yy4[i]=Yt[int(floor(double(i)*Nbins/1000/2))]-yy3[i];
	}
    double globalfit[1000];
    for(int t=0;t<1000;t++){
	    for(int i=0;i<1000;i++){
	        if(t-i+1>0)
	        LG2[t]+=yy3[i]*yy3[t-i]/A/25;
	    }
    }
    for(int t=0;t<1000;t++){
	    for(int i=0;i<1000;i++){
	        if(t-i+1>0)
	        LG3[t]+=LG2[i]*yy3[t-i]/A2/25;
	    }
	    globalfit[t]=LG3[t]+LG2[t]+yy3[t];
    }

    for(int i=0;i<1000;i++){
	    for(int j=0;j<1000;j++){
		    if(i+j<1000)
		        LG4[j+i]+=yy3[i]*LG3[j]*(yy3[i]+LG3[j])/2000000000;
	    }
    }
    
    // _____________________________________________________________
    // Calculating the MPV
    int wsk=0;
    for(int i=0; i<900; i++){
	    dy[i]=(yy3[49+i]-yy3[51+i])/(2*(double)range/1000);
	    if(dy[i-1]<0 && dy[i]>0){wsk=i; break;}
    }
    double MPV=Skx*((double)(wsk+50)/1000)*range - dy[wsk-1]/(-dy[wsk-1]+dy[wsk]);
    double rozdzielczosc=(Xt[2]-Xt[1])/2;
    double errMPV=rozdzielczosc/sqrt(12)/sqrt((double)P/4.)*sqrt(value/P/MPV);
    cout<<"MPV of fitted LG distribution: "<<MPV<<" Error +/- "<<errMPV<<endl;
    
    // _____________________________________________________________
    // Plotting the results
	auto hist = dynamic_cast<TH1*>(myHist->Clone());
	hist->GetXaxis()->SetTitle("ToT value");
	hist->SetStats(0);
	//hist->GetYaxis()->SetTitle("Number of counts");
	hist->Draw("PE1");

    TGraph* fit3= new TGraph(1000,xx,yy3);
	fit3->SetName("fit3");
    fit3->SetLineColor(6   );
    fit3->SetLineWidth(2);
	fit3->SetMarkerColor(6   );
	fit3->Draw("C");
	
	TGraph* fit5= new TGraph(1000,xx,LG2);
    fit5->SetLineColor(9   );
	fit5->SetMarkerColor(9   );
    fit5->SetLineWidth(2);
	fit5->SetName("fit5");
	fit5->Draw("C");
	
	TGraph* fit6= new TGraph(1000,xx,LG3);
    fit6->SetLineColor(kGreen+3   );
	fit6->SetMarkerColor(kGreen+3);
    fit6->SetLineWidth(2);
	fit6->SetName("fit6");
	fit6->Draw("C");
	
	TGraph* fit7= new TGraph(1000,xx,globalfit);
    fit7->SetLineColor(kOrange+9   );
	fit7->SetMarkerColor(kOrange+9   );
    fit7->SetLineWidth(3);
	fit7->SetName("fit7");
	fit7->Draw("C");
	
	auto legend2 = new TLegend(0.46,0.7,0.95,0.95);
   //legend2->SetHeader("","C"); // option "C" allows to center the header
    legend2->AddEntry(hist,"ToT histogram for all cluster sizes","f");
	legend2->AddEntry("fit3","Main LG distribution","l");
    legend2->AddEntry("fit5","Merged LG #otimes LG","l");
    legend2->AddEntry("fit6","Merged LG #otimes LG #otimes LG","l");
    legend2->AddEntry("fit7","Sum of all","l");
    legend2->Draw("SAME");
	canTest->Update();

    //gROOT->SetBatch(false);
}