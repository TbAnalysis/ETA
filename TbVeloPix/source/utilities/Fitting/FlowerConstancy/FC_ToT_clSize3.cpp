#include "ChargeFitter.h"
#include "TbJob.h"
#include "TbResults.h"
#include "TbGaudi.h"

// ROOT libriaryies
#include "TF2.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"

// RooFIT libriaryies
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooConstVar.h"
#include "RooFFTConvPdf.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooPlot.h"
#include "TLegend.h"

using namespace RooFit;
using namespace std;
////////////////////////////////////////////////////////////////////////////////////
//
void ChargeFitter::FC_ToT_clSize3() {
    //gROOT->SetBatch(true);
    TbGaudi::PrintBanner("INFO", "TbVeloPix:: FC (ToT, cluster size 3) is executed...");

    // _____________________________________________________________
    auto Landau = LoadLandauDataBase();

    // ____________________________________________________________
    // Get data to be fitted.
    auto myHist = ITHistPtr();

    auto canTest = new TCanvas(TString("c"+TString(myHist->GetName())), myHist->GetTitle(),700,600);
    myHist->Draw();

    TbFitter::NEntriesFitThreshold(10); // set 10 events as a fit trigger threshold
    std::cout << "[INFO]:: ChargeFitter:: FC:: Fit nEntries threshold: " << TbFitter::NEntriesFitThreshold() << std::endl;

    //
    // _____________________________________________________________
    // FC routines come here...


	//gROOT->SetBatch(false);
}