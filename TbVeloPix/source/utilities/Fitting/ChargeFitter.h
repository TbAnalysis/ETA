/*! \brief ChargeFitter factory declaration.
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date October-2018
*/

#ifndef TB_VELOPIX_CHARGE_FITTER_H
#define TB_VELOPIX_CHARGE_FITTER_H

// std libriaryies
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <memory>

// TbGaudi libraries
#include "TbFitter.h"
#include "Globals.h"

class TbJob;

using StdDoubleVec = std::vector<std::vector<double>>;

class ChargeFitter: public TbFitter{

	private:
		/// \brief Single Landau x Gauss convolution
		void RFLandauGaussConv();

		/// \brief Very first model implemented by Kazu
		void RFTwoLandauGaussConv();

		/// \brief Definition of LanGau fit with RooFit framework, including background modelling
		void RoofitWithOutOfSpillNoise();

		/// \brief FlowerConstancy specific methods
		StdDoubleVec LoadLandauDataBase();
		void FC_Charge();
		void FC_ToT_clSizeAll();
		void FC_ToT_clSize1();
		void FC_ToT_clSize2();
		void FC_ToT_clSize3();



public:

		///
		ChargeFitter() = delete;

		/// Standard constructor
		ChargeFitter(TbJobSharedPtr job) : TbFitter(job) {}

		///
		~ChargeFitter() = default;

		/// \brief Wrapper for RooFit framework methods
		void Roofit(const std::string& model){
			if(model.compare("LandauGaussConv")==0) 	RFLandauGaussConv();
			if(model.compare("TwoLandauGaussConv")==0)  RFTwoLandauGaussConv();
		}

		/// \brief Wrapper for FlowerConstancy framework methods
		void FC(int clSize = -1){
			if(DataName().compare("ToT")==0) {
				if (clSize == -1)FC_ToT_clSizeAll();
				if (clSize == 1) FC_ToT_clSize1();
				if (clSize == 2) FC_ToT_clSize2();
				if (clSize == 3) FC_ToT_clSize3();
			}
			else if(DataName().compare("Charge")==0) {
				FC_Charge();
			}
		}


};
#endif	// TB_VELOPIX_CHARGE_FITTER_H
