#include "TbVeloPixAnalysis.h"
#include "ITbResults.h"
#include "TbJobCollector.h"
#include "TThread.h"
#include "IRRAD.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::Ana_iCCE_Bartek()
{
    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: CCE for IntraPix of IRRAD sensors is executed (Bartek)...");

    // *** DEFINE PARALLEL PROCESSING ***
    // ___________________________________________________________________________________
    const unsigned nThreads = 4;
    //unsigned nThreads = std::thread::hardware_concurrency();
    ROOT::EnableImplicitMT(nThreads);
    std::cout << "INFO:: number of cores: " << nThreads << std::endl;

    // *** DEFINE ANALYSIS REQUIREMENTS ***
    // ___________________________________________________________________________________
    Dut::IntraPixStudy(true);
//    DutPix::NBinsX(20);
//    DutPix::NBinsY(20);
    DutPix::NBinsX(5);
    DutPix::NBinsY(5);
    Run::DefaultClusterSize(1);
    DutBinning::DefaultType("Elliptic"); // "Uniform"
    DutBinning::DefaultNBins(3);

    FluenceProfile::DefaultType("IRRAD");
    FluenceProfile::PerformEvaluation(true); // perform fluence mapping computation
                                      // -> needs to register a dedicated fluence profile
    FluenceProfile::AddCustomProfile(new IRRAD());
    // Event loop related data
    TbJobIO::ExportEventLoopDataToNTuple(true);
    TbJobIO::ExportEventLoopDataToPdf(true);

    // *** LOAD DATA ***
    // ___________________________________________________________________________________
    FillTbJobsCollection();


    // *** RUN ROUTINE DEDICATED TO THIS ANALYSIS ***
    // ___________________________________________________________________________________
    iCCE_Bartek_MpvMapMaker();
    //iCCE_Bartek_MTMpvMapMaker(); // Multithreading version


    // *** PLOT / COMBINE RESULTS ***
    // ___________________________________________________________________________________
    auto plotter = TbPlotterPtr();
    auto interactive = true;
    plotter->Draw("MPV",interactive);
    //plotter->Draw("Fluence",interactive);
    //plotter->DrawProfile("MPV",interactive);
    //plotter->DrawProfile("MPV","column",interactive);
    //plotter->DrawProfileCombined("MPV","column",interactive);
    //plotter->DrawProfileCombined("MPV","row",interactive);
    //plotter->Draw("MPV","Fluence",interactive);
    //plotter->DrawCombined("MPV","Fluence",interactive);

    //TbIO::OutputLocation("anywhere..."); // by default it's set to project_location/TbResults
    TbResults::ExportResults();

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: (Bartek) CCE for IntraPix is done.");
}



