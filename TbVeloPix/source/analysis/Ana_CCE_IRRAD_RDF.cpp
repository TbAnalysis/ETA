#include "TbVeloPixAnalysis.h"
#include "TbResults.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::Ana_CCE_IRRRAD_RDF()
{
    std::cout<<"[INFO]::TbVeloPixAnalysis::CCE with RDataFrame usage is executed... "<<std::endl;

    // *** DEFINE DATA ***
    TbData::Type(DataType::RDF);
    TbData::TbDBFile(TbGaudi::ProjectLocation()+"/TbVeloPix/data/TbDB_tutorial.dat");

    // *** DEFINE ANALYSIS REQUIREMENTS ***
    Run::DefaultClusterSize(2);
    DutBinning::DefaultType("Uniform");
    //DutBinning::DefaultType("Elliptic");
    DutBinning::DefaultNBins(20);

    RdfData::AddFilter("Clusters","clCharge > 1000"); // unphysical entries
    RdfData::AddFilter("Clusters","clSize < 20");     // max clusters size
    RdfData::AddFilter("Clusters","clIsTracked");     // process only associated clusters
    RdfData::PrintAllFilters();

    // *** LOAD DATA ***
    FillTbJobsCollection();

    // *** RUN ANALYSIS ***
    CCE_IRRAD_RDF_MpvMapMaker();

    // *** PLOT / COMBINE RESULTS ***
    auto tbResults = TbResults::GetInstance();
    //tbResults->DrawDutMap("MPV");

    //TbIO::OutputLocation("anywhere..."); // by default it's set to project_location/TbResults
    //tbResults->Write();
}