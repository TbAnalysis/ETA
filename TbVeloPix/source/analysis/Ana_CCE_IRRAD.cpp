#include "TbVeloPixAnalysis.h"
#include "TbResults.h"
#include "TbJobCollector.h"
#include "IRRAD.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::Ana_CCE_IRRAD()
{
    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::CCE of IRRAD sensors is executed...");

    // *** DEFINE ANALYSIS REQUIREMENTS ***
    // ___________________________________________________________________________________
    Run::DefaultClusterSize(-1);
    DutBinning::DefaultType("Elliptic"); // "Uniform"
    DutBinning::DefaultNBins(4);

    FluenceProfile::DefaultType("IRRAD");
    FluenceProfile::PerformEvaluation(true); // perform fluence mapping computation
                                      // -> needs to register a dedicated fluence profile
    FluenceProfile::AddCustomProfile(new IRRAD());                                   
    // Event loop related data
    TbJobIO::ExportEventLoopDataToNTuple(true);

    // *** LOAD DATA ***
    // ___________________________________________________________________________________
    FillTbJobsCollection();


    // *** RUN ROUTINE DEDICATED TO THIS ANALYSIS ***
    // ___________________________________________________________________________________
    CCE_IRRAD_MpvMapMaker();


    // *** PLOT / COMBINE RESULTS ***
    // ___________________________________________________________________________________
    auto plotter = TbPlotterPtr();
    auto interactive = true;
    //plotter->Draw("MPV",interactive);
    //plotter->Draw("Fluence",interactive);
    // plotter->DrawProfile("MPV",interactive);
    //plotter->DrawProfile("MPV","column",interactive);
    //plotter->DrawProfileCombined("MPV","column",interactive);
    //plotter->DrawProfileCombined("MPV","row",interactive);
    //plotter->Draw("MPV","Fluence",interactive);
    //plotter->DrawCombined("MPV","Fluence",interactive);

    //TbIO::OutputLocation("anywhere..."); // by default it's set to project_location/TbResults
    TbResults::ExportResults();

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::CCE is done.");
}



