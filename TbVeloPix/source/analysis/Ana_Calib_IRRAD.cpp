#include "TbVeloPixAnalysis.h"
#include "TbResults.h"
#include "TbJobCollector.h"
#include "IRRAD.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::Ana_Calib_IRRAD()
{
    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: Calibration of IRRAD sensors");
    // *** DEFINE ANALYSIS DATA ***
    std::string path = TbGaudi::ProjectLocation()+"/TbVeloPix/data/";
    std::string file_name = "CalibrationStudy.toml";
    TbData::TbDBFile(path+file_name);

    // *** DEFINE ANALYSIS REQUIREMENTS ***
    // ___________________________________________________________________________________
    Dut::DefaultIrradiationStatus(true); // i.e. DUT is irradiated
    FluenceProfile::DefaultType("IRRAD");
    Run::DefaultClusterSize(1);
                               
    // Event loop related data
    TbJobIO::ExportEventLoopDataToNTuple(true);

    FluenceProfile::AddCustomProfile(new IRRAD()); 

    // *** LOAD DATA AND RUN PROCESSING ***
    // ___________________________________________________________________________________
    // std::string dataCollection = "dutBinToPixMapping";
    // std::string dataCollection = "labCalibration";
    std::string dataCollection = "genRndCalibration";
    
    if(dataCollection=="dutBinToPixMapping"){
        DutBinning::ExportToCsv(true);  // Export Map of DutBinNumber and Pix (X,Y) to csv file
        FillTbJobsCollection(dataCollection);
    }
    else if(dataCollection=="labCalibration" || dataCollection=="genRndCalibration"){
        FluenceProfile::PerformEvaluation(true); // perform fluence mapping computation
                                                 // Note: needs to register a dedicated fluence profile:
        FluenceProfile::ExportToCsv(true); 
        FillTbJobsCollection(dataCollection);
    }

    // *** RUN ROUTINE DEDICATED TO THIS ANALYSIS ***
    // ___________________________________________________________________________________
    Calib_IRRAD_ChargeFitting();

    // *** Export results ***
    // ___________________________________________________________________________________
    TbResults::ExportResults();

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: Calibration of IRRAD sensors is done.");
}



