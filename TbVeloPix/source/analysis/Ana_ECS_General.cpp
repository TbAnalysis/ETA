#include "TbVeloPixAnalysis.h"
#include "TbResults.h"
#include "TbData.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::Ana_ECS_General()
{
    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::ECS data analysis is executed...");

    // *** DEFINE DATA ***
    TbData::TbDBFile(TbGaudi::ProjectLocation()+"/TbVeloPix/TbVeloPixAnalysis/data/Ecs-Dec18-IkrumScan.dat");

    // *** DEFINE ANALYSIS REQUIREMENTS ***
    Run::DefaultClusterSize(-1);

    // Event loop related data
    TbJobIO::ExportEventLoopDataToNTuple(true);

    // *** LOAD DATA ***
    TbData::Type(DataType::ECS);
    FillTbJobsCollection();

    // *** RUN METHOD DEDICATED TO THIS ANALYSIS ***
    ECS_General_MpvMapMaker();

    // *** PLOT / COMBINE RESULTS ***
    auto tbResults = TbResults::GetInstance();
    // tbResults->DrawDutMap("MPV");
    //tbResults->DrawDutProfile("MPV");
    //tbResults->DrawDutProfileCombined("MPV","column");
    //tbResults->DrawDutProfileCombined("MPV","row");

    //TbIO::OutputLocation("anywhere..."); // by default it's set to project_location/TbResults
    //tbResults->Write(); // export everything to .pdf files

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis::ECS data analysis is done.");
}



