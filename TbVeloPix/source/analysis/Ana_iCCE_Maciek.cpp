#include "TbVeloPixAnalysis.h"
#include "TbResults.h"
#include "TbJobCollector.h"
#include "IRRAD.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::Ana_iCCE_Maciek()
{
    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: CCE for IntraPix of IRRAD sensors is executed (Maciek)...");

    // *** DEFINE ANALYSIS REQUIREMENTS ***
    // ___________________________________________________________________________________
    Dut::IntraPixStudy(true);
    DutPix::NBinsX(10);
    DutPix::NBinsY(10);
    Run::DefaultClusterSize(1);
    DutBinning::DefaultType("Elliptic"); // "Uniform"
    DutBinning::DefaultNBins(4);

    FluenceProfile::DefaultType("IRRAD");
    FluenceProfile::PerformEvaluation(true); // perform fluence mapping computation
                                      // -> needs to register a dedicated fluence profile
    FluenceProfile::AddCustomProfile(new IRRAD());
    // Event loop related data
    TbJobIO::ExportEventLoopDataToNTuple(true);
    TbJobIO::ExportEventLoopDataToPdf(true);

    // *** LOAD DATA ***
    // ___________________________________________________________________________________
    FillTbJobsCollection();


    // *** RUN ROUTINE DEDICATED TO THIS ANALYSIS ***
    // ___________________________________________________________________________________
    iCCE_Maciek_MpvMapMaker();


    // *** PLOT / COMBINE RESULTS ***
    // ___________________________________________________________________________________
    auto plotter = TbPlotterPtr();
    auto interactive = true;
    plotter->Draw("MPV",interactive);
    //plotter->Draw("Fluence",interactive);
    //plotter->DrawProfile("MPV",interactive);
    //plotter->DrawProfile("MPV","column",interactive);
    //plotter->DrawProfileCombined("MPV","column",interactive);
    //plotter->DrawProfileCombined("MPV","row",interactive);
    //plotter->Draw("MPV","Fluence",interactive);
    //plotter->DrawCombined("MPV","Fluence",interactive);

    //TbIO::OutputLocation("anywhere..."); // by default it's set to project_location/TbResults
    TbResults::ExportResults();

    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: (Maciek) CCE for IntraPix is done.");
}



