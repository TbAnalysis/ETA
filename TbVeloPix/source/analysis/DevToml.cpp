#include "TbVeloPixAnalysis.h"
//#include "TbJobCollector.h"
#include "TbDataHandler.h"
#include "JobData.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::DevToml()
{
    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: TOML library implementation development");

    // todo: JobData::AddDefaultUnit("Voltage", "mV"); // todo: -> SetDefaultUnit
    // note: even before the "Volage" is not defined in the list of parameters (so create one)

    std::string path = TbGaudi::ProjectLocation()+"/TbVeloPix/data/";
    std::string file_name = "tbGaudi_examle.toml";

    TbData::TbDBFile(path+file_name);
    FillTbJobsCollection("analysis1");

    /*
    JobData::AddObligatoryParameter<std::string>("DutName");
    JobData::AddObligatoryParameter<unsigned>("RunNumber");
    JobData::AddObligatoryParameter<double>("Voltage");
    JobData::AddDefaultUnit("Voltage", "mV");

    auto parser = TomlInputParser(path+file_name);

    auto analysis_collection = parser.GetAnalysisCollection("analysis1");

    analysis_collection.front().PrintDetails();

    auto voltage_unit = analysis_collection.front().GetValue<std::string>("VoltageUnit");
    std::cout << "Read parameter: " << voltage_unit << std::endl;
    */
    TbGaudi::PrintBanner("INFO","TbVeloPixAnalysis:: TOML library implementation development");
}



