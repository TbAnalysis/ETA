#include "TbVeloPixAnalysis.h"
#include "TbJobCollector.h"
#include "IRRAD.h"
#include "TRandom3.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbVeloPixAnalysis::DevExportToCsv()
{
    TbGaudi::PrintBanner("INFO","TbGaudi:: Export to csv development");

    Dut::PixStudy(true);
    Dut::DefaultIrradiationStatus(true);
    FluenceProfile::AddCustomProfile(new IRRAD());

    auto job = std::make_shared<TbJob>();

    // Define manually user input parameterization
    job->SetParam("DutName", "S8");
    job->SetParam("RunNumber", "12345");
    job->SetParam("RunBiasVoltage", "200");
    job->SetParam("DutBinningType", "Elliptic");
    job->SetParam("DutFluenceProfileType", "IRRAD");
    job->SetParam("DutNBins", "9");
    job->SetParam("RunClusterSize", "2");
    job->SetParam("Temperature", "18"); // [Celsius]

    // add the job to the main store..
    TbJobCollector::GetInstance()->AddTbJob(job);

    auto dut = job->DutPtr();
    // define result interface tool
    auto jobIResults = std::make_shared<ITbJobResults>(job);

    // create a random number generator
    auto gRandom = std::make_unique<TRandom3>();

    // define dummy results:
    auto nBins = dut->NBins();
    double fluence = 1e15;
    for(unsigned bin=1; bin<=nBins; ++bin){
        // get random number from the Gaussian Distribution
        auto val = gRandom->Gaus(400.0-(bin*10),5.0);
        // fill the job
        jobIResults->InsertTbJobResult("MPV", bin, val, val*0.02);
        jobIResults->InsertTbJobResult("Fluence", bin, fluence, 100); 
        fluence+=1e15;
    }

    TbResults::ExportResults(); // export all types of produced results: pdf, csv, ntuple
    
    TbGaudi::PrintBanner("INFO","TbGaudi:: Export to csv development");
}