#include <pybind11/pybind11.h>


namespace py = pybind11;


void PyTbVeloPixAnalysis(py::module &m);

PYBIND11_MODULE(PyTbVeloPix, m) {
    py::module_::import("PyEta");
    PyTbVeloPixAnalysis(m);
}

