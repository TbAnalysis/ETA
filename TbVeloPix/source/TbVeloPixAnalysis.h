/*! \brief TbVeloPixAnalysis class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date May-2018
*/

#ifndef TB_VELOPIX_ANALYSIS_H
#define TB_VELOPIX_ANALYSIS_H

#include "TbGaudi.h"
#include "TbAnalysis.h"
#include "TbResults.h"
#include "DutBinning.h"
#include "ChargeFitter.h"
#include <vector>
#include <map>

////////////////////////////////////////////////////////////////////////////////////
///
class TbVeloPixAnalysis : public TbAnalysis {

	private:
		///
        void CCE_IRRAD_MpvMapMaker();

		///
        void Calib_IRRAD_ChargeFitting();

        ///
        void CCE_IRRAD_RDF_MpvMapMaker();

		///
		void ECS_General_MpvMapMaker();

		///
		void iCCE_Bartek_MpvMapMaker();
		void iCCE_Maciek_MpvMapMaker();
		void iCCE_Bartek_MTMpvMapMaker();

	public:
		///
		TbVeloPixAnalysis() = default;

		///
		~TbVeloPixAnalysis() = default;


		// JSI related methods:
        //___________________________________________________________
		/// Perform CCE analysis of the JSI sensors
		void Ana_CCE_JSI(){ std::cout<<"To be implemented."<<std::endl; }


        // IRRAD related methods:
        //___________________________________________________________
		/// Perform CCE analysis of the IRRAD sensors
		void Ana_CCE_IRRAD();
		void Ana_Calib_IRRAD();
		void Ana_iCCE_Robbert();
		void Ana_iCCE_Bartek();
		void Ana_iCCE_Maciek();
		void Ana_CCE_IRRRAD_RDF(); // analysis definition with RooDataFrame usage (under construction)


        // KIT related methods:
        // ___________________________________________________________
		/// Perform CCE analysis of the KIT sensors
		///
		void Ana_CCE_KIT(){ std::cout<<"To be implemented."<<std::endl; }


		/// Perform analysis of the ECS test-beam data
		// ___________________________________________________________
		///
		void Ana_ECS_General();

		///
		/// ___________________________________________________________
		///
		void DevToml();
		void DevExportToCsv();

};
#endif	// TB_VELOPIX_ANALYSIS_H