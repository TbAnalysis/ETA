# Set the callibration file
def RndCalibFile(SENSOR,IRRADIATION_STATUS):
   labDataPath = '../../../data/RndSurrogateParameterFiles/'
    
   if SENSOR=='S8' and IRRADIATION_STATUS=='AfterIrradiation': 
      calibFile = 'Bin8_s8_post_generated_by_means.csv'
   if SENSOR=='S8' and IRRADIATION_STATUS=='BeforeIrradiation':
      calibFile = 'S8_BeforeIRRAD.dat'

   return labDataPath+calibFile
