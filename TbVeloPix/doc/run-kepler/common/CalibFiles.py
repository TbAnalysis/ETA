# Set the callibration file
def CalibFile(SENSOR,IRRADIATION_STATUS):
   labDataPath = 'eos/lhcb/wg/testbeam/velo/timepix3/LabData/TP/SurrogateParameterFiles/'
    
   if SENSOR=='S8' and IRRADIATION_STATUS=='AfterIrradiation': 
	calibFile = 'S8_PostIRRAD_800V_postIrradEqualise_IKRUM20_29092015_surrog_fitpars_perpix_NNsmoothingON.dat'

   if SENSOR=='S8' and IRRADIATION_STATUS=='BeforeIrradiation':
        calibFile = 'S8_BeforeIRRAD_200V_TestPulse_SpidrTime_18May_surrog_fitpars_perpix_NNsmoothingON.dat'

   if SENSOR=='S11' and IRRADIATION_STATUS=='AfterIrradiation':
        calibFile = 'S11_PostIRRAD_600V_ikrum20_TestPulse_SpidrTime_29112016_surrog_fitpars_perpix_NNsmoothingON.dat'

   if SENSOR=='S11' and IRRADIATION_STATUS=='BeforeIrradiation':
        calibFile = 'S11_BeforeIRRAD_200V_TestPulse_SpidrTime_May19_surrog_fitpars_perpix_NNsmoothingON.dat'

   if SENSOR=='S25' and IRRADIATION_STATUS=='AfterIrradiation':
        calibFile = 'S25_postIrrad_800V_IKRUM20_20150928_surrog_fitpars_perpix_NNsmoothingON.dat'

   if SENSOR=='S25' and IRRADIATION_STATUS=='BeforeIrradiation':
        calibFile = 'S25_150V_TestPulse_SpdirTime_16062015_surrog_fitpars_perpix_NNsmoothingON.dat'

   if SENSOR=='S30' and IRRADIATION_STATUS=='AfterIrradiation':
        calibFile = 'S30_PostIRRAD_600V_TestPulse_25jan2016_surrog_fitpars_perpix_NNsmoothingON.dat'
	#calibFile = 'S30_postIrrad_800V_27092015_surrog_fitpars_perpix_NNsmoothingON.dat'

   if SENSOR=='S30' and IRRADIATION_STATUS=='BeforeIrradiation':
        calibFile = 'S30_BeforeIRRAD_Coarse64_12032015_200V_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat'
   
   if SENSOR=='S16' and IRRADIATION_STATUS=='AfterIrradiation':
        calibFile = 'S16_PostKIT_25072015_400V_surrog_fitpars_perpix_NNsmoothingON.dat'

   if SENSOR=='S4' and IRRADIATION_STATUS=='AfterIrradiation':
        calibFile = 'S4_PostKIT_1000V_07122015_surrog_fitpars_perpix_NNsmoothingON.dat'

   return labDataPath+calibFile
