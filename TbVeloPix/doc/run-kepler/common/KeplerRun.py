def KeplerRunConfig(SENSOR,CALIB_FILE,EVT_MAX,RUN,BIAS,OUTPUT_PATH):
    from Gaudi.Configuration import *
    from Configurables import Kepler
    Kepler().EvtMax = EVT_MAX
    data_path = "eos/lhcb/wg/testbeam/velo/timepix3/"
    alignment_path = "/eos/lhcb/user/t/tevans/public/testbeam/"	
    data_prefix = ""
    if int(RUN) < 2000:
        data_prefix += "July"
        data_prefix += "2014"
    elif int(RUN) < 2815:
        data_prefix += "Oct"
        data_prefix += "2014"
    elif int(RUN) < 4000:
        data_prefix += "Nov"
        data_prefix += "2014"
    elif int(RUN) < 4500:
        data_prefix += "Dec"
        data_prefix += "2014"
    elif int(RUN) < 6800:
        padata_prefixth += "May"
        data_prefix += "2015"
    elif int(RUN) < 10200:
        data_prefix += "July"
        data_prefix += "2015"
    elif int(RUN) < 13000:
        data_prefix += "Sep"
        data_prefix += "2015"
    elif int(RUN) < 14000:
        data_prefix += "Nov"
        data_prefix += "2015"
    elif int(RUN) < 17000:
        data_prefix += "May"
        data_prefix += "2016"
    else:
        data_prefix += "Aug"
        data_prefix += "2016"

    Kepler().Monitoring = False
    Kepler().WriteTuples = True
    Kepler().InputFiles =  [data_path + data_prefix + "/RawData/Run" + RUN + "/"]
    Kepler().PixelConfigFile = [CALIB_FILE]
    #Kepler().AlignmentFile   = path + '/RootFiles/Run' + RUN + '/Conditions/Alignment' + RUN + 'mille.dat'
    Kepler().AlignmentFile   = alignment_path + data_prefix + "/Run" + RUN + "/Alignment" + RUN + "mille.dat"
    Kepler().TupleFile = OUTPUT_PATH+"/"+SENSOR+"_"+RUN+"_"+BIAS+"V.root"
