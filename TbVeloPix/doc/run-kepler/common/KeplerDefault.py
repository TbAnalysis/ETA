def KeplerDefaultConfig():
    # Basic configuration file.
    from Gaudi.Configuration import *
    from Configurables import Kepler, LHCbApp

    MessageSvc().setWarning += ["TbTriggerMonitor"]

    # Set the configuration of the individual algorithms
    from Configurables import TbClustering
    TbClustering().PrintConfiguration = True

    from Configurables import TbClustering
    TbClustering().ClusterErrorMethod = 0

    from Configurables import TbClusterAssociator
    TbClusterAssociator().UseHits = False
    TbClusterAssociator().DUTs = [4]
    TbClusterAssociator().XWindow = 0.02   # default is 1 [mm]
    TbClusterAssociator().TimeWindow = 100 # default is 200

    from Configurables import TbSimpleTracking, TbTrackPlots
    TbSimpleTracking().PrintConfiguration = True
    TbSimpleTracking().TimeWindow = 10
    TbSimpleTracking().MaxDistance = 0
    TbSimpleTracking().MinPlanes = 8
    TbSimpleTracking().MaskedPlanes = [4]                 # this is for :: TbKernel/TbTrackFit
    TbSimpleTracking().MaxOpeningAngle = 0.005
    TbSimpleTracking().RecheckTrack = True
    TbSimpleTracking().ChargeCutLow = 0
    TbSimpleTracking().DoOccupancyCut = False #
    TbSimpleTracking().MaxClusterSize = 20
    TbSimpleTracking().MaxOccupancy = 7 # not inclusive
    TbTrackPlots().MaskedPlanes = [4]

    from Configurables import TbEventBuilder
    TbEventBuilder().MinPlanesWithHits = 4

    from Configurables import TbTupleWriter
    TbTupleWriter().DUT = 4
    TbTupleWriter().WriteDUTOnly = True
    TbTupleWriter().WriteTracks = False
    TbTupleWriter().WriteHits = False
    TbTupleWriter().WriteClusters = True
    TbTupleWriter().WriteClusterHits = False
    TbTupleWriter().WriteTriggers = False
