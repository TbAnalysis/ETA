# Run command with local build
# path_to_kepler/Kepler/build.$CMTCONFIG/run gaudirun.py PostIRRAD-S8-voltage-scan.py

# Configure basic KEPLER configuration file
import sys
import os
sys.path.append("../common")

# Configure the basic KEPLER configuration
from KeplerDefault import *

KeplerDefaultConfig()

# Configure the job configuration

# Set the sensor name and handle callibration file
Sensor = "S8"  # Set the sensor name
EvtMax = -1  # Set the number of events to be processed

# ---------------------------------------------
#RUN = "12024"
#BIAS = "1000"
# RUN = '12024'
# BIAS = '1000'
# ---------------------------------------------
# RUN = '12028'
# BIAS = '900'
# ---------------------------------------------
# RUN = '12032'
# BIAS = '800'
# ---------------------------------------------
# RUN = '12035'
# BIAS = '700'
# ---------------------------------------------
# RUN = '12038'
# BIAS = '600'
# ---------------------------------------------
# RUN = '12041'
# BIAS = '500'
# ---------------------------------------------
RUN = '12044'
BIAS = '400'
# ---------------------------------------------
#RUN = '12047'
#BIAS = '300'
# ---------------------------------------------
# RUN = '12050'
# BIAS = '200'
# ---------------------------------------------
#RUN = '12053'
#BIAS = '100'

# ------------------------------------------------------------------------------
# Set the NTuples output location
outputlocation = '/afs/cern.ch/user/b/brachwal/workspace/DATA/Upgrade/Testbeam/rndCalib'

from CalibFiles import *
from RndCalibFiles import *
from KeplerRun import *

calibFile = CalibFile(Sensor,'AfterIrradiation')
# calibFile = RndCalibFile(Sensor,'AfterIrradiation')
KeplerRunConfig(
    Sensor, calibFile, EvtMax, RUN, BIAS, outputlocation
)

from Configurables import TbTupleWriter

TbTupleWriter().DUT = 4
TbTupleWriter().WriteDUTOnly = True
TbTupleWriter().WriteTracks = False
TbTupleWriter().WriteClusters = True
TbTupleWriter().ClustersTrackedOnly = True

# ---------------------------------------------
