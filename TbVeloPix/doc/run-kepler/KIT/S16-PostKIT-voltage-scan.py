# Run command with local build
# path_to_kepler/Kepler/build.$CMTCONFIG/run gaudirun.py PostKIT-S4-voltage-scan.py

# Configure basic KEPLER configuration file
import sys
import os
sys.path.append("../common")

# Configure the basic KEPLER configuration
from KeplerDefault import *

KeplerDefaultConfig()

# Configure the job configuration

# Set the sensor name and handle callibration file
Sensor = "S16"  # Set the sensor name
EvtMax = -1  # Set the number of events to be processed


# ---------------------------------------------
RUN = "9361"
BIAS = "120"
# --------------------------------------------
# RUN = '9360'
# BIAS = '160'
# --------------------------------------------
# RUN = '9359'
# BIAS = '200'
# --------------------------------------------
# RUN = '9358'
# BIAS = '250'
# --------------------------------------------
# RUN = '9357'
# BIAS = '300'
# --------------------------------------------
# RUN = '9356'
# BIAS = '350'
# --------------------------------------------
# RUN = '9355' # beamspot top right corner
# RUN = '9347' # beamspot bottom left corner
# BIAS = '400'
# --------------------------------------------
# RUN = '9363'
# BIAS = '40'
# --------------------------------------------
# RUN = '9362'
# BIAS = '80'

# ------------------------------------------------------------------------------
# Set the NTuples output location
outputlocation = "/afs/cern.ch/user/b/brachwal/workspace/DATA/Upgrade/Testbeam"

from CalibFiles import *
from KeplerRun import *

KeplerRunConfig(
    Sensor, CalibFile(Sensor, "AfterIrradiation"), EvtMax, RUN, BIAS, outputlocation
)

from Configurables import TbTupleWriter

TbTupleWriter().DUT = 4
TbTupleWriter().WriteDUTOnly = True
TbTupleWriter().WriteTracks = False
TbTupleWriter().WriteClusters = True
TbTupleWriter().ClustersTrackedOnly = True


# ---------------------------------------------
