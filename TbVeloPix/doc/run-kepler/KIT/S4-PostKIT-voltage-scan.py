# Run command with local build
# path_to_kepler/Kepler/build.$CMTCONFIG/run gaudirun.py PostKIT-S4-voltage-scan.py

# Configure basic KEPLER configuration file
import sys
import os
sys.path.append("../common")

# Configure the basic KEPLER configuration
from KeplerDefault import *
KeplerDefaultConfig()

# Configure the job configuration

# Set the sensor name and handle callibration file
Sensor = 'S4'# Set the sensor name
EvtMax = -1  # Set the number of events to be processed

# ---------------------------------------------
# RUN = '7082'
# BIAS = '1000'
# --------------------------------------------
# RUN = '7090'
# BIAS = '900'
# --------------------------------------------
RUN = '7089'
BIAS = '800'
# --------------------------------------------
# RUN = '7088'
# BIAS = '700'
# --------------------------------------------
# RUN = '7087'
# BIAS = '600'
# --------------------------------------------
# RUN = '7091'
# BIAS = '500'
# --------------------------------------------
# RUN = '7092'
# BIAS = '400'
# --------------------------------------------
# RUN = '7093'
# BIAS = '300'
# --------------------------------------------
# RUN = '7094'
# BIAS = '200'
# --------------------------------------------
# RUN = '7095'
# BIAS = '100'

# ------------------------------------------------------------------------------
# Set the NTuples output location
outputlocation = '/afs/cern.ch/user/b/brachwal/workspace/DATA/Upgrade/Testbeam'

from CalibFiles import *
from KeplerRun import *
KeplerRunConfig(Sensor,CalibFile(Sensor,'AfterIrradiation'),EvtMax,RUN,BIAS,outputlocation)

from Configurables import TbTupleWriter
TbTupleWriter().DUT = 4
TbTupleWriter().WriteDUTOnly = True
TbTupleWriter().WriteTracks = False
TbTupleWriter().WriteClusters = True
TbTupleWriter().ClustersTrackedOnly = True


# ---------------------------------------------