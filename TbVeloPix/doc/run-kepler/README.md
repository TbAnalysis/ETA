#### NOTE: Usage of this package for building an analysis is relaying on the data preprocessed with KEPLER. Below and in different directories the configurations is defined.

## Build and Run KEPLER
### Setup the LHCb environment first in order to handle the right compiler:
``export CMTCONFIG=x86_64-centos7-gcc8-opt``  
``source /cvmfs/lhcb.cern.ch/group_login.sh``  

### Clone and build the Kepler package:
It seems that the newest changes in KEPLER repository (master branch) are not deployed, hence all the instructions defined here are for localy build KEPLER ([twiki instructions](https://lbtwiki.cern.ch/bin/view/VELO/Tpx3TestbeamSoftware)):  
``git clone https://gitlab.cern.ch/lhcb/Kepler.git``  
``cd KEPLER``  
``lb-project-init``  
``make``  

### Run KEPLER with your job file:

``build.$CMTCONFIG/run gaudirun.py myJob.py``

## KEPLER job configuration:
All the configuration is stored in one file and can be imported for the user processing from:  
``common/KeplerDefault.py`` 

In similar manner the calibration files for different sensors can be picked up trough the proxy file:  
``common/CalibFiles.py``

Finally, the testbeam data mapping and output file naming template is defined within:  
``common/KeplerRun.py``

The output file naming template is currently set to the following:  
**SensorName_RunNumber_BiasVoltageV.root**
