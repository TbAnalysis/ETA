# KEPLER preprocessed telescope data
The data are available on eos space:  
``/eos/lhcb/user/b/brachwal/DATA/Upgrade/testbeam/timepix3/``

## Testbeam data type:
There are different directories where the following test beam data type are stored:  
**DUTOnly** - data extracted only for the DUT plane of the telescope;  
**DUTOnly/Clusters** - all recostructed clusters are available  
**DUTOnly/ClustersTrackedOnly** - only associated clusters are available

## Data available for the analyses:

### Post IRRAD S8, [twiki documentation](https://lbtwiki.cern.ch/bin/view/VELO/S8%28S35-450-P3%29)  
___
#### Bias scan at perpendicular track incidence, [ELOG entry](https://lblogbook-run1.cern.ch/Timepix3+Testbeam/450)    
``/eos/lhcb/user/b/brachwal/DATA/Upgrade/testbeam/timepix3/DUTOnly/ClustersTrackedOnly``  

| Run Number | Bias Voltage | Note |
| --- | --- | --- |
| 12024 | 1000 |
| 12028 | 900 | also in .../DUTOnly/Clusters dir
| 12032 | 800 |
| 12035 | 700 |
| 12038 | 600 |
| 12041 | 500 | also in .../DUTOnly/Clusters dir
| 12044 | 400 |
| 12047 | 300 |
| 12050 | 200 | also in .../DUTOnly/Clusters dir
| 12053 | 100 |

### Post KIT S4, [twiki documentation](https://lbtwiki.cern.ch/bin/view/VELO/S4%28S39-600-P2%29) 
___
#### HV scan at nominal angle, [ELOG entry](https://lblogbook-run1.cern.ch/Timepix3+Testbeam/390)
``/eos/lhcb/user/b/brachwal/DATA/Upgrade/testbeam/timepix3/DUTOnly/ClustersTrackedOnly``  

| Run Number | Bias Voltage | Note |
| --- | --- | --- |
| 7082 | 1000 | 
| 7090 | 900 | 
| 7089 | 800 |
| 7088 | 700 |
| 7087 | 600 |
| 7091 | 500 |
| 7092 | 400 |
| 7093 | 300 |
| 7094 | 200 |
| 7095 | 100 |

### Post KIT S16, [twiki documentation](https://lbtwiki.cern.ch/bin/view/VELO/S16%28S35-450-P1%29) 
___
#### HV Scan Threshold 1000e, [ELOG entry](https://lblogbook-run1.cern.ch/Timepix3+Testbeam/424)
``/eos/lhcb/user/b/brachwal/DATA/Upgrade/testbeam/timepix3/DUTOnly/ClustersTrackedOnly`` 

| Run Number | Bias Voltage | Note |
| --- | --- | --- |
| 9355 | 400 |
| 9347 | 400 | beamspot bottom left corner
| 9356 | 350 |
| 9357 | 300 |
| 9358 | 250 |
| 9359 | 200 |
| 9360 | 160 |
| 9361 | 120 |
| 9362 | 80 |
| 9363 | 40 |