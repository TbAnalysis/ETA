# The LHCb VeloPix test beam analysis code

## Analysis modes - text UI
By running the **./TbVeloPix/TbVeloPixAnalysis --help** we can see available/implemented analysis methods.
Each method is described in details below (see section implemented analysis methods):

```
[INFO]:: Text UI mode - command line options
Usage:
  ./TbVeloPix/run [OPTION...]

 Analysis modes options:
      --CCE           Charge Collection Efficiency studies
      --iCCE_Robbert  Charge Collection Efficiency studies for intrapixel
      --iCCE_Bartek   Charge Collection Efficiency studies for intrapixel
      --Ecs           Getting started analysis with ECS data

 Job processing options:
      --NEvt NUMBER  Number of events to be pocessed in TTree event loop
                     (default -1, all events)
      --Input FILE   Specify input data file
```


## Implemented utilities:

### IRRAD fluence profile mapping
By inheriting from the TbGaudi general IRRAD profile, the per sensor details are introduced.
That handle the elliptical fluence profile alignment.

### Charge fitter
By handling the data trough the RooDataSets a dedicated charge fitter is implemented 
(inheriting from the TbGaudi general Fitter). This implements the Landau-Gaussian pdf.

### Charge Collection Efficiency
* [JIRA board](https://its.cern.ch/jira/secure/RapidBoard.jspa?rapidView=6440&projectKey=TBVELOPIX)
* TWIKI page: to be defined...

# GENERAL HOWTO:
### get the code and compile:
```
cd to_your_working_directory_on_lxplus6.cern.ch/  
git clone ssh://git@gitlab.cern.ch:7999/TbAnalysis/Tb.git
cd Tb/
setup setup-slc6-cvmfs.sh
mkdir build
cd build
cmake -DBUILD_TBVELOPIX=ON ../
make -j4
```
### run the analysis:
* see what is available:
```
./TbVeloPix/run --help
```
* run your analysis method
```
./TbVeloPix/run --iCCE_Robbert --NEvt 100000 --Input ../TbVeloPix/doc/data/S8_Bartek.dat
```
* current status:
  - Loading the data and produce the hit statistic for elliptical binning
  - everything is being dumped to .pdf and .root files
  
* next steps (?)  
  - Robbert: prepare your own .dat file (--Input)
  - Bartek: prepare methods to extract charge histograms for intrapixel data and for different fluences
  - Bartek: verify fluence mapping to the elliptical binning (IRRAD scheme)
  - Bartek: add KIT and JSI models
