# The LHCb UT test beam analysis code

## Analysis modes - text UI
By running the **./TbUt/TbUtAnalysis --cmd-help** we can see available/implemented analysis methods.
Each method is described in details below (see section implemented analysis methods).

```
    Text UI mode - command line options
    Usage:
      ./TbUt/TbUtAnalysis [OPTION...]
    
          --cmd-help  Print help
    
     Analysis modes options:
          --CCE         Charge Collection Efficiency studies
          --input FILE  Specify input data file
```


## Implemented utilities:

### Charge fitter
To be described...

## Implemented analysis methods:

### Charge Collection Efficiency
* [JIRA board](https://its.cern.ch/jira/secure/RapidBoard.jspa?projectKey=LHCBUTANA&rapidView=6447)
* TWIKI page: to be defined...