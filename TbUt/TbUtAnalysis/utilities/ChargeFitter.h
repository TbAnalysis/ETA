/*! \brief ChargeFitter factory declaration.
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date October-2018
*/

#ifndef TB_VELOPIX_CHARGE_FITTER_H
#define TB_VELOPIX_CHARGE_FITTER_H

// std libriaryies
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <memory>

// TbGaudi libraries
#include "TbFitter.h"
#include "Globals.h"

class TbJob;

class ChargeFitter: public TbFitter{

	public:

   		 /// \brief Definition of LanGau fit with RooFit framework
        void Roofit();

		/// Standard constructor
		ChargeFitter(TbJobSharedPtr job) : TbFitter(job) {}

		///
		~ChargeFitter() = default;


};
#endif	// TB_VELOPIX_CHARGE_FITTER_H
