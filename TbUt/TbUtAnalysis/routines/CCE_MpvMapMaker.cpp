#include "TbUtAnalysis.h"
#include "TbJobPlotter.h"
#include "TbGaudi.h"
#include "DutSvc.h"
#include "memory.h"
#include <functional>

#include "TH1D.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooCategory.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbUtAnalysis::CCE_MpvMapMaker()
{

    TbGaudi::PrintBanner("INFO","TbUtAnalysis::CCE_MpvMapMaker is executed.");
    auto jobs = GetTbJobsCollection();
    for (auto& iTbJob : jobs ) {
        //_______________________________________________________________________________________
        // SET GLOBALLY AVAILABLE TBRUN CURRENTLY BEING PROCESSED!
        TbJob::CurrentTbJob(iTbJob);

        //_______________________________________________________________________________________
        // List general info about the test-beam run being processed
        std::cout<<"[INFO]:: TbUtAnalysis:: processing: "<< std::endl;
        iTbJob->PrintAllInfo();

        //_______________________________________________________________________________________
        // Define data containers, ROOT hists, RooDataSets, RooCategories etc.
        iTbJob->DefineDutBinningHist("Charge",50,0.,50.);
        iTbJob->DefineRooRealVar("Charge",0.,50.,"ADC");       // template variable to construct RooDataSet instances
        //OBSOLETE iTbJob->DefineDutBinningDataSet("Charge",0.,50.,"ADC");
        auto jobDutTbRds = iTbJob->CreateDutTbRooDataSet();               // creates single RooDataSet instance 
        auto jobDutBinnedTbRds = iTbJob->CreateDutBinningTbRooDataSet();  // creates collection of RooDataSets according to DUT binning scheme
        //OBSOLETE iTbJob->DefineDutBinningDataSetType("Charge","Signal",1);   // RooCategory::defineType(...)
        //OBSOLETE iTbJob->DefineDutBinningDataSetType("Charge","Noise",0);    // RooCategory::defineType(...)


        //_______________________________________________________________________________________
        // TTree variables definition
        auto tree = iTbJob->RootDataPtr()->GetTTree("uttb");
        UInt_t var_stripI;
        UInt_t var_adcI;

        tree->SetBranchAddress("stripI", &var_stripI);
        tree->SetBranchAddress("adcI", &var_adcI);

        //_______________________________________________________________________________________
        // EVENT LOOP AND EVENT SELECTION
        auto Nentries = iTbJob->RootDataPtr()->GetNEntries("uttb");
        std::cout<<"[INFO]:: TbUtAnalysis::EventLoop:: NEntries "<< Nentries << std::endl;
        auto runNumber = iTbJob->GetParam("RunNumber");

        for (int it = 0; it < Nentries; it++) {
            tree->GetEntry(it);

            // Preselection:
            // if (var_clCharge < 1000) continue;    // unphysical entries
            // if (var_clSize > 20)     continue;    // max clusters size
            // if (!var_clIsTrk)        continue;    // process only associated clusters

            if(it%2000==0 && TbGaudi::Verbose()) {
                std::cout << "[INFO]:: TbUtAnalysis:: Run::"
                          << runNumber << " EventLoop:: Processed event "<< it << " ("
                          << std::setprecision(2)<< (static_cast<double>(it)/Nentries) * 100 << "%)"<< std::endl;
            }

            iTbJob->CountDutHit(var_stripI); // count a dut bin/strip statistic hit map

            // Fill histograms, RooDataSets, etc.
            auto dutBinNumber = iTbJob->DutBinNumber(var_stripI); //all strips lay in the same bin!
            if(dutBinNumber>=0) { // '-1' is returned for outsiders!
                iTbJob->DutHistPtr("Charge", dutBinNumber)->Fill(var_adcI);
                jobDutBinnedTbRds->At(dutBinNumber)->Fill("Charge",var_adcI);
                // OBSOLETE iTbJob->DutDataSetRCatPtr("Charge", dutBinNumber)->setIndex(1);   //temporary, set everything as signal
                // OBSOLETE iTbJob->DutDataSetRRVarPtr("Charge", dutBinNumber)->setVal(var_adcI);
                // OBSOLETE iTbJob->AddToDutDataSet("Charge", dutBinNumber);
            }
        }

        //iTbJob->PrintCountersStatistic();
        //iTbJob->DutBinningPtr()->PrintStatistic();
        iTbJob->PlotterPtr()->DrawDutStatistic();       // hists are automatically exported to TbResults

        //_______________________________________________________________________________________
        // Perform data fitting
        auto chargeFitter =  std::make_unique<ChargeFitter>(iTbJob);
        chargeFitter->SetData(iTbJob, "Charge", FitDataType::RooDataSet);
        chargeFitter->Roofit(); // run fitting

        //_______________________________________________________________________________________
        // At the end of each iteration release memory, loop related data are no needed anymore!
        iTbJob->ClearEventLoopCollectedData(); // one method to clear all!

    } // TbJobCollection() loop

    TbGaudi::PrintBanner("INFO","TbUtAnalysis::CCE_MpvMapMaker THE END!");
}

////////////////////////////////////////////////////////////////////////////////////
///