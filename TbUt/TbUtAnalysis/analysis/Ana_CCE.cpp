#include "TbUtAnalysis.h"
#include "TbResults.h"
#include "TbJobCollector.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbUtAnalysis::Ana_CCE()
{
    TbGaudi::PrintBanner("INFO","TbUtAnalysis::CCE is executed...");

    // *** DEFINE DATA ***
    TbData::TbDBFile(TbGaudi::ProjectLocation()+"/TbUt/TbUtAnalysis/data/TbDB_A10_gettingStart.dat");

    // *** DEFINE ANALYSIS REQUIREMENTS ***
    Dut::NStrips(128);
    Run::DefaultClusterSize(1);
    DutBinning::DefaultType("Strip");
    DutBinning::DefaultNBins(16);

    // Event loop related data
    TbJobIO::ExportEventLoopDataToNTuple(true);

    // *** LOAD DATA ***
    FillTbJobsCollection();

    // *** RUN METHOD DEDICATED TO THIS ANALYSIS ***
    CCE_MpvMapMaker();

    // *** PLOT / COMBINE RESULTS ***
    auto tbResults = TbResults::GetInstance();
    /*tbResults->DrawDutMap("MPV");
    tbResults->DrawDutProfile("MPV");
    tbResults->DrawDutProfileCombined("MPV","column");
    tbResults->DrawDutProfileCombined("MPV","row");
    */
    //TbIO::OutputLocation("anywhere..."); // by default it's set to project_location/TbResults
    //tbResults->Write(); // export everything to .pdf files

    TbGaudi::PrintBanner("INFO","TbUtAnalysis::CCE is done.");
}