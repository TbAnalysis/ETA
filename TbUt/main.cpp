// std libriaries
#include <iostream>
#include <memory>

// ROOT libriaries
#include <TROOT.h>
#include <TApplication.h>


// TbGaudi libriaries
#include "TbGaudi.h"
#include "TbStyle.h"
//#include "TbAnalysis.h"
//#include "TbJob.h"
//#include "TbResults.h"
#include "cxxopts.h"

// Project libriaries
#include "TbUtAnalysis.h"

int main(int argc, char **argv)
{
    TbGaudi::Severity("Debug");

    if (argc > 1) {
        cxxopts::Options options(argv[0], "[INFO]:: Text UI mode - command line options");
        try {
            options.positional_help("[optional args]");
            options.show_positional_help();
            options.add_options()("help", "Print help");
            options.add_options("Analysis modes")
                    ("CCE", "Charge Collection Efficiency studies")
                    ("input", "Specify input data file", cxxopts::value<std::string>(), "FILE");

            auto results = options.parse(argc, argv);
            if (results.count("help")) {
                std::cout << options.help({"", "Analysis modes"}) << std::endl;
                std::exit(EXIT_SUCCESS);
            }

            // --------------------------------------------------------------------
            auto cmdopts = std::move(results);

            // --------------------------------------------------------------------
            auto rootapp = std::make_unique<TApplication>("App", &argc, argv);
            auto MyAnalysis = std::make_unique<TbUtAnalysis>();

            // --------------------------------------------------------------------
            //if (cmdopts.count("input"))
            // TODO MyAnalysis->SetInputFile(cmdopts["input"].as<std::string>());

            if (cmdopts.count("CCE")) MyAnalysis->Ana_CCE();

            // --------------------------------------------------------------------
            //PressEnterToQuit();
            rootapp->Run(!gROOT->IsBatch());

        }
        catch (const cxxopts::OptionException& e) {
            std::cout << "error parsing options: " << e.what() << std::endl;
            std::exit(EXIT_FAILURE);
        }
    }
    else
        std::cout << "[ERROR]:: Command line options missing (use '" << argv[0] << " --help' if needed)" << std::endl;
    std::exit(EXIT_SUCCESS);
}
