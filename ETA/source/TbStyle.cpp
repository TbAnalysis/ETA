#include "TbStyle.h"

// ROOT libriaryies
#include "TROOT.h"
#include "TStyle.h"
#include "TPaveText.h"
#include "TColor.h"
#include "TLatex.h"
#include "TGaxis.h"
#include "TObject.h"

tStyle TbStyle::m_Style = tStyle::Bartek;
std::vector<int> TbStyle::MyColors;
std::vector<int> TbStyle::MyMarkers;
int TbStyle::m_i_color=0;
int TbStyle::m_i_marker=0;

////////////////////////////////////////////////////////////////////////////////////
TbStyle::TbStyle()
{
  std::cout<<"[INFO]::TbStyle::Initialization"<<std::endl;
  TbStyle::MyColors.push_back(1);  	// kBlack
  TbStyle::MyColors.push_back(629);	// kRed-3
  TbStyle::MyColors.push_back(632);	// kRed
  TbStyle::MyColors.push_back(635);	// kRed+3
  TbStyle::MyColors.push_back(600);	// kBlue
  TbStyle::MyColors.push_back(616);	// kMagenta
  TbStyle::MyColors.push_back(800);	// kOrange
  TbStyle::MyColors.push_back(807);	// kOrange+7
  TbStyle::MyColors.push_back(413);	// kGreen-3
  TbStyle::MyColors.push_back(416);	// kGreen
  TbStyle::MyColors.push_back(418);	// kGreen+2
  TbStyle::MyColors.push_back(429);	// kCyan-3
  TbStyle::MyColors.push_back(432);	// kCyan
  TbStyle::MyColors.push_back(435);	// kCyan+3
  TbStyle::MyColors.push_back(920); // kGrey

  // Fill set of markers for the framework
  TbStyle::MyMarkers.push_back(21); 		//
  TbStyle::MyMarkers.push_back(22); 		//
  TbStyle::MyMarkers.push_back(33); 		//
  TbStyle::MyMarkers.push_back(34); 		//

  switch(TbStyle::m_Style){
    case tStyle::LHCb:
      SetLHCbStyle();
      break;
    case tStyle::Kazu:
      SetKazuStyle();
      break;
    case tStyle::Bartek:
      SetBartekStyle();
      break;
  }

}

////////////////////////////////////////////////////////////////////////////////////
void TbStyle::Style(tStyle val){
//  std::cout<<"[INFO]::TbStyle::Style:: set style to "<< static_cast<int>(val) <<std::endl;
  m_Style = val;
}

////////////////////////////////////////////////////////////////////////////////////
int TbStyle::Color(int i){
  if(i<TbStyle::MyColors.size()){
    return TbStyle::MyColors.at(i);
  }
  else{
    std::cout<<"[WARNING]::TbStyle::Color:: no enough colors! Return kGrey"<<std::endl;
    return 920;
  }
}

////////////////////////////////////////////////////////////////////////////////////
int TbStyle::iColor() {
    auto icolor = TbStyle::Color(TbStyle::m_i_color);
    TbStyle::m_i_color++;
    return icolor;
}

////////////////////////////////////////////////////////////////////////////////////
int TbStyle::Marker(int i){
  if(i<TbStyle::MyMarkers.size()){
    return TbStyle::MyMarkers.at(i);
  }
  else{
    std::cout<<"[WARNING]::TbStyle::Marker:: no enough markers! Return circle"<<std::endl;
    return 34;
  }
}

////////////////////////////////////////////////////////////////////////////////////
int TbStyle::iMarker() {
    auto imarker = TbStyle::Marker(TbStyle::m_i_marker);
    TbStyle::m_i_marker++;
    return imarker;
}

////////////////////////////////////////////////////////////////////////////////////
void TbStyle::SetLHCbStyle(){
  std::cout<<"[INFO]::TbStyle::SetLHCbStyle"<<std::endl;
  //Int_t lhcbFontTitle = 22;
 Int_t lhcbFontTitle = 42;
 Int_t lhcbFontLabel = 42;
 gStyle->SetTitleFont (42);
 gStyle->SetLegendFont(42);
 lhcbFontTitle       = 42;
 lhcbFontLabel       = 42;

 lhcbFontTitle = 22;
 lhcbFontLabel = 22;
 gStyle->SetTitleFont (22);
 gStyle->SetLegendFont(22);
 Double_t lhcbWidth = 2.00;

 gStyle->SetFrameBorderMode(0);
 gStyle->SetCanvasBorderMode(0);
 gStyle->SetPadBorderMode(0);
 gStyle->SetPadColor(0);
 gStyle->SetCanvasColor(0);
 gStyle->SetStatColor(0);
 gStyle->SetPalette(1);
 // set the paper & margin sizes
 gStyle->SetPaperSize(20,26);
 gStyle->SetPadTopMargin(0.05);
 gStyle->SetPadRightMargin(0.15); // increase for colz plots!!
 gStyle->SetPadBottomMargin(0.13);
 gStyle->SetPadLeftMargin(0.12);
 // use large fonts
 gStyle->SetTextFont(lhcbFontLabel);
 gStyle->SetTextSize(0.04);
 gStyle->SetLabelFont(lhcbFontLabel,"x");
 gStyle->SetLabelFont(lhcbFontLabel,"y");
 gStyle->SetLabelFont(lhcbFontLabel,"z");
 gStyle->SetLabelSize(0.0342,"x");
 gStyle->SetLabelSize(0.0342,"y");
 gStyle->SetLabelSize(0.0342,"z");
 gStyle->SetTitleFont(lhcbFontTitle);
 gStyle->SetTitleFont(lhcbFontTitle, "x");
 gStyle->SetTitleFont(lhcbFontTitle, "y");
 gStyle->SetTitleFont(lhcbFontTitle, "z");
 gStyle->SetTitleSize(0.04,"x");
 gStyle->SetTitleSize(0.04,"y");
 gStyle->SetTitleSize(0.04,"z");
 // use bold lines and markers
 gStyle->SetLineWidth(lhcbWidth);
 gStyle->SetFrameLineWidth(2.0);
 gStyle->SetHistLineWidth(lhcbWidth*1.5);
 gStyle->SetFuncWidth(lhcbWidth*1.5);
 //gStyle->SetGridWidth(1.5);
 gStyle->SetGridStyle(1);
 gStyle->SetGridColor(kGray+1);
 gStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes
 //gStyle->SetMarkerStyle(15);
 gStyle->SetMarkerStyle(20);
 gStyle->SetMarkerSize(1.5);

 gStyle->SetLegendBorderSize(0);
 // label offsets
 gStyle->SetLabelOffset(0.007);

 // by default, do not display histogram decorations:
 gStyle->SetOptStat(0);
 //gStyle->SetOptStat(1110);  // show only nent, mean, rms
 gStyle->SetOptTitle(0);
 gStyle->SetOptFit(0);
 //gStyle->SetOptFit(1011); // show probability, parameters and errors

 // look of the statistics box:
 gStyle->SetStatBorderSize(1);
 gStyle->SetStatFont(lhcbFontLabel);
 gStyle->SetLabelFont(lhcbFontLabel);
 gStyle->SetLegendFont(lhcbFontLabel);
 gStyle->SetTitleFont(lhcbFontLabel);
 gStyle->SetStatFontSize(0.04);
 gStyle->SetStatX(0.75);
 gStyle->SetStatY(0.85);
 gStyle->SetStatW(0.25);
 gStyle->SetStatH(0.15);

 gStyle->SetFrameFillStyle(0);
 gStyle->SetFrameFillColor(0);
 gStyle->SetHistFillStyle(0);
 gStyle->SetFillStyle(0);
 gStyle->SetStatStyle(0); //to view data behind box too!
 gStyle->SetStatBorderSize(0);   // remove shade from stat box
 // put tick marks on top and RHS of plots
 gStyle->SetPadTickX(1);
 gStyle->SetPadTickY(1);

 // histogram divisions: only 5 in x to avoid label overlaps
 gStyle->SetNdivisions(505,"x");
 gStyle->SetNdivisions(510,"y");

 TPaveText *lhcbName = new TPaveText(0.65,0.8,0.9,0.9,"BRNDC");
 lhcbName->SetFillColor(0);
 lhcbName->SetTextAlign(12);
 lhcbName->SetBorderSize(0);
 lhcbName->AddText("LHCb");

 TText *lhcbLabel = new TText();
 lhcbLabel->SetTextFont(lhcbFontTitle);
 lhcbLabel->SetTextColor(1);
 lhcbLabel->SetTextSize(0.04);
 lhcbLabel->SetTextAlign(12);

 TLatex *lhcbLatex = new TLatex();
 lhcbLatex->SetTextFont(lhcbFontTitle);
 lhcbLatex->SetTextColor(1);
 lhcbLatex->SetTextSize(0.04);
 lhcbLatex->SetTextAlign(12);

 //////////////////////////////////////////////////////////////////////////////////////////////////////

 TColor *svc = new TColor();

 BlueGrey = svc->GetColor(230, 230, 255);
 DarkPink = svc->GetColor(102,   0,  51);
 Pink     = svc->GetColor(204,   0, 153);
 Azure    = svc->GetColor(  0, 153, 255);
 Purple   = svc->GetColor(102,   0, 204);
 Violet   = svc->GetColor( 51,   0, 153);
 Teal     = svc->GetColor( 51, 153, 102);

 chart1   = svc->GetColor(  0,  69, 134);
 chart2   = svc->GetColor(255,  66,  14);
 chart3   = svc->GetColor(255, 211,  32);
 chart4   = svc->GetColor( 87, 157,  28);
 chart6   = svc->GetColor(131, 202, 255);
 chart8   = svc->GetColor(174, 207,   0);
 chart12  = svc->GetColor(  0, 132, 209);
 grey40   = svc->GetColor(153, 153, 153);
 grey60   = svc->GetColor(102, 102, 102);
 sun3     = svc->GetColor(153, 153, 204);
 sun4     = svc->GetColor(204, 204, 255);

 LighterBlueGrey = sun4;
 LightBlueGrey = sun3;
 DarkGrey = grey60;
 LightGrey = grey40;
 CoolBlue = chart12;
 AvocadoGreen = chart8;
 LightBlue = chart6;
 DarkGreen = chart4;
 Yellow = chart3;
 Orange = chart2;
 DarkBlue = chart1;


 gStyle->SetCanvasColor(0);
 gStyle->SetFrameFillColor(0);
 gStyle->SetFrameBorderMode(0);
 gStyle->SetHistFillColor(0);
 gStyle->SetMarkerSize(1.2);
 gStyle->SetMarkerStyle(20);
 gStyle->SetMarkerColor(1);
 //gStyle->SetOptStat(10);
 gStyle->SetOptTitle(0);
 gStyle->SetPadColor(0);
 gStyle->SetPadGridX(1);
 gStyle->SetPadGridY(1);
 gStyle->SetPadTickX(1);
 gStyle->SetPadTickY(1);
 gStyle->SetPalette(1);
 gStyle->SetStatColor(0);
 gStyle->SetTitleFillColor(0);
 //gStyle->SetHistFillStyle(1001);
}

////////////////////////////////////////////////////////////////////////////////////
void TbStyle::SetKazuStyle(){
  std::cout<<"[INFO]::TbStyle::SetKazuStyle"<<std::endl;
  ///
  gROOT->SetStyle("Plain"); //no grey background on plots
  gStyle->SetStatStyle(0); //to view data behind box too!
  gStyle->SetStatBorderSize(0);   // remove shade from stat box
  gStyle->SetFrameLineWidth(1.0);
  gStyle->SetStatH(0.2);  //x,y,h,w define size and position
  gStyle->SetStatW(0.225);
  gStyle->SetStatX(0.9);
  gStyle->SetStatY(0.9);
  gStyle->SetPadGridY(1);
  gStyle->SetPadGridX(1);
  gStyle->SetFillStyle(1001);

  gStyle->SetStatBorderSize(0);
  gStyle->SetStatFormat("4.2g");

  gStyle->SetTitleBorderSize(0);  // remove shade from title box
  gStyle->SetOptStat(111111);
  gStyle->SetOptFit(111111);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  gStyle->SetFrameFillStyle(0);
  gStyle->SetFrameFillColor(0);
  gStyle->SetAxisColor(1, "X");  // remove shade from title box
  gStyle->SetAxisColor(1, "Y");  // remove shade from title box
  gStyle->SetTitleColor(1, "X");  // remove shade from title box
  gStyle->SetTitleColor(1, "Y");  // remove shade from title box
  gStyle->SetTitleOffset(1, "X");  //
  gStyle->SetTitleOffset(1.15, "Y");  //
  gStyle->SetLabelOffset(0.006,"X");  //
  gStyle->SetLabelOffset(0.006, "Y");  //
  gStyle->SetTitleX(0.25);
  gStyle->SetTitleW(0.75);
  gStyle->SetTitleH(.092);
  //gStyle->SetTitleFont(62);
  gStyle->SetTitleTextColor(1); //make the thing transparant??
  gStyle->SetTitleStyle(0); //make the thing transparant??
  //and axis
   gStyle->SetLabelSize(.03,"X");
   gStyle->SetLabelSize(.03,"Y");
   gStyle->SetLabelSize(.03,"Z");
  // //now the hist itself
  gStyle->SetLineWidth(2);
  gStyle->SetHistLineWidth(2);
  gStyle->SetHistLineColor(1);
  //gStyle->SetHistFillColor(0);
  gStyle->SetHistFillStyle(0);
  gStyle->SetFillStyle(0);

  const Int_t NRGBs = 5;
  const Int_t NCont = 64;

  Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
}

////////////////////////////////////////////////////////////////////////////////////
void TbStyle::SetBartekStyle(){
  std::cout<<"[INFO]::TbStyle::SetBartekStyle"<<std::endl;
  const Int_t NRGBs = 5;
    const Int_t NCont = 255;
    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);

    TGaxis::SetMaxDigits(3);

    gStyle->SetNdivisions(510,"x");
    gStyle->SetNdivisions(510,"z");
    gStyle->SetLabelSize(0.025,"xyz");
    gStyle->SetLabelOffset(0.015,"y");
    //gStyle->SetLabelOffset(0.,"z");
    gStyle->SetTitleSize(0.03,"xyz");
    gStyle->SetTitleOffset(1.00,"x");
    gStyle->SetTitleOffset(1,"y");

	  gStyle->SetTitleOffset(-0.15,"z");

	  gStyle->SetOptStat(0);
	  gStyle->SetNdivisions(510,"y");

    // gStyle->SetPaperSize(20,26);
    // gStyle->SetPadTopMargin(0.05);//
    // gStyle->SetPadRightMargin(0.2); // increase for colz plots
    // gStyle->SetPadBottomMargin(0.16);
    // gStyle->SetPadLeftMargin(0.14);
}
////////////////////////////////////////////////////////////////////////////////////
