/*! \brief TbGlobal  methods declarations.
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date March-2018
*
*/
#ifndef TB_GLOBALS_H
#define TB_GLOBALS_H

// std libriaryies
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdint>
#include <cstdlib>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <array>
#include <assert.h>
#include <stdio.h>
#include <iomanip>
#include <limits>
#include <thread>

// ROOT libriaryies
#include "TTree.h"
#include "TFile.h"
#include "TF2.h"
#include "TEllipse.h"
#include "TMath.h"
#include "TH1D.h"


/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Simple method to pause the application.
void PressEnterToQuit();

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Check if specified file exists.
/// \param m_Fname absolute path to the file
bool exists (const std::string& m_Fname);

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Read the TTree from a specified file/directory.
/// \param[in] m_filename absolute path to the .root file
/// \param[in] m_dir the name of the TDirectory
/// \param[in] m_tree the name of the TTree
/// \param[out] TTree pointer
TTree* GetTTreeFromTDir (std::string m_filename, std::string m_dir, std::string m_tree);

////////////////////////////////////////////////////////////////////////////////////////
template <typename T>
bool Includes(const std::vector<T>& stdVect, const T& val) {
	auto includes = std::find(std::begin(stdVect), std::end(stdVect), val);
	if (includes != std::end(stdVect))
		return true;
	else
		return false;
}

////////////////////////////////////////////////////////////////////////////////////////
template <typename T, typename K>
bool Includes(const std::map<T,K>& stdMap, const T& mKey) {
	if(stdMap.empty() || stdMap.find(mKey) == stdMap.end())
		return false;
	else
		return true;
}

////////////////////////////////////////////////////////////////////////////////////////
template <typename T>
bool CollectionScopeTest(const std::vector<T>& vector, unsigned index, const std::string& message){
	if (vector.size()<index){
		std::cout << "[ERROR]:: CollectionScopeTest::FAILED | " << message << std::endl;
		return false;
	} else
		return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief convert std::thread::id to string
std::string idtos(const std::thread::id& m_id);

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Simple function to convert double to string with exact precission
std::string dtos(double m_d, int m_precission);

/////////////////////////////////////////////////////////////////////////////////////////
///
std::string sto_lower(std::string data);

/////////////////////////////////////////////////////////////////////////////////////////
///
const std::string btos(bool val);

/////////////////////////////////////////////////////////////////////////////////////////
///
bool stob(const std::string& val);

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Simple function to convert double to string with precission adjusting
std::string dyn_dtos(double m_d, int m_max_precission);

/////////////////////////////////////////////////////////////////////////////////////////
std::string itos(int m_i);

/////////////////////////////////////////////////////////////////////////////////////////
unsigned stou(std::string m_s);

/////////////////////////////////////////////////////////////////////////////////////////
double GetTF2UniformBinIntegral(TF2* m_func,
							 std::pair<int,int> m_bin_min,
							 std::pair<int,int> m_bin_max,
							 double m_normalization);

/////////////////////////////////////////////////////////////////////////////////////////
bool IsInsideTEllipse(TEllipse* el, double px, double py);

/////////////////////////////////////////////////////////////////////////////////////////
void WriteHistos(std::string path, std::string fileName, std::vector<TH1D*>& histograms);

/////////////////////////////////////////////////////////////////////////////////////////
// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
std::string CurrentDateTime();

/////////////////////////////////////////////////////////////////////////////////////////
// Get current date format is YYYY-MM-DD
std::string CurrentDate();

// =======================================================================================
#endif	// GLOBALS_H
