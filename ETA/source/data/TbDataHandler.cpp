//
// Created by brachwal on 24.09.18.
//

#include "TbDataHandler.h"
#include "TomlInputParser.h"
#include <iostream>
#include <sstream>

#include "TbGaudi.h"
#include "Globals.h"
#include "TbJobCollector.h"
#include "RootData.h"
#include "EcsData.h"
#include "TbJob.h"
#include "DutBinning.h"
#include "FluenceProfile.h"


////////////////////////////////////////////////////////////////////////////////////
///
std::vector<JobData> TbDataHandler::GetJobDataCollection(const std::string& dbfile, const std::string& dataCollectionName){
    switch (TbDataHandler::m_parser_type){
        case ParserType::CUSTOM:
            ParseCustomDBFile(dbfile);
            //m_parser = std::make_unique<CustomInputParser>(dbfile);
            break;
        case ParserType::TOML:
            m_parser = std::make_unique<TomlInputParser>(dbfile); // Note: this reads whole toml file
            break;
    }

    m_parser->PrintAnalysisCollectionDetails(dataCollectionName);
    return m_parser->GetAnalysisCollection(dataCollectionName);

    /*
    OBSOLETE CODE TO BE REFACTORED FOR NEW PIPELINE OF TBJOBS INSANTIATION!
    PrintDBInfo();
    PrintTbDataInfo();
    
    switch (TbData::Type()){
        case DataType::ROOT:
            m_file_extension = ".root";
            GetTbNTupleData(dbfile);
            break;
        case DataType::ECS:
            m_file_extension = ".txt";
            GetTbEcsData(dbfile);
            break;
    }
    */
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbDataHandler::GetTbNTupleData(const std::string& dbfile){
    std::cout<< "[INFO]:: TbDataHandler::GetTbData: Reading-in NTuple data " << std::endl;

    auto jobCollector = TbJobCollector::GetInstance();
    // Loop over all specified tbjobs and fill the jobCollector
    unsigned nFiles = m_values.at(m_variables.at(0)).size();
    for (unsigned i = 0; i < nFiles; ++i) {
        auto name = GetFileName(i);
        auto path = GetFilePath(i);
        std::cout<< "[INFO]:: Handling file # ["<<i+1<<"] "<< name << std::endl;
        if(exists(path+name))
            std::cout<< "[INFO]:: Found in path "<< path << std::endl;
        else
            TbGaudi::RunTimeError("Couldn't find "+name+" in\n"+path);

        // Create and fill all details about a i-th TbJob
        auto tbJob = std::make_shared<TbJob>();
        bool dataIsLoaded{false};
        if(TbData::Type()==DataType::RDF)
            dataIsLoaded = tbJob->LoadTbNTuple(path+name,m_ttree_branch_mapping);
        else if(TbData::Type()==DataType::ROOT)
            dataIsLoaded = tbJob->LinkTbTTree(path + name, m_ttree_list);

        if(dataIsLoaded) {
            // Add two obligatory keys explicitly: RunNumber and DutName
            tbJob->SetParam("DutName", m_values.at("DutName").at(i));
            tbJob->SetParam("RunNumber", m_values.at("RunNumber").at(i));

            // The optional keys:
            // NOTE: They can be defined as a column in the TbDB file, hence for different runs
            //       the different schemes can be defined. If they are not found in the TbDB file
            //       the global configuration is used
            // *** DutBinningType ***
            if(Includes(m_variables,std::string("DutBinningType")))
                tbJob->SetParam("DutBinningType", m_values.at("DutBinningType").at(i));
            else tbJob->SetParam("DutBinningType", DutBinning::DefaultType());

            // *** DutNBins ***
            if(Includes(m_variables,std::string("DutNBins")))
                tbJob->SetParam("DutNBins", m_values.at("DutNBins").at(i));
            else tbJob->SetParam("DutNBins", itos(DutBinning::DefaultNBins()));

            // *** RunClusterSize ***
            if(Includes(m_variables,std::string("RunClusterSize")))
                tbJob->SetParam("RunClusterSize", m_values.at("RunClusterSize").at(i));
            else tbJob->SetParam("RunClusterSize", itos(Run::DefaultClusterSize()));

            // *** FluenceProfile::Name ***
            if(Includes(m_variables,std::string("DutFluenceProfile")))
                tbJob->SetParam("DutFluenceProfileType", m_values.at("DutFluenceProfile").at(i));
            else tbJob->SetParam("DutFluenceProfileType", FluenceProfile::DefaultType());

            // Add the rest of keys specified by the User
            for(auto& ivar : m_values){
                if(ivar.first.compare("RunNumber")!=0 &&
                   ivar.first.compare("DutName")!=0 &&
                   ivar.first.compare("DutBinningType")!=0 &&
                   ivar.first.compare("RunClusterSize")!=0 &&
                   ivar.first.compare("DutFluenceProfile")!=0 &&
                   ivar.first.compare("DutNBins")!=0 ){
                        tbJob->SetParam(ivar.first, ivar.second.at(i));
                }
            }
            jobCollector->AddTbJob(tbJob);
        }
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbDataHandler::GetTbEcsData(const std::string& dbfile) {
    std::cout << "[INFO]::TbDataHandler::GetTbData: Reading-in ECS data " << std::endl;

    if (!ParseEcsDbFile(dbfile)) return false;

    PrintDBInfo();

    PrintTbDataInfo();

    auto jobCollector = TbJobCollector::GetInstance();
    // Loop over all specified tbjobs and fill the jobCollector
    unsigned nFiles = m_values.at(m_variables.at(0)).size(); // TODO:: Refactor: nFiles -> nRuns
    unsigned nEcsFrames = 100; // TODO: temporary!
    for (unsigned i = 0; i < nFiles; ++i) {
        // Create and fill all details about a i-th TbJob
        auto tbJob = std::make_shared<TbJob>(); // TODO:: auto tbJob = jobCollector->DefineNewTbJob(); // mv deafult definitions there!
                                  // TODO:: AddUserInfo(tbJob,i) // this can call AddObligatoryUserInfo(...) and AddOptionalUserInfo()
                                  // TODO:: AddObligatoryUserInfo(tbJob,i) // this can get list of names from tbJob static list
                                  // TODO:: AddOptionalUserInfo(tbJob,i) // this can get list of names from tbJob static list
        for (unsigned j = 1; j < nEcsFrames+1; ++j) {
            auto name = nEcsFrames > 1 ? GetFileName(i,j) : GetFileName(i);
            auto path = GetFilePath(i);
            //std::cout << "[INFO]:: Handling file # ["<< i+1<<"]["<<j<<"] " << name << std::endl;
            if (!exists(path + name))
                TbGaudi::RunTimeError("Couldn't find " + path + name);
            tbJob->AddTbEcsFrame(path + name);
        }

        // Add two obligatory keys explicitly: RunNumber and DutName
        tbJob->SetParam("DutName", m_values.at("DutName").at(i));
        tbJob->SetParam("RunNumber", m_values.at("RunNumber").at(i));

        // The optional keys:
        // NOTE: They can be defined as a column in the TbDB file, hence for different runs
        //       the different schemes can be defined. If they are not found in the TbDB file
        //       the global configuration is used
        // *** DutBinningType ***
        if (Includes(m_variables, std::string("DutBinningType")))
            tbJob->SetParam("DutBinningType", m_values.at("DutBinningType").at(i));
        else tbJob->SetParam("DutBinningType", DutBinning::DefaultType());

        // *** DutNBins ***
        if (Includes(m_variables, std::string("DutNBins")))
            tbJob->SetParam("DutNBins", m_values.at("DutNBins").at(i));
        else tbJob->SetParam("DutNBins", itos(DutBinning::DefaultNBins()));

        // *** RunClusterSize ***
        if (Includes(m_variables, std::string("RunClusterSize")))
            tbJob->SetParam("RunClusterSize", m_values.at("RunClusterSize").at(i));
        else tbJob->SetParam("RunClusterSize", itos(Run::DefaultClusterSize()));

        // *** FluenceProfile::Name ***
        if (Includes(m_variables, std::string("DutFluenceProfile")))
            tbJob->SetParam("DutFluenceProfileType", m_values.at("DutFluenceProfile").at(i));
        else tbJob->SetParam("DutFluenceProfileType", FluenceProfile::DefaultType());

        // Add the rest of keys specified by the User
        for (auto &ivar : m_values) {
            if (ivar.first.compare("RunNumber") != 0 &&
                ivar.first.compare("DutName") != 0 &&
                ivar.first.compare("DutBinningType") != 0 &&
                ivar.first.compare("RunClusterSize") != 0 &&
                ivar.first.compare("DutFluenceProfile") != 0 &&
                ivar.first.compare("DutNBins") != 0) {
                    tbJob->SetParam(ivar.first, ivar.second.at(i));
            }
        }
        //tbJob->EcsDataPtr()->ListEcsFrameFiles();
        tbJob->EcsDataPtr()->Process();
        jobCollector->AddTbJob(tbJob);
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbDataHandler::ParseCustomDBFile(const std::string& dbfile){

    bool dbHeader(false);       // in header block
    bool gotDBHeader(false);    // header initialized

    bool gotNTuple(false);              // These three blocks should followed
    bool gotTTree(false);               // one after another!
    bool gotCompleteDataSet(false);     // gotNTuple && gotTTree && TBranches

    bool tbData(false);                 // in data block
    bool tbDataHeader(false);
    bool gotTbDataHeader(false);

    std::string rawline,line;
    std::ifstream file(dbfile.c_str());
    if (file.is_open()) {  // check if file exists
        std::cout<< "[INFO]::TbDataHandler::User's DB file "<< dbfile << std::endl;
        while (getline(file, rawline)) {
            if(rawline.length()==0) continue;   // process only not empty lines
            rawline.erase( std::remove_if( rawline.begin(), rawline.end(),  // erase white characters
                                              [](char c) { return std::isspace(c) ; } ), rawline.end() ) ;
            if(rawline.length()==0)  continue;   // check again after erasing white characters
            if(rawline.at(0) == '#') continue;   // get rid of commented out lines

            // comments at the end of the line (?)
            std::size_t lcomm = rawline.find('#');
            unsigned lend = lcomm != std::string::npos ? lcomm : rawline.length();
            line = rawline.substr(0, lend);

            //std::cout<< "[DEBUG]::TbDataHandler::Current line "<< line << std::endl;

            // Find the current location in the DB file being processed
            // ---------------------------------------------------------------------------------------
            if(!gotDBHeader) {
                if (!dbHeader && line.compare("<head>") == 0) dbHeader = true;  // Begin of the header block
                if (dbHeader && line.compare("</head>") == 0){                  // End of the header block
                    if(gotCompleteDataSet) gotDBHeader=true;
                    else TbGaudi::RunTimeError("The header block not fully initialised.");
                }
            }

            if(gotDBHeader && !tbData && line.find("<data>") !=std::string::npos) tbDataHeader = true; // Begin of the data block

            if(gotDBHeader && gotTbDataHeader) tbData = true;                       // Start of the actual data info

            // DB header block processing
            // ---------------------------------------------------------------------------------------
            if(dbHeader && !gotDBHeader) {                          // Process the header block
                if (line.find("<NTuple>") !=std::string::npos) {    // NTuple file naming convention
                    m_ntuple_name = line.substr(8, line.length());
                    gotNTuple = true;
                    continue;
                }
                if (line.find("<TTree>") !=std::string::npos) {      // TTree name stored within the NTuple
                    gotCompleteDataSet = false;                      // Start of declaring new TTree
                    if (gotNTuple) {
                        m_ttree_list.push_back( line.substr(7, line.length()) );
                        gotTTree = true;
                        if (TbData::Type()!=DataType::RDF)
                            gotCompleteDataSet = true;
                        continue;
                    } else TbGaudi::RunTimeError("No <NTuple> before <TTree> declaration.");
                }
                if (line.find("<TBranchList>") !=std::string::npos) { // List of TBranches to read from the TTree
                    if (gotNTuple && gotTTree) {
                        m_tbranch_list.push_back( line.substr(13, line.length()) );
                        gotCompleteDataSet = true;
                        gotTTree = false; // prepare for searching of the next TTree declaration
                        continue;
                    } else TbGaudi::RunTimeError("No <NTuple> or <TTree> before <TBranchList> declaration.");
                }

            } // Process the header block

            if(gotDBHeader && m_ttree_branch_mapping.empty() && TbData::Type()==DataType::RDF) {     // perform TTree, TBranches mapping
                std::cout<< "[DEBUG]::TbDataHandler:: Perform TTree, TBranches mapping..." << std::endl;
                for (unsigned i = 0; i < m_ttree_list.size(); ++i) {
                    std::string myTBranch;
                    std::vector<std::string> myTBranches;
                    std::istringstream ssLine(m_tbranch_list[i]);
                    while(std::getline(ssLine, myTBranch, ',')) myTBranches.push_back(myTBranch);
                    m_ttree_branch_mapping[m_ttree_list[i]] = myTBranches;
                }
            }


            // DB data header line processing
            // ---------------------------------------------------------------------------------------
            if(tbDataHeader && !gotTbDataHeader) {
                std::cout<< "[INFO]:: Found DB data header line:: " << std::endl;
                line = line.substr(6, line.length()); // erase <data> string from the line
                std::cout<< "[INFO]:: " << line << std::endl;
                std::string myVar;
                std::istringstream ssLine(line);
                while(std::getline(ssLine, myVar, ',')) m_variables.push_back(myVar);

                // Look for the obligatory keys: RunNumber and DutName
                if(find (m_variables.begin(), m_variables.end(), "RunNumber") == m_variables.end())
                    TbGaudi::RunTimeError("Obligatory key 'RunNumber' have not been specified.");
                if(find (m_variables.begin(), m_variables.end(), "DutName") == m_variables.end())
                    TbGaudi::RunTimeError("Obligatory key 'DutName' have not been specified.");

                // extract units, if given in the header
                for(auto& ivar:m_variables){
                    if(ivar.find("[") !=std::string::npos){
                        std::size_t c1 = ivar.find('[');
                        m_units.push_back( ivar.substr(c1, ivar.length()) );
                        ivar.erase(ivar.begin()+c1,ivar.end());

                    } else m_units.push_back(std::string()); // put empty string anyway
                }
                gotTbDataHeader = true;
                continue;
            }

            // DB data definition lines processing
            // ---------------------------------------------------------------------------------------
            if(tbData) {
                // Initialize the data mapping
                if(m_values.empty()) {
                    for (int i = 0; i < m_variables.size(); ++i) {
                        std::vector<std::string> myValues;
                        m_values[m_variables.at(i)] = myValues;
                    }
                }
                // at first path to the data should be given:
                if(line.find("<Path>") !=std::string::npos) {
                    m_data_location.push_back( line.substr(6, line.length()) );
                    continue;
                }

                // parse the actual data declaration
                if(line.find("<Path>") == std::string::npos && line.find("</data>") == std::string::npos){
                    if(m_data_location.empty()) // shouldn't be empty anymore!
                        TbGaudi::RunTimeError(" The NTuples location not specified");
                    std::string myVar;
                    std::istringstream ssLine(line);
                    std::vector<std::string> line_variables;
                    while(std::getline(ssLine, myVar, ',')) line_variables.push_back(myVar);
                    if(m_variables.size()!=line_variables.size())
                        TbGaudi::RunTimeError(" Number of column names differ from the read-in line values.");
                    // map read-in values with the column key
                    for (int i = 0; i < m_variables.size(); ++i) {
                        m_values.at(m_variables[i]).push_back(line_variables.at(i));
                        if(m_variables[i].compare("RunNumber")==0)
                            m_data_location_mapping[line_variables.at(i)] = m_data_location.size()-1;
                    }
                }
            }
        line.clear();
        }
        ParseDataFileName();
        m_initialised = true;
        return true;
    }
    else TbGaudi::RunTimeError("Specified file doesn't exists: "+dbfile);
    return false; //make compiler happy
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbDataHandler::ParseEcsDbFile(const std::string& dbfile) {

    bool dbHeader(false);       // in header block
    bool gotDBHeader(false);    // header initialized

    bool gotNTuple(false);              // These three blocks should followed
    bool gotTTree(false);               // one after another!
    bool gotCompleteDataSet(false);     // gotNTuple && gotTTree && TBranches

    bool tbData(false);                 // in data block
    bool tbDataHeader(false);
    bool gotTbDataHeader(false);

    std::string rawline,line;
    std::ifstream file(dbfile.c_str());
    if (file.is_open()) {  // check if file exists
        std::cout<< "[INFO]::TbDataHandler::User's DB file "<< dbfile << std::endl;
        while (getline(file, rawline)) {
            if(rawline.length()==0) continue;   // process only not empty lines
            rawline.erase( std::remove_if( rawline.begin(), rawline.end(),  // erase white characters
                                           [](char c) { return std::isspace(c) ; } ), rawline.end() ) ;
            if(rawline.length()==0)  continue;   // check again after erasing white characters
            if(rawline.at(0) == '#') continue;   // get rid of commented out lines

            // comments at the end of the line (?)
            std::size_t lcomm = rawline.find('#');
            unsigned lend = lcomm != std::string::npos ? lcomm : rawline.length();
            line = rawline.substr(0, lend);

            // Find the current location in the DB file being processed
            // ---------------------------------------------------------------------------------------
            if(!gotDBHeader) {
                if (!dbHeader && line.compare("<head>") == 0) dbHeader = true;    // Begin of the header block
                if (dbHeader && line.compare("</head>") == 0) gotDBHeader = true; // End of the header block
            }

            if(gotDBHeader && !tbData && line.find("<data>") !=std::string::npos) tbDataHeader = true; // Begin of the data block

            if(gotDBHeader && gotTbDataHeader) tbData = true;                     // Start of the actual data info

            // DB header block processing
            // ---------------------------------------------------------------------------------------
            if(dbHeader && !gotDBHeader) {                          // Process the header block
                if (line.find("<ECSFile>") !=std::string::npos) {   // ECS file naming convention
                    m_ntuple_name = line.substr(9, line.length());
                    gotNTuple = true;
                    continue;
                }
                //
                // ... any other header info can be added here
                //
            } // Process the header block

            // DB data header line processing
            // ---------------------------------------------------------------------------------------
            if(tbDataHeader && !gotTbDataHeader) {
                std::cout<< "[INFO]:: Found DB data header line:: " << std::endl;
                line = line.substr(6, line.length()); // erase <data> string from the line
                std::cout<< "[INFO]:: " << line << std::endl;
                std::string myVar;
                std::istringstream ssLine(line);
                while(std::getline(ssLine, myVar, ',')) m_variables.push_back(myVar);

                // Look for the obligatory keys: RunNumber and DutName
                if(find (m_variables.begin(), m_variables.end(), "RunNumber") == m_variables.end())
                    TbGaudi::RunTimeError("Obligatory key 'RunNumber' have not been specified.");
                if(find (m_variables.begin(), m_variables.end(), "DutName") == m_variables.end())
                    TbGaudi::RunTimeError("Obligatory key 'DutName' have not been specified.");

                // extract units, if given in the header
                for(auto& ivar:m_variables){
                    if(ivar.find("[") !=std::string::npos){
                        std::size_t c1 = ivar.find('[');
                        m_units.push_back( ivar.substr(c1, ivar.length()) );
                        ivar.erase(ivar.begin()+c1,ivar.end());

                    } else m_units.push_back(std::string()); // put empty string anyway
                }
                gotTbDataHeader = true;
                continue;
            }

            // DB data definition lines processing
            // ---------------------------------------------------------------------------------------
            if(tbData) {
                // Initialize the data mapping
                if(m_values.empty()) {
                    for (int i = 0; i < m_variables.size(); ++i) {
                        std::vector<std::string> myValues;
                        m_values[m_variables.at(i)] = myValues;
                    }
                }
                // at first path to the data should be given:
                if(line.find("<Path>") !=std::string::npos) {
                    m_data_location.push_back( line.substr(6, line.length()) );
                    continue;
                }

                // parse the actual data declaration
                if(line.find("<Path>") == std::string::npos && line.find("</data>") == std::string::npos){
                    if(m_data_location.empty()) // shouldn't be empty anymore!
                        TbGaudi::RunTimeError(" The ECS files location not specified");
                    std::string myVar;
                    std::istringstream ssLine(line);
                    std::vector<std::string> line_variables;
                    while(std::getline(ssLine, myVar, ',')) line_variables.push_back(myVar);
                    if(m_variables.size()!=line_variables.size())
                        TbGaudi::RunTimeError(" Number of column names differ from the read-in line values.");
                    // map read-in values with the column key
                    for (int i = 0; i < m_variables.size(); ++i) {
                        m_values.at(m_variables[i]).push_back(line_variables.at(i));
                        if(m_variables[i].compare("RunNumber")==0)
                            m_data_location_mapping[line_variables.at(i)] = m_data_location.size()-1;
                    }
                }
            }
            line.clear();
        }
        ParseDataFileName();
        m_initialised = true;
        return true;
    }
    else TbGaudi::RunTimeError("Specified file doesn't exists: "+dbfile);
    return false; //make compiler happy
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbDataHandler::ParseDataFileName() {

    size_t searchStart{0};
    size_t keyStart{0};
    size_t keyEnd{0};

    // At first, extract key values within the ${...}
    std::vector<std::string> keyNames;
    unsigned nKeys = std::count (m_ntuple_name.begin(), m_ntuple_name.end(), '$');
    for (int i = 0; i < nKeys; ++i) {
        keyStart = m_ntuple_name.find_first_of('{',searchStart);
        keyEnd = m_ntuple_name.find_first_of('}',searchStart+1);
        if(keyStart!=std::string::npos && keyEnd!=std::string::npos) {
            keyNames.push_back(m_ntuple_name.substr(keyStart+1, keyEnd-keyStart-1));
            //std::cout << "[DEBUG]:: ParseDataFileName: found key :" << keyNames.back() << std::endl;
        }
        searchStart = keyEnd;
    }
    // for(auto& iv:m_variables) std::cout << "[DEBUG]:: ParseDataFileName: ivariable :" << iv << std::endl;

    // Find indexes constituting the keys order within the variables container
    unsigned idx{0};
    for (const auto& ikey:keyNames){
        idx=0;
        for(const auto& ivar:m_variables){
            if(ikey.compare(ivar)==0)
                m_ntuple_name_idx.push_back(idx);
            idx++;
        }
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbDataHandler::PrintDBInfo() const {
    if(m_initialised) {
        std::cout << "[INFO]::TbDataHandler:: *** User's Test Beam Data Base ***" << std::endl;
        std::cout << ">> File naming: " << m_ntuple_name << m_file_extension << std::endl;
        int i(0);
        for (const auto& itree : m_ttree_branch_mapping) {
            std::cout << ">> ["<< i++ << "] "<<"TTree: [ " << itree.first << " ] => TBranches:[";
            for (const auto& ibranch : itree.second) std::cout << " " << ibranch;
            std::cout<<" ]"<<std::endl;
        }
    } else
        std::cout << "[WARNING]::TbDataHandler::Print:: User's Test Beam Data Base not initialised."<< std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbDataHandler::PrintTbDataInfo() const {
    if(m_initialised) {
        std::cout << "[INFO]::TbDataHandler:: *** User's Test Beam Data Sets ***" << std::endl;
        for(const auto& ivar:m_variables) {
            printf("%16s", ivar.c_str());
        }
        std::cout<<std::endl;
        unsigned nValues = m_values.at(m_variables.at(0)).size();
        for (int i = 0; i < nValues; ++i) {
            for (int j = 0; j < m_variables.size(); ++j) {
                printf("%16s", m_values.at(m_variables.at(j)).at(i).c_str());
            } std::cout<<std::endl;
        } std::cout<<std::endl;
    }else
        std::cout << "[WARNING]::TbDataHandler::Print:: User's Test Beam Data Base not initialised."<< std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
const std::string TbDataHandler::GetFileName(unsigned ifile) const {
    std::string name=m_ntuple_name;
    for (int i = 0; i < m_ntuple_name_idx.size(); ++i) {
        name.erase(name.find_first_of('$'),1);
        name.erase(name.find_first_of('{'),1);
        name.erase(name.find_first_of('}'),1);
    }
    for (auto& idx : m_ntuple_name_idx) {
        size_t rStart = name.find(m_variables.at(idx));
        size_t rLen = m_variables.at(idx).size();
        const std::string rVal = (m_values.at(m_variables.at(idx))).at(ifile);
        name.replace(rStart,rLen,rVal);
    }

    const std::string cname = name + m_file_extension;
    return cname;
}

////////////////////////////////////////////////////////////////////////////////////
///
const std::string TbDataHandler::GetFileName(unsigned ifile, unsigned iframe) const {

    std::string name = GetFileName(ifile);
    auto pos = name.find('*');
    name.replace(pos,1,std::to_string(iframe));
    const std::string cname = name;
    return cname;
}

////////////////////////////////////////////////////////////////////////////////////
///
const std::string TbDataHandler::GetFilePath(unsigned ifile) const {
    // Get the run number for the ifile
    auto runNumber = (m_values.at("RunNumber")).at(ifile);
    // Get the actual idx in the m_data_location
    unsigned idx = m_data_location_mapping.at(runNumber);
    const std::string cpath = m_data_location.at(idx);//+"/";
    return cpath;
}