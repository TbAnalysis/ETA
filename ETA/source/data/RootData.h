//
// Created by brachwal on 24.09.18.
//

#ifndef ROOTDATA_H
#define ROOTDATA_H

// project libraries
#include "Globals.h"
#include "TbGaudi.h"
#include "TbData.h"

class TFile;
class TTree;
class TString;

////////////////////////////////////////////////////////////////////////////////////
///
class RootData : public virtual TbData {

    protected:
        ///
        std::string m_NTuple;

        ///
        TFile* m_TFile;

        /// \brief Map of the default TBranches and the TTree they belongs to.
        std::map<std::string,std::vector<std::string>> m_TTree_TBranches;

        ///
        std::map<std::string,TTree*> m_TTrees;

        ///
        std::map<std::string,long int> m_number_of_events;

        ///
        static int m_usr_number_of_events;

        ///
        bool LinkTTrees();

        /// \brief Extract the TTree name in case the TDirectory is given as well.
        /// \param ttree The TTree name (with or w/o the TDirectory specified)
        std::string TTreeName(std::string ttree);

    public:
        RootData() = default;
        RootData(const std::string ntuple);
        RootData(const std::string ntuple,std::map<std::string,std::vector<std::string>>tbranches);
        ~RootData();

        ///
        bool LinkTTree(const std::string& ttree);

        ///
        bool LinkTTrees(std::vector<std::string> ttrees);

        ///
        TTree* GetTTree(std::string name);

        ///
        long int GetNEntries(std::string name);

        ///
        static int UsrNEntries(){ return m_usr_number_of_events;}

        ///
        static void UsrNEntries(int val){ m_usr_number_of_events=val;}

        ///
        void SetDataFile(const std::string& file) override;

        ///
        std::string GetDataFile() const override { return m_NTuple; }
};

#endif //ROOTDATA_H
