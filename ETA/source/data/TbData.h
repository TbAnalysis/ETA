//
// Created by brachwal on 06.02.19.
//

#ifndef TBGAUDI_TBDATA_H
#define TBGAUDI_TBDATA_H

#include <memory>
#include <string>

// ROOT - Standard Root TTree
// RDF  - RootDataFrame
// ECS  - Experimental raw data of frames
enum class DataType {ROOT, RDF, ECS};

////////////////////////////////////////////////////////////////////////////////////
///
class TbData {

    private:
        ///
        static DataType m_data_type;

        ///
        static std::string m_db_file;

    public:
        TbData()=default;
        virtual ~TbData()=default;

        ///
        static void Type(DataType type) { m_data_type = type;}

        ///
        static DataType Type() { return m_data_type;}

        ///
        static const std::string TbDBFile();

        ///
        static void TbDBFile(const std::string& db_file) { m_db_file=db_file;}

        ///
        virtual void SetDataFile(const std::string&) = 0;

        ///
        virtual std::string GetDataFile() const = 0;

};

using TbDataUniquePtr = std::unique_ptr<TbData>;

#endif //TBGAUDI_TBDATA_H
