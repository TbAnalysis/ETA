//
// Created by brachwal on 13.12.18.
//

#include "Dut.h"
#include "EcsData.h"
#include "memory.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TbAnalysis.h"
#include "Globals.h"

////////////////////////////////////////////////////////////////////////////////////
///
void EcsData::AddFrame(std::string ecs_frame_file){
    m_ecs_frames_files.push_back(ecs_frame_file);
}

////////////////////////////////////////////////////////////////////////////////////
///
void EcsData::ListEcsFrameFiles() const {
    for(auto& ifile: m_ecs_frames_files)
        std::cout << "[INFO]:: EcsData file # " << ifile << std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
bool EcsData::Process(){
    std::cout << "[INFO]:: EcsData:: Processing the set of "<< m_ecs_frames_files.size()<< " ECS frames" << std::endl;
    PerformClusterization();
    return true;
}

////////////////////////////////////////////////////////////////////////////////////
///
void EcsData::PerformClusterization(){

    //unsigned frameCounter = 0;
    for(auto& iFrameFile : m_ecs_frames_files ){
        //std::cout << "[INFO]:: EcsData:: Performing clusterization of the ECS frame..."<< std::endl;
        auto frame = ReadFrameFromFile(iFrameFile);
        //DumpFrame(frame);


        // the simplest way to avoid the clusterization algorithm run beyond the frame
        for(unsigned i = 0; i < Dut::NPixelsX(); ++i ) {
            frame.at(i).at(0) = 0;
            frame.at(i).at(Dut::NPixelsY() - 1) = 0;
        }
        for(unsigned i = 0; i < Dut::NPixelsY(); ++i ) {
            frame.at(0).at(i) = 0;
            frame.at(Dut::NPixelsX()-1).at(i) = 0;
        }
        for(unsigned i = 0; i < Dut::NPixelsX(); ++i ) {
            for (unsigned j = 0; j < Dut::NPixelsY(); ++j) {
                if (i>0 && i<255 && j>0 && j<255){
                    if(frame.at(i).at(j)>0) {
                        m_clusters.push_back(std::make_unique<EcsCluster>());
                        Clusterize(frame, i, j);
                        if (m_clusters.back()->m_ToT == 0) m_clusters.pop_back();
                    }
                }
            }
        }

    } // iFrameFile loop;
    DumpClusters();
    CreateClustersTTree();
}

////////////////////////////////////////////////////////////////////////////////////
///
void EcsData::Clusterize(EcsFrame& frame, unsigned col, unsigned row){
    if(frame[col][row]>0){
        m_clusters.back()->m_ToT += frame[col][row];
        m_clusters.back()->m_pixels.push_back(std::make_pair(col,row));
        frame[col][row] = 0;
        for (unsigned i = 0; i < 3; ++i) {
            for (unsigned j = 0; j < 3; ++j) {
                if(frame[col+i-1][row+j-1]>0){
                    Clusterize(frame,col+i-1,row+j-1);
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
EcsFrame EcsData::ReadFrameFromFile(const std::string& ecsFrameFileWithFullPath){
    //std::cout << "[INFO]:: EcsData:: Reading frame data from "<< ecsFrameFileWithFullPath << std::endl;
    EcsFrame frame;
    std::ifstream file(ecsFrameFileWithFullPath);

    for(unsigned i = 0; i < Dut::NPixelsY(); ++i ) {
        std::string line;
        std::getline( file, line );
        std::istringstream is( line );
        std::vector<unsigned> row;
        for (unsigned j = 0; j < Dut::NPixelsX(); ++j) {
            std::string sToT;
            std::getline( is, sToT, ',' );
            row.push_back(std::stoi(sToT));
        }
        frame.push_back(row);
        row.clear();
    }
    return frame;
}

////////////////////////////////////////////////////////////////////////////////////
///
void EcsData::DumpFrame(const EcsFrame& frame) const {
    std::cout<<std::endl;
    std::cout<<"[DEBUG]:: EcsData::DumpFrame..."<<std::endl;
    if(!frame.empty()){
        for(const auto& row : frame){
            for(const auto& iToT : row)
                std::cout<< iToT << " ";
            std::cout<<std::endl;
        }
    }
    std::cout<<std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void EcsData::DumpClusters() const {
    std::cout<<"[DEBUG]:: EcsData::DumpClusters..."<<std::endl;
    std::cout<<"[DEBUG]:: Number of processed ECS frames: " << m_ecs_frames_files.size()<<std::endl;
    std::cout<<"[DEBUG]:: Number of clusters found: " << m_clusters.size()<<std::endl;

    /*for(const auto& cluster : m_clusters){
        std::cout<<"[DEBUG]:: clSize " << cluster->m_pixels.size()<<std::endl;
        std::cout<<"[DEBUG]:: ToT " <<  cluster->m_ToT <<std::endl;
    }*/
}
////////////////////////////////////////////////////////////////////////////////////
///
void EcsData::CreateClustersTTree(){

    std::string path = TbIO::OutputLocation();
    if(path.at(path.length()-1)=='/') path+=CurrentDate();
    else                        path+="/"+CurrentDate();
    path+="/temp";
    // namespace fs = boost::filesystem;
    // fs::path dp (path);
    // if(!fs::exists (dp)){
    //     std::cout << "[INFO]:: EcsData:: Created output directory "<<std::endl;
    //     std::cout << "[INFO]:: EcsData:: "<< path <<std::endl;
    //     fs::create_directories(dp);
    // }

    m_TFile = new TFile(TString(path+"/ecs_data.root"),"recreate");
    auto tree = new TTree("Clusters","Clusters from ECS data");
    m_TTrees["Clusters"] = tree; // add TTree to the RootData container.
    //tree->SetDirectory(0);

    UInt_t clSize;
    Double_t clToT;
    Int_t hRow[200]; // max clSize as in KEPLER
    Int_t hCol[200];
    tree->Branch("clSize",&clSize,"clSize/i");
    tree->Branch("clToT",&clToT,"clToT/D");
    tree->Branch("hRow",hRow,"hRow[clSize]/I");
    tree->Branch("hCol",hCol,"hCol[clSize]/I");

    for(const auto& cluster : m_clusters){
        clSize = cluster->m_pixels.size();
        if(clSize < 200) {
            clToT = cluster->m_ToT;
            for (unsigned i = 0; i < clSize; ++i) {
                hCol[i] = cluster->m_pixels.at(i).first;
                hRow[i] = cluster->m_pixels.at(i).second;
            }
            tree->Fill();
        }
    }
    //tree->Write();
    m_TFile->Write();
}
