//
// Created by brachwal on 23.09.19.
//

#ifndef RDFDATA_H
#define RDFDATA_H

#include <algorithm>
#include <iostream>
#include <string>
#include "RootData.h"
#include "ROOT/RDataFrame.hxx"

////////////////////////////////////////////////////////////////////////////////////////
/// NOTE: this is very pleminary implementarion of data handling with RDF,
///       not ready yet!
////////////////////////////////////////////////////////////////////////////////////////

using RDataFrameFiltered = ROOT::RDF::RInterface<ROOT::Detail::RDF::RJittedFilter, void>;

////////////////////////////////////////////////////////////////////////////////////
///
class RdfData : public RootData {
    private:
        ///
        std::map<std::string,ROOT::RDataFrame*> m_RDataFrames;

        ///
        std::map<std::string,RDataFrameFiltered*> m_RDataFramesFiltered;

        /// \brief The RDataFrame and filters to be applied on it automatically.
        static std::map<std::string, std::vector<std::string>> m_rdf_filters;

    public:
        ///
        RdfData() = default;

        ///
        RdfData(const std::string& ntuple);

        ///
        RdfData(const std::string& ntuple, std::map<std::string,std::vector<std::string>>tbranches);

        ///
        ~RdfData();

        ///
        ROOT::RDataFrame* RDataFramePtr(const std::string& name);

        ///
        void ListFrames() const;

        ///
        RDataFrameFiltered* ApplyFilters(const std::string& name, std::vector<std::string>* filters);

        ///
        RDataFrameFiltered* ApplyFilter(const std::string& name, const std::string& filter);

        		///
		static void ClearAllFilters(){m_rdf_filters.clear();}

		///
		static void ClearFilters(std::string name){m_rdf_filters.at(name).clear();}

		///
		static void AddFilter(std::string name, std::string filter);

		///
		static void AddFilters(std::string name, std::vector<std::string> filters);

        ///
        static std::vector<std::string>* Filters(std::string name);

		///
		static void PrintAllFilters();

		///
		static void PrintFilters(std::string name);

};
#endif //RDFDATA_H
