//
// Created by brachwal on 03.01.20.
//

#include "JobData.h"
#include "TbGaudi.h"
#include "TbData.h"
#include <type_traits>

////////////////////////////////////////////////////////////////////////////////////
///
std::map<std::string, std::type_index> JobData::obligatory_parameters;
std::map<std::string, std::string> JobData::default_units;

////////////////////////////////////////////////////////////////////////////////////
///
JobData::JobData() {
    for (const auto& default_unit : JobData::default_units) {
        std::string key = default_unit.first + "Unit";
        SetValue(key, default_unit.second);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
bool JobData::Exists(const std::string& parameter) const {
    return m_parameters.find(parameter) != m_parameters.end() ? true : false;
}

////////////////////////////////////////////////////////////////////////////////////
///
bool JobData::ExistsAsString(const std::string& parameter) const {
    return m_parameters_as_string.find(parameter) != m_parameters_as_string.end() ? true : false;
}

////////////////////////////////////////////////////////////////////////////////////
///
void JobData::SetValue(const std::string& key, std::any value){
    if (Exists(key)) {
        if (m_parameters[key].type() != value.type()) {
            std::cout << "Bad type of parameter with key: " << key << std::endl;
            // ERROR: The specified parameter is of wrong type value     
            // TODO: handle this error!  
        }
        else {
            m_parameters[key] = value;
        }
    }
    else {
        m_parameters.emplace(key, value);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void JobData::SetValueAsString(const std::string& key, const std::string& value){
    if (ExistsAsString(key))
        m_parameters_as_string[key] = value;
    else 
        m_parameters_as_string.emplace(key, value);
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string JobData::GetValueAsString(const std::string& parameter_name) const{
    if (ExistsAsString(parameter_name))
        return m_parameters_as_string.at(parameter_name);
    return std::string();
}

////////////////////////////////////////////////////////////////////////////////////
///
void JobData::AddDefaultUnit(std::string quantity, std::string unit){
    JobData::default_units.insert(std::make_pair(quantity, unit));
}

////////////////////////////////////////////////////////////////////////////////////
///
void JobData::PrintDetails() const {
    for (auto& [key, val] : m_parameters){
        if (val.type() == typeid(std::string))
            std::cout << key << ": " << std::any_cast<std::string>(val) << std::endl;
        else if (val.type() == typeid(unsigned))
            std::cout << key << ": " << std::any_cast<unsigned>(val) << std::endl;
        else if (val.type() == typeid(double))
            std::cout << key << ": " << std::any_cast<double>(val) << std::endl;
        else if (val.type() == typeid(bool))
            std::cout << key << ": " << std::any_cast<bool>(val) << std::endl;
        else
            std::cout << "Unrecognised type in input job property: " << key << std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string JobData::GetDataFile() {
    if(!m_data_file.empty())
        return m_data_file;
    else if (m_data_file_pattern.empty())
        TbGaudi::RunTimeError("GetDataFile::There is no data file either data file pattern specified!");
    return BuildDataFileName();
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string JobData::BuildDataFileName(){
    auto usrKeys = ParseDataFileNamePattern();
    auto fileName = m_data_file_pattern;
    for(const auto& key : usrKeys){
        fileName.erase(fileName.find_first_of('$'),1);
        fileName.erase(fileName.find_first_of('{'),1);
        fileName.erase(fileName.find_first_of('}'),1);

        auto rStart = fileName.find(key);
        auto rLen = key.size();
        const std::string rVal = GetValueAsString(key);
        fileName.replace(rStart,rLen,rVal);
    }

    switch (TbData::Type()){
        case DataType::ROOT:
            fileName += ".root";
            break;
        case DataType::ECS:
            fileName += ".txt";
            break;
    }

    m_data_file = fileName;
    //std::cout << "[DEBUG]:: BUILD NAME: " << fileName << std::endl;
    return m_data_file;
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<std::string> JobData::ParseDataFileNamePattern() {
    size_t searchStart{0};
    size_t keyStart{0};
    size_t keyEnd{0};

    // Extract key values within the ${...}
    std::vector<std::string> keyNames;
    unsigned nKeys = std::count (m_data_file_pattern.begin(), m_data_file_pattern.end(), '$');
    for (int i = 0; i < nKeys; ++i) {
        keyStart = m_data_file_pattern.find_first_of('{',searchStart);
        keyEnd = m_data_file_pattern.find_first_of('}',searchStart+1);
        if(keyStart!=std::string::npos && keyEnd!=std::string::npos) 
            keyNames.push_back(m_data_file_pattern.substr(keyStart+1, keyEnd-keyStart-1));
        searchStart = keyEnd;
    }
    return keyNames;
}

