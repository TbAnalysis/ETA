//
// Created by brachwal on 13.12.18.
//

#ifndef ECSDATA_H
#define ECSDATA_H

// project libraries
#include "Globals.h"
#include "TbGaudi.h"
#include "RootData.h"

class TTree;

typedef std::pair<unsigned,unsigned > pixel;
typedef std::vector<std::vector<unsigned>> EcsFrame;

class EcsCluster {
     public:
         std::vector<pixel> m_pixels;
         unsigned m_ToT = 0;
};

////////////////////////////////////////////////////////////////////////////////////
///
class EcsData : public RootData {
    private:

        /// \brief The ECS data files constituting given run (list of files, full path included)
        std::vector<std::string> m_ecs_frames_files;

        ///
        std::vector<std::unique_ptr<EcsCluster>> m_clusters;

        ///
        void PerformClusterization();

        /// \brief Perform recurent clusterization procedure
        void Clusterize(EcsFrame& frame, unsigned col, unsigned row);

        ///
        void CreateClustersTTree();

        ///
        EcsFrame ReadFrameFromFile(const std::string& ecsFrameFileWithFullPath);

    public:
        EcsData() = default;
        ~EcsData() = default;

        ///
        void AddFrame(std::string ecs_frame_file);

        ///
        void ListEcsFrameFiles() const;

        ///
        bool Process();

        ///
        void WriteToNTuple() const;

        ///
        void DumpFrame(const EcsFrame& frame) const;

        ///
        void DumpClusters() const;
};

#endif //ECSDATA_H
