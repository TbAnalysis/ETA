//
// Created by brachwal on 08.10.18.
//

#include "Globals.h"
#include "RootData.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include <memory>

int RootData::m_usr_number_of_events=-1;

////////////////////////////////////////////////////////////////////////////////////
///
RootData::RootData(const std::string ntuple)
        : m_NTuple(ntuple)
        , m_TFile(nullptr)
{
    m_TFile = new TFile(TString(m_NTuple.c_str()));
}

////////////////////////////////////////////////////////////////////////////////////
///
RootData::RootData(const std::string ntuple,std::map<std::string,std::vector<std::string>>tbranches)
        : m_NTuple(ntuple)
        , m_TFile(nullptr)
        , m_TTree_TBranches(tbranches)
{
    m_TFile = new TFile(TString(m_NTuple.c_str()));
}

////////////////////////////////////////////////////////////////////////////////////
///
RootData::~RootData(){
    for(auto& ittree : m_TTrees) delete ittree.second;
    delete m_TFile;
}

////////////////////////////////////////////////////////////////////////////////////
///
void RootData::SetDataFile(const std::string& file){
     m_NTuple = file;
     m_TFile = new TFile(TString(m_NTuple.c_str()));
}

////////////////////////////////////////////////////////////////////////////////////
///
bool RootData::LinkTTrees(){
    for(auto& itree : m_TTree_TBranches) {
        std::string ttree_Name = TTreeName(itree.first);
        if(TbGaudi::Verbose() || TbGaudi::Debug())
            std::cout << "[INFO]:: RootData:: Linking TTree: "<< ttree_Name <<std::endl;
        m_TTrees[ttree_Name] = static_cast<TTree*>(m_TFile->Get(TString(itree.first.c_str())));
        if(!m_TTrees.at(ttree_Name))
            TbGaudi::RunTimeError("Couldn't find TTree "+itree.first);
        //m_TTrees.at(ttree_Name)->SetDirectory(0);
        m_number_of_events[ttree_Name] = m_TTrees.at(ttree_Name)->GetEntriesFast();

    }
}

////////////////////////////////////////////////////////////////////////////////////
///
bool RootData::LinkTTree(const std::string& ttree){
    auto ttree_Name = TTreeName(ttree);
    m_TTrees[ttree_Name] = static_cast<TTree*>(m_TFile->Get(TString(ttree.c_str())));
    if(!m_TTrees[ttree_Name])
        TbGaudi::RunTimeError("Couldn't find TTree "+ttree);
    m_number_of_events[ttree_Name] = m_TTrees.at(ttree_Name)->GetEntriesFast();
    std::cout << "[INFO]:: RootData:: TTree is linked: "<< ttree_Name <<std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
bool RootData::LinkTTrees(std::vector<std::string> ttrees){
    for(auto& itree : ttrees) LinkTTree(itree);
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string RootData::TTreeName(std::string ttree){
    if(ttree.find('/')!=std::string::npos)
        return ttree.substr(ttree.find('/')+1);
    else
        return ttree;
}

////////////////////////////////////////////////////////////////////////////////////
///
TTree* RootData::GetTTree(std::string name){
    std::cout << "[INFO]:: RootData:: GetTTree: "<< name <<std::endl;
    if(Includes<std::string>(m_TTrees,name))
        return m_TTrees.at(name);
    else{
        std::cout << "[ERROR]::RootData:: You are trying to get access to not existing TTree: "
        << name << std::endl;
        std::cout << "[INFO]::RootData:: Available TTrees: "<<std::endl;
        if(m_TTrees.empty()) std::cout << "[INFO]::RootData:: NONE!"<<std::endl;
        unsigned count{};
        for(auto& itree : m_TTrees)
            std::cout << "[INFO]::RootData:: ["<<count++<<"] "<<itree.second->GetName()<<std::endl;
        TbGaudi::RunTimeError("Accessing not existing key in the std::map");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
long int RootData::GetNEntries(std::string name) {
    if(Includes<std::string>(m_number_of_events,name)) {
        auto usrNEvents = RootData::UsrNEntries();
        auto tTreeEvents = m_number_of_events.at(name);
        if(usrNEvents==-1) return tTreeEvents;
        if(usrNEvents>tTreeEvents) {
            std::cout << "[WARNING]::RootData::GetNEntries:: User specified number of events to be processed"
                      << " is higher that available in the TTree"<<std::endl;
            return tTreeEvents;
        }
        else
            return usrNEvents;
    }
    else {
        std::cout << "[ERROR]::RootData:: You are trying to get access to not existing TTree: "
                  << name << std::endl;
    }
}