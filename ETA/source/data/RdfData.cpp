//
// Created by brachwal on 23.09.19.
//

#include "RdfData.h"

////////////////////////////////////////////////////////////////////////////////////
std::map<std::string, std::vector<std::string>> RdfData::m_rdf_filters;

////////////////////////////////////////////////////////////////////////////////////
///
RdfData::RdfData(const std::string& ntuple){

}

////////////////////////////////////////////////////////////////////////////////////
///
RdfData::RdfData(const std::string& ntuple, std::map<std::string,std::vector<std::string>>tbranches)
    : RootData(ntuple,tbranches)
{
    //ROOT::EnableImplicitMT();
    for(auto& itree : m_TTree_TBranches){
        std::string rdf_Name = "RDF"+TTreeName(itree.first);
        if(TbGaudi::Verbose() || TbGaudi::Debug())
            std::cout << "[INFO]:: RdfData:: Creating RDataFrame: "<< rdf_Name <<std::endl;
        m_RDataFrames[rdf_Name] = new ROOT::RDataFrame{itree.first,m_NTuple,itree.second};
    }

    m_TFile = new TFile(TString(m_NTuple.c_str()));
    LinkTTrees();
}

////////////////////////////////////////////////////////////////////////////////////
///
RdfData::~RdfData(){
    for(auto& irdf : m_RDataFrames) delete irdf.second;
}

////////////////////////////////////////////////////////////////////////////////////
///
ROOT::RDataFrame* RdfData::RDataFramePtr(const std::string& name) {
if(m_RDataFrames.find(name) != m_RDataFrames.end()){
        return m_RDataFrames.at(name);
    }
    else {
        std::cout << "[ERROR]::RdfData:: You are trying to get access to not existing RDataFrame: "<< name << std::endl;
        ListFrames();
        TbGaudi::RunTimeError("Accessing not existing key in the std::map");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void RdfData::ListFrames() const {
std::cout << "[INFO]::RdfData:: Available RDataFrames:"<<std::endl;
    unsigned idxRdf{0};
    unsigned idxCol{0};
    for(auto& irdf : m_RDataFrames){
        std::cout << "[INFO]::RdfData:: ["<<idxRdf++<<"] " << irdf.first << " with the columns:" << std::endl;
        std::vector<std::string> colNames = irdf.second->GetColumnNames();
        idxCol=0;
        for(auto& ic:colNames)
            std::cout << "[INFO]::RdfData:: [col "<<idxCol++<<"] " << ic << std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
RDataFrameFiltered* RdfData::ApplyFilters(const std::string& name, std::vector<std::string>* filters){
    for (auto ifilter : *filters)
        ApplyFilter(name, ifilter);
    return m_RDataFramesFiltered.at("RDFF"+name);
}

////////////////////////////////////////////////////////////////////////////////////
///
RDataFrameFiltered* RdfData::ApplyFilter(const std::string& name, const std::string& filter){
    std::cout << "[INFO]:: RdfData:: Employing the filter: " << name << ": " << filter << std::endl;

    if(m_RDataFrames.find("RDF"+name) == m_RDataFrames.end()){
        std::cout << "[ERROR]:: RdfData:: ApplyFilter:: There is no correspondig RDataFrame you want filter on:  "<< "RDF"+name <<std::endl;
        TbGaudi::RunTimeError("Extracting no existing key from the std::map");
    }

    if(m_RDataFramesFiltered.find("RDFF"+name) == m_RDataFramesFiltered.end()){
        m_RDataFramesFiltered["RDFF"+name] = new RDataFrameFiltered{m_RDataFrames.at("RDF"+name)->Filter(filter)};
    }
    else { // New filter is added the the previous one.
        m_RDataFramesFiltered.at("RDFF"+name)->Filter(filter);
    }
    return m_RDataFramesFiltered.at("RDFF"+name);
}

////////////////////////////////////////////////////////////////////////////////////
///
void RdfData::AddFilter(std::string name, std::string filter){

    if(m_rdf_filters.find(name) == m_rdf_filters.end()){
        std::vector<std::string> vfilter = {filter};
        m_rdf_filters[name] = vfilter;
        return;
    }

    if(std::find(m_rdf_filters.at(name).begin(),m_rdf_filters.at(name).end(), filter) != m_rdf_filters.at(name).end())
        std::cout << "[WARNING]::RdfData::AddRdfFilter:: Filter already present: "<<name << ": "<<filter << std::endl;
    else
        m_rdf_filters.at(name).push_back(filter);
}

////////////////////////////////////////////////////////////////////////////////////
///
void RdfData::AddFilters(std::string name, std::vector<std::string> filters){
    if(m_rdf_filters.find(name) != m_rdf_filters.end()) {
        std::cout << "[WARNING]::RdfData::AddRdfFilters:: Filters already present: "<<name << " - being replaced." << std::endl;
        RdfData::ClearFilters(name);
        for (auto &ifilter : filters) m_rdf_filters.at(name).push_back(ifilter);
    }
    else {
        m_rdf_filters[name] = filters;

    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<std::string>* RdfData::Filters(std::string name){
    if(m_rdf_filters.find(name) != m_rdf_filters.end())
        return &(m_rdf_filters.at(name));
    else return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
///
void RdfData::PrintAllFilters(){
    for (auto& ifilter : m_rdf_filters){
        std::cout << "[INFO]:: RDF Filter: " << ifilter.first << " : " << std::endl;
        for(auto& ival : ifilter.second)
            std::cout << "[INFO]::             " << ival << std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void RdfData::PrintFilters(std::string name){
    if(m_rdf_filters.find(name) != m_rdf_filters.end()) {
        std::cout << "[INFO]:: RDF Filter: " << name << " : " << std::endl;
        for(auto& ival : m_rdf_filters.at(name))
            std::cout << "[INFO]::             " << ival << std::endl;
    } else {
        std::cout << "[ERROR]:: You are trying to get not exisitng RDF Filter: " <<  name << std::endl;
        TbGaudi::RunTimeError("Extracting not existing key from the std::map");
    }
}