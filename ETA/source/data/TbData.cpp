//
// Created by brachwal on 06.02.19.
//

#include "TbData.h"
#include "TbGaudi.h"

DataType TbData::m_data_type = DataType::ROOT;
std::string TbData::m_db_file = "None";
////////////////////////////////////////////////////////////////////////////////////
///
const std::string TbData::TbDBFile() {
    if (m_db_file.compare("None") == 0) {
        std::cout << "[ERROR]::TbData::TbDBFile:: The data input file is not specifed!  " << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow");
    } else
        return m_db_file;
}