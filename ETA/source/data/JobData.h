
#ifndef JobData_H
#define JobData_H

#include <iostream>
#include <typeindex>
#include <vector>
#include <map>
#include <any>

class JobData {
	///
	friend class TomlInputParser;

	///
	private:
		///
		std::string m_data_path;

		///
		std::string m_data_file;

		///
		std::string m_data_file_pattern;

		///
		std::string m_data_ttree;

		///
		std::map<std::string, std::any> m_parameters;
		std::map<std::string, std::string> m_parameters_as_string;

		///
		static std::map<std::string, std::type_index> obligatory_parameters;

		///
		static std::map<std::string, std::string> default_units;

		///
		std::string BuildDataFileName();

		///
		std::vector<std::string> ParseDataFileNamePattern();

	public:
		///
		JobData();

		///
		~JobData() = default;

		///
		template<typename Type>
		static void AddObligatoryParameter(std::string parameter_name);

		///
		bool Exists(const std::string& parameter) const;

		///
		bool ExistsAsString(const std::string& parameter) const;

		///
		void SetValue(const std::string& key, std::any value);

		///
		void SetValueAsString(const std::string& key, const std::string& value);

		///
		template <typename T> T GetValue(const std::string& parameter_name) const;

		///
		std::string GetValueAsString(const std::string& parameter_name) const;

		///
		const std::map<std::string, std::string>& GetParametersAsStringCollection() const {return m_parameters_as_string;}

		///
		inline void SetDataPath(const std::string& data_path) {m_data_path = data_path;}

		///
		inline void SetDataFile(const std::string& data_file) {m_data_file = data_file;}

		///
		inline void SetDataFilePattern(const std::string& data_file_pattern) {m_data_file_pattern = data_file_pattern;}

		///
		inline void SetDataTTree(const std::string& data_ttree) {m_data_ttree = data_ttree;}

		///
		inline std::string GetDataPath() const {return m_data_path;}

		///
		inline std::string GetDataTTree() const {return m_data_ttree;}

		///
		std::string GetDataFile();

		///
		std::string GetDataFile() const { return m_data_file; }

		///
		static void AddDefaultUnit(std::string quantity, std::string unit);

		///
		void PrintDetails() const;
};

////////////////////////////////////////////////////////////////////////////////////
///
template<typename Type>
void JobData::AddObligatoryParameter(std::string parameter_name){
	obligatory_parameters.insert(std::make_pair(parameter_name, std::type_index(typeid(Type))));
}

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T> T JobData::GetValue(const std::string& parameter_name) const {
	try	{
		if (Exists(parameter_name)) {
			return std::any_cast<T>(m_parameters.at(parameter_name));
		}
		else {
			std::cout << "Parameter:  " << parameter_name << " doesn't not exist." << std::endl;
			// ERROR: The specified parameter is not defined
			// TODO:: handle this error! 
			return T{}; // zwracamy '0' danego typu
		}
	}
	catch (const std::bad_any_cast& e)	{
		//std::cout << e.what() << std::endl;
		return T{}; // zwracamy '0' danego typu
	}
}

#endif // JobData_H
