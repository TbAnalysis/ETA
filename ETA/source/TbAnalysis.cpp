#include <memory>
#include "TbGaudi.h"
#include "TbAnalysis.h"
#include "TbJobCollector.h"


////////////////////////////////////////////////////////////////////////////////////
///
bool TbAnalysis::FillTbJobsCollection(const std::string& collectionName){
    // Perform the User's defined data reading
    TbJobCollector::GetInstance()->FillTbJobsCollection(collectionName);
    return true;
}

////////////////////////////////////////////////////////////////////////////////////
///
const std::vector<std::shared_ptr<TbJob>>& TbAnalysis::GetTbJobsCollection(){
    return TbJobCollector::GetInstance()->TbJobs();
}

