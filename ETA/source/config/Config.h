//
// Created by brachwal on 14.06.19.
//

#ifndef CONFIG_H
#define CONFIG_H

#include <memory>
#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>

using MConfig = std::map<std::string,std::string>;

class Config {
    private:
        ///
        MConfig m_mconfig;

    public:
        ///
        Config() = default;

        ///
        virtual ~Config() = default;

        ///
        void SetParam(const std::string& name, const std::string& value){
            // Check if insertion is successful or not, if not replace the previous value
            if (m_mconfig.insert(std::make_pair(name, value)).second == false) {
                std::cout << "[WARNING]:: Config::SetParam:: The " << name << " value replaced already existed one."
                          << "\"" << m_mconfig[name] << "\" ==> \"" << value << "\"" << std::endl;
                m_mconfig[name] = value;
            }
        }

        ///
        template <typename T>
        const T GetParam(const std::string& name) const {
            const std::string config = GetParam(name);
            if (std::is_same<T, int>::value) 			return std::stoi(config);
            else if (std::is_same<T, unsigned>::value) 	return static_cast<unsigned>(std::stoi(config));
            else if (std::is_same<T, float>::value) 	return std::stof(config);
            else if (std::is_same<T, double>::value) 	return std::stod(config);
            else return  T(); // return zero of given type
        }

        ///
        const std::string GetParam(const std::string& name) const {
            auto search = m_mconfig.find(name);
            if(search != m_mconfig.end())
                return search->second;
            else
                return std::string(name+" (no config)");
        }

};
#endif //CONFIG_H
