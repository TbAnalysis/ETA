//
// Created by brachwal on 28.06.19.
//

#ifndef TBCONFIGURABLE_H
#define TBCONFIGURABLE_H

#include "Config.h"
#include <string>
#include <memory>

class TbConfiguration;

class TbConfigurable : public Config,
                       public std::enable_shared_from_this<TbConfigurable> {
    private:
        ///
        std::string m_name;

        ///
        TbConfiguration* m_global_configuration;

    public:
        ///
        TbConfigurable () = delete;

        ///
        TbConfigurable (const std::string& name);

        ///
        virtual ~TbConfigurable();
};
#endif //TBCONFIGURABLE_H
