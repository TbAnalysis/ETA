//
// Created by brachwal on 26.06.19.
//

#ifndef TBCONFIGURATION_H
#define TBCONFIGURATION_H

#include <memory>
#include <string>
#include <stdexcept>
#include "Config.h"

using ConfigSharedPtr = std::shared_ptr<Config>;
using ConfigCollection = std::map<std::string,ConfigSharedPtr>;

class TbConfiguration {
    private:
        ///
        ConfigCollection m_config_collection;

        ///
        TbConfiguration() = default;

        ///
        ~TbConfiguration() = default;

        /// Delete the copy and move constructors
        TbConfiguration(const TbConfiguration&) = delete;
        TbConfiguration& operator=(const TbConfiguration&) = delete;
        TbConfiguration(TbConfiguration&&) = delete;
        TbConfiguration& operator=(TbConfiguration&&) = delete;

    public:
        ///
        static TbConfiguration* GetInstance(){
            static TbConfiguration instance;
            return &instance;
        }

        ///
        void DefineTbConfig(const std::string& tbObjName){
            m_config_collection[tbObjName] = std::make_shared<Config>();
            std::cout << "[INFO]:: TbConfiguration::DefineTbConfig:: New TbConfig instance emplaced for"
                      << " *** "<<tbObjName<<" *** "<<std::endl;
        }

        ///
        void AddTbConfig(const std::string& tbObjName, ConfigSharedPtr config = nullptr){
            if(config){
                auto search = m_config_collection.find(tbObjName);
                if(search == m_config_collection.end()){
                    m_config_collection[tbObjName] = config;
                    std::cout << "[INFO]:: TbConfiguration::AddTbConfig:: New TbConfig instance registered for "
                              << " *** "<<tbObjName<<" *** "<<std::endl;
                } else{
                    // the old instance will be destroyed after this call:
                    search->second.reset();
                    search->second = config;
                    std::cout << "[WARNING]:: TbConfiguration::AddTbConfig:: Resetting the old TbConfig ptr!" <<std::endl;
                    std::cout << "[INFO]:: TbConfiguration::AddTbConfig:: New TbConfig instance registered"
                              << " ("<<tbObjName <<")"<<std::endl;
                }
            } else {
                DefineTbConfig(tbObjName);
            }
        }

        ///
        void RemoveTbConfig(const std::string& tbObjName){
            auto search = m_config_collection.find(tbObjName);
            if(search != m_config_collection.end()) {
                m_config_collection.at(tbObjName).reset();
                m_config_collection.erase(tbObjName);
            }
        }

        ///
        void SetParam(const std::string& tbObjName, const std::string& configName, const std::string& value){
            auto search = m_config_collection.find(tbObjName);
            if(search != m_config_collection.end()){
                search->second->SetParam(configName,value);
            } else {
                // Emplace new TbConfig instance if the user call doesn't exists:
                DefineTbConfig(tbObjName);
                m_config_collection.at(tbObjName)->SetParam(configName,value);
            }
        }

        ///
        template <typename T>
        const T GetParam(const std::string& tbName, const std::string& configName) const {
            try{
                auto config = m_config_collection.at(tbName); // throw std::out_of_range& if failure
                return config->GetParam(configName);
            } catch (const std::out_of_range& oor) {
                std::cout << "[ERROR]:: TbConfiguration::GetConfig Out of Range error: " << oor.what() << std::endl;
            }
        }

};
#endif //TBCONFIGURATION_H
