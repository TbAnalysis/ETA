//
// Created by brachwal on 28.06.19.
//

#include "TbConfigurable.h"
#include "TbConfiguration.h"
#include <iostream>

////////////////////////////////////////////////////////////////////////////////////
///
TbConfigurable::TbConfigurable(const std::string& name)
    : m_name(name)
    , m_global_configuration(TbConfiguration::GetInstance())
{
    m_global_configuration->AddTbConfig(m_name,this->shared_from_this());
}

////////////////////////////////////////////////////////////////////////////////////
///
TbConfigurable::~TbConfigurable(){
    m_global_configuration->RemoveTbConfig(m_name);
}