//
// Created by brachwal on 21.06.19.
//

#include "TbJobHistMaker.h"
#include "TbJob.h"
#include "DutBinning.h"
#include "DutStripBinning.h"
#include "TbGaudi.h"
#include "TH2D.h"

////////////////////////////////////////////////////////////////////////////////////
///
TH2DSharedPtr TbJobHistMaker::DefineTH2D(const std::string& name, unsigned nX, double xLow, double xUp, unsigned nY, double yLow, double yUp) {
    auto sname = std::string(name);
    auto dutName = ThisTbJobPtr()->GetParam("DutName");
    auto runNumber = ThisTbJobPtr()->GetParam("RunNumber");
    std::string hname = sname+"_" + dutName + "_" + runNumber;
    std::string htitle = sname + " " + dutName + " " + runNumber;
    htitle += ThisTbJobPtr()->TH2AxesTitles();
    auto hist = std::make_shared<TH2D>(TString(hname),TString(htitle),nX,xLow,xUp,nY,yLow,yUp);
    hist->SetDirectory(0);
    return hist;
}

////////////////////////////////////////////////////////////////////////////////////
///
TH2DSharedPtr TbJobHistMaker::MakeDutBinningStatisticHist() {
    auto name = "BinnedDutStatistic";
    auto hist = GetTbJobHist<TH2D>(name);
    if(hist) // nothing to do - this histogram already exists!
        return hist;

    if(!ThisTbJobPtr()->DutPtr()->IsBinned()) // User didn't created DUT binning
        return nullptr;

    std::cout << "[INFO]:: TbJobHistMaker:: Creating binned DUT statistic histogram..." << std::endl;
    auto dutBinning = ThisTbJobPtr()->DutPtr()->DutBinningPtr();
    if(dutBinning->IsBinningStatFilled()){ // TRUE if User called Dut::CountDutBinHit(...) within the event loop
        unsigned nX  = Dut::NPixelsX();
        double xUp = Dut::NPixelsX();
        unsigned nY  = Dut::NPixelsY();
        double yUp = Dut::NPixelsY();

        // Create histogram
        auto hist = DefineTH2D(name,nX,0.,xUp,nY,0.,yUp);
        // Fill the histogram with the data stored in the DutBin
        std::cout << "[INFO]:: TbJobHistMaker:: Filling DUT binning statistic TH2D histogram..." << std::endl;
        std::cout << "[INFO]:: TbJobHistMaker:: nX "<< nX << std::endl;
        std::cout << "[INFO]:: TbJobHistMaker:: xUp "<< xUp << std::endl;
        std::cout << "[INFO]:: TbJobHistMaker:: nY "<< nY << std::endl;
        std::cout << "[INFO]:: TbJobHistMaker:: yUp "<< yUp << std::endl;
        for (unsigned x = 0; x < nX; x++) {
            for (unsigned y = 0; y < nY; y++) {
                auto binNumber = dutBinning->GetBinNumber(x, y);
                if (binNumber >= 0) {
                    auto dutBin = dutBinning->DutBins().at(binNumber);
                    int binStat = dutBin->Statistic();
                    hist->SetBinContent(x + 1, y + 1, binStat);
                    hist->SetBinError(x + 1, y + 1, 0.0001);
                }
            }
        }
        InsertTbJobHist(name,hist); // push newly created hist into the TbJobOutcome container
        return hist;
    } else {
        std::cout << "[INFO]:: TbJobHistMaker:: DUT bins statistic is not filled!\n"
                     "[INFO]:: >> for this purpose you should call jobPtr->CountDutHit(x,y)"
                     " within the event loop!"<<std::endl;
        return nullptr;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH2DSharedPtr TbJobHistMaker::MakeDutPixelStatisticHist() {
    auto name = "PixelStatistic";
    auto hist = GetTbJobHist<TH2D>(name);
    if(hist) return hist; // nothing to do - this histogram already exists!
    else {
        std::cout << "[INFO]:: TbJobHistMaker:: Creating Pixel statistic histogram..." << std::endl;
        auto dutBinning = ThisTbJobPtr()->DutPtr()->DutBinningPtr();
        if (dutBinning->IsPixelStatFilled()) { // TRUE if User called Dut::CountDutBinHit(...) within the event loop
            auto dutName = ThisTbJobPtr()->GetParam("DutName");
            auto runNumber = ThisTbJobPtr()->GetParam("RunNumber");
            auto hname = "DutPixStat_" + dutName + "_" + runNumber;
            auto htitle = "Dut pixels statistic (hit map) " + dutName + " " + runNumber;
            htitle += ThisTbJobPtr()->TH2AxesTitles();

            unsigned nX  = Dut::NPixelsX();
            double xUp = Dut::NPixelsX();
            unsigned nY  = Dut::NPixelsY();
            double yUp = Dut::NPixelsY();

            // Create histogram
            auto hist = DefineTH2D(name,nX, 0., xUp, nY, 0., yUp );
            hist->SetName(TString(hname));
            hist->SetTitle(TString(htitle));
            // Fill the histogram with the data stored in the DutBin
            std::cout << "[INFO]:: TbJobHistMaker:: Filling DUT pixel statistic TH2D histogram..." << std::endl;
            auto pixelBinningCounter = dutBinning->PixelBinningCounter();
            for (unsigned x = 0; x < nX; x++) {
                for (unsigned y = 0; y < nY; y++) {
                    int binStat = pixelBinningCounter.at(x).at(y);
                    hist->SetBinContent(x + 1, y + 1, binStat);
                    hist->SetBinError(x + 1, y + 1, 0.0001);
                }
            }
            InsertTbJobHist(name, hist); // push newly created hist into the TbJobOutcome container
            return hist;
        } else {
            std::cout << "[INFO]:: TbJobHistMaker:: DUT pixel statistic is not filled!\n"
                         "[INFO]:: >> for this purpose you should call jobPtr->CountDutHit(x,y)"
                         " within the event loop!"<<std::endl;
            return nullptr;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
#include "TCanvas.h"
TH2DSharedPtr TbJobHistMaker::MakeDutHistMap(const std::string& resultsName){
    if(!ITbJobResults::Exists(resultsName)) {
        std::cout << "[WARNING]:: TbJobHistMaker:: Results do not exists: " << resultsName << std::endl;
        return std::shared_ptr<TH2D>(nullptr);
    }

    auto hist = GetTbJobHist<TH2D>(resultsName);
    if(hist) return hist; // nothing to do - this histogram already exists!

    std::cout << "[INFO]:: TbJobHistMaker:: Creating DUT map histogram for " << resultsName << std::endl;
    auto job = ThisTbJobPtr();
    auto dutName = job->GetParam("DutName");
    auto runNumber = job->GetParam("RunNumber");
    auto hname = "DutMap_" + dutName + "_" + runNumber;
    auto htitle = "Dut " + resultsName + " map (" + dutName + " " + runNumber+")";
    htitle += job->TH2AxesTitles();

    unsigned nX  = Dut::NPixelsX();
    double xUp = Dut::NPixelsX();
    unsigned nY  = Dut::NPixelsY();
    double yUp = Dut::NPixelsY();

    // Create histogram
    hist = DefineTH2D(resultsName,nX, 0., xUp, nY, 0., yUp );

    hist->SetName(TString(hname));
    hist->SetTitle(TString(htitle));
    auto jobOutcome = GetTbOutcome();
    auto results = jobOutcome->Get<MResult>(resultsName);

    for(unsigned x=0; x<nX; ++x){
        for(unsigned y=0; y<nY; ++y){
            auto dutBin = job->DutBinNumber(x,y);
            double val(0.), err(0.);
            // results do not necessarily can exists for given bin
            if(dutBin>=0 && Includes<unsigned>(*results, static_cast<unsigned>(dutBin))) {
                val = results->at(dutBin).first;
                err = results->at(dutBin).second;
            }
             hist->SetBinContent(x + 1, y + 1, val);
             hist->SetBinError(x + 1, y + 1, err);
        }
    }
    InsertTbJobHist(resultsName, hist); // push newly created hist into the TbJobOutcome container
    std::cout << "[DEBUG]:: TbJobHistMaker:: returning " << hist->GetName() << std::endl;
    return hist;

}
////////////////////////////////////////////////////////////////////////////////////
///
TH2DSharedPtr TbJobHistMaker::MakeDutPixHistMap(const std::string& resultsName){
    if(!ITbJobResults::Exists(resultsName)) {
        std::cout << "[WARNING]:: TbJobHistMaker:: Results do not exists: " << resultsName << std::endl;
        return std::shared_ptr<TH2D>(nullptr);
    }
    if(ThisTbJobPtr()->DutPtr()->IsBinned()){
        std::cout << "[ERROR]:: TbJobHistMaker:: MakeDutPixHistMap"
                  << ":: method used for out of scope study (this method is for not binned DUT)"<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    auto hist = GetTbJobHist<TH2D>(resultsName);
    if(hist) return hist; // nothing to do - this histogram already exists!

    std::cout << "[INFO]:: TbJobHistMaker:: Creating DUT map histogram for " << resultsName << std::endl;
    auto job = ThisTbJobPtr();
    auto dutName = job->GetParam("DutName");
    auto runNumber = job->GetParam("RunNumber");
    auto hname = "DutPixMap_" + dutName + "_" + runNumber;
    auto htitle = "Dut Pixel" + resultsName + " map (" + dutName + " " + runNumber+")";
    htitle += job->TH2AxesTitles();
    unsigned nX = DutPix::NBinsX();
    double xLow = 0., xUp = Dut::PitchSize();
    unsigned nY = DutPix::NBinsY();
    double yLow = 0., yUp = Dut::PitchSize();

    // Create histogram
    hist = DefineTH2D(resultsName,nX, xLow, xUp, nY, yLow, yUp );
    hist->SetName(TString(hname));
    hist->SetTitle(TString(htitle));
    auto jobOutcome = GetTbOutcome();
    auto results = jobOutcome->Get<MPixResult>(resultsName);

    for(unsigned x=0; x<nX; ++x){
        for(unsigned y=0; y<nY; ++y){
            double val = 0., err = 0.;
            auto pixelBin = std::make_pair(x,y);
            if(Includes<Pixel>(*results,pixelBin)){
                auto pixelResult = results->at(pixelBin);
                val = pixelResult.first;
                err = pixelResult.second;
            }

            hist->SetBinContent(x + 1, y + 1, val);
            hist->SetBinError(x + 1, y + 1, err);
        }
    }
    InsertTbJobHist(resultsName, hist); // push newly created hist into the TbJobOutcome container
    return hist;

}

////////////////////////////////////////////////////////////////////////////////////
///
TH2DSharedPtr TbJobHistMaker::MakeDutPixHistMap(const std::string& resultsName, unsigned dutBinNumber) {
    if (!ITbJobResults::Exists(resultsName)) {
        std::cout << "[WARNING]:: TbJobHistMaker:: Results do not exists: " << resultsName << std::endl;
        return std::shared_ptr<TH2D>(nullptr);
    }
    if (!ThisTbJobPtr()->DutPtr()->IsBinned()) {
        std::cout << "[ERROR]:: TbJobHistMaker:: MakeDutPixHistMap"
                  << ":: method used for out of scope study (this method is for binned DUT)"<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    // Note: for this type of binning there is a series of histograms/maps of given resultsName, hence:
    auto resultsNameForBin = resultsName + "_DutBin" +itos(dutBinNumber);
    auto hist = GetTbJobHist<TH2D>(resultsNameForBin);
    if(hist) return hist; // nothing to do - this histogram already exists!

    std::cout << "[INFO]:: TbJobHistMaker:: Creating DUT map histogram for " << resultsNameForBin << std::endl;
    auto job = ThisTbJobPtr();
    auto dutName = job->GetParam("DutName");
    auto runNumber = job->GetParam("RunNumber");
    auto hname = "DutPixMap_"+ dutName + "_" + runNumber + "_Bin" +itos(dutBinNumber);
    auto htitle = "Dut Pixel " + resultsName + " map (" + dutName + " " + runNumber+" Bin"+itos(dutBinNumber)+")";
    htitle += job->TH2AxesTitles();
    unsigned nX = DutPix::NBinsX();
    double xLow = 0., xUp = Dut::PitchSize();
    unsigned nY = DutPix::NBinsY();
    double yLow = 0., yUp = Dut::PitchSize();

    // Create histogram
    hist = DefineTH2D(resultsNameForBin,nX, xLow, xUp, nY, yLow, yUp );
    hist->SetName(TString(hname));
    hist->SetTitle(TString(htitle));
    auto jobOutcome = GetTbOutcome();
    auto dutBinningResults = jobOutcome->Get<MDutBinPixResult>(resultsName);
    if(!Includes<unsigned>(*dutBinningResults,dutBinNumber)){
        std::cout << "[WARNING]:: TbJobHistMaker:: Results do not exists for this DUT bin: " << dutBinNumber << std::endl;
        return std::shared_ptr<TH2D>(nullptr);
    }
    auto results = dutBinningResults->at(dutBinNumber);
    for(unsigned x=0; x<nX; ++x){
        for(unsigned y=0; y<nY; ++y){
            double val = 0., err = 0.;
            auto pixelBin = std::make_pair(x,y);
            if(Includes<Pixel>(results,pixelBin)){
                auto pixelResult = results.at(pixelBin);
                val = pixelResult.first;
                err = pixelResult.second;
                //std::cout << "[DEBUG]:: TbJobHistMaker:: pixel result ("<<x<<","<<y <<") results " << val <<"+/-"<<err << std::endl;
            } else {
                std::cout << "[INFO]:: TbJobHistMaker:: no result for pixel ("<<x<<","<<y << ")" << std::endl;
            }
            hist->SetBinContent(x + 1, y + 1, val);
            hist->SetBinError(x + 1, y + 1, err);
        }
    }
    InsertTbJobHist(resultsNameForBin, hist); // push newly created hist into the TbJobOutcome container
    return hist;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobHistMaker::MakeDutStripStatisticHist() {

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobHistMaker::MakeDutIntraPixelStatisticHist() {

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobHistMaker::MakeDutIntraStripStatisticHist() {

}