//
// Created by brachwal on 28.11.18.
//

#include "FluenceProfile.h"
#include "IRRADProfile.h"
#include "TbGaudi.h"
#include "Dut.h"

std::vector<std::unique_ptr<FluenceProfile>> FluenceProfile::m_fln_profiles 
= std::vector<std::unique_ptr<FluenceProfile>>();

std::map<std::string,std::shared_ptr<FluenceProfile>> FluenceProfile::m_instatiated_fln_profiles
= std::map<std::string,std::shared_ptr<FluenceProfile>>();

////////////////////////////////////////////////////////////////////////////////////
///
FluenceProfile::FluenceProfile(const std::string& type): m_type(type) {}

////////////////////////////////////////////////////////////////////////////////////
std::string FluenceProfile::m_default_type = "IRRAD";

////////////////////////////////////////////////////////////////////////////////////
bool FluenceProfile::m_evaluate=false;
bool FluenceProfile::m_export_to_csv=false;

////////////////////////////////////////////////////////////////////////////////////
///
void FluenceProfile::SetDutBinning(DutBinningSharedPtr ptr){
    if(!m_dut_binning.get())
        m_dut_binning=ptr;
    else {
        std::cout << "[WARNING]:: FluenceProfile:: SetDutBinning... ( >> RESET? <<< )"<<std::endl;
        m_dut_binning.reset();
        m_dut_binning=ptr;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string FluenceProfile::LinkedDutName() const {
    return DutBinningPtr() ? DutBinningPtr()->OwnerDut()->Name() : std::string();
}

////////////////////////////////////////////////////////////////////////////////////
///
FluenceProfile* FluenceProfile::AddCustomProfile(FluenceProfile* customFlnPtr){
    // Verify if a given fluence profile already exists in store!
    auto customFlnType = customFlnPtr->Type();
    for(const auto& flnProfile: m_fln_profiles ){
        if (flnProfile->Type()==customFlnType){
            std::cout << "[WARNING]:: FluenceProfile::AddCustomProfile: The \""<< customFlnType << "\" already exists in the store!" <<std::endl;
            delete customFlnPtr;
            return flnProfile.get();
         break;
        }
    }
    m_fln_profiles.push_back(std::unique_ptr<FluenceProfile>(customFlnPtr));
    std::cout << "[INFO]:: FluenceProfile::AddCustomProfile: The \""<< customFlnType << "\" added to the store!" <<std::endl;
    return customFlnPtr;
}

////////////////////////////////////////////////////////////////////////////////////
///
FluenceProfile* FluenceProfile::AddProfile(const std::string& flnType){
    if(flnType=="IRRAD"){
        m_fln_profiles.emplace_back(std::make_unique<IRRADProfile>());
    } else if(flnType=="KIT"){ // TODO
        std::cout << "[ERROR]:: FluenceProfile::AddProfile: The \""<< flnType << "\" is not implemented (yet)!" <<std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    } else {
        std::cout << "[ERROR]:: FluenceProfile::AddProfile: The \""<< flnType << "\" is not defined!" <<std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    std::cout << "[DEBUG]:: FluenceProfile::AddProfile size prof vec " << m_fln_profiles.size() << std::endl;

    auto flnPtr = m_fln_profiles.back().get();
    std::cout << "[INFO]:: FluenceProfile::AddProfile: The deafulted \""<< flnPtr->Type() << "\" added to the store!" <<std::endl;
    return flnPtr;
}

////////////////////////////////////////////////////////////////////////////////////
///
FluenceProfile* FluenceProfile::GetProfile(const std::string& flnType){
    for(const auto& flnProfile: m_fln_profiles ){
        if (flnProfile->Type()==flnType){
            return flnProfile.get();
        }
    }
    auto flnProfPtr = AddProfile(flnType);
    return flnProfPtr;
}

void FluenceProfile::Evaluate(){
        std::cout << "[WARNING]:: FluenceProfile evaluation is being performed for default alignemnt!" <<std::endl;

}