//
// Created by brachwal on 03.03.20.
//

#ifndef TbRooDataSetCollection_H
#define TbRooDataSetCollection_H

#include "TbRooDataSet.h"

/**
* This is utility class to work with collection of TbRooDataSet objects.
*/
class TbRooDataSetCollection {
    private:
        ///
        std::vector<std::unique_ptr<TbRooDataSet>> m_data;

    public:
        ///
        TbRooDataSetCollection() = default;

        ///
        TbRooDataSetCollection(const TbRooDataSetCollection&) = delete;

        ///
        ~TbRooDataSetCollection() = default;

        ///
        void Add(std::unique_ptr<TbRooDataSet> data);

        ///
        unsigned Size() const { return m_data.size(); }

        ///
        TbRooDataSet* At(unsigned i) { return m_data.at(i).get(); }

        ///
        bool Includes(const std::string& varName) const;

        ///
        std::vector<std::unique_ptr<TbRooDataSet>>& Data() { return m_data; }
};

#endif //TbRooDataSetCollection_H
