//
// Created by brachwal on 23.08.19.
//

#ifndef TOBJ_COLLECTION_H
#define TOBJ_COLLECTION_H

#include <map>
#include <memory>
#include <string>
#include "TObject.h"
#include "Globals.h"
#include "TbGaudi.h"

using TObjSharedPtr = std::shared_ptr<TObject>;
using MTObj = std::map<std::string,TObjSharedPtr>;

////////////////////////////////////////////////////////////////////////////////////
///
class TObjCollection {
private:

    MTObj m_objCollection; // works also for TH2 since it inherits from TH1

public:
    ///
    TObjCollection() = default;

    ///
    ~TObjCollection() = default;

    ///
    void Insert(const std::string& name, TObjSharedPtr obj);

    ///
    template <typename T>
    std::shared_ptr<T> Get(const std::string& name) const;

};

template <typename T>
std::shared_ptr<T> TObjCollection::Get(const std::string& name) const {
    if(Includes<std::string>(m_objCollection,name)){
        return std::dynamic_pointer_cast<T>(m_objCollection.at(name));
    } else {
        std::cout << "[WARNING]:: TObjCollection:: "<< name << " doesn't exists!"<< std::endl;
        std::cout << "[WARNING]:: TObjCollection:: *** the caller should handle nullptr being returned ***"<< std::endl;
        return nullptr;
    }
}
#endif //TOBJ_COLLECTION_H
