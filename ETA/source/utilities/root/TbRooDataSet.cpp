//
// Created by brachwal on 03.03.20.
//

#include "TbRooDataSet.h"
#include <thread>
#include "TThread.h"
#include "TbGaudi.h"
#include "Globals.h"

#include "RooCategory.h"

////////////////////////////////////////////////////////////////////////////////////
///
TbRooDataSet::TbRooDataSet(const char *name, const char *title, const RooArgSet &vars) {
    m_data_set = std::make_unique<RooDataSet>(name, title,vars);
    // to allow Event initialization when SetEventValue is being called
    SetEventStatus(true);
}

////////////////////////////////////////////////////////////////////////////////////
///
TbRooDataSet::TbRooDataSet(RooDataSet* other){
    m_data_set = std::unique_ptr<RooDataSet>(other);
    // to allow Event initialization when SetEventValue is being called
    SetEventStatus(true);
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbRooDataSet::GetEventStatus(){
    unsigned counter = 0;
    for(const auto& iVarStatus : m_event_status){
        if (iVarStatus.second == true) ++counter;
    }
    return counter==m_event_status.size() ? true : false;
}

////////////////////////////////////////////////////////////////////////////////////
///
TbRooDataSet* TbRooDataSet::Clone() const {
    // Ensure unique name when run in multithreaded mode
    const auto threadId = std::this_thread::get_id();
    auto ras = const_cast<RooArgSet*>(m_data_set->get());
    auto name = std::string(m_data_set->GetName())+"_Clone"+idtos(threadId);
    auto title = m_data_set->GetTitle();
    return new TbRooDataSet(name.c_str(),title,*ras);
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbRooDataSet::InitNextEvent(){
    // get internal RooArgSet
    auto ras = const_cast<RooArgSet*>(m_data_set->get());
    m_data_set->addFast(*ras);     // simply clone the previous event
    ResetEvent();             // and set zeros

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbRooDataSet::SetEventStatus(bool status){
    if(m_data_set) {
        RooRealVar *next = 0;
        // get internal RooArgSet
        auto ras = const_cast<RooArgSet*>(m_data_set->get());
        std::unique_ptr <TIterator> iterat(ras->createIterator());
        while ((0 != (next = dynamic_cast<RooRealVar*>(iterat->Next()))))
            m_event_status[next->GetName()] = true;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbRooDataSet::ResetEvent(){
    if(m_data_set) {
        RooRealVar *next = nullptr;
        // get internal RooArgSet
        auto ras = const_cast<RooArgSet *>(m_data_set->get());
        std::unique_ptr <TIterator> iterat(ras->createIterator());
        while ((nullptr != (next = dynamic_cast<RooRealVar *>(iterat->Next())))){
            next->setVal(.0);   // reset values
            next->removeError();
            // do not call SetEventStatus, just set values here:
            m_event_status[next->GetName()] = false;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbRooDataSet::Fill(const std::string& name, double var, int categoryIdx){
    if(GetEventStatus())
        InitNextEvent();
    auto ras = const_cast<RooArgSet*>(m_data_set->get());
    auto rrv = dynamic_cast<RooRealVar*>(ras->find(name.c_str()));
    auto rcategory = dynamic_cast<RooCategory*>(ras->find("RCategory"));
    if(!rrv){
        TThread::Lock();
        std::cout<<"[ERROR]::TbRooDataSet::Fill:: the variable not found" << name << std::endl;
        TThread::UnLock();
    }
    rrv->setVal(var);
    
    if( std::abs(-999 - categoryIdx) > 0.0001 ){ // are different
        if(rcategory)
            rcategory->setIndex(categoryIdx);
        else {
            TThread::Lock();
            std::cout<<"[WARNING]::TbRooDataSet::Fill:: the category index but the user RooCategory"
                     <<" hasn't been defined - this is dummy operation..."<< std::endl;
            TThread::UnLock();
        }
    } else {
        if(rcategory){
            TThread::Lock();
            std::cout<<"[ERROR]::TbRooDataSet::Fill:: category index not given but the user RooCategory"
                     <<" has been defined!"<< std::endl;
            TThread::UnLock();
        }
    }

    m_event_status.at(TString(name)) = true;

}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbRooDataSet::Includes(const std::string& varName) const {
    if(m_data_set){
        auto ras = const_cast<RooArgSet*>(m_data_set->get());
        auto rrv = dynamic_cast<RooRealVar*>(ras->find(varName.c_str()));
        if(rrv)
            return true;
    }
    return false;
}