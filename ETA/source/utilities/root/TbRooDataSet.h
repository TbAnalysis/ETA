//
// Created by brachwal on 03.03.20.
//

#ifndef TBROODATASET_H
#define TBROODATASET_H

#include <vector>
#include <map>
#include <memory>
#include <utility>
#include "RooDataSet.h"
#include "RooRealVar.h"

/**
* This is utility class to combine together all the necessery components 
* to work with the default ROOT::RooDataSet functionality. RooDataSet is 
* an unbinned dataset (a collection of points in N-dimensional space):
* RooDataSet d("d","d",RooArgSet(x,y,c)) ;
* Note: RooDataSet are not attached to the variables they are constructed from. 
*       Instead they are attached to an internal clone of the supplied set of arguments.
*       By default filling the RooDataSet within the event loop can be achieved by calling:
*       d.add(RooArgSet(x,y,c)) ; 
*       We must explicitly refer to x,y,c here to pass the values because d is not linked to them.
* The TbRooDataSet defined below allows to add semaparatly each variable value within given 
* event being processed in the event loop, e:g.:
* d.Fill("x",x);
* d.Fill("y",y);
*/

class TbRooDataSet {
    private:
        ///
        std::unique_ptr<RooDataSet> m_data_set;

        ///
        std::map<TString, bool> m_event_status;

        ///
        void InitNextEvent();

        ///
        bool GetEventStatus();

        ///
        void SetEventStatus(bool status);

        ///
        void ResetEvent();

    public:
        ///
        TbRooDataSet() = delete;

        ///
        TbRooDataSet(const char *name, const char *title, const RooArgSet &vars);

        /// Use Clone instead
        TbRooDataSet(const TbRooDataSet&) = delete;

        ///
        TbRooDataSet(RooDataSet* other);

        ///
        ~TbRooDataSet() = default;

        ///
        void Fill(const std::string& name, double var, int categoryIdx=-999);

        ///
        TbRooDataSet* Clone() const;

        ///
        RooDataSet* GetRooDataSetPtr() const { return m_data_set ? m_data_set.get() : nullptr; }

        ///
        bool Includes(const std::string& varName) const;
};

#endif //TBROODATASET_H
