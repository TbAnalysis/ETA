//
// Created by brachwal on 03.03.20.
//

#include "TbRooDataSetCollection.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbRooDataSetCollection::Add(std::unique_ptr<TbRooDataSet> data){
    m_data.push_back(std::move(data));
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbRooDataSetCollection::Includes(const std::string& varName) const {
    if(!m_data.empty())
        // each RooDataSet is being built on the same RooArgSet, hence
        return m_data.at(0)->Includes(varName);
    return false;
}