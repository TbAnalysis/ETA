//
// Created by brachwal on 21.06.19.
//

#ifndef TBJOBHISTMAKER_H
#define TBJOBHISTMAKER_H

#include "ITbJobResults.h"
#include "TbHistMaker.h"

class TbJobHistMaker : public TbHistMaker,
                       public ITbJobResults {
public:
    ///
    TbJobHistMaker() = delete;

    ///
    TbJobHistMaker(TbJobSharedPtr tbjob) : ITbJobResults(tbjob) {};

    ///
    ~TbJobHistMaker() = default;

    ///
    TH2DSharedPtr DefineTH2D(const std::string& name, unsigned nX, double xLow, double xUp, unsigned nY, double yLow, double yUp) override;

    ///
    TH2DSharedPtr MakeDutBinningStatisticHist();

    ///
    TH2DSharedPtr MakeDutPixelStatisticHist();

    ///
    TH2DSharedPtr MakeDutHistMap(const std::string& resultsName);

    ///
    TH2DSharedPtr MakeDutPixHistMap(const std::string& resultsName);

    ///
    TH2DSharedPtr MakeDutPixHistMap(const std::string& resultsName, unsigned dutBinNumber);

    ///
    void MakeDutStripStatisticHist();

    ///
    void MakeDutIntraPixelStatisticHist();

    ///
    void MakeDutIntraStripStatisticHist();
};
#endif //TBJOBHISTMAKER_H
