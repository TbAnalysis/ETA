#ifndef TomlInputParser_H
#define TomlInputParser_H

#include "InputParser.h"
#include "cpptoml.h"

class TomlInputParser : public InputParser {
	private:
		///
		std::shared_ptr<cpptoml::table> m_parsed_TOML;

		///
		size_t GetParamTableSize(const std::shared_ptr<cpptoml::table>& table);

		///
		void ParseAnalysisNames();

		///
		void ParseAnalysisCollections() override;

		///
		void ParseJobProperties(JobData& job, const std::shared_ptr<cpptoml::table> job_properties_table);

		///
		bool CheckObligatoryValues(const std::shared_ptr<cpptoml::table>& job_properties_table, std::vector<std::string>& missing_parameters) const;

		///
		std::string FindDataPathPattern(std::shared_ptr<cpptoml::table_array> jobs_tables);

		///
		std::string FindDataFilePattern(std::shared_ptr<cpptoml::table_array> jobs_tables);

		///
		std::string FindDataTTreePattern(std::shared_ptr<cpptoml::table_array> jobs_tables);

		///
		void PrintMissingParameters(const std::string analysis_name, std::vector<std::string>& missing_parameters);

	public:
		///
		TomlInputParser(const std::string& filename);
};

#endif // TomlInputParser_H