//
// Created by brachwal on 08.07.19.
//

#include "TbIO.h"
#include <iostream>
#include "TbGaudi.h"
#include "Globals.h"
#include "TbLog.h"
#include "TbJobCollector.h"
#include "TbJobIO.h"
#include "TbJob.h"


namespace fs = std::filesystem;

std::string TbIO::m_output_location = std::string();

////////////////////////////////////////////////////////////////////////////////////
///
const std::string TbIO::CreateCurrentDateDirIfNotExits(const std::string& path) const {
	std::string dateDirPath = path;
	auto date = CurrentDate();
	dateDirPath+=path.at(path.length()-1)=='/' ? date : "/"+date;
	fs::path dp (dateDirPath);
	if(!fs::exists (dp)){
		std::cout << "[INFO]:: Created directory: "<< date <<std::endl;
		fs::create_directories(dp);
	}
	return dateDirPath;
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string TbIO::GetOutputDirPath(){
    return CreateOutputDir();
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string TbIO::CreateOutputDir(const std::string& userArgPath) const {
    // define the output dir path
    std::string output_dir_path;
    auto project_local_dir_path = TbGaudi::ProjectLocation()+"/TbResults";
    auto user_dir_path = TbIO::OutputLocation(); // can also be set globally
    if(!userArgPath.empty())
        output_dir_path=userArgPath;
    else if(!user_dir_path.empty())
        output_dir_path = user_dir_path;
    else
        output_dir_path = project_local_dir_path;

    // create current day-date dir if not exists
    auto date_output_dir = CreateCurrentDateDirIfNotExits(output_dir_path);
    std::cout << "[INFO]:: TbIO:: Output location is defined as:\n"
                    "[INFO]:: "<< date_output_dir << std::endl;
    return date_output_dir;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbIO::DeleteFileIfExists(const std::string& file_full_path) const {
	fs::path fp = file_full_path;
	if(fs::exists(fp)){
		std::cout << "[INFO]:: Remove existing file:\n"
			   		 "[INFO]:: " << file_full_path <<std::endl;
		fs::remove(fp);
	}
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbIO::ExportResultsToPdfFile(const std::string& dir_path){
    
    auto plotsCollection = TbResultsPtr()->PlotsPtr();
    auto nPlots = plotsCollection->NPlots();
    if(nPlots>0){
        auto file = CreateOutputDir(dir_path)+"/jobs_compilation_results.pdf";
        std::cout << "[INFO]:: TbIO::ExportResults:: pdf file output: " << file << std::endl;
        plotsCollection->ExportToPdf(file);
    } else {
        std::cout << "[INFO]:: TbIO::ExportResults:: any pdf-like data to be exported" << std::endl;
    }

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbIO::ExportResultsToNTupleFile(const std::string& dir_path){
    std::cout << "[NOT IMPLEMENTED] [INFO]:: TbIO::ExportResults:: NTuple file " << std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbIO::ExportResultsToCsv(const std::string& dir_path){
    std::cout << "[NOT IMPLEMENTED] [INFO]:: TbIO::ExportResults:: CSV file " << std::endl;
}


////////////////////////////////////////////////////////////////////////////////////
/// Static function exporting all results from the job collection
void TbIO::ExportAllResults(const std::string& dir_path){
    
    TbGaudi::PrintBanner("INFO","TbResults::Exporting all results to local files");

    // Export job results
    auto jobCollection = TbJobCollector::GetInstance()->TbJobs();
    for(const TbJobSharedPtr& job : jobCollection) 
        job->TbIOPtr()->ExportResults(dir_path);
    
    // export compilation results
    std::cout<<"[INFO]:: Exporting compilation results" << std::endl;
    auto io = std::make_unique<TbIO>();
    io->ExportResults(dir_path);
}

////////////////////////////////////////////////////////////////////////////////////
///Exporting current instance result - this function should be overvritten in derived classes
void TbIO::ExportResults(const std::string& dir_path){
    TbLog::Info("[INFO]:: Exporting compilation results");
    this->ExportResultsToPdfFile(dir_path);
    this->ExportResultsToNTupleFile(dir_path);
    this->ExportResultsToCsv(dir_path);
}