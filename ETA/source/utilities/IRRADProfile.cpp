//
// Created by brachwal on 28.11.18.
//
#include "IRRADProfile.h"
#include "Dut.h"
#include "TbJob.h"
#include "TbIO.h"

////////////////////////////////////////////////////////////////////////////////////
///
IRRADProfile::IRRADProfile(): FluenceProfile("IRRAD"){}

////////////////////////////////////////////////////////////////////////////////////
///
void IRRADProfile::Initialize(){

    if(!m_isInitialized) {
        std::cout << "[INFO]:: IRRAD:: DEFAULT parameterization is used." << std::endl;
        m_dosimetry_mean_col = static_cast<double>(Dut::NPixelsX()) / 2; // midlle of the DUT
        m_dosimetry_mean_row = static_cast<double>(Dut::NPixelsY()) / 2; // midlle of the DUT
        m_dosimetry_sigma_col = 100.;   // arbitrary value
        m_dosimetry_sigma_row = 150.;   // arbitrary value
        m_mean_col = m_dosimetry_mean_col;
        m_mean_row = m_dosimetry_mean_row;

        m_dosimetry_fluence = 5e15; // arbitrary value

        m_isDefault = true;
        m_isInitialized = true;
    }
    else{
        std::cout << "[INFO]::IRRAD:: The default parameterization already initialized for this DUT." << std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::shared_ptr<FluenceProfile> IRRADProfile::Instantiate(DutBinningSharedPtr ptr){
    auto name = ptr->OwnerDut()->ThisTbJobPtr()->Label("DUT");
    std::cout << "[INFO]:: IRRAD:: Instatiating the fluence  profile for " << name << std::endl;
    if(m_instatiated_fln_profiles.find(name) != m_instatiated_fln_profiles.end()){
        std::cout << "[INFO]::IRRAD:: The fln  profile for " << name << " already instatiated." << std::endl;
    } else {
        // create new object once the derived class is implemented or not
        // (using copy constructor):
        auto new_instance = std::shared_ptr<FluenceProfile>(ClonePtr());
        new_instance->SetDutBinning(ptr);
        new_instance->Initialize();
        new_instance->DefineProfileFunction();
        m_instatiated_fln_profiles.insert(std::make_pair(name,new_instance) );
    }
    return m_instatiated_fln_profiles.at(name);
}

////////////////////////////////////////////////////////////////////////////////////
///
void IRRADProfile::DefineProfileFunction(){
    m_profile_func = TF2("GP","TMath::Gaus(x,[0],[1])*TMath::Gaus(y,[2],[3])",0.,255.,0.,255.);
    m_profile_func.SetTitle("The beam profile::2DGauss aligned with the noise run map");
	m_profile_func.SetNpy(256);
 	m_profile_func.SetNpx(256);
    // NOTE: Elena's studies and strategy: Set General Fluence Profile parameters from dosimetry
    // m_profile_func.SetParameters(m_dosimetry_mean_col,m_dosimetry_sigma_col, m_dosimetry_mean_row, m_dosimetry_sigma_row);
    // Bartek's direct approach: shift the profile centre according to the results from the activation map fits:
    m_profile_func.SetParameters(m_mean_col,m_dosimetry_sigma_col, m_mean_row, m_dosimetry_sigma_row);
}

////////////////////////////////////////////////////////////////////////////////////
///
void IRRADProfile::Evaluate(){
        std::cout << "[INFO]::IRRAD:: Fluence Profile evaluation is being performed!" <<std::endl;
        auto elipiticBinningPtr = dynamic_cast<DutEllipticBinning*>(DutBinningPtr().get());
        auto nBins = elipiticBinningPtr->NBins();
        auto dos_fln = FluenceProfile::GetDosimetryFluence();
        std::cout << "[INFO]::IRRAD:: Fluence normalization "<< dos_fln << " [n_eq/cm^2]" << std::endl;
        //std::cout << "[DEBUG]::IRRAD:: Evaluate  NBins "<< nBins <<std::endl;
        double totalIntegral=0;
        auto dutArea = elipiticBinningPtr->OwnerDut()->Area() / 100; // [mm^2 -> cm^2]
        auto normalization = dos_fln * dutArea;
        //std::cout << "[DEBUG]::IRRAD:: Fln profile normalization "<< normalization << std::endl;
        for(int i=1; i<=nBins;++i){
            auto binIntegral = elipiticBinningPtr->BinIntegral(m_profile_func,i,normalization);
            auto binArea = elipiticBinningPtr->BinArea(i) / 100; // [mm^2 -> cm^2]
            totalIntegral+=binIntegral; // absolute number of particles
            binIntegral /= binArea;
            std::cout<< "[DEBUG]::IRRAD:: Integrated fluence #"<<i<<" bin: "
                    << binIntegral << " [n_eq/cm^2]" << " ; bin area: " << binArea << " [cm^2]" << std::endl;
            m_binned_fluence.emplace_back(binIntegral);
        }
        m_evaluated_fluence = totalIntegral/dutArea;
        std::cout << "[INFO]::IRRAD:: Total evaluated fluence "<< m_evaluated_fluence << " [n_eq/cm^2]" <<std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void IRRADProfile::ExportFluenceBinningToCsv(){
    std::cout << "[INFO]::IRRAD:: Evaluated fluence export to csv..." <<std::endl;
    
    auto elipiticBinningPtr = dynamic_cast<DutEllipticBinning*>(DutBinningPtr().get());

    auto io = TbIO();
	auto output_dir_path = io.GetOutputDirPath();
    auto name = DutBinningPtr()->GetName()+"_Evaluated.csv";
    auto file = output_dir_path+"/"+name;
	std::cout << "[INFO]:: Writing to the file: " << name << std::endl;

    std::string sep = io.GetCsvSeparator();
    std::ofstream outFile;
    outFile.open(file.c_str(), std::ios::out);
    outFile << "Total dosimetry fluence: " << FluenceProfile::GetDosimetryFluence() << " [n_eq/cm^2]" << std::endl;
    outFile << "Total evaluated fluence: " << GetEvaluatedFluence() << " [n_eq/cm^2]" << std::endl;
    outFile << "DUT bin"<< sep << "DUT bin area [cm^2]" << sep <<"Fluence [n_eq/cm^2]"<< std::endl; // data header
    auto nBins = m_binned_fluence.size(); //elipiticBinningPtr->NBins();

    std::ostringstream buffer;
    for (size_t i=1; i <= nBins; ++i){  // bins are being numerated from 1
        auto fln = m_binned_fluence.at(i-1);
        auto binArea = elipiticBinningPtr->BinArea(i) / 100; // [mm^2 -> cm^2]
        buffer << i << sep << binArea << sep << fln << std::endl;
        
    }
    outFile << buffer.str() << std::endl;
    buffer.clear();
    outFile.close();
}


////////////////////////////////////////////////////////////////////////////////////
///
double IRRADProfile::GetDosimetryFluence(int binNumber){ return 0.; }

////////////////////////////////////////////////////////////////////////////////////
///
double IRRADProfile::GetDosimetryFluence(int col, int row){ return 0.; }

////////////////////////////////////////////////////////////////////////////////////
///
double IRRADProfile::GetDosimetryBinArea(int binNumber){return 0.; }

////////////////////////////////////////////////////////////////////////////////////
///
int IRRADProfile::GetDosimetryBin(int col, int row){ return 0; }

////////////////////////////////////////////////////////////////////////////////////
///
void IRRADProfile::DrawDosimetryMeasurement(){}

////////////////////////////////////////////////////////////////////////////////////
///
int IRRADProfile::GetBin(double){ return 0; }

////////////////////////////////////////////////////////////////////////////////////
///
int IRRADProfile::GetBin(int, int){ return 0; }

////////////////////////////////////////////////////////////////////////////////////
///
double IRRADProfile::GetFluence(int bin){ 
    auto dutBinning = std::dynamic_pointer_cast<DutEllipticBinning>(m_dut_binning);
    if (bin < dutBinning->BoundaryEllipsesCount()) {
        return dutBinning->BinIntegral(m_profile_func, bin, 1.0,  0.0, 360.0);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
double IRRADProfile::GetFluence(int xx, int yy) const {
    unsigned dut_size_x = Dut::NPixelsX();
    unsigned dut_size_y = Dut::NPixelsY();
    double x = static_cast<double>(xx)/dut_size_x * m_dut_binning->OwnerDut()->DutSizeX();
    double y = static_cast<double>(yy)/dut_size_y * m_dut_binning->OwnerDut()->DutSizeY();
    return GetFluence(x, y);
}

double IRRADProfile::GetFluence(double x, double y) const {
    return m_profile_func.Eval(x, y);
}

////////////////////////////////////////////////////////////////////////////////////
///
double IRRADProfile::GetPixFluence(int Lx, int Ly){ return 0.; }

////////////////////////////////////////////////////////////////////////////////////
///
std::tuple<double, double, double> IRRADProfile::GetMinPixel() const {
    double x_axis[2] {0.0, static_cast<double>(m_dut_binning->OwnerDut()->DutSizeX())};
    double y_axis[2] {0.0, static_cast<double>(m_dut_binning->OwnerDut()->DutSizeY())};

    //double dut_size_x = static_cast<double>(m_dut_binning->OwnerDut()->DutSizeX());
    //double dut_size_y = static_cast<double>(m_dut_binning->OwnerDut()->DutSizeY());

    std::tuple<double, double, double> pixel = GetMaxPixel();
    
    double val;
    double min_x = std::get<0>(pixel);
    double min_y = std::get<1>(pixel);
    double min = std::get<2>(pixel);

    for (int i=0; i<2; i++) {
        for (int j=0; j<2; j++) {
            val = GetFluence(x_axis[i], y_axis[j]);
            if (min > val) {
                min = val;
                min_x = x_axis[i];
                min_y = y_axis[j];
                pixel = std::make_tuple(min_x, min_y, min);
            }
        }
    }

    return pixel;
}

std::tuple<double, double, double> IRRADProfile::GetMaxPixel() const {
    return std::make_tuple(m_mean_col, m_mean_row, GetFluence(m_mean_col, m_mean_row));
}

double IRRADProfile::GetXFromFluence(double fluence_value) {
    
}


double IRRADProfile::GetYFromFluence(double fluence_value) {

}

////////////////////////////////////////////////////////////////////////////////////
///
void IRRADProfile::DrawBinning(TPad* pad, std::string opts){}

