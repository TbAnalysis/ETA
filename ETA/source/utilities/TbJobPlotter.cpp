//
// Created by brachwal on 30.05.19.
//
#include "TbJobPlotter.h"
#include "TbJob.h"

////////////////////////////////////////////////////////////////////////////////////
///
TbJobPlotter::TbJobPlotter(TbJobSharedPtr tbjob) : ITbJobResults(tbjob) {
    // emplace if was not emplaced before for the given tbjob
    TbResultsPtr()->EmplaceTbJobPlots(tbjob);
    //
    m_hmaker = std::make_unique<TbJobHistMaker>(tbjob);
    m_gmaker = std::make_unique<TbJobGraphMaker>(tbjob);
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobPlotter::DrawDutStatistic(bool draw) {
    auto cDutPixelHist = Plot<TH2D>(m_hmaker->MakeDutPixelStatisticHist(),"COLZ",draw);
    if(cDutPixelHist)
        InsertTbJobPlot(std::string("PixelStatistic"),cDutPixelHist);

    auto cDutBinnningHist = Plot<TH2D>(m_hmaker->MakeDutBinningStatisticHist(),"COLZ",draw);
    if(cDutBinnningHist)
        InsertTbJobPlot(std::string("DutBinningStatistic"),cDutBinnningHist);
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobPlotter::Draw(const std::string& name, bool interactive ){
    //_____________________________________________________________________________________
    if(Dut::PixStudy()) {
        auto cDutHistMap = Plot<TH2D>(m_hmaker->MakeDutHistMap(name), "COLZ", interactive);
        if (cDutHistMap) {
            auto cName = static_cast<std::string>(cDutHistMap->GetName());
            std::cout << "[DEBUG]:: TbJobPlotter:: created canvas " << cName << std::endl;
            InsertTbJobPlot(cName, cDutHistMap);
        }
    }

    //_____________________________________________________________________________________
    if(Dut::IntraPixStudy()) {
        if (ThisTbJobPtr()->DutPtr()->IsBinned()) {
            auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
            for (int iBin = 0; iBin < nDutBins; ++iBin) { // this is the loop over the DutBinning
                auto cDutPixHistMap = Plot<TH2D>(m_hmaker->MakeDutPixHistMap(name, iBin), "COLZ", interactive);
                if (cDutPixHistMap) {
                    auto cName = static_cast<std::string>(cDutPixHistMap->GetName());
                    InsertTbJobPlot(cName, cDutPixHistMap);
                }
            }
        } else {
            auto cDutPixHistMap = Plot<TH2D>(m_hmaker->MakeDutPixHistMap(name), "COLZ", interactive);
            if (cDutPixHistMap) {
                auto cName = static_cast<std::string>(cDutPixHistMap->GetName());
                InsertTbJobPlot(cName, cDutPixHistMap);
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobPlotter::DrawProfile(const std::string& name, bool interactive){
    auto mgraph = m_gmaker->MakeDutProfiles(name);
    auto cDutProfiles = Plot<TMultiGraph>(mgraph,"A",interactive);
    if(cDutProfiles) {
        auto cName = static_cast<std::string>(cDutProfiles->GetName());
        auto gName = static_cast<std::string>(mgraph->GetName());
        // check if the legend was built for this plot:
        auto legend = GetTbJobTObject<TLegend>("Legend_"+gName);
        if (legend)
            DrawLegend(cDutProfiles,legend);
        InsertTbJobPlot(cName, cDutProfiles);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobPlotter::DrawProfile(const std::string& name, const std::string& axis, bool interactive){
    auto cDutProfile = Plot<TGraphErrors>(m_gmaker->MakeDutProfile(name,axis),"AP",interactive);
    if(cDutProfile) {
        auto cName = static_cast<std::string>(cDutProfile->GetName());
        InsertTbJobPlot(cName, cDutProfile);
    }
}