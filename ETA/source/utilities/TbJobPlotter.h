//
// Created by brachwal on 30.05.19.
//

#ifndef ITBJOBPLOTS_H
#define ITBJOBPLOTS_H

#include <memory>
#include "TbPlotter.h"
#include "ITbJobResults.h"
#include "TbJobHistMaker.h"
#include "TbJobGraphMaker.h"

using TbJobHistMakerUniquePtr = std::unique_ptr<TbJobHistMaker>;
using TbJobGraphMakerUniquePtr = std::unique_ptr<TbJobGraphMaker>;

class TbJobPlotter : public ITbJobResults,
                     public TbPlotter {
    private:
        ///
        TbJobHistMakerUniquePtr m_hmaker;

        ///
        TbJobGraphMakerUniquePtr m_gmaker;

    public:
        ///
        TbJobPlotter() = delete;

        ///
        TbJobPlotter(TbJobSharedPtr tbjob);

        ///
        ~TbJobPlotter() = default;

        ///
        void DrawDutStatistic(bool draw = false);

        ///
        void Draw(const std::string& name, bool interactive = false) override;

        ///
        void DrawProfile(const std::string& name, bool interactive = false) override;

        ///
        void DrawProfile(const std::string& name, const std::string& axis, bool interactive = false) override;

};

#endif //ITBJOBPLOTS_H
