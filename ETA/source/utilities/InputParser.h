#ifndef InputParser_H
#define InputParser_H

#include <vector>
#include <map>
#include "JobData.h"

///
enum ParserType {
    CUSTOM,
    TOML
};

class InputParser {
	protected:
		/// toml source file
		std::string m_source_file;

		///
		std::map<std::string, std::vector<JobData> > m_analysis_collection;

		///
		std::vector<std::string> m_analysis_names;

		/// This method is supposed to be called from the ctr of the final class
		/// implementation i order to define the container of the JobData objects
		virtual void ParseAnalysisCollections() = 0 ;

	public:
		///
		InputParser(const std::string& filename);

		///
		std::vector <std::string> GetAnalysisCollectionNames() const { return m_analysis_names; }

		///
		std::vector<JobData> GetAnalysisCollection(const std::string& analysis_collection_name) const;

		///
		void PrintAnalysisCollectionDetails(const std::string& analysisCollectioName=std::string()) const;
};

#endif // InputParser_H