/*! \brief TbFitter factory declaration.
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date October-2018
*/

#ifndef TB_FITTER_H
#define TB_FITTER_H

// std libriaryies
#include <stdio.h>
#include <vector>
#include <memory>
#include <functional>
#include <map>
#include <utility>
#include <string>

#include "ITbJobResults.h"

class TH1D;
class RooDataSet;
class RooRealVar;
class TbJobPlotter;
class TbRooDataSet;

class RooAbsPdf;

enum class FitDataType {RooDataSet, TH1};

class TbFitter : public ITbJobResults {

    private:
        ///
        std::string m_data_name = "None";

        ///
        FitDataType m_fit_data_type = FitDataType::RooDataSet;

        ///
        bool m_data_initialized = false;

        /// The histogram data
        std::vector<TH1D*>* m_dut_binning_th1_data = nullptr;

        ///
        TH1D* m_dut_th1_data;

        ///
        std::vector<TbRooDataSet*> m_tbroodataset_collection;

        ///
        static unsigned m_roofit_binning;

        ///
        static unsigned m_nentries_fit_threshold;

        /// \brief Mpping of the simple indexing with the indexes of the data pointers containers
        std::map<unsigned, unsigned> m_good_dataset;

        ///
        bool IsGoodTHist(unsigned bin) const;

        ///
        bool IsGoodTHist() const;

        ///
        void InitializeRooDataSet();

        ///
        void InitializeTH1Data();

    protected:
        ///
        std::string GetFitTitle(RooDataSet* rdsPtr) const;

	public:
        ///
        TbFitter() = delete;

        ///
        TbFitter(TbJobSharedPtr tbJob);

		///
		~TbFitter()=default;

		///
		void SetData(TbJobSharedPtr tbJob, std::string dataName, FitDataType dataType);

		///
		const std::string& DataName() const { return m_data_name; }

        ///
        static unsigned RooFitBinning(unsigned val){ return m_roofit_binning = val; }

        ///
        static unsigned RooFitBinning(){ return m_roofit_binning; }

        ///
        static unsigned NEntriesFitThreshold(unsigned val){ return m_nentries_fit_threshold = val; }

        ///
        static unsigned NEntriesFitThreshold(){ return m_nentries_fit_threshold; }

        ///\brief Return the number of DataSets to be fitted (above the NEntriesFitThreshold())
        size_t NGoodDataSets();

        ///\brief Initialize DataSets and return NGoodDataSets() 
        size_t InitializeData();

        ///
        TH1D* ITHistPtr(unsigned goodDataIdx);

        ///
        TH1D* ITHistPtr() {return m_dut_th1_data;}

        ///
        double GetRunVoltage() const;

        ///
        unsigned DutNBins() const;

        ///
        int GetDutBin(RooDataSet* rds) const;

        ///
        std::pair<int, int> GetPixBin(RooDataSet* rds) const;

        ///
        std::vector<TbRooDataSet*>& GetTbRooDataSetCollection() { return m_tbroodataset_collection; }

        ///
        std::unique_ptr<TH1F> GetTH1FUniquePtr(RooDataSet* rdsPtr, const std::string& cutRange=std::string());

        ///
        RooRealVar* GetRooRealVarPtr(RooDataSet* rdsPtr);

        ///
        void StoreFitResult(RooDataSet* rdsPtr, const std::string& rName, double val, double err);

        ///
        void StoreFitPlot(RooDataSet* rdsPtr, const std::string& dataName, RooAbsPdf* modelPdf
                                                                           ,const std::string& modelName
                                                                           ,const std::string& component1=std::string()
                                                                           ,const std::string& component2=std::string()
                                                                           ,const std::string& component3=std::string());

};
#endif	// TB_FITTER_H
