﻿#include "TomlInputParser.h"
#include "Globals.h"
#include "TbData.h"
#include <iostream>

////////////////////////////////////////////////////////////////////////////////////
///
TomlInputParser::TomlInputParser(const std::string& filename) : InputParser(filename) {
	try	{
		m_parsed_TOML = cpptoml::parse_file(m_source_file);
		ParseAnalysisNames();
		ParseAnalysisCollections();
	}
	catch (const cpptoml::parse_exception& e){
		std::cerr << "Failed to parse " << m_source_file << ": " << e.what() << std::endl;
	}
}

////////////////////////////////////////////////////////////////////////////////////
///
size_t TomlInputParser::GetParamTableSize(const std::shared_ptr<cpptoml::table>& table){
	size_t size(0);
	for (const auto& param : *table) ++size;
	return size;
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TomlInputParser::CheckObligatoryValues(const std::shared_ptr<cpptoml::table>& job_properties_table, std::vector<std::string>& missing_parameters) const {
	for(auto obligatory_parameter : JobData::obligatory_parameters) {
		if (obligatory_parameter.second == typeid(std::string))	{
			const auto& obligatory_parameter_value = job_properties_table->get_qualified_as<std::string>(obligatory_parameter.first);
			if (!obligatory_parameter_value) {
				missing_parameters.push_back(obligatory_parameter.first);
			}
		}
		else if (obligatory_parameter.second == typeid(double))	{
			const auto& obligatory_parameter_value = job_properties_table->get_qualified_as<double>(obligatory_parameter.first);
			if (!obligatory_parameter_value) {
				missing_parameters.push_back(obligatory_parameter.first);
			}
		}
		else if (obligatory_parameter.second == typeid(unsigned)) {
			const auto& obligatory_parameter_value = job_properties_table->get_qualified_as<unsigned>(obligatory_parameter.first);
			if (!obligatory_parameter_value) {
				missing_parameters.push_back(obligatory_parameter.first);
			}
		}
		else if (obligatory_parameter.second == typeid(bool)) {
			const auto& obligatory_parameter_value = job_properties_table->get_qualified_as<bool>(obligatory_parameter.first);
			if (!obligatory_parameter_value) {
				missing_parameters.push_back(obligatory_parameter.first);
			}
		}
		else {
			std::cout << "Unrecognised parameter type " << obligatory_parameter.second.name() << " in " << obligatory_parameter.first << std::endl;
		}
	}
	if (missing_parameters.size() == 0) {
		return true;
	}
	else {
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////////
///
void TomlInputParser::ParseAnalysisNames() {
	for (const auto& analysis_pair : *m_parsed_TOML) {
		m_analysis_names.push_back(analysis_pair.first);
	}
}

////////////////////////////////////////////////////////////////////////////////////
///
void TomlInputParser::ParseJobProperties(JobData& job, const std::shared_ptr<cpptoml::table> job_properties_table) {
	for (const auto& property_pair_value : *job_properties_table){
		auto param = property_pair_value.first;
		auto value = property_pair_value.second;
		if (typeid(*value) == typeid(cpptoml::value<std::string>)) {
			if (param != "DataPath" && param != "DataFile") {
				auto qvalue = job_properties_table->get_qualified_as<std::string>(param);
				job.SetValue(param,*qvalue);
				job.SetValueAsString(param,*qvalue);
			}
		}
		else if (typeid(*value) == typeid(cpptoml::value<std::int64_t>)){
			auto qvalue = job_properties_table->get_qualified_as<unsigned>(param);
			job.SetValue(param, *qvalue);
			job.SetValueAsString(param,std::to_string(*qvalue));
		}
		else if (typeid(*value) == typeid(cpptoml::value<double>)) {
			auto qvalue = job_properties_table->get_qualified_as<double>(param);
			job.SetValue(param, *qvalue);
				job.SetValueAsString(param,dyn_dtos(*qvalue,2));
		}
		else if (typeid(*value) == typeid(cpptoml::value<bool>)) {
			auto qvalue = job_properties_table->get_qualified_as<bool>(param);
			job.SetValue(param, *qvalue);
			job.SetValueAsString(param,(*qvalue)?"true":"false");
		}
		else {
			std::cout << "Bad type of object " << param << std::endl;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string TomlInputParser::FindDataPathPattern(std::shared_ptr<cpptoml::table_array> jobs_tables) {
	// NOTE: This paramater is expected to be as single entry in the table
	for (const auto& job_properties_table : *jobs_tables) {
		if(GetParamTableSize(job_properties_table)!=1)
			continue; 
		auto new_data_path = job_properties_table->get_qualified_as<std::string>("DataPath");
		if(new_data_path)
			return *new_data_path;
	}
	return std::string();
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string TomlInputParser::FindDataFilePattern(std::shared_ptr<cpptoml::table_array> jobs_tables) {
	// NOTE: This paramater is expected to be as single entry in the table
	for (const auto& job_properties_table : *jobs_tables) {
		if(GetParamTableSize(job_properties_table)!=1)
			continue;
		auto new_file_name_pattern = job_properties_table->get_qualified_as<std::string>("DataFileNamePattern");
		if(new_file_name_pattern)
			return *new_file_name_pattern;
	}
	return std::string();
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string TomlInputParser::FindDataTTreePattern(std::shared_ptr<cpptoml::table_array> jobs_tables) {
	// NOTE: This paramater is expected to be as single entry in the table
	for (const auto& job_properties_table : *jobs_tables) {
		if(GetParamTableSize(job_properties_table)!=1)
			continue;
		auto new_file_name_pattern = job_properties_table->get_qualified_as<std::string>("DataTTree");
		if(new_file_name_pattern)
			return *new_file_name_pattern;
	}
	return std::string();
}

////////////////////////////////////////////////////////////////////////////////////
///
void TomlInputParser::PrintMissingParameters(const std::string analysis_name, std::vector<std::string>& missing_parameters){
	std::string missing_parameters_information("Lack of parameters in one of jobs: ");
	for(auto missing_parameter : missing_parameters){
		missing_parameters_information += missing_parameter + " ";
	}
	missing_parameters_information += "in " + analysis_name;
	std::cout << missing_parameters_information << std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TomlInputParser::ParseAnalysisCollections(){
	for (auto analysis_name : m_analysis_names)	{
		std::vector<JobData> jobs;
		auto analysis_table = m_parsed_TOML->get_table_array(analysis_name);

		std::string global_data_path_in_analysis = FindDataPathPattern(analysis_table);
		//if(!global_data_path_in_analysis.empty())
		//	std::cout << "DEBUG  Got global data path " << global_data_path_in_analysis << std::endl;

		std::string global_data_file_pattern_in_analysis = FindDataFilePattern(analysis_table);
		//if(!global_data_file_pattern_in_analysis.empty())
		//	std::cout << "DEBUG  Got global file pattern " << global_data_file_pattern_in_analysis << std::endl;

		std::string global_data_ttree_pattern_in_analysis = FindDataTTreePattern(analysis_table);
		//if(!global_data_ttree_pattern_in_analysis.empty())
		//	std::cout << "DEBUG  Got global ttree pattern " << global_data_ttree_pattern_in_analysis << std::endl;

		for (const auto& job_table : *analysis_table) {
			std::vector<std::string> missing_parameters;
			if (CheckObligatoryValues(job_table, missing_parameters)) {
				auto job = JobData();
				auto new_data_path = job_table->get_qualified_as<std::string>("FilePath");
				if (new_data_path)
					job.SetDataPath(*new_data_path);
				else 
					job.SetDataPath(global_data_path_in_analysis);

				auto new_data_file = job_table->get_qualified_as<std::string>("FileName");
				if (new_data_file)
					job.SetDataFile(*new_data_file);
				else 
					job.SetDataFilePattern(global_data_file_pattern_in_analysis);

				auto new_data_ttree = job_table->get_qualified_as<std::string>("TTree");
				if (new_data_ttree)
					job.SetDataTTree(*new_data_ttree);
				else 
					job.SetDataTTree(global_data_ttree_pattern_in_analysis);

				ParseJobProperties(job, job_table);

				if (job.GetDataPath().empty())
					std::cout << "Missing data path in one of jobs in " << analysis_name << std::endl;
				else if(job.GetDataFile().empty())
					std::cout << "Missing data file in one of jobs in " << analysis_name << std::endl;
				else if(job.GetDataTTree().empty() && ( TbData::Type()==DataType::ROOT || TbData::Type()==DataType::RDF) )
					std::cout << "Missing data ttree in one of jobs in " << analysis_name << std::endl;
				else {
					jobs.push_back(job);
				}
			}
			else if ( ((*job_table).contains("DataPath")
					  || (*job_table).contains("DataFileNamePattern") 
					  || (*job_table).contains("DataTTree") ) &&
				missing_parameters.size() == JobData::obligatory_parameters.size() )
					continue;
			else
				PrintMissingParameters(analysis_name, missing_parameters);
		}
		m_analysis_collection.insert(std::pair<std::string, std::vector<JobData>>(analysis_name, jobs));
	}
}
