#ifndef FP_FluenceProfile_H
#define FP_FluenceProfile_H

// std libriaryies
#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <array>

// my libriaryies
#include "TbGaudi.h"
#include "Globals.h"

// ROOT libriaryies
#include "TPad.h"

////////////////////////////////////////////////////////////////////////////////////
///
class DutBinning;
using DutBinningSharedPtr = std::shared_ptr<DutBinning>;

class FluenceProfile {

	protected:
	/// \brief The DutBinning pointer the given FluenceProfile instantiation is related to.
		std::shared_ptr<DutBinning> m_dut_binning = nullptr;

		/// \brief The name of the fluence profile instantiation
		std::string m_type = "NOT SPECIFIED";

		/// \brief The value of the total fluence associated to the given DUT
		double m_dosimetry_fluence = 0.;

		/// The value of the total fluence associated to the given profile instantiation
		double m_evaluated_fluence = 0;

		///
		std::vector<double> m_binned_fluence;

		///
		bool m_isDefault = false;

		///
		bool m_isInitialized = false;

		///
		static bool m_evaluate;

		///
		static bool m_export_to_csv;

		///
		static std::string m_default_type;

		/// Template of fln profiles of given type (e.g. "IRRAD")
		static std::vector<std::unique_ptr<FluenceProfile>> m_fln_profiles;

		/// Fln profile instatiated for given DUT parameterization
		static std::map<std::string,std::shared_ptr<FluenceProfile>> m_instatiated_fln_profiles;

		///
		static FluenceProfile* AddProfile(const std::string& flnType);

	public:
		///
		FluenceProfile() = delete;

		/// \brief Constructor
		FluenceProfile(const std::string& type);

		/// \brief Destructor
		virtual ~FluenceProfile() = default;

		///
		virtual void Initialize()=0;

		///
		static FluenceProfile* AddCustomProfile(FluenceProfile* customFlnPtr);

		///
		static FluenceProfile* GetProfile(const std::string& flnType);

		///
		void SetDutBinning(DutBinningSharedPtr ptr);

		/// \brief Info whether the default parameterization was overridden
		bool IsDefault() const { return m_isDefault; };

		/// \brief Get the total fln evaluated for the given DUT profile
		double GetEvaluatedFluence() const { return m_evaluated_fluence; }

		///
		std::string LinkedDutName() const;

		///
		std::shared_ptr<DutBinning> DutBinningPtr() const {return m_dut_binning; }

		///
		static bool PerformEvaluation(){return m_evaluate;}

		///
		static void PerformEvaluation(bool val){m_evaluate=val;}

		///
		static bool ExportToCsv() {return m_export_to_csv; }

		///
		static void ExportToCsv(bool val){m_export_to_csv=val;}

		///
		static void DefaultType(const std::string& val){m_default_type = val;}

		///
		static std::string DefaultType(){return m_default_type;}

		///
		const std::string& Type() const {return m_type;}

		///
		virtual std::shared_ptr<FluenceProfile> Instantiate(DutBinningSharedPtr) = 0;

		///
		virtual void Evaluate() = 0;

		///
		virtual FluenceProfile* ClonePtr() = 0;

		///
		virtual void DefineProfileFunction(){}

		///
		virtual void SetProfileAlignment(){}

		///
		virtual void ExportFluenceBinningToCsv(){}

		// ______________________________________________
		// Dosimetry measurement results interface:

		/// \brief Get the total value of dosimetry measurement
		double GetDosimetryFluence() const {return m_dosimetry_fluence;}

		/// \brief Get the fluence value associated to the given dosimetry bin number
		virtual double GetDosimetryFluence(int) = 0;

		/// \brief Get the fluence value associated to the dosimetry bin number mapped with the given pixel
		virtual double GetDosimetryFluence(int, int) = 0;

		/// \brief Get the value of the dosimetry bin area for the given bin number
		virtual double GetDosimetryBinArea(int) = 0;

		/// \brief Get the dosimetry bin number mapped to the given DUT pixel
		virtual int GetDosimetryBin(int, int) = 0;

		/// \brief Draw the dosimetry measurement map.
		virtual void DrawDosimetryMeasurement() = 0;

		// ______________________________________________
		// The actual fluence profile interface:

		/// \brief Get the fluence bin number corresponding to the given fluence level
		virtual int GetBin(double) = 0;

		/// \brief Get the fluence bin number corresponding to the given DUT pixel
		virtual int GetBin(int, int) = 0;

		/// \brief Get the fluence value from the given fluence bin number
		virtual double GetFluence(int) = 0;

		/// \brief Get the fluence value associated to the fluence bin mapped with the given pixel
		virtual double GetFluence(int, int) const = 0;

		/// \brief Get the fluence value associated to the given DUT pixel
		virtual double GetPixFluence(int Lx, int Ly) = 0;

		virtual double GetFluence(double x, double y) const = 0;

		/// \brief Get min fluence value and coordinates of associated pixel
		virtual std::tuple<double, double, double> GetMinPixel() const = 0;

		/// \brief Get max fluence value and coordinates of associated pixel
		virtual std::tuple<double, double, double> GetMaxPixel() const = 0;

		/// \brief Draw the fluence profile binning
		virtual void DrawBinning(TPad*  pad, std::string opts) = 0;
};
#endif // FP_FluenceProfile_H