#include <pybind11/pybind11.h>
#include "TbPlotter.h"

namespace py = pybind11;

void PyTbPlotter(py::module &m) {
    py::class_<TbPlotter>(m, "TbPlotter")
    .def(py::init<>());
}