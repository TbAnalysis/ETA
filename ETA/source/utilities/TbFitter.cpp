#include "TbFitter.h"
#include "TbJob.h"
#include "Globals.h"
#include "TbJobPlotter.h"
#include "TbRooDataSetCollection.h"

// std libriaryies
#include <iostream>

// ROOT libriaryies
#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"

// RooFIT libriaryies
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooFFTConvPdf.h"
#include "RooPlot.h"

unsigned TbFitter::m_roofit_binning = 50000;
unsigned TbFitter::m_nentries_fit_threshold = 20;

using namespace RooFit;

////////////////////////////////////////////////////////////////////////////////////
//
TbFitter::TbFitter(TbJobSharedPtr tbJob):ITbJobResults(tbJob){}

////////////////////////////////////////////////////////////////////////////////////
//
void TbFitter::SetData(TbJobSharedPtr tbJob, std::string dataName, FitDataType dataType){
    m_data_name = dataName;
    m_fit_data_type = dataType;
}

////////////////////////////////////////////////////////////////////////////////////
//
size_t TbFitter::InitializeData() {
    std::cout<<"[INFO]:: TbFitter:: *** FITTING DATA INITIALIZATION *** "<<std::endl;
    switch (m_fit_data_type) {
        case FitDataType::RooDataSet:
            InitializeRooDataSet();
            break;
        case FitDataType::TH1:
            InitializeTH1Data();
            break;
    }
    m_data_initialized = true;
    return NGoodDataSets();
}

////////////////////////////////////////////////////////////////////////////////////
//
void TbFitter::InitializeRooDataSet(){
    auto nEntriesThr = TbFitter::NEntriesFitThreshold();
    // Get ptrs to all RooDataSets that include the required variable
    // Note: the data are supposed to related to single variable, 
    //       hence all can be kept in a container of ptr to TbRooDataSet

    auto dutDataPtr = ThisTbJobPtr()->GetDutTbRooDataSetPtr(m_data_name);
    if(dutDataPtr){
        if(dutDataPtr->GetRooDataSetPtr()->sumEntries() > nEntriesThr )
            m_tbroodataset_collection.push_back(dutDataPtr);
    }
    
    auto dutDataCollectionPtr = ThisTbJobPtr()->GetTbRooDataSetCollectionPtr(m_data_name);
    if(dutDataCollectionPtr){
        auto size = dutDataCollectionPtr->Size();
        for(unsigned i=0; i<size;++i){
            auto itbdutDataPtr = dutDataCollectionPtr->At(i);
            if(itbdutDataPtr->GetRooDataSetPtr()->sumEntries() > nEntriesThr)
                m_tbroodataset_collection.push_back(itbdutDataPtr);
        }
    }

    std::cout<<"[INFO]:: TbFitter:: Got " << m_tbroodataset_collection.size() <<" RooDataSets" <<std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
//
void TbFitter::InitializeTH1Data(){
    if(!ThisTbJobPtr()->DutPtr()->IsBinned()) { // == DutNBins() == 1
        m_dut_th1_data = ThisTbJobPtr()->DutHistPtr(m_data_name);
        return;
    }
    if(m_good_dataset.empty())
        m_dut_binning_th1_data = ThisTbJobPtr()->DutBinningTH1VectorPtr(m_data_name);
    else {
        TbGaudi::PrintBanner("INFO", "TbFitter:: NEntriesFitThreshold has been changed, perform new mapping...");
        m_good_dataset.clear();
    }
    if (m_dut_binning_th1_data && !m_dut_binning_th1_data->empty()) {
        unsigned good_data_idx(0);
        for (int i = 0; i < m_dut_binning_th1_data->size(); ++i){
            if(IsGoodTHist(i)){
                m_good_dataset[good_data_idx++] = i;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
//
size_t TbFitter::NGoodDataSets() {
    if(!m_data_initialized)
        InitializeData();

    unsigned n(0);

    switch (m_fit_data_type) {
        ///______________________________________________________________________
        
        case FitDataType::TH1:
            if (m_dut_binning_th1_data && !m_dut_binning_th1_data->empty()) {
                for (int i = 0; i < m_dut_binning_th1_data->size(); ++i) {
                    if (IsGoodTHist(i)) ++n;
                }
                return n;
            } else if (m_dut_th1_data) {
                if (IsGoodTHist()) return 1;
            }
            break;
        ///______________________________________________________________________
        case FitDataType::RooDataSet:
            return m_tbroodataset_collection.size();
        ///______________________________________________________________________
        default:
            return 0;
    }
}


////////////////////////////////////////////////////////////////////////////////////
//
bool TbFitter::IsGoodTHist(unsigned bin) const {
    if(m_dut_binning_th1_data && m_dut_binning_th1_data->size()>bin) {
        if (m_dut_binning_th1_data->at(bin)->GetEntries() > TbFitter::NEntriesFitThreshold())
            return true;
        else
            return false;
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////////
//
bool TbFitter::IsGoodTHist() const {
    if(m_dut_th1_data) {
        if (m_dut_th1_data->GetEntries() > TbFitter::NEntriesFitThreshold())
            return true;
        else
            return false;
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////////
//
TH1D* TbFitter::ITHistPtr(unsigned goodDataIdx) {
    if(NGoodDataSets()!=m_good_dataset.size())  //  mapping was done for different threshold
        InitializeTH1Data();                    // initialize again...
    if(m_dut_binning_th1_data && m_good_dataset.size()>goodDataIdx)
        return m_dut_binning_th1_data->at(m_good_dataset.at(goodDataIdx));
    return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
//
double TbFitter::GetRunVoltage() const {
    return ThisTbJobPtr()->GetParam<double>("RunBiasVoltage");
}

////////////////////////////////////////////////////////////////////////////////////
//
unsigned TbFitter::DutNBins() const {
    return ThisTbJobPtr()->GetParam<unsigned>("DutNBins");
}

////////////////////////////////////////////////////////////////////////////////////
//
int TbFitter::GetDutBin(RooDataSet* rds) const {
    int bin(-1);
    if(rds==nullptr) 
        return bin;
    if(!ThisTbJobPtr()->DutPtr()->IsBinned())
        return bin;
    auto rdsName = static_cast<std::string>(rds->GetName());
    auto search = rdsName.find("DutBin"); // Such a label is added when the per bin objects are being created
    if(search != std::string::npos) // This RDS has been created based on the DUT binning scheme
        bin = std::stoi(rdsName.substr(search+6,rdsName.length()));
    //std::cout << "[DEBUG]:: TbFitter::DutBin: " << bin << std::endl;
    return bin;
}

////////////////////////////////////////////////////////////////////////////////////
//
std::pair<int, int> TbFitter::GetPixBin(RooDataSet* rds) const {
    // TODO: implement me.
    return std::make_pair<int, int>(-1,-1);
}

////////////////////////////////////////////////////////////////////////////////////
//
std::unique_ptr<TH1F> TbFitter::GetTH1FUniquePtr(RooDataSet* rdsPtr,const std::string& cutRange){
    auto nBins = TbFitter::RooFitBinning();
    auto ras = const_cast<RooArgSet*>(rdsPtr->get());
    auto rrv = dynamic_cast<RooRealVar*>(ras->find(m_data_name.c_str()));
    std::string histName = cutRange.empty() ? "TH1F" : "TH1F_"+cutRange;
    if(cutRange.empty())
        return std::unique_ptr<TH1F>(static_cast<TH1F*>(rdsPtr->createHistogram(histName.c_str(),
                                                                            *rrv,
                                                                            Binning(nBins))));
    else
        return std::unique_ptr<TH1F>(static_cast<TH1F*>(rdsPtr->createHistogram(histName.c_str(),
                                                                            *rrv,
                                                                            Binning(nBins),
                                                                            CutRange(cutRange.c_str()))));
}

////////////////////////////////////////////////////////////////////////////////////
//
RooRealVar* TbFitter::GetRooRealVarPtr(RooDataSet* rdsPtr){
    auto ras = const_cast<RooArgSet*>(rdsPtr->get());
    return dynamic_cast<RooRealVar*>(ras->find(m_data_name.c_str()));
}

////////////////////////////////////////////////////////////////////////////////////
//
void TbFitter::StoreFitResult(RooDataSet* rdsPtr, const std::string& rName, double val, double err){
    auto dutBin = GetDutBin(rdsPtr);
    if(dutBin<0){ // this rds is not from DUT binning scheme
        if(Dut::IntraPixStudy()){ // this rds is related to binned pixel study
            auto pixBin = GetPixBin(rdsPtr);
            InsertTbJobResult(rName, pixBin, val, err);
            return;
        }
        InsertTbJobResult(rName,val,err); // the most general case.
        return;
    }
    auto udutBin = static_cast<unsigned>(dutBin);
    if(Dut::PixStudy()){
        InsertTbJobResult(rName, udutBin, val, err);
        return;
    }

    if(Dut::IntraPixStudy()){
        auto pixBin = GetPixBin(rdsPtr);
        InsertTbJobResult(rName, udutBin, pixBin, val, err);
    }
}

////////////////////////////////////////////////////////////////////////////////////
//
std::string TbFitter::GetFitTitle(RooDataSet* rdsPtr) const {
    auto dutBin = GetDutBin(rdsPtr);
    std::string binPostfix;
    if(dutBin<0){ // Only pixels are being binned (not DUT)
        if(Dut::IntraPixStudy()){
            auto pixel = GetPixBin(rdsPtr);
            binPostfix = "_PixBin_"+itos(pixel.first)+"_"+itos(pixel.second);
        }
    } else { // DUT is being binned
        binPostfix = "_DutBin_"+itos(dutBin);
        if(Dut::IntraPixStudy()){
            auto pixel = GetPixBin(rdsPtr);
            binPostfix += "_PixBin_"+itos(pixel.first)+"_"+itos(pixel.second);
        }
    }
    return ThisTbJobPtr()->Label("JOB")+binPostfix;
}

////////////////////////////////////////////////////////////////////////////////////
//
void TbFitter::StoreFitPlot(RooDataSet* rdsPtr, const std::string& dataName, RooAbsPdf* modelPdf
                                                                 ,const std::string& modelName
                                                                 ,const std::string& component1
                                                                 ,const std::string& component2
                                                                 ,const std::string& component3){
    auto fitTitle = GetFitTitle(rdsPtr);
    auto can = std::make_shared<TCanvas>(TString("Canvas_"+fitTitle),TString(fitTitle),660,600);
    can->cd();
    auto rrv = GetRooRealVarPtr(rdsPtr);
    auto frame = rrv->frame(Binning(TbFitter::RooFitBinning()),Title(fitTitle.c_str()));
    rdsPtr->plotOn(frame, MarkerStyle(8), MarkerSize(0.8), Name(dataName.c_str()));
    modelPdf->plotOn(frame, LineColor(kRed), LineWidth(3), Name(modelName.c_str()) );

    if(!component1.empty())
        modelPdf->plotOn(frame, LineColor(kBlue), LineWidth(3), LineStyle(2), Components(component1.c_str()) );
    if(!component2.empty())
        modelPdf->plotOn(frame, LineColor(kAzure), LineWidth(3), LineStyle(2), Components(component2.c_str()) );
    if(!component3.empty())
        modelPdf->plotOn(frame, LineColor(kOrange), LineWidth(3), LineStyle(2), Components(component3.c_str()) );

    modelPdf->paramOn(frame);
    frame->SetTitleSize(0.03,"X");
    frame->SetTitleSize(0.03,"Y");
    frame->SetLabelSize(0.025,"X");
    frame->SetLabelSize(0.025,"Y");
    frame->getAttText()->SetTextSize(0.022);
    frame->Draw();
    InsertTbJobPlot(fitTitle,can);
}