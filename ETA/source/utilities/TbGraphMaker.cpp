//
// Created by brachwal on 04.02.19.
//

#include "TbGraphMaker.h"
#include "DutBinning.h"
#include "DutStripBinning.h"
#include <cstring>

////////////////////////////////////////////////////////////////////////////////////
///
TGraphSharedPtr TbGraphMaker::DefineTGraphErrors(const std::string& name, const std::string& title,
                                               VDouble& x, VDouble& xErr,
                                               VDouble& y, VDouble& yErr){
    auto g = std::make_shared<TGraphErrors>(x.size(),&x[0],&y[0],&xErr[0],&yErr[0]);
    g->SetName(TString(name));
    g->SetTitle(TString(title));
    g->SetMarkerStyle(20);
    return g;
}

////////////////////////////////////////////////////////////////////////////////////
///
TMGraphSharedPtr TbGraphMaker::DefineTMultiGraph(const std::string& name, const std::string& title){
    return std::make_shared<TMultiGraph>(TString(name),TString(title));
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbGraphMaker::AdjustTMultiGraph(TMGraphSharedPtr mgrah, double min, double max){
    // TODO Implement me.
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string TbGraphMaker::BuildTObjName(const std::string& resultsName, const std::string& objType){
    // TODO Implement me.
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string TbGraphMaker::BuildTObjTitle(const std::string& resultsName, const std::string& objType, const std::string& xAxis, const std::string& yAxis){
    // TODO Implement me.
}

////////////////////////////////////////////////////////////////////////////////////
///
TLegendSharedPtr TbGraphMaker::BuildTMGraphLegend(TMGraphSharedPtr mgraph){

    auto legend = std::make_shared<TLegend>();
    auto mgName = TString(mgraph->GetName());
    auto mgTitle = mgraph->GetTitle();
    legend->SetName("Legend_"+mgName);
    auto mgTitle_non_const = std::make_unique<char[]>(std::strlen(mgTitle)+1); // +1 for the null terminator
    std::strcpy(mgTitle_non_const.get(), mgTitle);
    const char* legHeaderText = std::strtok(mgTitle_non_const.get(),";");       // take this string until first ';'
    legend->SetHeader(legHeaderText,"C"); // option "C" allows to center the header
    auto graphsList = mgraph->GetListOfGraphs();
    for(unsigned igr=0; igr<graphsList->GetEntries();++igr){
        auto igraph = static_cast<TGraphErrors*>(graphsList->At(igr));
        auto gTitle = igraph->GetTitle();
        auto gTitle_non_const = std::make_unique<char[]>(std::strlen(gTitle)+1); // +1 for the null terminator
        std::strcpy(gTitle_non_const.get(), gTitle);
        const char* legEntryText = std::strtok(gTitle_non_const.get(),"(");       // take this string until first ';'
        legend->AddEntry(igraph,TString(legEntryText),"p");
    }
    return legend;
}

////////////////////////////////////////////////////////////////////////////////////
///
TLegendSharedPtr TbGraphMaker::BuildTGraphLegend(TGraphSharedPtr graph){

}