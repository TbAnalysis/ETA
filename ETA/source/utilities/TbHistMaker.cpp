//
// Created by brachwal on 04.02.19.
//

#include "TbHistMaker.h"
#include "TbJob.h"
#include "TbResults.h"
#include "DutBinning.h"
#include "DutStripBinning.h"
#include "TbGaudi.h"
#include "TH2D.h"

////////////////////////////////////////////////////////////////////////////////////
///
TH2DSharedPtr TbHistMaker::DefineTH2D(const std::string& name, unsigned nX, double xLow, double xUp, unsigned nY, double yLow, double yUp) {
    auto hist = std::make_shared<TH2D>(TString(name),TString(name),nX,xLow,xUp,nY,yLow,yUp);
    hist->SetDirectory(0);
    return hist;
}
