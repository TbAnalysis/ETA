//
// Created by brachwal on 01.07.19.
//

#ifndef TBJOBGRAPHMAKER_H
#define TBJOBGRAPHMAKER_H

#include "ITbJobResults.h"
#include "TbGraphMaker.h"

class TbJobGraphMaker : public TbGraphMaker,
                        public ITbJobResults {
private:
    ///
    std::string BuildTObjName(const std::string& resultsName, const std::string& objType) override;

    ///
    std::string BuildTObjTitle(const std::string& resultsName, const std::string& objType, const std::string& xAxis, const std::string& yAxis) override;


public:
    ///
    TbJobGraphMaker() = delete;

    ///
    TbJobGraphMaker(TbJobSharedPtr tbjob) : ITbJobResults(tbjob) {};

    ///
    ~TbJobGraphMaker() = default;

    ///
    TMGraphSharedPtr MakeDutProfiles(const std::string& resultsName);

    ///
    TGraphSharedPtr MakeDutProfile(const std::string& resultsName, const std::string& axis);

    ///
    void AdjustTMultiGraph(TMGraphSharedPtr mgrah, double min, double max) override;
};

#endif //TBJOBGRAPHMAKER_H
