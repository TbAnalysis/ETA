//
// Created by brachwal on 04.02.19.
//

#ifndef TBGAUDI_TBHISTMAKER_H
#define TBGAUDI_TBHISTMAKER_H

#include <memory>
#include "ITbResults.h"


class TH2D;

using TH2DSharedPtr  = std::shared_ptr<TH2D>;

class TbHistMaker : public ITbResults {

    public:

        ///
        TbHistMaker() = default;

        ///
        virtual ~TbHistMaker() = default;

        ///
        virtual TH2DSharedPtr DefineTH2D(const std::string& name, unsigned nX, double xLow, double xUp, unsigned nY, double yLow, double yUp);

};

#endif //TBLHCBANA_TBHISTMAKER_H
