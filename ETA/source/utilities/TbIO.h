//
// Created by brachwal on 05.07.19.
//

#ifndef TBIO_H
#define TBIO_H

#include "ITbResults.h"
#include <filesystem>
#include "memory.h"
#include <string>

class TbIO : public ITbResults {
    private:
        ///
        static std::string m_output_location;

        ///
        const std::string CreateCurrentDateDirIfNotExits(const std::string& path) const;

        ///
        std::string m_csv_separator = ";";

    public:
        ///
        TbIO() = default;

        ///
        ~TbIO() = default;

        ///
        std::string CreateOutputDir(const std::string& userPath=std::string()) const;

        ///
        void DeleteFileIfExists(const std::string& file_full_path) const;

        ///
        std::string GetOutputDirPath();

        ///
        static std::string OutputLocation() {return m_output_location;}

        ///
        static void OutputLocation(const std::string& path) { m_output_location=path;}

        ///
        virtual void ExportResultsToPdfFile(const std::string& dir_path);

        ///
        virtual void ExportResultsToNTupleFile(const std::string& dir_path);

        ///
        virtual void ExportResultsToCsv(const std::string& dir_path=std::string());

        ///
        std::string GetCsvSeparator() { return m_csv_separator; }

        /// Static function exporting all results from the job collection
        static void ExportAllResults(const std::string& dir_path);

        ///Exporting current instance result - this function should be overvritten in derived classes
        virtual void ExportResults(const std::string& dir_path);
};
#endif //TBIO_H
