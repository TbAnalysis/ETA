#ifndef FP_IRRADFluenceProfile_H
#define FP_IRRADFluenceProfile_H

// std libriaryies
#include <cmath>
#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
#include <array>
#include <memory>

// my libriaryies
#include "TbGaudi.h"
#include "Globals.h"
#include "FluenceProfile.h"
#include "DutEllipticBinning.h"
#include "DutUniformBinning.h"

#include "TF2.h"

/// \brief The IRRAD profile class with the default parameterization.
/// The given analysis specific class should inherit from this profile,
/// and detailed information about the shape and alignment can be specified.
class IRRADProfile: public FluenceProfile {

	protected:

		///
		double m_dosimetry_mean_col;

		///
		double m_dosimetry_mean_row;

		///
		double m_dosimetry_sigma_col;

		///
		double m_dosimetry_sigma_row;

		///
		double m_mean_col;

		///
		double m_mean_row;

		///
		double m_theta;

		///
		TF2 m_profile_func;

		///
		void DefineProfileFunction() override;

	public:

		/// \brief Default constructor
		IRRADProfile();

		/// \brief Default destructor
		virtual ~IRRADProfile() = default;

		/// \brief Perform profile parameterization
		virtual void Initialize() override;

		///
		virtual std::shared_ptr<FluenceProfile> Instantiate(DutBinningSharedPtr ptr) override;

		///
		virtual FluenceProfile* ClonePtr() override { return new IRRADProfile(*this); }

		///
		void Evaluate() override;

		///
		void ExportFluenceBinningToCsv() override;

		TF2& GetProfileFunc() { return m_profile_func; }


		// ______________________________________________
		// Dosimetry measurement results interface:

		/// \biref Get the fluence value associated to the given dosimetry bin number.
		/// Virtual base class implementation.
		double GetDosimetryFluence(int binNumber) override;

		/// \biref Get the fluence value associated to the dosimetry bin number mapped with the given pixel
		/// Virtual base class implementation.
		double GetDosimetryFluence(int col, int row) override;

		/// \biref Get the value of the dosimetry bin area for the given bin number
		/// Virtual base class implementation.
		double GetDosimetryBinArea(int binNumber) override;

		/// \biref Get the dosimetry bin number mapped to the given DUT pixel
		/// Virtual base class implementation.
		int GetDosimetryBin(int col, int row) override;

		/// \brief Draw the dosimetry measurement map.
		/// Virtual base class implementation.
		void DrawDosimetryMeasurement() override;

		// ______________________________________________
		// The actual fluence profile interface:

		///
		inline double GetSx() {return m_mean_col;}

		///
		inline double GetSy() {return m_mean_row;}

		///
		inline double GetWidthFactor() {return m_dosimetry_sigma_col/m_dosimetry_sigma_row;}

		/// \brief Get the fluence bin number corresponding to the given fluence level
		/// Virtual base class implementation.
		int GetBin(double);

		/// \brief Get the fluence bin number corresponding to the given DUT pixel
		/// Virtual base class implementation.
		int GetBin(int, int);

		/// \brief Get the fluence value from the given fluence bin number
		/// Virtual base class implementation.
		double GetFluence(int);

		/// \brief Get the fluence value associated to the fluence bin mapped with the given pixel
		/// Virtual base class implementation.
		double GetFluence(int, int) const;

		/// \brief Get the fluence value associated to the given DUT pixel
		/// Virtual base class implementation.
		double GetPixFluence(int Lx, int Ly);

		double GetFluence(double x, double y) const;

		/// \brief Draw the fluence profile binning
		/// Virtual base class implementation.
		void DrawBinning(TPad*  pad, std::string opts);

		double GetXFromFluence(double fluence_value);
		double GetYFromFluence(double fluence_value);

		/// \brief Get min fluence value and coordinates of associated pixel
		/// Virtual base class implementation.
		virtual std::tuple<double, double, double> GetMinPixel() const;

		/// \brief Get max fluence value and coordinates of associated pixel
		/// Virtual base class implementation.
		virtual std::tuple<double, double, double> GetMaxPixel() const;

};

#endif // FP_IRRADFluenceProfile_H