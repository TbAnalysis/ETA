//
// Created by brachwal on 31.05.19.
//
#include "TbPlotter.h"
#include "TbJob.h"
#include "TbJobPlotter.h"
#include "TbJobCollector.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbPlotter::Draw(const std::string& name, bool interactive){
    for (auto& iJob : TbJobCollector::GetInstance()->TbJobs() )
        iJob->PlotterPtr()->Draw(name,interactive);
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbPlotter::DrawProfile(const std::string& name, bool interactive) {
    for (auto& iJob : TbJobCollector::GetInstance()->TbJobs() )
        iJob->PlotterPtr()->DrawProfile(name,interactive);
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbPlotter::DrawProfile(const std::string& name, const std::string& axis, bool interactive) {
    for (auto& iJob : TbJobCollector::GetInstance()->TbJobs() )
        iJob->PlotterPtr()->DrawProfile(name,axis,interactive);
}
////////////////////////////////////////////////////////////////////////////////////
///
void TbPlotter::DrawProfileCombined(const std::string& name, const std::string& axis, bool interactive){
    // TODO loop and draw for all jobs together in single plot
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbPlotter::Draw(const std::string& xObservable, const std::string& yObservable, bool interactive){
    // TODO loop and draw for all jobs separately
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbPlotter::DrawCombined(const std::string& xObservable, const std::string& yObservable, bool interactive){
    // TODO loop and draw for all jobs together in single plot
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbPlotter::DrawLegend(TCanvasSharedPtr canvas, TLegendSharedPtr legend){
    canvas->cd();
    legend->SetX1(0.6);
    legend->SetX2(0.9);
    legend->SetY1(0.6);
    legend->SetY2(0.9);
    legend->Draw();
}