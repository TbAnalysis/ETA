//
// Created by brachwal on 31.05.19.
//

#ifndef ITBPLOTS_H
#define ITBPLOTS_H

#include <memory>
#include <iostream>
#include <utility>
#include "TbPlots.h"
#include "TObject.h"
#include "TCanvas.h"
#include "ITbResults.h"
#include "TbHistMaker.h"
#include "TbGraphMaker.h"
#include "TROOT.h"

using TbHistMakerUniquePtr = std::unique_ptr<TbHistMaker>;
using TbGraphMakerUniquePtr = std::unique_ptr<TbGraphMaker>;

class TbPlotter : virtual public ITbResults {
    private:
        ///
        TbHistMakerUniquePtr m_hmaker = std::make_unique<TbHistMaker>();

        ///
        TbGraphMakerUniquePtr m_gmaker = std::make_unique<TbGraphMaker>();

    public:
        ///
        TbPlotter() = default;

        ///
        ~TbPlotter() = default;

        ///
        template <typename T>
        TCanvasSharedPtr Plot(std::shared_ptr<T> obj, const std::string& drawOpts, bool interactive);

        ///
        void DrawLegend(TCanvasSharedPtr canvas, TLegendSharedPtr legend);

        ///
        virtual void Draw(const std::string& name, bool interactive = false);

        ///
        virtual void Draw(const std::string& xObservable, const std::string& yObservable, bool interactive = false);

        ///
        void DrawCombined(const std::string& xObservable, const std::string& yObservable, bool interactive = false);

        ///
        virtual void DrawProfile(const std::string& name, bool interactive = false);

        ///
        virtual void DrawProfile(const std::string& name, const std::string& axis, bool interactive = false);

        ///
        void DrawProfileCombined(const std::string& name, const std::string& axis, bool interactive = false);



};

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
TCanvasSharedPtr TbPlotter::Plot(std::shared_ptr<T> obj, const std::string& drawOpts, bool interactive){
        if(!obj)
                return std::shared_ptr<TCanvas>(nullptr);
        if(!interactive)
                gROOT->SetBatch(true);
        auto objName = static_cast<std::string>(obj->GetName());
        auto message = objName + (interactive ? "":" (Silent mode)");
        std::cout<<"[INFO]:: TbPlotter::Plot :: " << message << std::endl;
        auto cPlot = std::make_shared<TCanvas>(TString("c" + objName), TString(objName), 650, 600);
        cPlot->cd();
        if (drawOpts.find("COL") != std::string::npos)
                cPlot->SetRightMargin(0.175);
        cPlot->SetLeftMargin(0.15);
        auto th1 = dynamic_cast<TH1*>(obj.get());
        if(th1) th1->SetStats(false);
        obj->Draw(TString(drawOpts));
        // TPad::BuildLegend() default placement values are such that they trigger
        // the automatic placement (ROOT version 6.09/03)
        // cPlot->BuildLegend(); // not so nice - it takes whole TObject title as a legend entry
        gROOT->SetBatch(false);
        return cPlot;
}

#endif //ITBPLOTS_H
