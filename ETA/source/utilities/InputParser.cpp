﻿#include "InputParser.h"
#include <iostream>

////////////////////////////////////////////////////////////////////////////////////
///
InputParser::InputParser(const std::string& filename) : m_source_file(filename) {}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<JobData> InputParser::GetAnalysisCollection(const std::string& analysis_collection_name) const {
	return m_analysis_collection.at(analysis_collection_name);
}

////////////////////////////////////////////////////////////////////////////////////
///
void InputParser::PrintAnalysisCollectionDetails(const std::string& analysisCollectioName) const {
	if(!analysisCollectioName.empty()){
		std::cout<<"[INFO]:: ANALYSIS COLLECTION DETAILS OF \"" << analysisCollectioName << "\"" << std::endl;
		auto collection = GetAnalysisCollection(analysisCollectioName);
		for (const auto& job : collection)
			job.PrintDetails();
	} else {
		for (const auto& collectionName : m_analysis_names){
			std::cout<<"[INFO]:: ANALYSIS COLLECTION DETAILS OF \"" << collectionName << "\"" << std::endl;
			PrintAnalysisCollectionDetails(collectionName);
		}
	}
}
