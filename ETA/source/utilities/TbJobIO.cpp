//
// Created by brachwal on 05.07.19.
//
#include "TbJobIO.h"
#include "TbGaudi.h"
#include "TbAnalysis.h"
#include "TbPlots.h"
#include "TbJobPlotter.h"
#include "TbLog.h"

bool TbJobIO::m_dumpEventLoopDataToNTuple = false;
bool TbJobIO::m_dumpEventLoopDataToPdf = false;

namespace fs = std::filesystem;

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobIO::DumpEventLoopDataToNTuple(){

    TbGaudi::PrintBanner("INFO","TbJobIO::Exporting histograms to .root files.");

    auto project_local_dir_path = TbGaudi::ProjectLocation()+"/TbResults";
    auto user_dir_path = TbIO::OutputLocation();
    auto path = user_dir_path.empty() ? project_local_dir_path : user_dir_path;

    if(path.at(path.length()-1)=='/') path+=CurrentDate();
    else                        path+="/"+CurrentDate();
    fs::path dp (path);
    if(!fs::exists (dp)){
        std::cout << "[INFO]:: TbJobIO:: Created output directory "<<std::endl;
        std::cout << "[INFO]:: TbJobIO:: "<< path <<std::endl;
        fs::create_directories(dp);
    }

    std::string fname = ThisTbJobPtr()->Label("JOB")+"_eventLoopHists.root";

    std::cout << "[INFO]:: TbJobIO:: Exporting histograms into the ROOT file:\n"
                 "[INFO]:: TbJobIO:: " << fname <<std::endl;

    fs::path fp = path+"/"+fname;
    if(fs::exists(fp)){
        std::cout << "[INFO]:: TbJobIO:: Remove existing results: " << fname <<std::endl;
        fs::remove(fp);
    }

    auto outfile = std::make_unique<TFile>(TString(path+"/"+fname), "RECREATE");
    auto hList = std::make_unique<TList>();// list of histograms to store
    for(auto& ihistNamed : ThisTbJobPtr()->Dut1DHistDataCollection()){
//        if( m_is_dumped.at(ihistNamed.second->GetName()) == false) {
        hList->Add(ihistNamed.second);
//            m_is_dumped.at(ihistNamed.second->GetName()) = true;
//        }
    }
    for(auto& ihistNamed : ThisTbJobPtr()->DutBinned1DHistDataCollection()){
        for(auto& ihist : ihistNamed.second) {
//            if( m_is_dumped.at(ihist->GetName()) == false){
            hList->Add(ihist);
//                m_is_dumped.at(ihist->GetName()) = true;
//            }
        }
    }
    for(auto& ihistNamed : ThisTbJobPtr()->DutBinned2DHistDataCollection()){
        for(auto& ihist : ihistNamed.second) {
//            if( m_is_dumped.at(ihist->GetName()) == false){
            hList->Add(ihist);
//                m_is_dumped.at(ihist->GetName()) = true;
//            }
        }
    }
    hList->Write();
    outfile->Close();
    std::cout << "[INFO]:: TbJobIO:: Created " << fname <<std::endl<<std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobIO::DumpEventLoopDataToPdf(){
    TbGaudi::PrintBanner("INFO","TbJobIO::Exporting histograms to .pdf files.");

    auto project_local_dir_path = TbGaudi::ProjectLocation()+"/TbResults";
    auto user_dir_path = TbIO::OutputLocation();
    auto path = user_dir_path.empty() ? project_local_dir_path : user_dir_path;

    if(path.at(path.length()-1)=='/') path+=CurrentDate();
    else                        path+="/"+CurrentDate();
    fs::path dp (path);
    if(!fs::exists (dp)){
        std::cout << "[INFO]:: TbJobIO:: Created output directory "<<std::endl;
        std::cout << "[INFO]:: TbJobIO:: "<< path <<std::endl;
        fs::create_directories(dp);
    }

    std::string fname = ThisTbJobPtr()->Label("JOB")+"_eventLoopHists.pdf";

    std::cout << "[INFO]:: TbJobIO:: Exporting histograms into the pdf file:\n"
                 "[INFO]:: TbJobIO:: " << fname <<std::endl;

    MTCanvas canCollection;
    auto jobPlotter = ThisTbJobPtr()->PlotterPtr();
    gROOT->SetBatch(true);

    auto hist1D = ThisTbJobPtr()->DutBinned1DHistDataCollection();
    for(auto& ihistNamed : hist1D){
        for(auto& ihist : ihistNamed.second) {
            auto hName = static_cast<std::string>(ihist->GetName());
            canCollection[hName] = std::make_shared<TCanvas>(TString("c" + hName), TString(hName), 650, 600);
            canCollection.at(hName)->cd();
            canCollection.at(hName)->SetRightMargin(0.15);
            canCollection.at(hName)->SetLeftMargin(0.15);
            ihist->SetStats(false);
            ihist->Draw("PE1");
        }
    }

    auto hist2D = ThisTbJobPtr()->DutBinned2DHistDataCollection();
    for(auto& ihistNamed : hist2D){
        for(auto& ihist : ihistNamed.second) {
            auto hName = static_cast<std::string>(ihist->GetName());
            canCollection[hName] = std::make_shared<TCanvas>(TString("c" + hName), TString(hName), 650, 600);
            canCollection.at(hName)->cd();
            canCollection.at(hName)->SetRightMargin(0.175);
            canCollection.at(hName)->SetLeftMargin(0.15);
            ihist->SetStats(false);
            ihist->Draw("COLZ");
        }
    }


    auto file_path = std::string(path+"/"+fname);
    DeleteFileIfExists(file_path);
    TbPlots::ExportToPdf(file_path,canCollection);
    gROOT->SetBatch(false);
}
////////////////////////////////////////////////////////////////////////////////////
///
void TbJobIO::ExportResultsToPdfFile(const std::string& dir_path){
    auto jobPtr = ThisTbJobPtr();
    std::cout << "[INFO]:: TbJobIO::ExportResultsToCsv: " << jobPtr->Title() << std::endl;
    std::string pdf_file = jobPtr->Label("JOB")+".pdf";
    auto pdf_file_path = CreateOutputDir(dir_path)+"/"+pdf_file;
    DeleteFileIfExists(pdf_file_path);

    // export plots:
    auto jobPlots = GetTbJobPlots();
    jobPlots->ExportToPdf(pdf_file_path);

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobIO::ExportResultsToNTupleFile(const std::string& dir_path){
    std::cout << "[INFO]:: TbJobIO::ExportResults:: NTuple file output for (implement me.) " << std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobIO::ExportResultsToCsv(const std::string& dir_path){
    auto jobPtr = ThisTbJobPtr();
    std::cout << "[INFO]:: TbJobIO::ExportResultsToCsv: " << jobPtr->Title() << std::endl;
    std::string csv_file = jobPtr->Label("JOB")+".csv";
    auto csv_file_path = CreateOutputDir(dir_path)+"/"+csv_file;
    DeleteFileIfExists(csv_file_path);
    auto jobOutcome = GetTbOutcome();
    jobOutcome->ExportToCsv(jobPtr, csv_file_path);
}

////////////////////////////////////////////////////////////////////////////////////
///Exporting All job results (this instance)
void TbJobIO::ExportResults(const std::string& dir_path){
    TbLog::Info("Exporting " + ThisTbJobPtr()->Title());//Logger
    this->ExportResultsToPdfFile(dir_path);
    this->ExportResultsToNTupleFile(dir_path);
    this->ExportResultsToCsv(dir_path);
}