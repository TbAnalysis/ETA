//
// Created by brachwal on 05.02.19.
//

#ifndef TBGAUDI_TBGRAPHMAKER_H
#define TBGAUDI_TBGRAPHMAKER_H

#include <memory>
#include "ITbResults.h"

#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TLegend.h"

using TLegendSharedPtr  = std::shared_ptr<TLegend>;
using TGraphSharedPtr  = std::shared_ptr<TGraphErrors>;
using TGraphWeakPtr  = std::weak_ptr<TGraphErrors>;
using TMGraphSharedPtr  = std::shared_ptr<TMultiGraph>;
using VDouble = std::vector<double>;

class TbGraphMaker : public ITbResults  {

    protected:
        ///
        virtual std::string BuildTObjName(const std::string& resultsName, const std::string& objType);

        ///
        virtual std::string BuildTObjTitle(const std::string& resultsName, const std::string& objType, const std::string& xAxis, const std::string& yAxis);

        ///
        TLegendSharedPtr BuildTMGraphLegend(TMGraphSharedPtr mgraph);

        ///
        TLegendSharedPtr BuildTGraphLegend(TGraphSharedPtr graph);

    public:
        ///
        TbGraphMaker() = default;

        ///
        virtual ~TbGraphMaker() = default;

        ///
        TGraphSharedPtr DefineTGraphErrors(const std::string& name, const std::string& title, VDouble& x, VDouble& xErr, VDouble& y, VDouble& yErr);

        ///
        TMGraphSharedPtr DefineTMultiGraph(const std::string& name, const std::string& title);

        ///
        virtual void AdjustTMultiGraph(TMGraphSharedPtr mgrah, double min, double max);
};

#endif //TBGAUDI_TBGRAPHMAKER_H
