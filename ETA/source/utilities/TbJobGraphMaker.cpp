//
// Created by brachwal on 01.07.19.
//

#include "TbJob.h"
#include "TbJobGraphMaker.h"
#include "DutStripBinning.h"
#include "DutEllipticBinning.h"

////////////////////////////////////////////////////////////////////////////////////
///
std::string TbJobGraphMaker::BuildTObjName(const std::string& resultsName, const std::string& objType){

    auto job = ThisTbJobPtr();
    auto dutName = job->GetParam("DutName");
    auto runNumber = job->GetParam("RunNumber");
    return std::string(resultsName+"_"+objType+"_" + dutName + "_" + runNumber);
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string TbJobGraphMaker::BuildTObjTitle(const std::string& resultsName,
                                            const std::string& objType,
                                            const std::string& xAxis,
                                            const std::string& yAxis){
    auto job = ThisTbJobPtr();
    auto dutName = job->GetParam("DutName");
    auto runNumber = job->GetParam("RunNumber");
    return std::string(resultsName+" "+objType+" ("+ dutName + " " + runNumber+");"+xAxis+";"+yAxis);
}

////////////////////////////////////////////////////////////////////////////////////
///
TMGraphSharedPtr TbJobGraphMaker::MakeDutProfiles(const std::string& resultsName) {

    //______________________________________________________________________________
    // Check is the called results exist!
    if (!ITbJobResults::Exists(resultsName)) {
        std::cout << "[WARNING]:: TbJobGraphMaker:: Results do not exists: " << resultsName << std::endl;
        return std::shared_ptr<TMultiGraph>(nullptr);
    }

    //______________________________________________________________________________
    auto gname = BuildTObjName(resultsName, "Profiles");
    auto mgraph = GetTbJobTObject<TMultiGraph>(gname);
    if (mgraph) return mgraph; // nothing to do - this graph already exists!

    //______________________________________________________________________________
    std::cout << "[INFO]:: TbJobGraphMaker:: Creating DUT profiles graphs for " << resultsName << std::endl;


    auto gtitle = BuildTObjTitle(resultsName, "profiles", "# pixel", resultsName);
    mgraph = DefineTMultiGraph(gname, gtitle);

    // make graphs and add to mgraph
    auto pXSharedPtr = MakeDutProfile(resultsName, "column");
    if (pXSharedPtr)
        mgraph->Add(dynamic_cast<TGraphErrors*>(pXSharedPtr->Clone()), "LPE1"); // Clone returns TObject ptr

    auto pYSharedPtr = MakeDutProfile(resultsName, "row");
    if (pYSharedPtr)
        mgraph->Add(dynamic_cast<TGraphErrors*>(pYSharedPtr->Clone()), "LPE1"); // Clone returns TObject ptr

    //______________________________________________________________________________
    AdjustTMultiGraph(mgraph,MinValue(resultsName),MaxValue(resultsName));
    InsertTbJobTObject(mgraph);
    InsertTbJobTObject(BuildTMGraphLegend(mgraph));

    return mgraph;
}

////////////////////////////////////////////////////////////////////////////////////
///
TGraphSharedPtr TbJobGraphMaker::MakeDutProfile(const std::string& resultsName, const std::string& axis){
    //______________________________________________________________________________
    // Check is the called results exist!
    if(!ITbJobResults::Exists(resultsName)) {
        std::cout << "[WARNING]:: TbJobGraphMaker:: Results do not exists: " << resultsName << std::endl;
        return std::shared_ptr<TGraphErrors>(nullptr);
    }
    //______________________________________________________________________________
    std::string gname;
    if(axis.compare("column")==0)
        gname = BuildTObjName(resultsName,"ProfileX");
    if(axis.compare("row")==0)
        gname = BuildTObjName(resultsName,"ProfileY");

    TGraphSharedPtr graph = GetTbJobTObject<TGraphErrors>(gname);
    if(graph) return graph;

    //______________________________________________________________________________
    std::cout << "[INFO]:: TbJobGraphMaker:: Creating DUT profile ( "<<axis<<" ) graph for " << resultsName << std::endl;
    std::string gtitle;

    //TODO "# pixel <<< make sth similar to job->TH2AxesTitles();
    if(axis.compare("column")==0)
        gtitle = BuildTObjTitle(resultsName, "profile X","# pixel",resultsName);

    else if(axis.compare("row")==0)
        gtitle = BuildTObjTitle(resultsName, "profile Y","# pixel",resultsName);

    auto job = ThisTbJobPtr();
    auto dutBinning = job->DutPtr()->DutBinningPtr();

    // Get x-axis data
    auto xVal = dutBinning->GetMiddleBinVector(axis);
    VDouble xErr;
    for (int i = 0; i < xVal.size(); ++i) xErr.push_back(0.0001); // arbitrary small number

    // Get y-axis data
    VDouble yVal, yErr;
    auto jobOutcome = GetTbOutcome();
    auto results = jobOutcome->Get<MResult>(resultsName); // results are being mapped with dutBin as a key
    if(!results){
        std::cout << "[WARNING]:: TbJobGraphMaker::MakeDutProfile:: No results container found for " << resultsName <<std::endl;
        return std::shared_ptr<TGraphErrors>(nullptr);
    }
    auto binningType = job->GetParam("DutBinningType");

    double val, valErr;
    unsigned nSum, iDutBin,nDutBinsToAverage;
    for (unsigned x = 0; x < xVal.size(); x++) {
        val=0;
        valErr=0;
        nSum=1;
        if(binningType.compare("Uniform")==0) {
            nDutBinsToAverage = sqrt(dutBinning->NBins());
            nSum=0;
            for (unsigned y = 0; y < nDutBinsToAverage; y++) {
                if(axis.compare("column")==0)
                    iDutBin = x+y*nDutBinsToAverage;
                else if(axis.compare("row")==0)
                    iDutBin = x*nDutBinsToAverage+y;
                std::cout << "[INFO]:: >> iDutBin "<< iDutBin <<std::endl;
                if(Includes<unsigned>(*results,iDutBin)){
                    std::cout << "[INFO]:: >> val "<< results->at(iDutBin).first << " +/- "<< results->at(iDutBin).second <<std::endl;
                    val    += results->at(iDutBin).first;
                    valErr += results->at(iDutBin).second*results->at(iDutBin).second;
                    ++nSum;
                }
            }
        }
        else if(binningType.compare("Elliptic")==0) {
            unsigned col, row;
            if(axis.compare("column")==0){
                col = static_cast<unsigned>(xVal.at(x));
                row = std::dynamic_pointer_cast<DutEllipticBinning>(dutBinning)->GetDutCentreY();

            }
            else if(axis.compare("row")==0){
                col = std::dynamic_pointer_cast<DutEllipticBinning>(dutBinning)->GetDutCentreX();
                row = static_cast<unsigned>(xVal.at(x));
            }
            iDutBin = dutBinning->GetBinNumber(col,row);
            val    = results->at(iDutBin).first;
            valErr = results->at(iDutBin).second*results->at(iDutBin).second;
        }
        else {
            val = 0.;
            valErr = 0.;
        }
        yVal.push_back(val/nSum);
        yErr.push_back(sqrt(valErr));
    }

    std::cout << "[INFO]:: TbJobGraphMaker:: Values: "<<std::endl;
    for (int i = 0; i < yVal.size(); ++i)
        std::cout << "[INFO]:: >> bin "<< i << ") x= "<<xVal.at(i)<<" y= "<<yVal.at(i)<<" +/- "<< yErr.at(i) <<std::endl;

    graph = DefineTGraphErrors(gname,gtitle,xVal,xErr,yVal,yErr);
    InsertTbJobTObject(graph);
    return graph;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobGraphMaker::AdjustTMultiGraph(TMGraphSharedPtr mgrah, double min, double max){
    unsigned nX  = ThisTbJobPtr()->DutPtr()->IsStripBinning() ? Dut::NStrips() : Dut::NPixelsX();
    mgrah->GetXaxis()->SetLimits(0,nX);
    mgrah->GetHistogram()->SetMinimum(0.95*min);
    mgrah->GetHistogram()->SetMaximum(1.15*max);
}