//
// Created by brachwal on 05.07.19.
//

#ifndef TBJOBIO_H
#define TBJOBIO_H

#include <memory>
#include "TbIO.h"
#include "ITbJobResults.h"

class TbJobIO : public TbIO,
                public ITbJobResults {
    private:
        ///
        static bool m_dumpEventLoopDataToNTuple;

        ///
        static bool m_dumpEventLoopDataToPdf;

    public:
        ///
        TbJobIO() = delete;

        ///
        explicit TbJobIO(TbJobSharedPtr tbjob) : ITbJobResults(tbjob) {}

        ///
        ~TbJobIO() = default;

        ///
        static bool ExportEventLoopDataToNTuple(){return m_dumpEventLoopDataToNTuple;}

        ///
        static void ExportEventLoopDataToNTuple(bool val){m_dumpEventLoopDataToNTuple=val;}

        ///
        static bool ExportEventLoopDataToPdf(){return m_dumpEventLoopDataToPdf;}

        ///
        static void ExportEventLoopDataToPdf(bool val){m_dumpEventLoopDataToPdf=val;}

        ///
        void DumpEventLoopDataToNTuple();

        ///
        void DumpEventLoopDataToPdf();

        ///
        void ExportResultsToPdfFile(const std::string& dir_path) override;

        ///
        void ExportResultsToNTupleFile(const std::string& dir_path) override;

        ///
        void ExportResultsToCsv(const std::string& dir_path=std::string()) override;

        /// Exporting All job results (this instance)
        void ExportResults(const std::string& dir_path) override;
};

#endif //TBJOBIO_H
