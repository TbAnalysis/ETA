//
// Created by brachwal on 04.02.19.
//

#ifndef TBGAUDI_DUTSTRIP_H
#define TBGAUDI_DUTSTRIP_H

#include "Dut.h"

class DutStrip : public Dut {
    private:

    public:
        DutStrip() = default;
        ~DutStrip() = default;
};
#endif //TBGAUDI_DUTSTRIP_H
