/*! \brief DutUniformBinning class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date May-2018
*/

#ifndef DUT_UNIFORM_BINNING_H
#define DUT_UNIFORM_BINNING_H

// std libriaryies
#include <vector>

// project libraries
#include "DutBinning.h"

////////////////////////////////////////////////////////////////////////////////////
///
class DutUniformBinning : public DutBinning {

	private:
		///binnig boundaries:
		std::vector<int> bin_boundary_col;
		std::vector<int> bin_boundary_row;

    	void InstantiateDutBinMapping() override;

	public:
        ///
		DutUniformBinning(Dut* owner, std::string name, unsigned nBins);

        ///
        virtual ~DutUniformBinning();

		///
		void Initialize();

		///
		void InitializeDutPitchHitCounter() override;

		///
		void ResetDutPitchHitCounter() override;

		/// TODO: BinIntegral definition
        //double BinIntegral(TF2* func, int bin, double normalization);

        /// TODO: BinArea definition
        //double BinArea(int bin);

        void Draw() const override;

};
#endif	// DUT_UNIFORM_BINNING_H
