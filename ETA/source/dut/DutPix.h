//
// Created by brachwal on 04.02.19.
//

#ifndef TBGAUDI_DUTPIX_H
#define TBGAUDI_DUTPIX_H

#include "Dut.h"
#include "math.h"

class DutPix : public Dut {
    private:
        /// The read-out pixel/strip pitch binning
        static unsigned m_pitch_n_bin_x;
        static unsigned m_pitch_n_bin_y;

    public:
        ///
        DutPix() = default;

        ///
        ~DutPix() = default;

        /// Get the position in pixel unit
        /// \param[in] m_X The global Dut position in [mm]
        /// \param[out] position in pixel unit in [mum]}
        static double IntraPixPosition(Double_t m_X) { return fmod(m_X*1000,Dut::PitchSize());}

        ///
        static unsigned NBinsX() { return m_pitch_n_bin_x; }
        static unsigned NBinsY() { return m_pitch_n_bin_y; }
        static unsigned BinX(double inPixPosition);
        static unsigned BinY(double inPixPosition);

        ///
        static void NBinsX(unsigned val) { m_pitch_n_bin_x = val; }
        static void NBinsY(unsigned val) { m_pitch_n_bin_y = val; }

        ///
        static unsigned GlobalBinNumber(unsigned dutBin, unsigned xBin, unsigned yBin) {
            auto xNBins = DutPix::NBinsX();
            auto yNBins = DutPix::NBinsY();
            return dutBin *  xNBins * yNBins + xBin * yNBins + yBin ;
        }

        /// Without DUT binning
        static unsigned GlobalBinNumber( unsigned xBin, unsigned yBin) {
            return DutPix::GlobalBinNumber(0,xBin,yBin);
        }

};
#endif //TBGAUDI_DUTPIX_H
