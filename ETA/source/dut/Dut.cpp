#include "Dut.h"
#include "DutBinning.h"
#include "DutUniformBinning.h"
#include "DutEllipticBinning.h"
#include "DutStripBinning.h"
#include "FluenceProfile.h"
#include "TbJob.h" // for current TbJob info
#include "RootSvc.h"

unsigned  Dut::m_n_pix_x = 256;
unsigned  Dut::m_n_pix_y = 256;
unsigned  Dut::m_pitch_size = 55;
unsigned  Dut::m_n_strip = 128;
float  Dut::m_strip_length = 98;
bool Dut::m_pix_study = true;
bool Dut::m_intra_pix_study = false;
bool Dut::m_strip_study = false;
bool Dut::m_intra_strip_study = false;
std::string Dut::m_deafult_irradiation_status = "false";

////////////////////////////////////////////////////////////////////////////////////
///
void Dut::Initialize(TbJobSharedPtr tbjob_ptr){
    ThisTbJobPtr(tbjob_ptr);
    m_name = tbjob_ptr->GetParam("DutName");

    // Verify if expected parameters have been defined by user's input
    // - if not, fill them with default values
    if(tbjob_ptr->GetParam("DutBinningType")=="-999"){
        std::cout << "[INFO]:: DUT::Init:: The DutBiningType NOT Specified. Using the default value: " << DutBinning::DefaultType() << std::endl;
        tbjob_ptr->SetParam("DutBinningType", DutBinning::DefaultType());
    }

    if(tbjob_ptr->GetParam("DutBinningMethod")=="-999"){
        std::cout << "[INFO]:: DUT::Init:: The DutBinningMethod NOT Specified. Using the default value: " << DutBinning::DefaultMethod() << std::endl;
        tbjob_ptr->SetParam("DutBinningMethod", DutBinning::DefaultMethod());
    }

    if(tbjob_ptr->GetParam("DutNBins")=="-999"){
        std::cout << "[INFO]:: DUT::Init:: The DutNBins NOT Specified. Using the default value: " << DutBinning::DefaultNBins() << std::endl;
        tbjob_ptr->SetParam("DutNBins", itos(DutBinning::DefaultNBins()));
    }

    if(tbjob_ptr->GetParam("DutFluenceProfileType")=="-999"){
        std::cout << "[INFO]:: DUT::Init:: The DutFluenceProfileType NOT Specified. Using the default value: " << FluenceProfile::DefaultType() << std::endl;
        tbjob_ptr->SetParam("DutFluenceProfileType", FluenceProfile::DefaultType());
    }
    
    if(tbjob_ptr->GetParam("DutIrradiationStatus")=="-999"){
        std::cout << "[INFO]:: DUT::Init:: The DutIrradiationStatus NOT Specified. Using the default value: " << Dut::DefaultIrradiationStatus() << std::endl;
        tbjob_ptr->SetParam("DutIrradiationStatus", Dut::DefaultIrradiationStatus());
    }

    DutSizeInitialization();
    CreateBinning();
}

////////////////////////////////////////////////////////////////////////////////////
///
void Dut::CreateBinning(){
    auto jobPtr = ThisTbJobPtr();

    auto name = Label();
    auto nBins = jobPtr->GetParam<unsigned>("DutNBins");

    if(nBins==1 || IsUniformBinning())
        m_dut_binning = std::make_shared<DutUniformBinning>(this,name,nBins);

    if(IsEllipticBinning())
        m_dut_binning = std::make_shared<DutEllipticBinning>(this,name,nBins);

    if(IsStripBinning())
        m_dut_binning = std::make_shared<DutStripBinning>(this, name, nBins);

    auto is_irradiated = IrradiationStatus();
    if(is_irradiated)
        m_dut_binning->SetupFluenceProfile();

    m_dut_binning->Initialize();
    
    m_is_binned = true;
}
////////////////////////////////////////////////////////////////////////////////////
///
void Dut::DutSizeInitialization(){
    if(IsStripBinning() && !DutStripBinning::Is2DimMapping()){
        m_size_x = static_cast<float>(Dut::PitchSize() * Dut::NStrips()) / 1000;   // [mm]
        m_size_y = Dut::StripLength();                                             // [mm]
    }
    else {
        m_size_x = static_cast<float>(Dut::PitchSize() * Dut::NPixelsX()) / 1000; // [mm]
        m_size_y = static_cast<float>(Dut::PitchSize() * Dut::NPixelsY()) / 1000; // [mm]
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
bool Dut::IsStripBinning() const {
    return !ThisTbJobPtr()->GetParam("DutBinningType").compare("Strip");
}

////////////////////////////////////////////////////////////////////////////////////
///
bool Dut::IsUniformBinning() const {
    return !ThisTbJobPtr()->GetParam("DutBinningType").compare("Uniform");
}

////////////////////////////////////////////////////////////////////////////////////
///
bool Dut::IsEllipticBinning() const {
    return !ThisTbJobPtr()->GetParam("DutBinningType").compare("Elliptic");
}

////////////////////////////////////////////////////////////////////////////////////
///
unsigned Dut::NBins() const {
    return ThisTbJobPtr()->GetParam<unsigned>("DutNBins");
}

////////////////////////////////////////////////////////////////////////////////////
///
bool Dut::IrradiationStatus() const {
    return ThisTbJobPtr()->GetParam<bool>("DutIrradiationStatus");
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string Dut::FluenceProfileType() const {
    return ThisTbJobPtr()->GetParam("DutFluenceProfileType");
}

////////////////////////////////////////////////////////////////////////////////////
///
void Dut::PixStudy(bool val) {
    m_pix_study = val;
    if(m_pix_study){
        m_intra_pix_study = false;
        m_strip_study = false;
        m_intra_strip_study = false;
        RootSvc::SetHistUnit(HistUnit::DutPositioning);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void Dut::IntraPixStudy(bool val) {
    m_intra_pix_study = val;
    if(m_intra_pix_study){
        m_pix_study = false;
        m_strip_study = false;
        m_intra_strip_study = false;
        RootSvc::SetHistUnit(HistUnit::PitchSize);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void Dut::StripStudy(bool val) {
    m_strip_study = val;
    if(m_pix_study){
        m_pix_study = false;
        m_intra_pix_study = false;
        m_intra_strip_study = false;
        RootSvc::SetHistUnit(HistUnit::DutPositioning);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void Dut::IntraStripStudy(bool val) {
    m_intra_strip_study = val;
    if(m_intra_pix_study){
        m_pix_study = false;
        m_intra_pix_study = false;
        m_strip_study = false;
        RootSvc::SetHistUnit(HistUnit::PitchSize);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
const std::string Dut::Label() const {
    std::string label = m_name;
    auto jobPtr = ThisTbJobPtr();
    label+="_"+jobPtr->GetParam("DutBinningType");
    label+="_"+jobPtr->GetParam("DutNBins")+"Bins";
    auto is_irradiated = IrradiationStatus();
    if(is_irradiated)
        label+="_"+jobPtr->GetParam("DutFluenceProfileType");;
    return label;
}

////////////////////////////////////////////////////////////////////////////////////
///
const std::string Dut::StudyType(){
    if(Dut::IntraStripStudy()){
        return "IntraStripStudy";
    }
    else if(Dut::StripStudy()){
        return "StripStudy";
    }
    else if(Dut::IntraPixStudy()){
        return "IntraPixStudy";
    }
    else if(Dut::PixStudy()){
        return "IntraPixStudy";
    }
    return std::string();
}

////////////////////////////////////////////////////////////////////////////////////
///
bool Dut::PitchScopeTest(double inPix_x, double inPix_y){
    if(inPix_x > Dut::PitchSize() || inPix_y > Dut::PitchSize()) {
        std::cout << "[ERROR]:: Dut::PitchScopeTest in pixel coordinates"
                  << " x ("<<inPix_x << ") "
                  << " y ("<<inPix_y << ") "
                  << " are out of scope!" << std::endl;
        return false;
    }
    else
        return true;
}