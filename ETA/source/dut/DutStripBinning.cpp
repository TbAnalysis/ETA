#include "DutStripBinning.h"
#include "IRRADProfile.h"
#include "TbAnalysis.h"
#include "DutBinning.h"
#include "Globals.h"
#include "TbGaudi.h"
#include "Dut.h"
#include "TbJob.h"

// By default the stripp binning is for strip like DUTs (e.g. LHCb UT)
bool DutStripBinning::m_2dim_mapping = false;
////////////////////////////////////////////////////////////////////////////////////
///
DutStripBinning::DutStripBinning(Dut* owner, std::string name, unsigned nBins)
    : DutBinning(owner,name,nBins)
{
    std::cout<< "[INFO]:: DutStripBinning construction for "<< name << " and nBins="<<nBins<<std::endl;
    m_binning_type = DutBinningType::Strip;
}

////////////////////////////////////////////////////////////////////////////////////
///
DutStripBinning::~DutStripBinning(){
	//for (auto iline : m_bin_boundary_lines) delete iline;
    //m_bin_boundary_lines.clear();
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutStripBinning::Initialize(){
    if(!IsInitialized()) {
        // TODO: implement fluence profile for the UT in general
        // verify the fluence profile correctness (the strip binning make sense only for non IRRAD type!)
        /*auto fluence_profile_type =  m_owner->GetDutInfo("FluenceProfileType");
        if(fluence_profile_type.compare("IRRAD")==0){
            std::cout << "[ERROR]:: DutStripBinning:: Trying to create the strip binning for the \"IRRAD\" profile."
                         " It make no sense." << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
        }*/

        if (DutStripBinning::Is2DimMapping())
            InstantiateDutBinMapping();
        else
            strips_mapping();

        for (int i = 0; i < NBins(); ++i)
            m_dut_bins.at(i)->DumpDutBinningMapping();
        m_is_initialized = true;
        std::cout << "[INFO]:: DutStripBinning::Initialization::DONE." << std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutStripBinning::InstantiateDutBinMapping(){

    // TODO: implement this case, which is needed for the DUT pixel-like sensors, eg. KIT scheme
    // m_dut_bins.at(bin)->AddPixel(x,y);
    // m_dut_bins.at(bin)->AddStrip(x);
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutStripBinning::strips_mapping(){

    // Hard coded case: one dimension
    int nBinsX = NBins();
    int dpix = Dut::NStrips() / nBinsX;

    // Data needed to make lines of bins boundaries
    // and graph like results plotting
    m_bin_boundary_lines.clear();
    m_middle_bin_column.clear();
    m_middle_bin_row.clear();       // this will filled with zeros

    for (int i=1; i <= nBinsX; ++i){
        if(i < nBinsX) {  // doesn't include sensor edge boundaries
            m_bin_boundary_lines.push_back(i * dpix);
        }
        m_middle_bin_column.push_back(i*dpix-dpix/2);
    }

    unsigned bin=0;

    auto nBins = m_bin_boundary_lines.size();
    for (unsigned x = 0; x < Dut::NStrips(); x++) {
        for (int i=0; i<nBins;++i){
            if ( i < nBins-1 && x >= m_bin_boundary_lines.at(i)) {
                if (x < m_bin_boundary_lines.at(i+1)) {
                    bin = i+1;
                    break;
                }
            }
            else if (i == nBins-1){ // got the very last bin
                bin = i+1;
                break;
            }
            else {
                bin = i;
                break;
            }
        }
        m_dut_bins.at(bin)->AddStrip(x);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutStripBinning::InitializeDutPitchHitCounter(){
    if(m_dut_pixel_binning_counter.empty()) {
        for (int x = 0; x < Dut::NStrips(); x++) {
            std::vector<unsigned> row;
            for (int y = 0; y < 1; y++)
                row.emplace_back(unsigned(0));
            m_dut_pixel_binning_counter.push_back(row);
        }
    } else {
        ResetDutPitchHitCounter();
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutStripBinning::ResetDutPitchHitCounter(){
    if(!m_dut_pixel_binning_counter.empty()) {
        for (int x = 0; x < Dut::NStrips(); x++) {
            for (int y = 0; y < 1; y++)
                m_dut_pixel_binning_counter.at(x).at(y) = 0;
        }
    } else {
        InitializeDutPitchHitCounter();
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutStripBinning::Draw() const { // TODO // 
}