//
// Created by brachwal on 04.02.19.
//

#ifndef TBGAUDI_DUTINTRASTRIP_H
#define TBGAUDI_DUTINTRASTRIP_H

#include "DutStrip.h"

class DutIntraStrip : public DutStrip {
    private:

    public:
        DutIntraStrip() = default;
        ~DutIntraStrip() = default;
};
#endif //TBGAUDI_DUTINTRASTRIP_H
