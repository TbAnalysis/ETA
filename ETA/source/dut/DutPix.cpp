//
// Created by brachwal on 13.09.19.
//

#include "DutPix.h"

unsigned DutPix::m_pitch_n_bin_x = 5;
unsigned DutPix::m_pitch_n_bin_y = 5;

////////////////////////////////////////////////////////////////////////////////////
///
unsigned DutPix::BinX(double inPixPosition){
    auto xNBins = DutPix::NBinsX();
    auto xBinWidth = static_cast<double>(Dut::PitchSize())/xNBins;
    return static_cast<unsigned>(inPixPosition/xBinWidth);
}

////////////////////////////////////////////////////////////////////////////////////
///
unsigned DutPix::BinY(double inPixPosition){
    auto yNBins = DutPix::NBinsY();
    auto yBinWidth = static_cast<double>(Dut::PitchSize())/yNBins;
    return static_cast<unsigned>(inPixPosition/yBinWidth);

}
