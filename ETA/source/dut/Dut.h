/*! \brief Dut class definition
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef Dut_H
#define Dut_H

#include "DutBinning.h"

// std libriaries
#include <iostream>
#include <string>
#include <map>
#include "Rtypes.h"
#include "TbJobNode.h"
#include "Globals.h"

using DutSharedPtr = std::shared_ptr<Dut>;

////////////////////////////////////////////////////////////////////////////////////
///
class Dut : public TbJobNode {

	private:
		///
		std::string m_name;

		///
		DutBinningSharedPtr m_dut_binning = nullptr;

		///
		bool m_is_binned = false;

		/// The read-out pixel/strip pitch size in [nm]
		static unsigned m_pitch_size;

		/// \brief The read-out dut pixels in X
		static unsigned m_n_pix_x;

		/// \brief The read-out dut pixels in Y
        static unsigned m_n_pix_y;

		/// \brief The read-out dut strips
        static unsigned m_n_strip;

        /// Length of read-out strip in [mm]
        static float m_strip_length;

        /// \brief DUT size in X in [mm]
        float m_size_x;

        /// \brief DUT size in Y in [mm]
        float m_size_y;

        ///
        static bool m_pix_study;
        static bool m_intra_pix_study;
        static bool m_strip_study;
        static bool m_intra_strip_study;

		/// \brief Default configuration value for
		// Pre (false) of after (true) DUT irradiation status
		static std::string m_deafult_irradiation_status;

        ///
        void DutSizeInitialization();

	public:
		
		///
		Dut() = default;
		
		///
		virtual ~Dut() = default;

		///
		void Initialize(TbJobSharedPtr tbjob_ptr);

		///
		bool IsStripBinning() const;

		///
		bool IsUniformBinning() const;

		///
		bool IsEllipticBinning() const;

		///
		unsigned NBins() const;

		///
		static unsigned PitchSize(){return m_pitch_size;}

		///
		static void PitchSize(unsigned val){ m_pitch_size = val;}

		///
		static bool PitchScopeTest(double inPix_x, double inPix_y);

		///
		static unsigned NPixelsX(){return m_n_pix_x;}

        ///
        static void NPixelsX(unsigned val){ m_n_pix_x = val;}

        ///
        static unsigned NPixelsY(){return m_n_pix_y;}

        ///
        static void NPixelsY(unsigned val){m_n_pix_y = val;}

		///
		static unsigned NStrips(){return m_n_strip;}

		///
		static void NStrips(unsigned val){ m_n_strip = val;}

		///
		static float StripLength(){return m_strip_length;}

		///
		static void StripLength(float val){ m_strip_length = val;}

        ///
        static void PixStudy(bool val);

        ///
        static bool PixStudy() {return m_pix_study;}

		///
		static void IntraPixStudy(bool val);

		///
		static bool IntraPixStudy() {return m_intra_pix_study;}

		///
		static void StripStudy(bool val);

		///
		static bool StripStudy() {return m_strip_study;}

		///
		static void IntraStripStudy(bool val);

		///
		static bool IntraStripStudy() {return m_intra_strip_study;}

		///
		static const std::string StudyType();

		///
		static std::string DefaultIrradiationStatus(const std::string& val) { return m_deafult_irradiation_status = val; }
		static std::string DefaultIrradiationStatus(bool val) { return m_deafult_irradiation_status = btos(val); }

		///
		static std::string DefaultIrradiationStatus() { return m_deafult_irradiation_status; }

		///
		bool IrradiationStatus() const;

        ///
		inline DutBinningSharedPtr DutBinningPtr(){ return m_dut_binning;}

		/// \brief The DUT binning definition according to the DutBinningType assigned to given TbJob.
		void CreateBinning();

        ///
        bool IsBinned() const {return m_is_binned;}

        ///
		float DutSizeX() { return m_size_x; }

		///
		float DutSizeY() { return m_size_y; }

		///
		const std::string Label() const;

        ///
		const std::string& Name() const {return m_name;}

		///
		std::string FluenceProfileType() const;

		///
		float Area() const { return m_size_x*m_size_y; } 

};
#endif	// Dut_H
