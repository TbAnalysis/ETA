/*! \brief DutBinning class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef DDUTBINING_H
#define DDUTBINING_H

#include <vector>
#include <string>
#include <memory>
#include "DutBin.h"
#include <iostream>

class Dut;
class DutBinning;
class FluenceProfile;

using DutBinningSharedPtr = std::shared_ptr<DutBinning>;
using FlnProfileSharedPtr = std::shared_ptr<FluenceProfile>;

////////////////////////////////////////////////////////////////////////////////////
///
enum class DutBinningType {Uniform, Elliptic, Strip};

enum class DutBinningMethod {Uniform, Gradient};

////////////////////////////////////////////////////////////////////////////////////
///
class DutBinning : public std::enable_shared_from_this<DutBinning> {
    
    ///
    friend class Dut;

	protected:
        ///
        std::string m_name;

		///
		Dut* m_owner;

		///
		std::vector<DutBin*> m_dut_bins;

        /// \brief Pointer to the fluence profile associated to the given instantiation of the DutBinning.
        FlnProfileSharedPtr m_fluence_profile = nullptr;

        ///
        std::vector<std::vector<unsigned >> m_dut_pixel_binning_counter;

        ///
		std::vector<double> m_middle_bin_column;

		///
		std::vector<double> m_middle_bin_row;

		///
        static DutBinningType m_default_binning_type;

        ///
        static DutBinningMethod m_default_binning_method;

        ///
        DutBinningType m_binning_type = DutBinningType::Elliptic;

        ///
        DutBinningMethod m_binning_method = DutBinningMethod::Uniform;

        ///
        static unsigned m_default_nbins;

        ///
        unsigned m_nbins;

		///
		virtual void InstantiateDutBinMapping()=0;

		///
		bool m_is_binning_stat_filled = false;

		///
		bool m_is_pixel_stat_filled = false;

		///
		bool m_is_initialized = false;

        ///
        static bool m_export_dutframes_to_csv;

        ///
		void ExportDutFramesToCsv();


	public:
        ///
		DutBinning(Dut* owner, const std::string& name, unsigned nBins);
        // DutBinning(Dut* owner, const std::string& name, unsigned nBins, DutBinningMethod binning_method);

        ///
        virtual ~DutBinning();

		///
		static void DefaultNBins(unsigned val){m_default_nbins = val;}

		///
		static unsigned DefaultNBins(){return m_default_nbins;}

		///
		unsigned NBins() const {return m_nbins;}

		///
		static void DefaultType(std::string type);

        ///
        static void DefaultMethod(std::string type);
        ///
        void SetupFluenceProfile();

        ///
        FlnProfileSharedPtr FluenceProfilePtr() const {return m_fluence_profile;};

		///
		virtual void InitializeDutPitchHitCounter() = 0;

        ///
        virtual void ResetDutPitchHitCounter() = 0;

		///
		static std::string DefaultType();

        ///
        static std::string DefaultMethod();

		///
		std::string Type() const;

        ///
        std::string Method() const; 
        void Method(std::string method); 

		///
		int GetBinNumber(unsigned X, unsigned Y=0);

        ///
        void CountDutBin(unsigned X, unsigned Y=0);

        ///
        void CountDutPixel(unsigned X, unsigned Y);

		///
		void CountDutBin(int binNumber);

        ///
        unsigned BinStatistic(unsigned binNumber);

        ///
        inline bool IsBinningStatFilled() const {return m_is_binning_stat_filled;}

        ///
        inline bool IsPixelStatFilled() const {return m_is_pixel_stat_filled;}

        ///
        void ResetStatistic();

        ///
        void PrintStatistic();

        ///
        inline const std::vector<DutBin*>& DutBins() {return m_dut_bins;}

        ///
        inline const std::vector<std::vector<unsigned >>& PixelBinningCounter() {return m_dut_pixel_binning_counter;}

        ///
        std::vector<double>& GetMiddleBinVector(std::string m_axis);

        ///\brief Initialization of the actual binning being created.
        virtual void Initialize() = 0;

        ///
        inline bool IsInitialized() const { return m_is_initialized; }

        ///
        inline void IsInitialized(bool val) { m_is_initialized = val; }

        ///
        inline Dut* OwnerDut() const { return m_owner;}

        ///
        static bool ExportToCsv() { return m_export_dutframes_to_csv; }

        ///
        static bool ExportToCsv(bool val) { return m_export_dutframes_to_csv = val; }

        ///
        std::string GetName() const { return m_name; }

        ///
        virtual void Draw() const = 0;

};
#endif	// DDUTBINING_H
