#include "DutBinning.h"
#include "Dut.h"
#include "TbGaudi.h"
#include "Globals.h"
#include "FluenceProfile.h"
#include "TbIO.h"
#include "TbLog.h"

////////////////////////////////////////////////////////////////////////////////////
DutBinningType DutBinning::m_default_binning_type  = DutBinningType::Elliptic;

////////////////////////////////////////////////////////////////////////////////////
DutBinningMethod DutBinning::m_default_binning_method = DutBinningMethod::Uniform;

////////////////////////////////////////////////////////////////////////////////////
bool DutBinning::m_export_dutframes_to_csv = false;

////////////////////////////////////////////////////////////////////////////////////
unsigned DutBinning::m_default_nbins = 1; // by default the DUT area is not binned.

////////////////////////////////////////////////////////////////////////////////////
///
#include "TbJob.h"
DutBinning::DutBinning(Dut* owner, const std::string& name, unsigned nBins)
    : m_owner(owner)
    , m_name(name)
    , m_nbins(nBins)
{}

// DutBinning::DutBinning(Dut* owner, const std::string& name, unsigned nBins, DutBinningMethod binning_method)
//     : m_owner(owner)
//     , m_name(name)
//     , m_nbins(nBins)
//     , m_binning_method(binning_method)
// {}
////////////////////////////////////////////////////////////////////////////////////
///
DutBinning::~DutBinning() {
    for (auto ib : m_dut_bins) delete ib;
    m_dut_bins.clear();
}

////////////////////////////////////////////////////////////////////////////////////
///
int DutBinning::GetBinNumber(unsigned X, unsigned Y){
    this->Initialize();
    if(m_nbins==1) return 0;    // booster :)
    for (int i = 0; i < m_nbins ; ++i) {
        if (m_dut_bins.at(i)->This(X, Y))
            return i;
    }
    return -1; // the given pixel was not mapped to any Dut bin
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::DefaultType(std::string type){
    bool flag = false;
    if (type.compare("Elliptic")==0){
        std::cout<< "[INFO]:: DutBinning::Type:: Setting the GLOBAL binning type: Elliptic"<< std::endl;
        DutBinning::m_default_binning_type =  DutBinningType::Elliptic;
        flag=true;
    }
    if (type.compare("Uniform")==0){
        std::cout<< "[INFO]:: DutBinning::Type:: Setting the GLOBAL binning type: Uniform"<< std::endl;
        DutBinning::m_default_binning_type =  DutBinningType::Uniform;
        flag=true;
    }
    if (type.compare("Strip")==0){
        std::cout<< "[INFO]:: DutBinning::Type:: Setting the GLOBAL binning type: Strip"<< std::endl;
        DutBinning::m_default_binning_type =  DutBinningType::Strip;
        flag=true;
    }
    if(!flag){
        std::cout<< "[ERROR]:: DutBinning::Type:: Unrecognized binning type: "<< type << std::endl;
        TbGaudi::RunTimeError("Error in User's analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::DefaultMethod(std::string type){
    bool flag = false;
    if (type.compare("Uniform")==0){
        std::cout<< "[INFO]:: DutBinning::Method:: Setting the GLOBAL binning method: Uniform"<<std::endl;
        DutBinning::m_default_binning_method = DutBinningMethod::Uniform;
        flag = true;
    }
    if (type.compare("Gradient")==0){
        std::cout<< "[INFO]:: DutBinning::Method:: Setting the GLOBAL binning method: Gradient"<<std::endl;
        DutBinning::m_default_binning_method = DutBinningMethod::Gradient;
        flag = true;
    }
    if(!flag){
        std::cout<< "[ERROR]:: DutBinning::Type:: Unrecognized binning method: "<< type << std::endl;
        TbGaudi::RunTimeError("Error in User's analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string DutBinning::DefaultMethod(){
    switch (m_default_binning_method) {
        case DutBinningMethod::Uniform:   return "Uniform";
        case DutBinningMethod::Gradient:  return "Gradient";
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::Method(std::string method){
    if(method=="Uniform") m_binning_method = DutBinningMethod::Uniform;
    else if(method=="Gradient") m_binning_method = DutBinningMethod::Gradient;
} 

////////////////////////////////////////////////////////////////////////////////////
///
std::string DutBinning::Method() const {
    switch (m_binning_method) {
        case DutBinningMethod::Uniform:   return "Uniform";
        case DutBinningMethod::Gradient:  return "Gradient";
        default:
            std::cout<< "[ERROR]:: DutBinning::Method:: Sth went wrong with binnig method initialization!" << std::endl;
            TbGaudi::RunTimeError("Error in User's analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string DutBinning::DefaultType(){
    switch (m_default_binning_type) {
        case DutBinningType::Uniform:   return "Uniform";
        case DutBinningType::Elliptic:  return "Elliptic";
        case DutBinningType::Strip:     return "Strip";
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string DutBinning::Type() const {
    switch (m_binning_type) {
        case DutBinningType::Uniform:   return "Uniform";
        case DutBinningType::Elliptic:  return "Elliptic";
        case DutBinningType::Strip:     return "Strip";
        default:
            std::cout<< "[ERROR]:: DutBinning::Type:: Sth went wrong with binnig type initialization!" << std::endl;
            TbGaudi::RunTimeError("Error in User's analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::SetupFluenceProfile(){
    auto flnType = m_owner->FluenceProfileType();
    auto flnProfilePtr = FluenceProfile::GetProfile(flnType);
    std::cout << "[INFO]:: DutBinning::SetupFluenceProfile: The \""<< flnProfilePtr->Type() << "\""
                " is used for " << m_name << std::endl;
    auto isFlnDefault = flnProfilePtr->IsDefault();
    if(!isFlnDefault)
        m_name+="_DefaultFlnAlignment";
    else
        m_name+="_CustomFlnAlignment";

    m_fluence_profile = flnProfilePtr->Instantiate(this->shared_from_this());
    
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::CountDutBin(int binNumber){
    m_is_binning_stat_filled = true;
    if(binNumber<m_dut_bins.size()){
        m_dut_bins.at(binNumber)->Count();
    }
    else{
        std::cout<< "[ERROR]:: DutBinning:: You want count a bin which doesn't exist"<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::CountDutBin(unsigned X, unsigned Y){
    m_is_binning_stat_filled = true;
    auto binNumber = GetBinNumber(X,Y);
    if(binNumber>=0) CountDutBin(binNumber);
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::CountDutPixel(unsigned X, unsigned Y){
    m_is_pixel_stat_filled = true;

    if(m_dut_pixel_binning_counter.empty())
        InitializeDutPitchHitCounter();

    if(X < m_dut_pixel_binning_counter.size()){
        if(Y<m_dut_pixel_binning_counter.at(X).size()){
            m_dut_pixel_binning_counter.at(X).at(Y)++;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
unsigned DutBinning::BinStatistic(unsigned binNumber){
    if(m_is_binning_stat_filled ) {
        if (binNumber < m_dut_bins.size()) {
            return m_dut_bins.at(binNumber)->Statistic();
        } else {
            std::cout << "[ERROR]:: DutBinning:: You call for statistic from a bin which doesn't exist" << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
            return 0; // make compiler happy
        }
    } else {
        std::cout << "[ERROR]:: DutBinning:: You call for statistic but it was not filled."<<std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return 0; // make compiler happy
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::ResetStatistic(){
    for(auto& bin : m_dut_bins) bin->ResetCounter();
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::PrintStatistic(){
    unsigned bincounter{0};
    for(auto& bin : m_dut_bins){
        std::cout<< "[INFO]:: Statistic:: #"<<++bincounter<<" DUT bin entries : "
                 << bin->Statistic()<<std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<double>& DutBinning::GetMiddleBinVector(std::string m_axis){

    bool getVectorX = m_axis.find("column")!=std::string::npos   ? true : false;
    bool getVectorY = m_axis.find("row")!=std::string::npos      ? true : false;
    if(getVectorX)
        return m_middle_bin_column;
    else if (getVectorY)
        return m_middle_bin_row;
    else {
        std::cout << "[ERROR]:: DutBinning:: Wrong axis specified for GetMiddleBinVector(...) "
                  << "given "<<m_axis<<". You can choose \"column\" or \"row\""<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutBinning::ExportDutFramesToCsv(){
	//
	// TEMP FIXED: This would be different for Strip DUTs ???!!!
	//
	auto getDutBinNumber = [](int x, int y, const DutFramesVector& framesVector){
		int binCounter(0);
		for (const auto& ibinPixFrame : framesVector ){
			auto isThisDutBin = ibinPixFrame.at(x).at(y);
			if(isThisDutBin)
				return binCounter;
			else 
				++binCounter;
		}
		return binCounter;
	};

	auto nPixX = Dut::NPixelsX();
	auto nPixY = Dut::NPixelsY();
	std::cout << "[EXPORT]:: DutBin:: Export Dut Frames To Csv file" << std::endl;
	auto io = TbIO();
	auto output_dir_path = io.GetOutputDirPath();
    auto name = GetName();
    auto file = output_dir_path+"/"+name+".csv";
	std::cout << "[INFO]:: Writing to the file " << file << std::endl;

    auto dutFramesVector = DutBin::m_binned_readout_channels.at(name);
    
    std::string sep = io.GetCsvSeparator();
    
    std::ofstream outFile;
    outFile.open(file.c_str(), std::ios::out);
    outFile << "col"<< sep <<"row"<< sep <<"bin" << std::endl; // file header
    std::ostringstream dutFrame;
    for(int x=0;x<nPixX;++x){
        for(int y=0;y<nPixY;++y){
            dutFrame << x << sep << y << sep << getDutBinNumber(x,y,dutFramesVector) << std::endl;
        }
    }
    outFile << dutFrame.str() << std::endl;
    dutFrame.clear();
    outFile.close();
}
