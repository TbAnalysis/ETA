/*! \brief DutStripBinning class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date December-2018
*/

#ifndef DUT_STRIP_BINNING_H
#define DUT_STRIP_BINNING_H

// std libriaryies
#include <vector>

// project libraries
#include "DutBinning.h"

// ROOT libriaryies
#include "TF2.h"

// TODO:: introduce angle parameterization eg. KIT case (then with pixel_mapping)

class Dut;

////////////////////////////////////////////////////////////////////////////////////
///
class DutStripBinning : public DutBinning {

	private:
		///binnig boundaries:
		std::vector<unsigned> m_bin_boundary_lines; // TODO: refactor this to TLine type

        ///
        void InstantiateDutBinMapping() override;

        ///
        void strips_mapping();

		/// \brief Needs to distinguish 1dim from 2dim in case when the strip like binning
		/// will be on top of pixel like DUT (e.g. KIT irradiation profile for VeloPix)
		static bool m_2dim_mapping;

	public:
        ///
		DutStripBinning(Dut* owner, std::string name, unsigned nBins);

        ///
        virtual ~DutStripBinning();

        ///
        void Initialize();

        ///
        void Set2DimMapping(bool val) { m_2dim_mapping=val; }

        ///
		static bool Is2DimMapping() { return m_2dim_mapping; }

		///
		void InitializeDutPitchHitCounter() override;

        ///
		void ResetDutPitchHitCounter() override;

		/// TODO
        //double BinIntegral();

        /// TODO
        //double BinArea(int bin);

        void Draw() const override;

};
#endif	// DUT_STRIP_BINNING_H
