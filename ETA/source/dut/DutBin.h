/*! \brief DutBin class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef DUTBIN_H
#define DUTBIN_H

#include <vector>
#include <map>

class DutBinning;

// DutReadoutChannelsFrame:: [nPitchX] x [nPitchY]
using DutReadoutChannelsFrame = std::vector<std::vector<unsigned short>>;

// DutFramesVector:: [ nBins] x [DutReadoutChannelsFrame]
using DutFramesVector =  std::vector<DutReadoutChannelsFrame>;

// DutFramesMap:: [ DutName, DutFramesVector]
using DutFramesMap = std::map<std::string,DutFramesVector>;

////////////////////////////////////////////////////////////////////////////////////
///
class DutBin {
	///
	friend class DutBinning;
	
	private:

		/// \brief Unique name for the DutBin instance.
		/// This can be defined e.g. DUT name (e.g. "S8"), binning type (e.g. "Elliptic") and number of bins.
		/// NOTE: It's needed since each bin is added to to the global/static map of DutBins using this name.
		std::string m_name;

		///
		unsigned int m_bin_number;

		///
		DutBinning* m_owner = nullptr;

		/// \brief Map of pixels frame (one for each Dut bin) of pixels constituting given DUT bin.
		/// The pixel vector constituting the given DUT is mapped with the name (defined from the name (e.g. "S8"),
		/// binning type (e.g. "Elliptic") and number of bins.
		static DutFramesMap m_binned_readout_channels;

		///
		static DutReadoutChannelsFrame& ReadoutChannelsFrame(std::string name,unsigned bin);

		///
		static unsigned NReadoutChannels(const std::string& name);

		///
		static std::vector<unsigned short>& Strips(std::string name,unsigned bin);

		///
		unsigned m_statistic_counter;
		
	public:
		DutBin(DutBinning* owner, unsigned int binNumber);
		~DutBin() = default;

		///
		void AddPixel(unsigned X, unsigned Y);

		///
		void AddStrip(unsigned X);

		///
		bool This(unsigned X, unsigned Y=0);

		///
		inline void Count(){++m_statistic_counter;}

		///
		inline void ResetCounter(){m_statistic_counter=0;}

		///
		inline unsigned Statistic(){ return m_statistic_counter;}

		///
		static void PrintListOfDutBinningPixelsMaps();

		///
		void DumpDutBinningMapping() const;
};
#endif	// DUTBIN_H
