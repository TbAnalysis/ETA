#include "DutEllipticBinning.h"
#include "IRRADProfile.h"
#include "TbAnalysis.h"
#include "DutBinning.h"
#include "Globals.h"
#include "TbGaudi.h"
#include "Dut.h"
#include "TbJob.h"

////////////////////////////////////////////////////////////////////////////////////
///
DutEllipticBinning::DutEllipticBinning(Dut* owner, std::string name, unsigned nBins)
    : DutBinning(owner,name,nBins)
    , m_sx(static_cast<double>(Dut::NPixelsX())/2)
    , m_sy(static_cast<double>(Dut::NPixelsY())/2)
    , m_width_factor(2./3.)
{
    std::cout<< "[INFO]:: DutEllipticBinning constructed (name: "<< name << ", nBins: "<<nBins<<")"<<std::endl;
    m_binning_type = DutBinningType::Elliptic;
}

////////////////////////////////////////////////////////////////////////////////////
///
DutEllipticBinning::~DutEllipticBinning(){
	for (auto ie : m_bin_boundary_ellipses) delete ie;
    m_bin_boundary_ellipses.clear();
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutEllipticBinning::Initialize(){
    if(!IsInitialized()){
        std::cout<<"[INFO]:: DutEllipticBinning:: Initialization..."<<std::endl;
        if(m_fluence_profile) {
            //m_fluence_profile->Initialize();
            // NOTE: the elliptical binning make sense only for IRRAD type!
            auto irradProfile = dynamic_cast<IRRADProfile*>(m_fluence_profile.get());
            // verify the fluence profile correctness
            if (!irradProfile) {
                std::cout<<"[ERROR]:: DutEllipticBinning:: Trying to initialize"
                           " the elliptical binning for the profile other than \"IRRAD\"."
                           " It make no sense." << std::endl;
                TbGaudi::RunTimeError("Error in analysis workflow.");
            }

            // redefine binning alignment with the values from the defined profile
            std::cout<<"[INFO]:: DutEllipticBinning:: Taking the ellipses alignemnt based on"
                       " the defined fluence profile"<<std::endl;

            if(irradProfile->IsDefault())
                std::cout<<"[WARNING]:: FluenceProfile:: It seems that the default parameterization is used!"<<std::endl;

            m_sx = irradProfile->GetSx();
            m_sy = irradProfile->GetSy();
            m_width_factor = irradProfile->GetWidthFactor();
        }
        else if (OwnerDut()->IrradiationStatus()==true) {
            std::cout << "[WARNING]:: DUT is configured as irradiated device, however the "
                         "elliptic binning Initialization will be performed "
                         "\nw/o any fluence profile being defined, it means that the "
                         "default parameterization is used." << std::endl;
        }

        std::cout<<"[INFO]:: DutEllipticBinning:: Initialization starts..."<<std::endl;
        
        // Update the method name
        Method(OwnerDut()->ThisTbJobPtr()->GetParam("DutBinningMethod"));
        std::cout<<"[INFO]:: DutEllipticBinning:: Using " << Method() << " method of binning construction."<<std::endl;

        if (m_binning_method == DutBinningMethod::Uniform)
            CreateUniformBinning();
        else
            CreateGradientBinning();

        std::sort (m_middle_bin_column.begin(),m_middle_bin_column.end());
        std::sort (m_middle_bin_row.begin(),m_middle_bin_row.end());
        InstantiateDutBinMapping();
        m_is_initialized = true;
        std::cout<<"[INFO]:: DutEllipticBinning:: Initialization:: DONE."<<std::endl;
        
        //for(int i=0;i<NBins();++i)
        //    m_dut_bins.at(i)->DumpDutBinningMapping();
        
        if(DutBinning::ExportToCsv()) 
            DutBinning::ExportDutFramesToCsv();

        if(m_fluence_profile) {
            if(FluenceProfile::PerformEvaluation()){
                m_fluence_profile->Evaluate();
                if(FluenceProfile::ExportToCsv())
                    m_fluence_profile->ExportFluenceBinningToCsv();
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutEllipticBinning::CreateUniformBinning() {
    double x_offset = fabs(m_sx - m_owner->DutSizeX() / 2.0);
    double y_offset = fabs(m_sy - m_owner->DutSizeY() / 2.0);
    double max_radius = sqrt(pow(x_offset + m_owner->DutSizeX()/2, 2.0) +  pow(y_offset + m_owner->DutSizeY()/2, 2.0)); //Dut::NPixelsY()*Dut::NPixelsY() / 4.0);
    double dx = max_radius/NBins()*m_width_factor;
    double dy = max_radius/NBins();
    double diff = fabs(NBins()*dx - max_radius) * m_width_factor;
    dx += diff/NBins();
    dy += diff/NBins();
    
    double middleBin;
    m_bin_boundary_ellipses.clear();
    m_middle_bin_column.clear();
    m_middle_bin_row.clear();
    for (int ie=1; ie <= NBins(); ie++){
        m_bin_boundary_ellipses.push_back(new TEllipse(m_sx,m_sy, dx*ie, dy*ie, 0., 360., 0.));
        if(ie<2){
            m_middle_bin_column.push_back(m_sx);
            m_middle_bin_row.push_back(m_sy);
        } else {
            // NOTE: add middleBin only if it's localized within DUT area!
            middleBin = m_sx + ie * dx - dx / 2; // x-axis: up from the centre
            if(middleBin < Dut::NPixelsX())
                m_middle_bin_column.push_back(middleBin);
            middleBin = m_sx - ie * dx + dx / 2; // x-axis: down from the centre
            if(middleBin>0)
                m_middle_bin_column.push_back(middleBin);
            middleBin = m_sy + ie * dy - dy / 2;  // y-axis: up from the centre
            if(middleBin<Dut::NPixelsY())
                m_middle_bin_row.push_back(middleBin);
            middleBin = m_sy - ie * dy + dy / 2;  // y-axis: down from the centre
            if(middleBin>0)
                m_middle_bin_row.push_back(middleBin);
        }

        if(TbGaudi::Debug()) {
            std::cout << "[DEBUG]:: #Ellipse " << ie;
            std::cout << " :: Centre: Sx: "<< std::setprecision(4) << m_sx << " Sy: "<< m_sy;
            std::cout << "  Radiuses: Rx: "<< std::setprecision(4) << dx * ie << " Ry: "<< dy * ie << " [pix]"<< std::endl;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutEllipticBinning::CreateGradientBinning() {
    auto irradProfile = dynamic_cast<IRRADProfile*>(m_fluence_profile.get());
    auto profileFunc = irradProfile->GetProfileFunc();
    
    double max_func_value = irradProfile->GetFluence(m_sx, m_sy);
    double normalization_factor = irradProfile->FluenceProfile::GetDosimetryFluence() / max_func_value;
    double x_offset = fabs(m_sx - m_owner->DutSizeX() / 2.0);
    double y_offset = fabs(m_sy - m_owner->DutSizeY() / 2.0);
    double max_radius = sqrt(pow(x_offset + m_owner->DutSizeX()/2, 2.0) +  pow(y_offset + m_owner->DutSizeY()/2, 2.0));
    m_bin_boundary_ellipses.clear();
    m_bin_boundary_ellipses.push_back(new TEllipse(m_sx, m_sy, max_radius, max_radius, 0.0, 360., 0.));
    
    double val_delta = BinIntegral(profileFunc, 1, normalization_factor, 0.0, 360.0)/NBins();
    m_bin_boundary_ellipses.clear();

    double delta = 0.1;
    double dx = delta * m_width_factor;
    double dy = delta;
    double x = m_sx - dx;
    double y = m_sy - dy;
    double integral_value;

    std::function<double (int)> IntegralLambda = [this, &integral_value, &profileFunc, &normalization_factor] (int i) {
                                 integral_value = BinIntegral(profileFunc, i, normalization_factor, 0.0, 360.0); 
                                 return integral_value;
                                };

    for (int i=1; i<NBins(); i++) {
        m_bin_boundary_ellipses.push_back(new TEllipse(m_sx, m_sy, m_sx - x, m_sy-y, 0.0, 360., 0.));
        while (IntegralLambda(i) < val_delta) {
            m_bin_boundary_ellipses.pop_back();
            x-=dx;
            y-=dy;
            m_bin_boundary_ellipses.push_back(new TEllipse(m_sx, m_sy, m_sx - x, m_sy-y,  0., 360., 0.));
        }
        std::cout << "[INFO]:: DutEllipticBinning:: Bin "<< i <<" fluence integral value: " << integral_value << std::endl;
    }
}


////////////////////////////////////////////////////////////////////////////////////
///
void DutEllipticBinning::InstantiateDutBinMapping(){
    std::cout<<"[INFO]:: DutEllipticBinning:: Instantiate the DutBin(s) for " << m_name <<std::endl;
    
    for (int i=0; i < m_nbins; ++i){
        m_dut_bins.emplace_back(new DutBin{this,i});
    }
    
    unsigned bin=0;
    unsigned nEllipses = m_bin_boundary_ellipses.size();
    for (unsigned x = 0; x < Dut::NPixelsX(); x++) {
        for (unsigned y = 0; y < Dut::NPixelsY(); y++) {
            for (int i=nEllipses-1; i>=0;--i){ // check from outside to inside
                if (IsInsideTEllipse(m_bin_boundary_ellipses.at(i), x, y))
                    bin = i;
                else break;
            }
            m_dut_bins.at(bin)->AddPixel(x,y);

        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
double DutEllipticBinning::BinIntegral(TF2& func, int bin, double normalization, double phimin, double phimax) {

    // the scale_factor handles the issue that func my be not normalized to 1
    double func_Xmin, func_Ymin, func_Xmax, func_Ymax;
    func.GetRange(func_Xmin, func_Ymin, func_Xmax, func_Ymax);
    auto scale_factor = func.Integral(func_Xmin, func_Xmax, func_Ymin, func_Ymax);

    auto Npx = func.GetNpx();
    auto Npy = func.GetNpy();

    double fdx = (func_Xmax - func_Xmin)/Npx;
    double fdy = (func_Ymax-func_Ymin)/Npy;
    
    TEllipse* el_low = nullptr;
    TEllipse* el_high= nullptr;

    if (bin > m_bin_boundary_ellipses.size()){
        std::cout<< "[ERROR]::DutEllipticBinning::BinIntegral:: to highest bin given" << std::endl;
        return 0.;
    }

    double x0, y0;
    double r1_low, r2_low, r1_high, r2_high;

    if (bin>1){
        el_low = m_bin_boundary_ellipses[bin-2];
        el_high = m_bin_boundary_ellipses[bin-1];
        r1_low = el_low->GetR1();
        r2_low = el_low->GetR2();
    }
    else{
        el_high = m_bin_boundary_ellipses[bin-1];
        r1_low = 0.;
        r2_low = 0.;
    }

    if (el_high){
        // common centre
        x0 = el_high->GetX1();
        y0 = el_high->GetY1();

        r1_high = el_high->GetR1();
        r2_high = el_high->GetR2();
    }
    else {
        std::cout << "[ERROR]:: DutEllipticBinning::BinIntegral: Couldn't find bin elliptical boundaries!" <<std::endl;
        return 0.;
    }

    double integral_low = 0.;
    double integral_high = 0.;

    double y, x;
    x = func_Xmin+1;
    while (x < Npx){
        y = func_Ymin+1;
        while (y < Npy){
            Double_t p[2] = {x,y};
            if ( func.IsInside(p)){
                double phi = TMath::ATan2(y - y0, x - x0)*180./TMath::Pi();
                if (phi > phimin && phi < phimax ){
                    if (el_low && IsInsideTEllipse(el_low, x, y) )
                            integral_low+=(func.Eval(x,y))*normalization/scale_factor;
                    if (el_high && IsInsideTEllipse(el_high, x, y) )
                        integral_high+=(func.Eval(x,y))*normalization/scale_factor;
                }
            }
            y+=fdy;
        }
        x+=fdx;
    }
    auto integral = integral_high - integral_low;
    return integral;
}

////////////////////////////////////////////////////////////////////////////////////
///
double DutEllipticBinning::BinArea(int bin, double phimin, double phimax){
    // NOTE: The area can't be simply calculated from the formula
    // A = Pi * r1 * r2
    // because part of the elliptical bin may be placed outside the DUT area!
    // It's being done by: A = number_of_pixels * pix_area

    // per pixel step
    double fdx = 1.;
    double fdy = 1.;
    double pix_area = 0.055*0.055; // mm^2; // TODO: to be imported from global parameter


    TEllipse* el_low = nullptr;
    TEllipse* el_high= nullptr;

    if (bin > m_bin_boundary_ellipses.size()){
        std::cout<< "[ERROR]::DutEllipticBinning::BinArea:: to highest bin given" << std::endl;
        return 0.;
    }

    double x0, y0;

    if (bin>1){
        el_low = m_bin_boundary_ellipses[bin-2];
        el_high = m_bin_boundary_ellipses[bin-1];
    }
    else{
        el_high = m_bin_boundary_ellipses[bin-1];
    }

    if (el_high){
        // common centre
        x0 = el_high->GetX1();
        y0 = el_high->GetY1();
    }

    double x = 1.;
    double y = 1.;

    int pix_counter_low = 0;
    int pix_counter_high = 0;

    while (x<= 256.){     // ToDo:: loop inside the box serrounding the ellipse (?)
        y = 1.;
        while (y<= 256){
            double phi = TMath::ATan2(y - y0, x - x0)*180./TMath::Pi();
            if (phi > phimin && phi < phimax ){
                if (el_low){
                    if ( IsInsideTEllipse(el_low, x, y) ){
                        ++pix_counter_low;
                    }}
                if (el_high){
                    if ( IsInsideTEllipse(el_high, x, y) ){
                        ++pix_counter_high;
                    }}
            }
            y+=fdy;
        }
        x+=fdx;
    }
    auto nBinPix = pix_counter_high - pix_counter_low;
    auto areaBinPix = nBinPix*pix_area; 
    //std::cout << "[DEBUG]:: Bin "<< bin << " area: " << areaBinPix   <<std::endl;
    return areaBinPix; // [mm^2]
}

////////////////////////////////////////////////////////////////////////////////////
///
double DutEllipticBinning::GetDutCentreX(){
    return m_bin_boundary_ellipses.at(0)->GetX1();
}

////////////////////////////////////////////////////////////////////////////////////
///
double DutEllipticBinning::GetDutCentreY(){
    return m_bin_boundary_ellipses.at(0)->GetY1();
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutEllipticBinning::InitializeDutPitchHitCounter(){
    if(m_dut_pixel_binning_counter.empty()) {
        for (int x = 0; x < Dut::NPixelsX(); x++) {
            std::vector<unsigned> row;
            for (int y = 0; y < Dut::NPixelsY(); y++)
                row.emplace_back(unsigned(0));
            m_dut_pixel_binning_counter.push_back(row);
        }
    } else {
        ResetDutPitchHitCounter();
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutEllipticBinning::ResetDutPitchHitCounter(){
    if(!m_dut_pixel_binning_counter.empty()) {
        for (int x = 0; x < Dut::NPixelsX(); x++) {
            for (int y = 0; y < Dut::NPixelsY(); y++)
                m_dut_pixel_binning_counter.at(x).at(y) = 0;
        }
    } else {
        InitializeDutPitchHitCounter();
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
#include "TCanvas.h"
void DutEllipticBinning::Draw() const {
    auto canvas = new TCanvas(TString(m_name),TString(m_name),650,650);
    auto pad = canvas->cd();
    pad->Range(0,0,255,255);
    for(auto v : m_bin_boundary_ellipses){
        v->Print();
        v->SetFillStyle(0);
        v->SetLineColor(2);
        v->SetLineWidth(2);
        v->Draw();
	}
}