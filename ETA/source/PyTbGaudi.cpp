#include <pybind11/pybind11.h>
#include "TbGaudi.h"

namespace py = pybind11;

void PyTbGaudi(py::module &m) {
    py::class_<TbGaudi, std::unique_ptr<TbGaudi, py::nodelete>>(m, "TbGaudi")
    .def_static("GetInstance", &TbGaudi::GetInstance, py::return_value_policy::reference)
    .def_static("Severity", &TbGaudi::Severity)
    .def_static("Verbose", &TbGaudi::Verbose)
    .def_static("Debug", &TbGaudi::Debug)
    .def_static("Silent", &TbGaudi::Silent)
    .def_static("TB_BATCH", py::overload_cast<>(&TbGaudi::TB_BATCH))
    .def_static("TB_BATCH", py::overload_cast<bool>(&TbGaudi::TB_BATCH))
    .def_static("ProjectLocation", &TbGaudi::ProjectLocation)
    .def_static("RunTimeError", &TbGaudi::RunTimeError)
    .def_static("PrintBanner", &TbGaudi::PrintBanner);
}
