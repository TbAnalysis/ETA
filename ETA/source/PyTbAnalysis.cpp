#include <pybind11/pybind11.h>
#include "TbAnalysis.h"

namespace py = pybind11;


void PyTbAnalysis(py::module &m) {
    py::class_<TbAnalysis>(m, "TbAnalysis")
    .def(py::init<>())
    .def("TbPlotterPtr", &TbAnalysis::TbPlotterPtr)
    .def("FillTbJobsCollection", &TbAnalysis::FillTbJobsCollection)
    .def("GetTbJobsCollection", &TbAnalysis::GetTbJobsCollection);
}

