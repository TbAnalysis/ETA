#include <pybind11/pybind11.h>

namespace py = pybind11;


void PyRunNode(py::module &m);
void PyRunSvc(py::module &m);
void PyDutNode(py::module &m);
void PyDutSvc(py::module &m);
void PyTbJobNode(py::module &m);
void PyRootSvc(py::module &m);
void PyMTRootSvc(py::module &m);
void PyRooFitSvc(py::module &m);
void PyMTRooFitSvc(py::module &m);
void PyTbJob(py::module &m);
void PyTbPlotter(py::module &m);
void PyTbAnalysis(py::module &m);
void PyTbGaudi(py::module &m);


PYBIND11_MODULE(PyEta, m) {

    //The order matters and it's determined by hierarchy of inheritance

    PyRunNode(m);
    PyRunSvc(m);
    PyDutNode(m);
    PyDutSvc(m);
    PyTbJobNode(m);
    PyRootSvc(m);
    PyMTRootSvc(m);
    PyRooFitSvc(m);
    PyMTRooFitSvc(m);
    PyTbJob(m);
    PyTbPlotter(m);
    PyTbAnalysis(m);
    PyTbGaudi(m);
}

