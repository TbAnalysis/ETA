//
// Created by brachwal on 08.10.18.
//

#include "TbJobCollector.h"
#include "TbDataHandler.h"
#include "DutBinning.h"
#include "TbGaudi.h"
#include "TbJob.h"
#include <memory>

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobCollector::FillTbJobsCollection(const std::string& dataCollectionName){
    // Define the obligatory parameters needed to the TbJob instantiaion,
    // these have to be delivered by the user input data:
    JobData::AddObligatoryParameter<std::string>("DutName");
    JobData::AddObligatoryParameter<unsigned>("RunNumber");
    JobData::AddObligatoryParameter<double>("Voltage");     // internaly defined as "RunBiasVoltage"

    // Parse the user input
    auto tbdata_handler = std::make_unique<TbDataHandler>();
    auto ana_collection = tbdata_handler->GetJobDataCollection(TbData::TbDBFile(),dataCollectionName);

    // TbJob instantiation based on the parsed user data
    for(const auto& job_data : ana_collection){
        AddTbJob(job_data);
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobCollector::AddTbJob(const JobData& job_data){
    std::cout<< "[INFO]:: TbJobCollector::Adding new TbJob... " << std::endl;
    // DEBUG: job_data.PrintDetails();

    // Create and fill all details of TbJob instance
    auto tbJob = std::make_shared<TbJob>();

    // Once the optional keys can be find which are predefined in a sense that they define the TbJob instance
    // NOTE: Hence for single testbeam run the different schemes can be defined for the analysis.
    //       If the predefined keys are not found in the TbDB file the default configuration is used.
    auto userParameters = job_data.GetParametersAsStringCollection();
    for(const auto& param : userParameters){
        // Some parameter labels has to be translated to native TbJob definition keys
        // - in other side - the acronims or abreviations for different labels can be
        // defined here.
        if(param.first == "Voltage"){
            tbJob->SetParam("RunBiasVoltage", param.second);
            continue;
        }
        if(param.first == "Binning"){
            tbJob->SetParam("DutBinningType", param.second);
            continue;
        }
        if(param.first == "NBins"){
            tbJob->SetParam("DutNBins", param.second);
            continue;
        }
        if(param.first == "ClusterSize" || param.first == "clSize"){
            tbJob->SetParam("RunClusterSize", param.second);
            continue;
        }
        if(param.first == "FluenceProfile"){
            tbJob->SetParam("DutFluenceProfileType", param.second);
            continue;
        }
        tbJob->SetParam(param.first, param.second);
    }

    // Initialize internally and add the newly created TbJob instance to the collection!
    TbJob::CurrentTbJob(tbJob); // this is obsolete, however have to be checked if could be removed
    tbJob->Initialize();
    
    // Set the physics data file for this job
    auto filePath = job_data.GetDataPath();
    auto fileName = job_data.GetDataFile();
    switch(TbData::Type()){
        case DataType::ROOT:
            auto data_ptr = tbJob->DataPtr<RootData>();
            data_ptr->SetDataFile(filePath+fileName);
            data_ptr->LinkTTree(job_data.GetDataTTree());
            break;
        //case DataType::RDF:
            // TODO
            //break;
        //case DataType::ECS:
            // TODO
            //break;
    }

    // Add new TbJob to the main container
    m_tbjobs.push_back(tbJob);
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobCollector::AddTbJob(TbJobSharedPtr tbJob){
    TbJob::CurrentTbJob(tbJob);
    tbJob->Initialize();
    m_tbjobs.push_back(tbJob);
    std::cout<< "[INFO]:: TbJobCollector::Added new TbJob:: " << std::endl;
    if(TbGaudi::Debug())
        tbJob->PrintAllInfo();
    else
        tbJob->PrintInfo();
}

////////////////////////////////////////////////////////////////////////////////////
///
TbJobSharedPtr TbJobCollector::GetTbJob(std::string runNumber,std::string runIdentifier){
    for(auto irun : m_tbjobs){
        if(runIdentifier.empty()) {
            if (irun->GetParam("RunNumber").compare(runNumber) == 0) return irun;
        }
        else {
            if (irun->GetParam("RunNumber").compare(runNumber) == 0){
                std::size_t separator = runIdentifier.find("::");
                if(separator== std::string::npos)
                    TbGaudi::RunTimeError("GetTbJob::Couldn't recognize the separator in runIdentifier, you should use'::'");
                auto key = runIdentifier.substr(0, separator);
                auto val = runIdentifier.substr(separator+2, runIdentifier.length());
                auto info = irun->GetParam(key);
                if(info.empty())
                    TbGaudi::RunTimeError("GetTbJob::The specified run identifier key ("+key+") was not found.");
                if(info.compare(val)==0)
                    return irun;
            }
        }
    }
    return nullptr;

}