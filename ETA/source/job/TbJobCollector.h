//
// Created by brachwal on 24.09.18.
//

#ifndef TBJOBCOLLECTOR_H
#define TBJOBCOLLECTOR_H

// project libraries
#include "Globals.h"
#include "JobData.h"

class TbJob;
using TbJobSharedPtr = std::shared_ptr<TbJob>;
using TbJobCollection = std::vector<TbJobSharedPtr>;

////////////////////////////////////////////////////////////////////////////////////
///
class TbJobCollector {
    private:

        TbJobCollector() = default;

        ~TbJobCollector() = default;

        // Delete the copy and move constructors
        TbJobCollector(const TbJobCollector&) = delete;
        TbJobCollector& operator=(const TbJobCollector&) = delete;
        TbJobCollector(TbJobCollector&&) = delete;
        TbJobCollector& operator=(TbJobCollector&&) = delete;

        /// \brief List of test beam runs to be analysed in the main loop.
        TbJobCollection m_tbjobs;

    public:

        static TbJobCollector* GetInstance(){
            static TbJobCollector instance;
            return &instance;
        }
        void FillTbJobsCollection(const std::string& dataCollectionName=std::string());

        ///
        void AddTbJob(TbJobSharedPtr tbJob);

        ///
        void AddTbJob(const JobData& job_data);

        ///
        inline const TbJobCollection& TbJobs(){ return m_tbjobs; }

        // NOTE: The following call is ambiguous if there are different TbJobs with the same
        //       run number (e.g. but with different binning type, or clSize),
        //       Needs to define the method with additional runIdentifier:
        // it can be any same flag as defined by the user in the *.dat file, w/o prefixes, eg.
        // "BinningType::Elliptic"
        // if the specified flag do not exists (was not read-in from *.dat file) the error will be thrown
        TbJobSharedPtr GetTbJob(std::string runNumber, std::string runIdentifier=std::string());

};

#endif //TBJOBCOLLECTOR_H
