#include "Globals.h"
#include "TbJob.h"
#include "TbJobIO.h"
#include "DutPix.h"
#include "DutIntraPix.h"
#include "DutStrip.h"
#include "DutIntraStrip.h"
#include "TbJobPlotter.h"
#include "RdfData.h"
#include "EcsData.h"

////////////////////////////////////////////////////////////////////////////////////
///
std::shared_ptr<TbJob> TbJob::m_current_tbjob = nullptr;

TbJob::~TbJob(){
    //ClearEventLoopCollectedData(); // TODO uncomment this after making the fla is_dumped working.
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::SetJobTools(){
    if(!m_plotter)
        m_plotter = std::make_shared<TbJobPlotter>(this->shared_from_this());
    if(!m_io)
        m_io = std::make_shared<TbJobIO>(this->shared_from_this());
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::Initialize() {

    std::cout << "[INFO]:: TbJob::Initialize() ..." <<std::endl;

    AcknowledgeStudiesConfiguration();

    // create and initialize a new Dut
    if(Dut::PixStudy())
        m_dut = std::make_shared<DutPix>();
    else if (Dut::IntraPixStudy())
        m_dut = std::make_shared<DutIntraPix>();
    else if (Dut::StripStudy())
        m_dut = std::make_shared<DutStrip>();
    else if (Dut::IntraStripStudy())
        m_dut = std::make_shared<DutIntraStrip>();
    m_dut->Initialize(this->shared_from_this());

    // create and initialize a new Run
    m_run = std::make_shared<Run>();
    m_run->Initialize(this->shared_from_this()); 

    // Set this TbJob ptr to services
    RunSvc::ThisRunPtr(m_run);
    DutSvc::ThisDutPtr(m_dut);
    RootSvc::ThisTbJobPtr(this->shared_from_this());
    RooFitSvc::ThisTbJobPtr(this->shared_from_this());

    // Instantiate the data object
    switch(TbData::Type()){
        case DataType::ROOT:
            m_data = std::make_unique<RootData>();
            break;
        case DataType::RDF:
            m_data = std::make_unique<RdfData>();
            break;
        case DataType::ECS:
            m_data = std::make_unique<EcsData>();
            break;
    }

    SetJobTools();

    std::cout << "[INFO]:: TbJob::Initialization:: DONE" <<std::endl;

}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbJob::AcknowledgeStudiesConfiguration() const {
    unsigned count{0};
    if(Dut::PixStudy()) ++count;
    if(Dut::IntraPixStudy()) ++count;
    if(Dut::StripStudy()) ++count;
    if(Dut::IntraStripStudy()) ++count;
    if(count != 1){
        std::cout << "[ERROR]:: TbJob:: RatifyStudies:: "
                     " There is wrong number of DUT study type set." << std::endl;
        std::cout << "[INFO]:: You should set only one of the following:\n"
                     "\t(1) Dut::PixStudy(true) or\n"
                     "\t(2) Dut::IntraPixStudy(true) or\n"
                     "\t(3) Dut::StripStudy(true) or\n"
                     "\t(4) Dut::IntraStripStudy(true)" << std::endl;
        TbGaudi::RunTimeError("Ambiguous analysis setup.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::SetParam(const std::string& name, const std::string& value){

    // Check if insertion is successful or not, if not replace the previous value
    if (m_config_map.insert(std::make_pair(name, value)).second == false) {
         std::cout << "[WARNING]:: TbJob::SetParam:: The " << name << " value replaced already existed one."
                   << "\"" << m_config_map[name] << "\" ==> \"" << value << "\"" << std::endl;
        m_config_map[name] = value;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///

template <> const int TbJob::GetParam(const std::string& name) const {
	auto config = GetParam(name);
	return std::stoi(config);
}

template <> const unsigned TbJob::GetParam(const std::string& name) const {
	auto config = GetParam(name);
	return static_cast<unsigned>(std::stoi(config));
}

template <> const float TbJob::GetParam(const std::string& name) const {
	auto config = GetParam(name);
	return std::stof(config);
}

template <> const double TbJob::GetParam(const std::string& name) const {
	auto config = GetParam(name);
	return std::stod(config);
}

template <> const bool TbJob::GetParam(const std::string& name) const {
	auto config = GetParam(name);
	return stob(config);
}
////////////////////////////////////////////////////////////////////////////////////
///
bool TbJob::LoadTbNTuple(const std::string fullPathToNTuple, std::map<std::string,std::vector<std::string>> ttree_tbranches_map){
    std::cout<< "[INFO]:: TbJob loading the test-beam data stored in the NTuple: " << std::endl;
    std::cout<< "[INFO]:: " << fullPathToNTuple << std::endl;

    if(m_data){
        std::cout<< "[WARNING]:: The ROOT data already loaded for this run. Deleting it." << std::endl;
        m_data.reset();
    }
    m_data = std::make_unique<RootData>(fullPathToNTuple, ttree_tbranches_map);
    return true;
}

////////////////////////////////////////////////////////////////////////////////////
/// Link to the TFile and assign the corresponding TTree pointer
bool TbJob::LinkTbTTree(const std::string fullPathToNTuple, std::vector<std::string> ttrees){
    if(m_data){
        std::cout<< "[WARNING]:: The ROOT data already loaded for this run. Deleting it." << std::endl;
        m_data.reset();
    }
    m_data = std::make_unique<RootData>(fullPathToNTuple);
    dynamic_cast<RootData*>(m_data.get())->LinkTTrees(ttrees);
    std::cout<< "[INFO]:: TbJob::LinkTbTTree:: The ROOT data linked with the TTrees:";
    for(const auto& itree : ttrees) std::cout<< " " << itree; std::cout<<std::endl;
    return true;
}

////////////////////////////////////////////////////////////////////////////////////
///
bool TbJob::AddTbEcsFrame(const std::string& fullPathToEcsFrameFile){
    if(!m_data) m_data = std::make_unique<EcsData>();
    dynamic_cast<EcsData*>(m_data.get())->AddFrame(fullPathToEcsFrameFile);
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::PrintInfo() const {
    std::cout << std::endl;
    std::cout << "[INFO]:: >>  DUT: " << GetParam("DutName") << " run: " <<GetParam("RunNumber") << std::endl;
    std::cout << std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::PrintAllInfo() const { // todo: rename to PrintConfig()
    std::cout << std::endl;
    //PrintDutInfo();
    //PrintRunInfo();
    for(auto& config : m_config_map )
        std::cout << "[INFO]:: >> " << config.first << " :: " << config.second << std::endl;
    std::cout << std::endl;

}

////////////////////////////////////////////////////////////////////////////////////
///
const std::string TbJob::Label(std::string type) const {
    if(type.compare("JOB")==0){
        auto clSize = GetParam<int>("RunClusterSize");
        if(clSize<0)
            return m_dut->Label()+"_"+m_run->Label();
        else
            return m_dut->Label()+"_clS"+GetParam("RunClusterSize")+"_"+m_run->Label();
    }
    else if(type.compare("DUT")==0) return m_dut->Label();
    else if (type.compare("RUN")==0) return m_run->Label();
    else {
        std::cout << "[ERROR]:: TbJob:: DutLabel:: Trying to extract label of type ("<<type<<") which do not exists!"
                     " You can choose \"JOB\", \"DUT\" or \"RUN\" " << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    return std::string();
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string TbJob::Title() {
    std::string title;
    title+=GetParam("DutName");
    title+=" "+GetParam("RunNumber");
    title+=" "+GetParam("RunBiasVoltage")+"[V]";
    title+=" clS"+GetParam("RunClusterSize");
    return title;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJob::ClearEventLoopCollectedData(){
    if(TbJobIO::ExportEventLoopDataToNTuple())
        m_io->DumpEventLoopDataToNTuple();
    if(TbJobIO::ExportEventLoopDataToPdf())
        m_io->DumpEventLoopDataToPdf();
    RootSvc::ClearAllDutHist();
    RooFitSvc::ClearAllRooDataSet();
}

////////////////////////////////////////////////////////////////////////////////////
///
DutPixSharedPtr TbJob::DutPixPtr(){
    if(Dut::IntraPixStudy()){
        return std::dynamic_pointer_cast<DutPix>(m_dut);
    }
    else {
        std::cout << "[ERROR]:: TbJob:: DutPixPtr:: Trying to cast Dut to DutPix,"
                  <<"but the current analysis type is "<< Dut::StudyType() << std::endl;
        std::cout << "[INFO]:: You should call Dut::IntraPixStudy(true) in you analysis code."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}