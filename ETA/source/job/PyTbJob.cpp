#include <pybind11/pybind11.h>
#include <string>
#include "TbGaudi.h"
#include "TbJob.h"
#include "RunSvc.h"
#include "RunNode.h"

namespace py = pybind11;

void PyTbJob(py::module &m) {
    py::class_<TbJob, RunSvc, DutSvc, MTRootSvc, MTRooFitSvc, std::shared_ptr<TbJob>>(m, "TbJob", py::multiple_inheritance())
    .def(py::init<>())
    .def("SetParam", &TbJob::SetParam)
    .def("GetParam", &TbJob::GetParam<std::string>)
    .def("GetParam", &TbJob::GetParam<int>)
    .def("GetParam", &TbJob::GetParam<unsigned>)
    .def("GetParam", &TbJob::GetParam<float>)
    .def("GetParam", &TbJob::GetParam<double>)
    .def("GetParam", &TbJob::GetParam<bool>);
    
}