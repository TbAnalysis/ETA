/*! \brief TbJob class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date May-2018
*/

#ifndef TB_JOB_H
#define TB_JOB_H

#include "TbGaudi.h"
#include "DutSvc.h"
#include "RunSvc.h"
#include "MTRootSvc.h"
#include "MTRooFitSvc.h"
#include "RdfData.h"
#include "EcsData.h"
#include <type_traits>

/// TODO:: TbJob::ReduceHotPixels(true);

class TbJobIO;
class TbJobPlotter;
using TbJobIOSharedPtr = std::shared_ptr<TbJobIO>;
using TbJobPlotterSharedPtr = std::shared_ptr<TbJobPlotter>;
using ConfigMap = std::map<std::string,std::string>;

////////////////////////////////////////////////////////////////////////////////////
///
class TbJob: public RunSvc,
             public DutSvc,
             public MTRootSvc, 		// -> RootSvc
             public MTRooFitSvc,	// -> RooFitSvc
			 public std::enable_shared_from_this<TbJob> {

	private:

		///
		TbDataUniquePtr m_data;

		///
        DutSharedPtr m_dut;

		///
		RunSharedPtr m_run;

		///
		TbJobPlotterSharedPtr m_plotter;

		///
		TbJobIOSharedPtr m_io;

		//
		ConfigMap m_config_map;

		///
		static std::shared_ptr<TbJob> m_current_tbjob;

		///
		bool AcknowledgeStudiesConfiguration() const;

	public:
        ///
		TbJob() = default;

        ///
		~TbJob();

		/// \brief Set the current TbJob within any processing loop
		static void CurrentTbJob(std::shared_ptr<TbJob> run) {m_current_tbjob = run;}

        /// \brief Get the current TbJob within any processing loop
		static std::shared_ptr<TbJob> CurrentTbJob() {return m_current_tbjob;}

		///
		void SetParam(const std::string& name, const std::string& value);

		///
		template <typename T=std::string> const T GetParam(const std::string& name) const;

		///
		void SetJobTools();

		///
		void Initialize();

		///
        DutSharedPtr DutPtr(){ return m_dut; }

        ///
		DutPixSharedPtr DutPixPtr(); // works for Dut::IntraPixStudy(true)

        ///
        RunSharedPtr RunPtr(){ return m_run; }

	    /// \brief Link the test-beam data stored in the NTuple to the RDataFrame
		/// \param fullPathToNTuple the full path to the file (file name and its extension included).
		/// \param ttree_tbranches_map Map of the TTree names and corresponding container of default TBranches.
		bool LoadTbNTuple(const std::string fullPathToNTuple, std::map<std::string,std::vector<std::string>> ttree_tbranches_map);

        /// \brief Link the test-beam data stored in the NTuple to the list of pointers to the TTree objects
        /// \param fullPathToNTuple the full path to the file (file name and its extension included).
        /// \param ttrees list of TTree names (existing in the NTuple)
        bool LinkTbTTree(const std::string fullPathToNTuple, std::vector<std::string> ttrees);

        ///
        bool AddTbEcsFrame(const std::string& fullPathToEcsFrameFile);

		/// \brief Get the TTree pointer for a given TbJob
		/// \param ttreeName the TTree name of interest.
		//TTree* GetTTree(const std::string ttreeName) const;

		///
		void PrintInfo() const;

		///
		void PrintAllInfo() const;

		/// \brief Unique label for a given job or its components
		/// \param type: JOB, DUT, RUN
		const std::string Label(const std::string type) const;

		///
		std::string Title();

		template <typename T = RootData>
		T* DataPtr(const std::string& name = std::string());

		///
		RootData* RootDataPtr() {return DataPtr<RootData>(); }

		///
		RdfData* RdfDataPtr() {return DataPtr<RdfData>(); }

		///
		EcsData* EcsDataPtr() {return DataPtr<EcsData>(); }

		///
		std::string RootDataFile() const {return m_data->GetDataFile(); }

        ///
		void ClearEventLoopCollectedData();

		///
		TbJobPlotterSharedPtr PlotterPtr() const { return m_plotter; }

		///
		TbJobIOSharedPtr TbIOPtr() const {return m_io;}

};

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T=std::string>
const T TbJob::GetParam(const std::string& name) const {
	std::string config = "-999";
	auto search = m_config_map.find(name);
    if(search != m_config_map.end())
        config = search->second;
	return config;
}
template <> const int TbJob::GetParam(const std::string& name) const;
template <> const unsigned TbJob::GetParam(const std::string& name) const;
template <> const float TbJob::GetParam(const std::string& name) const;
template <> const double TbJob::GetParam(const std::string& name) const;
template <> const bool TbJob::GetParam(const std::string& name) const;


////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
T* TbJob::DataPtr(const std::string& name) {
	return dynamic_cast<T*>(m_data.get());
}

#endif	// TB_JOB_H