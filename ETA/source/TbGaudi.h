/*! \brief TbGaudi factory declaration.
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date March-2018
*/

#ifndef TBGAUDI_H
#define TBGAUDI_H

// project libriaryies
#include "Globals.h"
#include <algorithm>
#include <iostream>
#include <string>

//class TbStyle;

////////////////////////////////////////////////////////////////////////////////////
/// Definition of the Messaging levels
enum class tSeverity {Silent, Verbose, Debug};

////////////////////////////////////////////////////////////////////////////////////
/// The TbGaudi singleton class definition.
class TbGaudi{
	private:
		TbGaudi();
		~TbGaudi() = default;

		// Delete the copy and move constructors
		TbGaudi(const TbGaudi&) = delete;
		TbGaudi& operator=(const TbGaudi&) = delete;
		TbGaudi(TbGaudi&&) = delete;
		TbGaudi& operator=(TbGaudi&&) = delete;

		///
		//TbStyle* m_style;

		///
		static tSeverity m_Severity;
		///
		static bool m_BATCH;

		///
		static std::string m_project_location;

	public:
		static TbGaudi* GetInstance(){
			static TbGaudi instance;
  		return &instance;
		}

		static void Severity(const std::string val){
		    if(val.compare("Debug")==0 || val.compare("DEBUG")==0) m_Severity = tSeverity::Debug;
		    if(val.compare("Verbose")==0 || val.compare("VERBOSE")==0) m_Severity = tSeverity::Verbose;
		    if(val.compare("Silent")==0 || val.compare("SILENT")==0) m_Severity = tSeverity::Silent;
		}

	    ///
	    static bool Verbose(){ return m_Severity==tSeverity::Verbose ? true : TbGaudi::Debug(); }

        ///
        static bool Debug(){ return m_Severity==tSeverity::Debug ? true : false; }

        ///
        static bool Silent(){ return m_Severity==tSeverity::Silent ? true : false; }


		///
		static bool TB_BATCH(){ return m_BATCH; }

		///
		static void TB_BATCH(bool val){ m_BATCH = val; }

        ///
        static std::string ProjectLocation() {return m_project_location;}

		///
		static void RunTimeError(const std::string& error){ throw std::runtime_error(error); }

		///
		static void PrintBanner(std::string type, std::string text);

};
#endif	// TBGAUDI_H

#define gTbGaudi TbGaudi::GetInstance()
