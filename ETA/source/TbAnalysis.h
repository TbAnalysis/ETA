/*! \brief TbAnalysis class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef TB_ANALYSIS_H
#define TB_ANALYSIS_H

#include <memory>
#include <vector>
#include <map>

#include "TbJob.h"
#include "TbJobIO.h"
#include "TbPlotter.h"
#include "FluenceProfile.h"

using TbPlotterSharedPtr = std::shared_ptr<TbPlotter>;

////////////////////////////////////////////////////////////////////////////////////
///
class TbAnalysis {
	private:
		///
		static bool m_save_control_plots;

		///
		TbPlotterSharedPtr m_plotter = std::make_shared<TbPlotter>();

	public:
		///
		TbAnalysis() = default;

		///
		virtual ~TbAnalysis() = default;

		///
		TbPlotterSharedPtr TbPlotterPtr() const {return m_plotter;}

		/// \brief Simple wrapper to TbJobCollector::LoadData()
		bool FillTbJobsCollection(const std::string& collectionName=std::string());

        /// \brief Simple wrapper to TbJobCollector::TbJobs()
		const std::vector<std::shared_ptr<TbJob>>& GetTbJobsCollection();
};
#endif	// TB_ANALYSIS_H