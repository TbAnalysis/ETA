//
// Created by brachwal on 05.08.20.
//
#include "DutNode.h"
#include "Dut.h"
#include "TbGaudi.h"
#include <string>

////////////////////////////////////////////////////////////////////////////////////
///
DutNode::DutNode(DutSharedPtr dut_ptr):m_dut(dut_ptr) { }

////////////////////////////////////////////////////////////////////////////////////
///
void DutNode::ThisDutPtr(DutSharedPtr dut_ptr) {
    m_dut = dut_ptr;
}

////////////////////////////////////////////////////////////////////////////////////
///
DutSharedPtr DutNode::ThisDutPtr() const {
    return m_dut;
}

////////////////////////////////////////////////////////////////////////////////////
///
void DutNode::DutPtrNotInitializedError(const std::string& caller) const {
    if (!ThisDutPtr()) {
        std::cout << "[ERROR]:: DutNode ptr has been called";
        if (!caller.empty()) std::cout << " (by the " << caller << ")";
        std::cout << " but it was not initialized before!" << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}