//
// Created by brachwal on 15.06.19.
//

#ifndef RUNNODE_H
#define RUNNODE_H

#include <memory>

class Run;
using RunSharedPtr = std::shared_ptr<Run>;

class RunNode {
    private:
        ///
        RunSharedPtr m_run = nullptr;

    public:
        ///
        RunNode() = default ;

        ///
        RunNode(RunSharedPtr run_ptr);

        ///
        virtual ~RunNode() = default;

        ///
        void ThisRunPtr(RunSharedPtr run_ptr);

        ///
        RunSharedPtr ThisRunPtr() const;

        ///
        void RunPtrNotInitializedError(const std::string& caller=std::string()) const;
};

#endif //RUNNODE_H
