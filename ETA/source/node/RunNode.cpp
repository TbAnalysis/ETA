//
// Created by brachwal on 05.08.20.
//
#include "RunNode.h"
#include "Run.h"
#include "TbGaudi.h"
#include <string>

////////////////////////////////////////////////////////////////////////////////////
///
RunNode::RunNode(RunSharedPtr run_ptr):m_run(run_ptr) { }

////////////////////////////////////////////////////////////////////////////////////
///
void RunNode::ThisRunPtr(RunSharedPtr run_ptr) {
    m_run = run_ptr;
}

////////////////////////////////////////////////////////////////////////////////////
///
RunSharedPtr RunNode::ThisRunPtr() const {
    return m_run;
}

////////////////////////////////////////////////////////////////////////////////////
///
void RunNode::RunPtrNotInitializedError(const std::string& caller) const {
    if (!ThisRunPtr()) {
        std::cout << "[ERROR]:: RunNode ptr has been called";
        if (!caller.empty()) std::cout << " (by the " << caller << ")";
        std::cout << " but it was not initialized before!" << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}