#include <pybind11/pybind11.h>
#include "TbJobNode.h"

namespace py = pybind11;

void PyTbJobNode(py::module &m) {
    py::class_<TbJobNode, std::shared_ptr<TbJobNode>>(m, "TbJobNode")
    .def(py::init<>());
}