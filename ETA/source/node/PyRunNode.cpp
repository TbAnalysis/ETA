#include <pybind11/pybind11.h>
#include "RunNode.h"

namespace py = pybind11;

void PyRunNode(py::module &m) {
    py::class_<RunNode, std::shared_ptr<RunNode>>(m, "RunNode")
    .def(py::init<>());
}