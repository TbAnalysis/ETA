#include <pybind11/pybind11.h>
#include "DutNode.h"

namespace py = pybind11;

void PyDutNode(py::module &m) {
    py::class_<DutNode, std::shared_ptr<DutNode>>(m, "DutNode")
    .def(py::init<>());
}