//
// Created by brachwal on 04.08.20.
//
#include "TbJobNode.h"
#include "TbJob.h"
#include <string>

////////////////////////////////////////////////////////////////////////////////////
///
TbJobNode::TbJobNode(TbJobSharedPtr tbjob_ptr):m_tbjob(tbjob_ptr) { }

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobNode::ThisTbJobPtr(TbJobSharedPtr tbjob_ptr) {
    m_tbjob = tbjob_ptr;
}

////////////////////////////////////////////////////////////////////////////////////
///
TbJobSharedPtr TbJobNode::ThisTbJobPtr() const {
    return m_tbjob;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbJobNode::TbJobPtrNotInitializedError(const std::string& caller) const {
    if (!ThisTbJobPtr()) {
        std::cout << "[ERROR]:: TbJobNode ptr has been called";
        if (!caller.empty()) std::cout << " (by the " << caller << ")";
        std::cout << " but it was not initialized before!" << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}