//
// Created by brachwal on 14.06.19.
//

#ifndef TBJOBNODE_H
#define TBJOBNODE_H

#include <memory>

class TbJob;
using TbJobSharedPtr = std::shared_ptr<TbJob>;

class TbJobNode {
    private:
        ///
        TbJobSharedPtr m_tbjob = nullptr;

    public:
        ///
        TbJobNode() = default ;

        ///
        TbJobNode(TbJobSharedPtr tbjob_ptr);

        ///
        virtual ~TbJobNode() = default;

        ///
        void ThisTbJobPtr(TbJobSharedPtr tbjob_ptr);

        ///
        TbJobSharedPtr ThisTbJobPtr() const;

        ///
        void TbJobPtrNotInitializedError(const std::string& caller=std::string()) const;
};

#endif //TBJOBNODE_H
