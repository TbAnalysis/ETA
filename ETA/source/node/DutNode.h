//
// Created by brachwal on 14.06.19.
//

#ifndef DUTNODE_H
#define DUTNODE_H

#include <memory>

class Dut;
class DutPix;
using DutSharedPtr = std::shared_ptr<Dut>;
using DutPixSharedPtr = std::shared_ptr<DutPix>;

/// The way we look at the DUT geometry
enum class SegmentationScheme {
    None,       // no segmentation, processing of whole DUT area
    Dut,        // inherits from DUT binning scheme
    Pixel,      // inherits from Pixel binning scheme
    DutPixel    // inherits from DUT and Pixel binning scheme
};

class DutNode {
    private:
        ///
        DutSharedPtr m_dut = nullptr;

    public:
        ///
        DutNode() = default ;

        ///
        DutNode(DutSharedPtr dut_ptr);

        ///
        virtual ~DutNode() = default;

        ///
        void ThisDutPtr(DutSharedPtr dut_ptr);

        ///
        DutSharedPtr ThisDutPtr() const;

        ///
        void DutPtrNotInitializedError(const std::string& caller=std::string()) const;
};

#endif //DUTNODE_H
