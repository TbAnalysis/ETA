#pragma once


#include <map>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/dist_sink.h>
#include <spdlog/sinks/basic_file_sink.h>




class TbLog{
private:

    TbLog()=default;


    TbLog(const TbLog&) = delete;
	TbLog& operator=(const TbLog&) = delete;
	TbLog(TbLog&&) = delete;
	TbLog& operator=(TbLog&&) = delete;

    ///Reset pattern of default logger
    static void SetDefaultPattern();

    ///Change pattern of default logger (used when user-defined logger doesn't exists)
    static void SetCustomPattern(std::string logger_name);

    ///main console sink
    static std::shared_ptr<spdlog::sinks::stdout_color_sink_mt> m_console_sink;

    ///main file sink
    static std::shared_ptr<spdlog::sinks::basic_file_sink_mt> m_file_sink;

    ///main logger instance, uses to send messages to all sinks
    static std::shared_ptr<spdlog::logger> m_logger;
   
    ///map contains all available loggers at the moment
    static std::map<const std::string, std::shared_ptr<spdlog::logger>> m_logger_list;



public:


    static TbLog* GetInstance(){
        static TbLog instance;
        return &instance;
    }
    
    
    ///Add user-defined logger with given name 
    static void AddLogger(std::string logger_name);

    ///Add user-defined logger with given name and logging level
    static void AddLogger(std::string logger_name, std::string level);

    ///Delete user-defined logger
    static void DeleteLogger(std::string logger_name);

   ///Set level of the main console messages (Trace, Debug, Info, Warn, Error, Critical)
    static void SetLevel(std::string level);

    ///Set level of the user-defined logger messages (Trace, Debug, Info, Warn, Error, Critical)
    static void SetLevel(std::string logger_name, std::string level);



    //Message methods

    ///Trace message to the default logger
    static void Trace(std::string message);

    ///Trace message to the user defined logger if exists, if not it creates a new logger with given name
    static void Trace(std::string logger_name, std::string message);

    ///Debug message to the default logger
    static void Debug(std::string message);

    ///Debug message to the user defined logger
    static void Debug(std::string logger_name, std::string message);

    ///Info message to the default logger
    static void Info(std::string message);

    ///Info message to the user defined logger
    static void Info(std::string logger_name, std::string message);

    ///Warn message to the default logger
    static void Warn(std::string message);

    ///Warn message to the user defined logger
    static void Warn(std::string logger_name, std::string message);

    ///Error message to the default logger
    static void Error(std::string message);

    ///Error message to the user defined logger
    static void Error(std::string logger_name, std::string message);

    ///Critical message to the default logger
    static void Critical(std::string message);

    ///Critical message to the user defined logger
    static void Critical(std::string logger_name, std::string message);
    



};