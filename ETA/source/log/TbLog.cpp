#include "TbLog.h"
#include "TbGaudi.h"

std::shared_ptr<spdlog::sinks::stdout_color_sink_mt> TbLog::m_console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();

std::shared_ptr<spdlog::sinks::basic_file_sink_mt> TbLog::m_file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>("/afs/cern.ch/work/p/pkorytow/ETA/logs/TbGaudi.log", true);

std::shared_ptr<spdlog::logger> TbLog::m_logger = std::make_shared<spdlog::logger>("default", spdlog::sinks_init_list({TbLog::m_console_sink, TbLog::m_file_sink}));

std::map<const std::string, std::shared_ptr<spdlog::logger>> TbLog::m_logger_list = {{"default", m_logger}};



void TbLog::AddLogger(std::string logger_name){

    auto logger = m_logger_list.find(logger_name);
    
    if (logger==m_logger_list.end())
    {
        auto userLogger = spdlog::basic_logger_mt(logger_name, "/afs/cern.ch/work/p/pkorytow/ETA/logs/"+logger_name+".log", true);
        userLogger->set_level(spdlog::level::trace);
        m_logger_list.insert({logger_name, userLogger});

    }
}

void TbLog::AddLogger(std::string logger_name, std::string level){

    auto logger = m_logger_list.find(logger_name);
    
    if (logger==m_logger_list.end())
    {
        AddLogger(logger_name);
        SetLevel(logger_name, level);
    }
}


void TbLog::DeleteLogger(std::string logger_name){

    if (logger_name!="default"){
        auto logger = m_logger_list.find(logger_name);

        if(logger!=m_logger_list.end()){
            m_logger_list.erase(logger);
        }
    }
        
}


void TbLog::SetLevel(std::string level){

    std::transform(level.begin(), level.end(), level.begin(), [](unsigned char c){return std::tolower(c);});
    if(level.compare("trace")==0)       {m_console_sink->set_level(spdlog::level::trace);}
    if(level.compare("debug")==0)       {m_console_sink->set_level(spdlog::level::debug);}
    if(level.compare("info")==0)        {m_console_sink->set_level(spdlog::level::info);}
    if(level.compare("warn")==0)        {m_console_sink->set_level(spdlog::level::warn);}
    if(level.compare("error")==0)       {m_console_sink->set_level(spdlog::level::err);}
    if(level.compare("critical")==0)    {m_console_sink->set_level(spdlog::level::critical);}

}


void TbLog::SetLevel(std::string logger_name, std::string level){

    std::transform(level.begin(), level.end(), level.begin(), [](unsigned char c){return std::tolower(c);});
    if (logger_name.compare("default")==0)
    {
        SetLevel(level);
    }
    else
    {
        auto logger = m_logger_list.find(logger_name);

        if(logger!=m_logger_list.end())
        {
            auto instance = logger->second;
            if(level.compare("trace")==0)       {instance->set_level(spdlog::level::trace);}
            if(level.compare("debug")==0)       {instance->set_level(spdlog::level::debug);}
            if(level.compare("info")==0)        {instance->set_level(spdlog::level::info);}
            if(level.compare("warn")==0)        {instance->set_level(spdlog::level::warn);}
            if(level.compare("error")==0)       {instance->set_level(spdlog::level::err);}
            if(level.compare("critical")==0)    {instance->set_level(spdlog::level::critical);}

        }
        
    }

}

void TbLog::SetDefaultPattern()
{
    m_logger->set_pattern("%+");
}

void TbLog::SetCustomPattern(std::string logger_name)
{
    m_logger->set_pattern("[%Y-%m-%d %T:%e] ["+logger_name+"] [%^%l%$] %v");

}


//message methods

void TbLog::Trace(std::string message)
{
    m_logger->trace(message);
    m_logger->flush();
}

void TbLog::Trace(std::string logger_name, std::string message)
{
    auto logger = m_logger_list.find(logger_name);
        
    if (logger==m_logger_list.end())
    {
        TbLog::Warn("["+logger_name+"] logger doesn't exists. Redirected message below:");
        TbLog::SetCustomPattern(logger_name);
        TbLog::Trace("["+logger_name+"]: "+message);
        TbLog::SetDefaultPattern();
    }
    else
    {
        logger->second->trace(message);
        logger->second->flush();
    }
    
}

void TbLog::Debug(std::string message)
{
    m_logger->debug(message);
    m_logger->flush();
}

void TbLog::Debug(std::string logger_name, std::string message)
{
    auto logger = m_logger_list.find(logger_name);
        
    if (logger==m_logger_list.end())
    {   
        TbLog::Warn("["+logger_name+"] logger doesn't exists. Redirected message below:");
        TbLog::SetCustomPattern(logger_name);
        TbLog::Debug(message);
        TbLog::SetDefaultPattern();
    }
    else
    {
        logger->second->debug(message);
        logger->second->flush();
    }
}

void TbLog::Info(std::string message)
{
    m_logger->info(message);
    m_logger->flush();
}

void TbLog::Info(std::string logger_name, std::string message)
{
    auto logger = m_logger_list.find(logger_name);
        
    if (logger==m_logger_list.end())
    {
        TbLog::Warn("["+logger_name+"] logger doesn't exists. Redirected message below:");
        TbLog::SetCustomPattern(logger_name);
        TbLog::Info(message);
        TbLog::SetDefaultPattern();
    }
    else
    {
        logger->second->info(message);
        logger->second->flush();
    }
}

void TbLog::Warn(std::string message)
{
    m_logger->warn(message);
    m_logger->flush();
}

void TbLog::Warn(std::string logger_name, std::string message)
{
    auto logger = m_logger_list.find(logger_name);
        
    if (logger==m_logger_list.end())
    {
        TbLog::Warn("["+logger_name+"] logger doesn't exists. Redirected message below:");
        TbLog::SetCustomPattern(logger_name);
        TbLog::Warn(message);
        TbLog::SetDefaultPattern();
    }
    else
    {
        logger->second->warn(message);
        logger->second->flush();
    }
}

void TbLog::Error(std::string message)
{
    m_logger->error(message);
    m_logger->flush();
}

void TbLog::Error(std::string logger_name, std::string message)
{
    auto logger = m_logger_list.find(logger_name);
        
    if (logger==m_logger_list.end())
    {
        TbLog::Warn("["+logger_name+"] logger doesn't exists. Redirected message below:");
        TbLog::SetCustomPattern(logger_name);
        TbLog::Error(message);
        TbLog::SetDefaultPattern();
    }
    else
    {
        logger->second->error(message);
        logger->second->flush();
    }   
}

void TbLog::Critical(std::string message)
{
    m_logger->critical(message);
    m_logger->flush();
}

void TbLog::Critical(std::string logger_name, std::string message)
{
    auto logger = m_logger_list.find(logger_name);
        
    if (logger==m_logger_list.end())
    {
        TbLog::Warn("["+logger_name+"] logger doesn't exists. Redirected message below:");
        TbLog::SetCustomPattern(logger_name);
        TbLog::Critical(message);
        TbLog::SetDefaultPattern();
    }
    else
    {
        logger->second->critical(message);
        logger->second->flush();
    }
}




