#include "ITbResults.h"
#include "TbGaudi.h"
#include "Globals.h"
#include "TbJobCollector.h"
#include "TbJob.h"
#include "TbJobIO.h"
#include "TbOutcome.h"

////////////////////////////////////////////////////////////////////////////////////
///
TbResults::TbResults()
{
    m_results = std::make_unique<TbOutcome>();
    m_plots = std::make_unique<TbPlots>();
}

////////////////////////////////////////////////////////////////////////////////////
///
std::weak_ptr<TbOutcome> TbResults::EmplaceTbJobResults(TbJobSharedPtr tbjob){
    if(!Includes<TbJobSharedPtr>(m_job_results,tbjob))
        m_job_results[tbjob] = std::make_shared<TbOutcome>();
    return std::weak_ptr(m_job_results.at(tbjob));
}

////////////////////////////////////////////////////////////////////////////////////
///
std::weak_ptr<TbPlots> TbResults::EmplaceTbJobPlots(TbJobSharedPtr tbjob){
    if(!Includes<TbJobSharedPtr>(m_job_plots,tbjob))
        m_job_plots[tbjob] = std::make_shared<TbPlots>();
    return std::weak_ptr(m_job_plots.at(tbjob));
}

////////////////////////////////////////////////////////////////////////////////////
///
std::weak_ptr<TbHists> TbResults::EmplaceTbJobHists(TbJobSharedPtr tbjob){
    if(!Includes<TbJobSharedPtr>(m_job_histograms,tbjob))
        m_job_histograms[tbjob] = std::make_shared<TbHists>();
    return std::weak_ptr(m_job_histograms.at(tbjob));
}

////////////////////////////////////////////////////////////////////////////////////
///
std::weak_ptr<TObjCollection> TbResults::EmplaceTbJobTObjCollection(TbJobSharedPtr tbjob){
    if(!Includes<TbJobSharedPtr>(m_job_tobjects,tbjob))
        m_job_tobjects[tbjob] = std::make_shared<TObjCollection>();
    return std::weak_ptr(m_job_tobjects.at(tbjob));
}

////////////////////////////////////////////////////////////////////////////////////
///
[[deprecated("Use TbIO::ExportAllResults(dir) instead.")]]
void TbResults::ExportResults(const std::string& outputDirPath){
    TbGaudi::PrintBanner("INFO","TbResults::Exporting all results to local files");
    // Export job results
    auto jobCollection = TbJobCollector::GetInstance()->TbJobs();
    for(const auto& job : jobCollection) {
        std::cout<<"[INFO]:: Exporting "<< job->Title() << std::endl;
        job->TbIOPtr()->ExportResultsToPdfFile(outputDirPath);
        job->TbIOPtr()->ExportResultsToNTupleFile(outputDirPath);
        job->TbIOPtr()->ExportResultsToCsv(outputDirPath);
    }

    // export compilation results
    std::cout<<"[INFO]:: Exporting compilation results" << std::endl;
    auto io = std::make_unique<TbIO>();
    io->ExportResultsToPdfFile(outputDirPath);
    io->ExportResultsToNTupleFile(outputDirPath);
    io->ExportResultsToCsv(outputDirPath);
}