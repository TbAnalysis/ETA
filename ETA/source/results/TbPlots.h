/*! \brief Plots class definition
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date May-2019
*/

#ifndef PLOTS_H
#define PLOTS_H

#include <map>
#include <memory>
#include "TCanvas.h"

using TCanvasSharedPtr = std::shared_ptr<TCanvas>;
using MTCanvas = std::map<std::string,TCanvasSharedPtr>;

////////////////////////////////////////////////////////////////////////////////////
///
class TbPlots {
    private:

        MTCanvas m_canCollection;

    public:
        ///
        TbPlots() = default;

        ///
        ~TbPlots() = default;

        ///
        size_t NPlots() const { return m_canCollection.size(); }

        ///
        void Insert(const std::string& name, TCanvasSharedPtr plot);

        ///
        void ExportToPdf(const std::string& file_path);

        ///
        static void ExportToPdf(const std::string& file_path, const MTCanvas& canCollection);

};

#endif	// PLOTS_H
