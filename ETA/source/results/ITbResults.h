//
// Created by brachwal on 31.05.19.
//

#ifndef ITBRESULTS_H
#define ITBRESULTS_H

#include <memory>
#include <iostream>
#include <utility>
#include "TbResults.h"

class ITbResults {
    private:

    public:
        ///
        ITbResults() = default;

        ///
        virtual ~ITbResults() = default;

        ///
        TbResults* TbResultsPtr() {return TbResults::GetInstance();}

        ///
        virtual bool Exists(const std::string& name) const;

        ///
        //template <typename T>
        //TODO :void InsertTbResult(const std::string& name, T* result);

        ///
        // TODO: void InsertTbResult(const std::string& name, float val, float err);

        ///
        // TODO: void InsertTbResult(const std::string& name, unsigned bin, float val, float err);

        ///
        //template <typename T>
        //std::shared_ptr<T> ResultsPtr(const std::string& name);

        ///
        void ExportResults(const std::string& outputDirPath=std::string());



};

#endif //ITBRESULTS_H
