//
// Created by brachwal on 23.05.19.
//

#ifndef ITBJOBRESULTS_H
#define ITBJOBRESULTS_H

#include <iostream>
#include <utility>
#include "TbJobNode.h"
#include "ITbResults.h"
#include "TbOutcome.h"
#include "TbPlots.h"
#include "TbHists.h"
#include "TObjCollection.h"

class ITbJobResults : public TbJobNode,
                      public virtual ITbResults {
    private:
        ///
        friend class ITbResults;

        ///
        std::shared_ptr<TbOutcome> m_tb_outcome;

        ///
        std::shared_ptr<TbPlots> m_tb_plots;

        //
        std::shared_ptr<TbHists> m_tb_hist;

        ///
        std::shared_ptr<TObjCollection> m_tobj_collection;

        ///
        void EmplaceJobOutcome();

        ///
        void LinkJobOutcome();

        ///
        bool IsOutcomeEmplaced() const;

    public:
        ///
        ITbJobResults() = delete;

        ///
        explicit ITbJobResults(TbJobSharedPtr tbjob);

        ///
        virtual ~ITbJobResults() = default;

        ///
        bool Exists(const std::string& name) const override;

        ///
        void InsertTbJobPlot(const std::string& name, TCanvasSharedPtr plot);

        ///
        void InsertTbJobHist(const std::string& name, TH1SharedPtr hist);

        ///
        void InsertTbJobTObject(TObjSharedPtr obj);

        ///
        template <typename IT>
        std::shared_ptr<IT> GetTbJobHist(const std::string& name);

        ///
        template <typename IT>
        std::shared_ptr<IT> GetTbJobTObject(const std::string& name);

        ///
        template < typename IT >
        void InsertTbJobResult(const std::string& name, IT* val);

        ///
        void InsertTbJobResult(const std::string& name, float val, float err);

        ///
        void InsertTbJobResult(const std::string& name, unsigned bin, float val, float err);

        ///
        void InsertTbJobResult(const std::string& name, std::pair<unsigned, unsigned> inPixBin, float val, float err);

        ///
        void InsertTbJobResult(const std::string& name, unsigned inDutBin, std::pair<unsigned, unsigned> inPixBin, float val, float err);

        ///
        double MinValue(const std::string& name);

        ///
        double MaxValue(const std::string& name);

        ///
        std::shared_ptr<TbOutcome> GetTbOutcome() const { return m_tb_outcome; }

        ///
        std::shared_ptr<TbPlots> GetTbJobPlots() const { return m_tb_plots; }

};

////////////////////////////////////////////////////////////////////////////////////
///
template < typename IT >
std::shared_ptr<IT> ITbJobResults::GetTbJobHist(const std::string& name){
        return m_tb_hist->Get<IT>(name);
}

////////////////////////////////////////////////////////////////////////////////////
///
template < typename IT >
std::shared_ptr<IT> ITbJobResults::GetTbJobTObject(const std::string& name){
        return m_tobj_collection->Get<IT>(name);
}

////////////////////////////////////////////////////////////////////////////////////
///
template < typename IT >
void ITbJobResults::InsertTbJobResult(const std::string& name, IT* val){
        m_tb_outcome->Add(name,val);
}
#endif //ITBJOBRESULTS_H
