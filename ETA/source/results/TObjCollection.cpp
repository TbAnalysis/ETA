//
// Created by brachwal on 23.08.19.
//

#include "TObjCollection.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TObjCollection::Insert(const std::string& name, TObjSharedPtr obj){
    if(!Includes<std::string>(m_objCollection,name)){
        m_objCollection[name] = obj;
    } else {
        std::cout << "[WARNING]:: TObjCollection:: replacing previous TbObject instance"
                  << " ("<< name << ")" << std::endl;
        m_objCollection.at(name).reset();
        m_objCollection.at(name) = obj;
    }
}