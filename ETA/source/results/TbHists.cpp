//
// Created by brachwal on 21.06.19.
//

#include "TbHists.h"

////////////////////////////////////////////////////////////////////////////////////
///
void TbHists::Insert(const std::string& name, TH1SharedPtr hist){
    if(!Includes<std::string>(m_histCollection,name)){
        m_histCollection[name] = hist;
    } else {
        std::cout << "[WARNING]:: TbHists:: replacing previous histogram instance"
                  << " ("<< name << ")" << std::endl;
        m_histCollection.at(name).reset();
        m_histCollection.at(name) = hist;
    }
}