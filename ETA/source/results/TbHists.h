//
// Created by brachwal on 21.06.19.
//

#ifndef TBHISTS_H
#define TBHISTS_H

#include <map>
#include <memory>
#include <string>
#include "TH1.h"
#include "Globals.h"
#include "TbGaudi.h"

using TH1SharedPtr = std::shared_ptr<TH1>;
using MTH1 = std::map<std::string,TH1SharedPtr>;

////////////////////////////////////////////////////////////////////////////////////
///
class TbHists {
private:

    MTH1 m_histCollection; // works also for TH2 since it inherits from TH1

public:
    ///
    TbHists() = default;

    ///
    ~TbHists() = default;

    ///
    void Insert(const std::string& name, TH1SharedPtr hist);

    ///
    template <typename T>
    std::shared_ptr<T> Get(const std::string& name) const;

};

template <typename T>
std::shared_ptr<T> TbHists::Get(const std::string& name) const {
    if(Includes<std::string>(m_histCollection,name)){
        return std::dynamic_pointer_cast<T>(m_histCollection.at(name));
    } else {
        std::cout << "[INFO]:: TbHists:: "<< name << " doesn't exists yet..."<< std::endl;
        return nullptr;
    }
}
#endif //TBHISTS_H
