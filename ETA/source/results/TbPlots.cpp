#include "TbPlots.h"
#include "TbGaudi.h"


////////////////////////////////////////////////////////////////////////////////////
///
void TbPlots::Insert(const std::string& name, TCanvasSharedPtr plot){
    if(!Includes<std::string>(m_canCollection,name)){
        m_canCollection[name] = plot;
    } else {
        std::cout << "[WARNING]:: TbPlots:: replacing previous canvas instance"
                  << " ("<< name << ")" << std::endl;
        m_canCollection.at(name).reset();
        m_canCollection.at(name) = plot;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbPlots::ExportToPdf(const std::string& file_path){

    auto msg = [&file_path](){std::cout << "[INFO]:: TbPlots::Export to the file:\n"
                                 "[INFO]:: "<< file_path << std::endl; };

    auto nPlots = m_canCollection.size();
    switch (nPlots){
        case 0 : // any plot to be exported
            std::cout << "[INFO]:: TbPlots:: any plots to be exported."<<std::endl;
            break;
        case 1:  // only one plot exists in the collection
            msg();
            m_canCollection.begin()->second->Print(TString(file_path.c_str()), "pdf");
            break;
        default:
            msg();
            int ican = 0;
            for (auto& can : m_canCollection){
                if (ican == 0) {
                    can.second->Print(TString(file_path.c_str()) + "[", "pdf");
                }
                if (ican <= nPlots-1 ) {
                    can.second->Print(TString(file_path.c_str()), "pdf");
                }
                if (ican == nPlots-1 ) {
                    can.second->Print(TString(file_path.c_str()) + "]", "pdf");
                }
                ican++;
            }
    }

}

////////////////////////////////////////////////////////////////////////////////////
///
void TbPlots::ExportToPdf(const std::string& file_path, const MTCanvas& canCollection){
    auto msg = [&file_path](){std::cout << "[INFO]:: TbPlots::Export to the file:\n"
                                           "[INFO]:: "<< file_path << std::endl; };

    auto nPlots = canCollection.size();
    switch (nPlots){
        case 0 : // any plot to be exported
            std::cout << "[INFO]:: TbPlots:: any plots to be exported."<<std::endl;
            break;
        case 1:  // only one plot exists in the collection
            msg();
            canCollection.begin()->second->Print(TString(file_path.c_str()), "pdf");
            break;
        default:
            msg();
            int ican = 0;
            for (auto& can : canCollection){
                if (ican == 0) {
                    can.second->Print(TString(file_path.c_str()) + "[", "pdf");
                }
                if (ican <= nPlots-1 ) {
                    can.second->Print(TString(file_path.c_str()), "pdf");
                }
                if (ican == nPlots-1 ) {
                    can.second->Print(TString(file_path.c_str()) + "]", "pdf");
                }
                ican++;
            }
    }
}