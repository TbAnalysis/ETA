//
// Created by brachwal on 23.05.19.
//
#include "ITbResults.h"
#include "ITbJobResults.h"
#include "TbJobCollector.h"
#include "TbAnalysis.h"
#include "TbJobIO.h"
#include "Globals.h"

////////////////////////////////////////////////////////////////////////////////////
///
//template <typename T>
//void ITbResults::InsertTbResult(const std::string& name, T* result){ // TODO replace in the app code (exists as TbResults::InsertResults)
//    m_tbResults->ResultsPtr()->Add(name,result);
//}

////////////////////////////////////////////////////////////////////////////////////
///
//template <typename T>
//std::shared_ptr<T> ITbResults::ResultsPtr(const std::string& name){
//    return TbResultsPtr()->ResultsPtr()->Get<T>(name);
//}

////////////////////////////////////////////////////////////////////////////////////
///
bool ITbResults::Exists(const std::string& name) const {

}

////////////////////////////////////////////////////////////////////////////////////
///
void ITbResults::ExportResults(const std::string& outputDirPath){
    TbGaudi::PrintBanner("INFO","ITbResults::Exporting all results to local files");
    // define the output dir path
    // std::string output_dir_path;
    // auto project_local_dir_path = TbGaudi::ProjectLocation()+"/TbResults";
    // auto user_dir_path = TbIO::OutputLocation();
    // auto arg_dir_path = outputDirPath;
    // if(!arg_dir_path.empty())
    //     output_dir_path=arg_dir_path;
    // else if(!user_dir_path.empty())
    //     output_dir_path = user_dir_path;
    // else
    //     output_dir_path = project_local_dir_path;

    // // create current day-date dir if not exists
    // auto date_output_dir = CreateCurrentDateDirIfNotExits(output_dir_path);
    // std::cout << "[INFO]:: ITbResults::ExportResults:: output location is defined as:\n"
    //              "[INFO]:: "<< date_output_dir << std::endl;

    // export job-like results
    // for(auto job : TbJobCollector::GetInstance()->TbJobs()) {
    //     job->TbIOPtr()->ExportResultsToPdfFile(date_output_dir);
    //     job->TbIOPtr()->ExportResultsToNTupleFile(date_output_dir);
    //     //ITbJobResults::ExportResultsToASCIIFile(date_output_dir);
    // }

    // export compilation results
    // auto io = std::make_unique<TbIO>();
    // io->ExportResultsToPdfFile(date_output_dir);

    /*
    //____________________________________________
    // All runs compilation plots
    std::string fname = "TbResults_AllRunsCompilation.pdf";
    fs::path fp = path+"/"+fname;
    if(fs::exists(fp)){
        std::cout << "[INFO]:: TbResults:: Remove existing results: " << fname <<std::endl;
        fs::remove(fp);
    }
    //____________________________________________
    // Create plots if not created yet.
    gROOT->SetBatch(true);
    if(m_job_results.size()>0 && TbJobResultsExists(m_job_results.begin()->first,"MPV")) {
        DrawDutProfileCombined("MPV", "column");
        DrawDutProfileCombined("MPV", "row");
    }
    //
    // ...add any other compilation plots to be created here.
    //
    gROOT->SetBatch(false);
    //____________________________________________
    auto nPlots = m_plots.size();
    switch (nPlots){
        case 0 : // shouldn't happen
            break;
        case 1:  // only one plot exists for given run
            m_plots.begin()->second->Print(TString(path.c_str())+"/"+TString(fname.c_str()), "pdf");
            break;
        default:
            int ican = 0;
            for (auto& can : m_plots){
                if (ican == 0) {
                    can.second->Print(TString(path.c_str()) + "/" + TString(fname.c_str()) + "[", "pdf");
                }
                if (ican <= nPlots-1 ) {
                    can.second->Print(TString(path.c_str()) + "/" + TString(fname.c_str()), "pdf");
                }
                if (ican == nPlots-1 ) {
                    can.second->Print(TString(path.c_str()) + "/" + TString(fname.c_str()) + "]", "pdf");
                }
                ican++;
            }
    }
     */
}