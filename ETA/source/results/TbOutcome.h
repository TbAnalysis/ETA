/*! \brief TbOutcome class definition
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef RESULTS_H
#define RESULTS_H

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <memory>
#include <utility>
#include <typeinfo>
#include <typeindex>
#include "TbGaudi.h"

class TbJob;

using Pixel = std::pair<unsigned,unsigned>;
using Result = std::pair<float,float>;
using VResult = std::vector<Result>;
using MResult = std::map<unsigned, Result>;
using MPixResult = std::map<Pixel, Result>;
using MDutBinPixResult = std::map<unsigned, MPixResult>;

////////////////////////////////////////////////////////////////////////////////////
///
enum class OutcomeType {
  FResult,
  VResult,
  MResult,
  MPixResult,
  MDutBinPixResult
};

////////////////////////////////////////////////////////////////////////////////////
///
class OutcomeInterface {
	private:
		OutcomeType m_type;
	public:
		OutcomeInterface() = default;
		virtual ~OutcomeInterface() = default;
		void SetType(OutcomeType type) {m_type=type;}
		const OutcomeType& GetType() const {return m_type;}
};

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
class OutcomeInstance : public OutcomeInterface {
	public:
		OutcomeInstance(std::shared_ptr<T> res):m_result(res){};
		~OutcomeInstance()= default;
		std::shared_ptr<T> m_result;
		std::shared_ptr<T> Get(){return m_result;}
};

////////////////////////////////////////////////////////////////////////////////////
///
class TbOutcome {
	private:

		///
		std::map<std::string, std::shared_ptr<OutcomeInterface> > m_results;

	public:

		///
		TbOutcome()=default;

        ///
        ~TbOutcome()=default;

        unsigned Size() { return m_results.size(); }

		template <typename T>
		void Add(const std::string& name, T* result);

		template <typename T>
        std::shared_ptr<T> Get(const std::string& name) const;

		///
		void Reset(){ m_results.clear(); }

		///
		bool Exists(const std::string& name) const;

		///
        std::pair<std::string, std::string> GetCsvStrings() const;

		/// Get list of inserted results
		std::vector<std::string> GetNames() const;

		///
		std::map<std::string, std::shared_ptr<VResult>> GetAllVResults() const;

		///
		std::map<std::string, std::shared_ptr<MResult>> GetAllMResults() const;

		///
		void ExportToCsv(std::shared_ptr<TbJob> job, const std::string& file_full_path);
};

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
void TbOutcome::Add(const std::string& name, T* result){
	if(m_results.empty() || !Includes<std::string>(m_results, name)){
		OutcomeInterface* iresult = new OutcomeInstance<T>(std::shared_ptr<T>(result));
		m_results.insert(std::make_pair(std::move(name), std::shared_ptr<OutcomeInterface>(iresult)));
		auto typeIdx = std::type_index(typeid(T));
		if(typeIdx==std::type_index(typeid(Result))) m_results.at(name)->SetType(OutcomeType::FResult);
		else if(typeIdx==std::type_index(typeid(VResult))) m_results.at(name)->SetType(OutcomeType::VResult);
		else if(typeIdx==std::type_index(typeid(MResult))) m_results.at(name)->SetType(OutcomeType::MResult);
		else if(typeIdx==std::type_index(typeid(MPixResult))) m_results.at(name)->SetType(OutcomeType::MPixResult);
		else if(typeIdx==std::type_index(typeid(MDutBinPixResult))) m_results.at(name)->SetType(OutcomeType::MDutBinPixResult);
	}
	else {
        std::cout << "[ERROR]:: TbOutcome:: You are adding outcome type"
                  <<" which is already added before ("<<name<<")"<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
	}
}

////////////////////////////////////////////////////////////////////////////////////
///
template <typename T>
std::shared_ptr<T> TbOutcome::Get(const std::string& name) const {
	if(Includes<std::string>(m_results, name)){
	    auto resultInstance = std::dynamic_pointer_cast<OutcomeInstance<T>>(m_results.at(name));
	    return resultInstance->Get();
	}
    else{
		std::cout << "[ERROR]:: Result:: You are trying to extract result"
				  << " ("<<name << ") which doesn't exists!"<< std::endl;
		TbGaudi::RunTimeError("Error in analysis workflow.");
    } return std::shared_ptr<T>(nullptr);
}
#endif	// RESULTS_H
