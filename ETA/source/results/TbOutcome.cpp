#include "TbOutcome.h"
#include "TbJob.h"

////////////////////////////////////////////////////////////////////////////////////
///
bool TbOutcome::Exists(const std::string& name) const {
    if(m_results.empty()) return false;
    return Includes<std::string>(m_results, name);
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<std::string> TbOutcome::GetNames() const {
    std::vector<std::string> results_name;
    for(const auto& iresult : m_results)
        results_name.push_back(iresult.first);
    return results_name;
}

////////////////////////////////////////////////////////////////////////////////////
///
std::map<std::string, std::shared_ptr<VResult>> TbOutcome::GetAllVResults() const{
    std::map<std::string, std::shared_ptr<VResult>> vResults;
    for(const auto& result : m_results){
        if(result.second->GetType()==OutcomeType::VResult){
            auto vResult = Get<VResult>(result.first);
            vResults.insert(std::make_pair(result.first,vResult));
        }
    }
    return vResults;
}

////////////////////////////////////////////////////////////////////////////////////
///
std::map<std::string, std::shared_ptr<MResult>> TbOutcome::GetAllMResults() const{
    std::map<std::string, std::shared_ptr<MResult>> mResults;
    for(const auto& result : m_results){
        if(result.second->GetType()==OutcomeType::MResult){
            auto mResult = Get<MResult>(result.first);
            mResults.insert(std::make_pair(result.first,mResult));
        }
    }
    return mResults;
}

////////////////////////////////////////////////////////////////////////////////////
///
void TbOutcome::ExportToCsv(std::shared_ptr<TbJob> job, const std::string& file_full_path){
    std::cout << "[INFO]:: TbOutcome::ExportToCsv:: output file:" << std::endl;
    std::cout << "[INFO]:: "<<  file_full_path << std::endl;

    std::ofstream outFile;
    outFile.open(file_full_path.c_str(), std::ios::out);

    // --------------------------------------------------------
    // general job obligatory configuration
    // --------------------------------------------------------
    auto dutName = job->GetParam("DutName");
    auto runNumber = job->GetParam("RunNumber");
    auto runBias = job->GetParam("RunBiasVoltage");
    auto nBins = job->DutPtr()->NBins();

    auto resultsNames = GetNames();
    std::ostringstream buffer;
    
    buffer << "DUT;RUN;BIAS;";
    
    if(nBins>1) buffer<<"DUT_BIN;";
    for(const auto& name : resultsNames){
        buffer << name << ";"<< name+"_ERR;";
    }
    outFile << buffer.str() << std::endl;
    std::cout<<"[DEBUG]: HEADER:: "<< buffer.str() << std::endl;
    buffer.str(std::string()); // clear

    // --------------------------------------------------------
    // Handle the OutcomeType::VResult, see TbOutcome.h docs
    // Note: this is DUT global like results
    // --------------------------------------------------------
    /*
    auto vResults = GetAllVResults();
    std::cout<<"[DEBUG]: Available results of VResult type: "<<std::endl;
    for(const auto& ir : vResults)
        std::cout<<"\t[DEBUG]: " << ir.first <<std::endl;
    // to be continued...
    */
    
    // --------------------------------------------------------
    // Handle the OutcomeType::MResult, see TbOutcome.h docs
    // Note: this is typically for binned DUT scheme.
    // --------------------------------------------------------
    auto mResults = GetAllMResults();
    if(!mResults.empty()){
        for(unsigned iBin=1;iBin<=nBins;++iBin){
            buffer<<dutName<<";"<<runNumber<<";"<<runBias<<";"<<iBin;
            for(const auto& ir : mResults){
                float val = 0;
                float err = 0;
                const auto& binMResult = ir.second->find(iBin);
                if(binMResult != ir.second->end()){
                    const auto& result = binMResult->second; // second value of iterator object
                    val = result.first;
                    err = result.second;
                }
                buffer<<";"<<val<<";"<<err;
            }
            outFile << buffer.str() << std::endl;
            buffer.str(std::string()); // clear
        }
    }
    outFile.close();
}