/*! \brief Results class definition
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef TB_RESULTS_H
#define TB_RESULTS_H

#include <vector>
#include <memory>
#include <string>
#include "TbOutcome.h"
#include "TbPlots.h"
#include "TbHists.h"
#include "TObjCollection.h"
#include "TbJobNode.h"

using MJobResults = std::map<TbJobSharedPtr, std::shared_ptr<TbOutcome>>;
using MJobPlots = std::map<TbJobSharedPtr, std::shared_ptr<TbPlots>>;
using MJobHists = std::map<TbJobSharedPtr, std::shared_ptr<TbHists>>;
using MJobTObjects = std::map<TbJobSharedPtr, std::shared_ptr<TObjCollection>>;
////////////////////////////////////////////////////////////////////////////////////
///
class TbResults {
	private:
        ///
        TbResults();

        ///
        ~TbResults() = default;

        // Delete the copy and move constructors
        TbResults(const TbResults&) = delete;
        TbResults& operator=(const TbResults&) = delete;
        TbResults(TbResults&&) = delete;
        TbResults& operator=(TbResults&&) = delete;

        ///
        std::shared_ptr<TbOutcome> m_results;

        ///
        std::shared_ptr<TbPlots> m_plots;

        ///
        std::shared_ptr<TbHists> m_histograms;

        ///
        std::shared_ptr<TObjCollection> m_tobjects;

        ///
        MJobResults m_job_results;

        ///
        MJobPlots m_job_plots;

        ///
        MJobHists m_job_histograms;

        ///
        MJobTObjects m_job_tobjects;

	public:

        static TbResults* GetInstance(){
            static TbResults instance;
            return &instance;
        }

        ///
        std::weak_ptr<TbOutcome> EmplaceTbJobResults(TbJobSharedPtr tbjob);

        ///
        std::weak_ptr<TbPlots> EmplaceTbJobPlots(TbJobSharedPtr tbjob);

        ///
        std::weak_ptr<TbHists> EmplaceTbJobHists(TbJobSharedPtr tbjob);

        ///
        std::weak_ptr<TObjCollection> EmplaceTbJobTObjCollection(TbJobSharedPtr tbjob);

        ///
        std::shared_ptr<TbOutcome> ResultsPtr() { return m_results; }

        ///
        std::shared_ptr<TbPlots> PlotsPtr() { return m_plots; }

        ///
        MJobResults& JobsResults() { return m_job_results; }

        ///
        std::shared_ptr<TbOutcome> GetJobResultsPtr(TbJobSharedPtr job) const { return m_job_results.at(job); }

        ///
        MJobPlots& JobsPlots() { return m_job_plots; }

        ///
        MJobHists& JobsHistograms() { return m_job_histograms; }

        ///
        MJobTObjects& JobsTObjects() { return m_job_tobjects; }

		///
		void Reset(){};	// TODO: implement me.

        ///
        static void ExportResults(const std::string& outputDirPath=std::string());

};

#endif	// TB_RESULTS_H
