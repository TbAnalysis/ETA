//
// Created by brachwal on 31.05.19.
//

#include "ITbJobResults.h"
#include "TbResults.h"
#include "TbJob.h"
#include "Dut.h"

////////////////////////////////////////////////////////////////////////////////////
///
ITbJobResults::ITbJobResults(TbJobSharedPtr tbjob):TbJobNode(tbjob){
    //std::cout << "[Debug]:: ITbJobResults:: Is being created"<<std::endl;
    auto outcomeEmplaced = IsOutcomeEmplaced();
    if(!outcomeEmplaced)
        EmplaceJobOutcome(); 
    else
        LinkJobOutcome();
}

////////////////////////////////////////////////////////////////////////////////////
///
bool ITbJobResults::Exists(const std::string& name) const {
    return m_tb_outcome->Exists(name);
}

////////////////////////////////////////////////////////////////////////////////////
///
bool ITbJobResults::IsOutcomeEmplaced() const {
    auto jobPtr = ThisTbJobPtr();
    auto jobsResults = TbResults::GetInstance()->JobsResults();
    if(jobsResults.find(jobPtr) != jobsResults.end())
        return true;
    return false;
}

////////////////////////////////////////////////////////////////////////////////////
///
void ITbJobResults::LinkJobOutcome(){
    //std::cout << "[Debug]:: ITbJobResults:: Linking TbJob Outcomes instances"<<std::endl;
    auto jobPtr = ThisTbJobPtr();
    auto tbResults = TbResults::GetInstance();
    m_tb_outcome = tbResults->JobsResults().at(jobPtr);
    m_tb_plots = tbResults->JobsPlots().at(jobPtr);
    m_tb_hist = tbResults->JobsHistograms().at(jobPtr);
    m_tobj_collection = tbResults->JobsTObjects().at(jobPtr);
}

////////////////////////////////////////////////////////////////////////////////////
///
void ITbJobResults::EmplaceJobOutcome(){
    //std::cout << "[Debug]:: ITbJobResults:: Emplacing TbJob new Outcomes instances"<<std::endl;
    auto gotError = [](){
        std::cout << "[ERROR]:: ITbJobResults::EmplaceJobOutcome"
                  << " sth went wrong with job results initialization"<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    };

    auto jobPtr = ThisTbJobPtr();
    auto tbResults = TbResults::GetInstance();

    // emplace new instances into the collections if not emplaced before for the given tbjob:
    auto resultsOutcome = tbResults->EmplaceTbJobResults(jobPtr);
    if(!resultsOutcome.expired())
        m_tb_outcome = resultsOutcome.lock();
    else gotError();

    auto plotsOutcome = tbResults->EmplaceTbJobPlots(jobPtr);
    if(!plotsOutcome.expired())
        m_tb_plots = plotsOutcome.lock();
    else gotError();

    auto histOutcome = tbResults->EmplaceTbJobHists(jobPtr);
    if(!histOutcome.expired())
        m_tb_hist = histOutcome.lock();
    else gotError();

    auto tObjOutcome = tbResults->EmplaceTbJobTObjCollection(jobPtr);
    if(!tObjOutcome.expired())
        m_tobj_collection = tObjOutcome.lock();
    else gotError();

}

////////////////////////////////////////////////////////////////////////////////////
///
void ITbJobResults::InsertTbJobResult(const std::string& name, float val, float err){
    std::cout<<"[INFO]:: ITbJobResults:: Adding new result to TbResults:: "<< name<< " = "<<val<< "+/-"<<err<<std::endl;
    TbJobPtrNotInitializedError("InsertTbJobResult");
    if(!m_tb_outcome->Exists(name))
        m_tb_outcome->Add(name, new VResult());
    m_tb_outcome->Get<VResult>(name)->push_back(std::make_pair(val,err));

}

////////////////////////////////////////////////////////////////////////////////////
///
void ITbJobResults::InsertTbJobResult(const std::string& name, unsigned bin, float val, float err){
    std::cout<<"[INFO]:: ITbJobResults:: Adding new result to TbResults:: ";
    std::cout<< "DutBin "<<bin<<" :: "<< name<< " = "<<val<< "+/-"<<err<<std::endl;
    if(!Dut::PixStudy() || !ThisTbJobPtr()->DutPtr()->IsBinned()) {
        std::cout << "[ERROR]:: ITbJobResults::InsertTbJobResult"
                  << ":: method used for out of scope study."<< std::endl;
        if(!ThisTbJobPtr()->DutPtr()->IsBinned())
            std::cout << "[INFO]:: Dut is not binned"<<std::endl;
        if(!Dut::PixStudy())
            std::cout << "[INFO]:: Type of study is different from Dut::PixStudy"<<std::endl;
        TbGaudi::RunTimeError("Error in user analysis workflow.");
    }
    TbJobPtrNotInitializedError("InsertTbJobResult");

    if(!m_tb_outcome->Exists(name))
        m_tb_outcome->Add<MResult>(name, new MResult());
    auto mresult = m_tb_outcome->Get<MResult>(name);
    mresult->emplace(bin,std::make_pair(val,err));
}

////////////////////////////////////////////////////////////////////////////////////
///
void ITbJobResults::InsertTbJobResult(const std::string& name, unsigned inDutBin, std::pair<unsigned, unsigned> inPixBin, float val, float err){
    std::cout<<"[INFO]:: ITbJobResults:: Adding new result to TbResults:: ";
    std::cout<< "DutBin "<<inDutBin<<" Pixel("<<inPixBin.first<<","<<inPixBin.second<<"):: "<< name<< " = "<<val<< "+/-"<<err<<std::endl;

    if(!Dut::IntraPixStudy() || !ThisTbJobPtr()->DutPtr()->IsBinned()) {
        std::cout << "[ERROR]:: ITbJobResults::InsertTbJobResult"
                  << ":: method used for out of scope study."<< std::endl;
        if(!ThisTbJobPtr()->DutPtr()->IsBinned())
            std::cout << "[INFO]:: Dut is not binned"<<std::endl;
        if(!Dut::PixStudy())
            std::cout << "[INFO]:: Type of study is different from Dut::IntraPixStudy"<<std::endl;
        TbGaudi::RunTimeError("Error in user analysis workflow.");
    }

    TbJobPtrNotInitializedError("InsertTbJobResult");

    if(!m_tb_outcome->Exists(name))
        m_tb_outcome->Add(name, new MDutBinPixResult());             // see TbOutcome definitions

    m_tb_outcome->Get<MDutBinPixResult>(name)->emplace(inDutBin, MPixResult());
    auto& dutBinResults = m_tb_outcome->Get<MDutBinPixResult>(name)->at(inDutBin);
    dutBinResults.emplace(inPixBin, std::make_pair(val,err));
}

////////////////////////////////////////////////////////////////////////////////////
///
void ITbJobResults::InsertTbJobResult(const std::string& name, std::pair<unsigned, unsigned> inPixBin, float val, float err){
    std::cout<<"[INFO]:: ITbJobResults:: Adding new result to TbResults:: ";
    std::cout<< " Pixel("<<inPixBin.first<<","<<inPixBin.second<<"):: "<< name<< " = "<<val<< "+/-"<<err<<std::endl;

    if(!Dut::IntraPixStudy() || ThisTbJobPtr()->DutPtr()->IsBinned()) {
        std::cout << "[ERROR]:: ITbJobResults::InsertTbJobResult"
                  << ":: method used for out of scope study."<< std::endl;
        if(ThisTbJobPtr()->DutPtr()->IsBinned())
            std::cout << "[INFO]:: Dut is binned (you should add DutBinNr argument)"<<std::endl;
        if(!Dut::PixStudy())
            std::cout << "[INFO]:: Type of study is different from Dut::IntraPixStudy"<<std::endl;
        TbGaudi::RunTimeError("Error in user analysis workflow.");
    }

    TbJobPtrNotInitializedError("InsertTbJobResult");

    if(!m_tb_outcome->Exists(name))
        m_tb_outcome->Add(name, new MPixResult());             // see TbOutcome definitions
    m_tb_outcome->Get<MPixResult>(name)->emplace(inPixBin, std::make_pair(val,err));

}

////////////////////////////////////////////////////////////////////////////////////
///
void ITbJobResults::InsertTbJobPlot(const std::string& name, TCanvasSharedPtr plot){
    if(plot) m_tb_plots->Insert(name, std::move(plot));
}

////////////////////////////////////////////////////////////////////////////////////
///
void ITbJobResults::InsertTbJobHist(const std::string& name, TH1SharedPtr hist){
    if(hist) m_tb_hist->Insert(name, std::move(hist));
}

////////////////////////////////////////////////////////////////////////////////////
///
void ITbJobResults::InsertTbJobTObject(TObjSharedPtr obj){
    auto name = static_cast<std::string>(obj->GetName()); // casting from TString
    if(obj) m_tobj_collection->Insert(name, std::move(obj));
}

////////////////////////////////////////////////////////////////////////////////////
///
double ITbJobResults::MinValue(const std::string& name){
    auto results = m_tb_outcome->Get<MResult>(name);
    if(!results){
        std::cout << "[WARNING]:: ITbJobResults::MinValue:: No results container found for " << name <<std::endl;
        return 0.;
    }
    auto min = results->begin()->second.first;
    for(auto& ir : *results){
        min = min < ir.second.first ? min : ir.second.first;
    }
    return min;
}

////////////////////////////////////////////////////////////////////////////////////
///
double ITbJobResults::MaxValue(const std::string& name){
    auto results = m_tb_outcome->Get<MResult>(name);
    if(!results){
        std::cout << "[WARNING]:: ITbJobResults::MaxValue:: No results container found for " << name <<std::endl;
        return 0.;
    }
    auto max = double(0.);
    for(auto& ir : *results){
        max = max > ir.second.first ? max : ir.second.first;
    }
    return max;
}
