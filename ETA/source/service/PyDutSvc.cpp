#include <pybind11/pybind11.h>
#include "DutSvc.h"

namespace py = pybind11;


void PyDutSvc(py::module &m) {
    py::class_<DutSvc, std::shared_ptr<DutSvc>, DutNode>(m, "DutSvc")
    .def(py::init<>());
}