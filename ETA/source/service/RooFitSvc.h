//
// Created by brachwal on 11.10.18.
//

#ifndef DUTROOFITSVC_H
#define DUTROOFITSVC_H

#include "TbJobNode.h"
#include "TbRooDataSet.h"
#include "TbRooDataSetCollection.h"
#include "DutNode.h" // needed for DUT SegmentationScheme definition

#include "RooCategory.h"

#include <set>

class RooDataSet;
class RooRealVar;
class RooArgSet;

class RooFitSvc : public virtual TbJobNode {
    private:
        ///
        void CreateTbRooDataSetCollection(const SegmentationScheme& segScheme);
    
    protected:

        ///\brief Set to keep info if for this TbJob the RooCategory (and series of Types)
        /// has beed defined - RooCategory::defineType(...)
        /// Note The method defineType itself returns Error status if category with given name
        /// or index is already defined, that's why it's being controled with this helper variable.
        std::set<std::string> m_rcategories;

        ///
        std::unique_ptr<RooCategory> m_rcategory;

        ///
        std::vector<std::unique_ptr<RooRealVar>> m_rrv_collection;

        ///
        std::unique_ptr<TbRooDataSet> m_tbroodataset;

        ///
        std::unique_ptr<TbRooDataSetCollection> m_tbroodataset_collection;

    public:

        ///
        RooFitSvc() = default;

        ///
        RooFitSvc(TbJobSharedPtr tbjob_ptr):TbJobNode(tbjob_ptr){}

        ///
        virtual ~RooFitSvc();

        ///
        void DefineRooRealVar(const std::string& name, double xLow, double xUp, const std::string& unit=std::string());

        ///
        void DefineRooCategory(const std::string& categoryName, int categoryIdx);

        ///
        RooRealVar* GetRooRealVar(const std::string& name);

        ///
        TbRooDataSet* CreateDutTbRooDataSet();

        ///\brief Get the raw ptr to the TbRooDataSet
        ///\param varName - variable name to be verified if is being included in the RooDataSet,
        /// if it's not inside - returns nullptr!
        TbRooDataSet* GetDutTbRooDataSetPtr(const std::string& varName=std::string());

        ///
        TbRooDataSetCollection* CreateDutBinningTbRooDataSet();

        ///\brief Get the raw ptr to the TbRooDataSet collection
        ///\param varName - variable name to be verified if is being included in the RooDataSet,
        /// if it's not inside - returns nullptr!
        TbRooDataSetCollection* GetTbRooDataSetCollectionPtr(const std::string& varName=std::string());

        /// \brief Remove all the RooDataSets
        virtual void ClearAllRooDataSet();

};
#endif //DUTROOFITSVC_H
