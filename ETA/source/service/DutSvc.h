//
// Created by brachwal on 12.10.18.
//

#ifndef DUTSVC_H
#define DUTSVC_H

#include "DutNode.h"
#include "Dut.h"
#include "DutPix.h"
#include "Rtypes.h"

class DutSvc : public DutNode {
    private:

        std::map<unsigned, unsigned>    m_event_counter;
        std::map<unsigned, std::string> m_event_counter_name;
        std::vector<unsigned>           m_event_counter_priv_idx;

    public:
        DutSvc()=default;

        DutSvc(DutSharedPtr dut_ptr):DutNode(dut_ptr){}

        virtual ~DutSvc()=default;

        bool IsGoodClusterSize(UInt_t clSize);

        bool IsClusterInsideDutBin(Int_t* col,Int_t* row, UInt_t size);

        void DefineEventCounter(std::string name, unsigned idx);

        unsigned& EventCounter(unsigned idx);

        const unsigned& GetCounterStatistics(unsigned idx);

        void PrintCountersStatistic();

        ///
        void CountDutHit(unsigned col, unsigned row=0);

        ///
        int DutBinNumber(unsigned X, unsigned Y);

        ///
        int DutBinNumber(unsigned X);
};
#endif //DUTSVC_H
