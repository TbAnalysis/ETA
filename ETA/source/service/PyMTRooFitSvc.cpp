#include <pybind11/pybind11.h>
#include "MTRooFitSvc.h"

namespace py = pybind11;

void PyMTRooFitSvc(py::module &m) {
    py::class_<MTRooFitSvc, std::shared_ptr<MTRooFitSvc>, RooFitSvc>(m, "MTRooFitSvc")
    .def(py::init<>());
}