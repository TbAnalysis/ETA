//
// Created by brachwal on 11.10.18.
//

#include "RooFitSvc.h"
#include "DutBinning.h"
#include "TbGaudi.h"
#include "TbJob.h"
#include "Globals.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooCategory.h"
#include "DutPix.h"

////////////////////////////////////////////////////////////////////////////////////
///
RooFitSvc::~RooFitSvc() {
    ClearAllRooDataSet();
}

////////////////////////////////////////////////////////////////////////////////////
///
void RooFitSvc::DefineRooRealVar(const std::string& name, double xLow, double xUp, const std::string& unit){
    bool took_reset = false;
    for(auto& ivar : m_rrv_collection){
        if(ivar->GetName()==TString(name)){
            std::cout<<"[WARNING]::RooFitSvc::DefineRooRealVar:: Variable already present,"
                     << " replacing previous instantiation." << std::endl;
            ivar.reset(new RooRealVar(name.c_str(),name.c_str(),xLow,xUp,unit.c_str()));
            took_reset = true;
        }
    }
    if(!took_reset)
        m_rrv_collection.emplace_back(std::make_unique<RooRealVar>(name.c_str(),name.c_str(),xLow,xUp,unit.c_str()));
}

////////////////////////////////////////////////////////////////////////////////////
///
void RooFitSvc::DefineRooCategory(const std::string& categoryName, int categoryIdx){
    
        if(!m_rcategory){
            auto title = ThisTbJobPtr()->Title();
            m_rcategory = std::make_unique<RooCategory>("RCategory", title.c_str());
        }

        if(m_rcategories.count(categoryName)==0 ){
            m_rcategories.insert(categoryName);
            m_rcategory->defineType(TString(categoryName),Int_t(categoryIdx));
        }
}

////////////////////////////////////////////////////////////////////////////////////
///
RooRealVar* RooFitSvc::GetRooRealVar(const std::string& name){
    for(auto& ivar : m_rrv_collection){
        if(ivar->GetName()==TString(name))
            return ivar.get();
    }
    if(TbGaudi::Debug())
        std::cout << "[ERROR]:: RooFitSvc::GetRooRealVar given variable ("<<name<<") doesn't exists!" << std::endl;
    TbGaudi::RunTimeError("Error in analysis workflow.");
    return nullptr;
}


////////////////////////////////////////////////////////////////////////////////////
///
TbRooDataSet* RooFitSvc::CreateDutTbRooDataSet(){
    if(TbGaudi::Debug())
        std::cout << "[INFO]:: RooFitSvc:: A new DUT RooDataSet has been created for a current job" << std::endl;
    // Build RooDataSet on top of all User defined variables
    if(m_rrv_collection.empty()){
        std::cout << "[ERROR]:: RooFitSvc::CreateDutTbRooDataSet:"
                     " any RooRealVar has been defined. Call DefineRooRealVar(...) for a given TbJob" << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    RooArgSet ras;
    for(auto& ivar : m_rrv_collection){
        std::cout << "[INFO]:: Including variable: " << ivar->GetName() << std::endl;
        ras.add(*(ivar.get())); // Note: RooArgSet is not the owner here!
    }

    if(m_rcategory){
        //auto states = m_rcategory->states(); // TODO for some reason it doesn't work!!!
        //for (const auto& idxAndName : states)
        //    std::cout << "[INFO]:: Including category: "<<idxAndName.first<<" (Idx:"<<idxAndName.second<<")"<< std::endl;
        std::cout << "[INFO]:: Including users RooCategories "<< std::endl;
        ras.add(*(m_rcategory.get()));
    }

    auto name = "RDS_"+ThisTbJobPtr()->Label("JOB");
    auto title = ThisTbJobPtr()->Title();
    m_tbroodataset = std::make_unique<TbRooDataSet>(name.c_str(),title.c_str(),ras);
    std::cout << "[STATUS]:: RooFitSvc::CreateDutTbRooDataSet: DONE" << std::endl;  
    return m_tbroodataset.get();  
}

////////////////////////////////////////////////////////////////////////////////////
///
TbRooDataSetCollection* RooFitSvc::CreateDutBinningTbRooDataSet(){
    if(!m_tbroodataset_collection)
        CreateTbRooDataSetCollection(SegmentationScheme::Dut);
    return m_tbroodataset_collection.get();
}

////////////////////////////////////////////////////////////////////////////////////
///
void RooFitSvc::CreateTbRooDataSetCollection(const SegmentationScheme& segScheme){
    if(TbGaudi::Debug())
        std::cout << "[INFO]:: RooFitSvc:: Creating a new set DUT RooDataSet collection for a current job..." << std::endl;
    
    // Build RooDataSet on top of all User defined variables
    if(m_rrv_collection.empty()){
        std::cout << "[ERROR]:: RooFitSvc::CreateTbRooDataSetCollection:"
                     " any RooRealVar has been defined. Call DefineRooRealVar(...) for a given TbJob" << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    
    // The main instance of the data collection
    m_tbroodataset_collection = std::make_unique<TbRooDataSetCollection>();

    RooArgSet ras;
    for(auto& ivar : m_rrv_collection){
        std::cout << "[INFO]:: Including variable: " << ivar->GetName() << std::endl;
        ras.add(*(ivar.get())); // Note: RooArgSet is not the owner here!
    }

    if(m_rcategory){
        std::cout << "[INFO]:: Including users RooCategories"<< std::endl;
        ras.add(*(m_rcategory.get()));
    }

    auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
    auto nPixXBins = DutPix::NBinsX();
    auto nPixYBins = DutPix::NBinsY();
    auto name_prefix = "RDS_"+ThisTbJobPtr()->Label("JOB");
    auto title = ThisTbJobPtr()->Title();

    switch(segScheme){
        // ___________________________________________________________________________________
        case SegmentationScheme::Dut: // inherit from DUT binning scheme only
            std::cout << "[INFO]:: Taking the DUT binning segmentation..." << std::endl;
            std::cout << "[INFO]:: Creating " << nDutBins << " TbRooDataSets..." << std::endl;
            for (unsigned i = 0; i < nDutBins; ++i) {
                auto name = name_prefix + "_DutBin"+std::to_string(i);
                std::cout << "[INFO]:: Creating " << name << std::endl;
                m_tbroodataset_collection->Add(std::make_unique<TbRooDataSet>(name.c_str(),title.c_str(),ras) );
            }
            break;
        // ___________________________________________________________________________________    
        case SegmentationScheme::Pixel: // inherit from Pixel binning scheme only
            std::cout << "[INFO]:: Taking the Pixel binning segmentation..." << std::endl;
            std::cout << "[INFO]:: Creating " << nPixXBins*nPixYBins<< " TbRooDataSets..." << std::endl;
            for (unsigned iPixX = 0; iPixX < nPixXBins; ++iPixX) {       // DUT pixel X binning
                for (unsigned iPixY = 0; iPixY <nPixYBins; ++iPixY) {    // DUT pixel Y binning
                    auto name = name_prefix + "_PixXY"+std::to_string(iPixX)+std::to_string(iPixY);
                    m_tbroodataset_collection->Add(std::make_unique<TbRooDataSet>(name.c_str(),title.c_str(),ras) );
                }
            }
            break;
        // ___________________________________________________________________________________
        case SegmentationScheme::DutPixel: // inherit from DUT and Pixel binning scheme (Dut binned IntraPixel study)
            std::cout << "[INFO]:: Taking the DUT and Pixel binning segmentation..." << std::endl;
            std::cout << "[INFO]:: Creating " << nDutBins*nPixXBins*nPixYBins<< " TbRooDataSets..." << std::endl;
            for (unsigned i = 0; i < nDutBins; ++i) {                               // DUT binning
                for (unsigned iPixX = 0; iPixX < nPixXBins; ++iPixX) {       // DUT pixel X binning
                    for (unsigned iPixY = 0; iPixY < nPixYBins; ++iPixY) {   // DUT pixel Y binning
                        auto name = name_prefix + "_DutBin"+std::to_string(i)+"_PixXY"+std::to_string(iPixX)+std::to_string(iPixY);
                        m_tbroodataset_collection->Add(std::make_unique<TbRooDataSet>(name.c_str(),title.c_str(),ras) );
                    }
                }
            }
            break;
    }
    std::cout << "[DEBUG]:: collection size " << m_tbroodataset_collection->Size() << std::endl;
    std::cout << "[STATUS]:: RooFitSvc::CreateTbRooDataSetCollection: DONE" << std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void RooFitSvc::ClearAllRooDataSet(){

    m_tbroodataset.reset(nullptr);

    m_tbroodataset_collection.reset(nullptr);

    m_rcategory.reset(nullptr);

    for (auto &irrv : m_rrv_collection)
        irrv.reset(nullptr);

}

////////////////////////////////////////////////////////////////////////////////////
///
TbRooDataSet* RooFitSvc::GetDutTbRooDataSetPtr(const std::string& varName) {
    if(m_tbroodataset) {
        if(varName.empty()) // No variable to be check if it's included in the RooDataSet
            return m_tbroodataset.get();
        else {
            if(m_tbroodataset->Includes(varName))
                return m_tbroodataset.get();
        }
        std::cout << "[WARNING]:: RooFitSvc::Trying to get TbRooDataSet ptr for not exisitng variable: "<<varName << std::endl;
        return nullptr;
    }
    std::cout << "[DEBUG]:: RooFitSvc::Trying to get TbRooDataSet ptr for not exisitng data "<< std::endl;
    return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
///
TbRooDataSetCollection* RooFitSvc::GetTbRooDataSetCollectionPtr(const std::string& varName){
    if(m_tbroodataset_collection){
        if(varName.empty()) // No variable to be check if it's included in the RooDataSet
            return m_tbroodataset_collection.get();
        else{
            if(m_tbroodataset_collection->Includes(varName))
                return m_tbroodataset_collection.get();
        }
        std::cout << "[WARNING]:: RooFitSvc::Trying to get TbRooDataSetCollection ptr for not exisitng variable: "<<varName << std::endl;
        return nullptr;
    }
    std::cout << "[DEBUG]:: RooFitSvc::Trying to get TbRooDataSetCollection ptr for not exisitng data "<< std::endl;
    return nullptr;
}

