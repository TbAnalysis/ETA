#include <pybind11/pybind11.h>
#include "RunSvc.h"

namespace py = pybind11;

void PyRunSvc(py::module &m) {
    py::class_<RunSvc, std::shared_ptr<RunSvc>, RunNode>(m, "RunSvc")
    .def(py::init<>());
}