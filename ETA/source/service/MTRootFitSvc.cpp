//
// Created by brachwal on 31.01.20.
//

#include "MTRooFitSvc.h"
#include "DutBinning.h"
#include "TbGaudi.h"
#include "TbJob.h"
#include "Globals.h"
#include "RooRealVar.h"
#include "RooArgSet.h"
#include "RooCategory.h"
#include "DutPix.h"
#include "TThread.h"

////////////////////////////////////////////////////////////////////////////////////
///
MTRooFitSvc::~MTRooFitSvc() {
    ClearAllRooDataSet();
}

////////////////////////////////////////////////////////////////////////////////////
///
void MTRooFitSvc::ClearAllRooDataSet(){

//    for(auto& iRRV:m_mt_dut_binning_data_var) {
//        for (auto &ival:iRRV.second)
//            delete ival;
//    }
//
//    m_mt_dut_binning_data_var.clear();
//
//    for (auto &ival:m_mt_dut_data_var)
//        delete ival.second;
//
//    m_mt_dut_data_var.clear();

}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<TbRooDataSet*> MTRooFitSvc::ThisSlotRooDataSet(){
    // TODO:: implement me.
    return std::vector<TbRooDataSet*>();
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<TbRooDataSet*> MTRooFitSvc::ThisSlotRooDataSetCollection(){
    TThread::Lock();
    const auto thisThreadID = std::this_thread::get_id();
    //std::cout<< "[INFO]:: MTRooFitSvc: Extracting DutPixDataSetCollection for " << thisThreadID << std::endl;
    auto collection = std::vector<TbRooDataSet*>(); // to be filled and returned.
    // initialize if this was called the first time
    if(m_mt_dut_pix_data_set.empty()){
        CreateTThreadedRooDataSetCollection();
    }
    for(const auto& iThreadedTbRds : m_mt_dut_pix_data_set)
        collection.push_back(iThreadedTbRds->Get().get()); // fill with raw pointers
    TThread::UnLock();
    return collection;
}

////////////////////////////////////////////////////////////////////////////////////
///
void MTRooFitSvc::CreateTThreadedDutRooDataSet(){
    std::cout << "[INFO]:: MTRootSvc:: Defining new set of DUT binning"
              << " TThreadedObject<TbRooDataSet>" << std::endl;
    // TODO:: implement me.
}

////////////////////////////////////////////////////////////////////////////////////
///
void MTRooFitSvc::CreateTThreadedRooDataSetCollection(){
    std::cout << "[INFO]:: MTRootSvc:: Defining new set of DUT binning (and pixel binned)"
              << " TThreadedObject<TbRooDataSet>" << std::endl;

    // build on top of all User defined variables
    if(m_rrv_collection.empty()){
        std::cout << "[ERROR]:: MTRooFitSvc::CreateTThreadedRooDataSetCollection:"
                     " any RooRealVar has been defined. Call DefineRooRealVar(...) for a given TbJob" << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    RooArgSet ras;
    for(auto& ivar : m_rrv_collection){
        std::cout << "[INFO]:: Including variable: " << ivar->GetName() << std::endl;
        ras.add(*(ivar.get())); // RooArgSet is not the owner here!
    }
//    std::cout << "[DEBUG]:: MTRooFitSvc:: got RooArgSet with size " << ras.getSize() << std::endl;
    unsigned counter(0);
    auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
    for (unsigned i = 0; i < nDutBins; ++i) {                               // DUT binning
        for (unsigned iPixX = 0; iPixX < DutPix::NBinsX(); ++iPixX) {       // DUT pixel X binning
            for (unsigned iPixY = 0; iPixY < DutPix::NBinsY(); ++iPixY) {   // DUT pixel Y binning
                //rdsCollection.push_back(new RooDataSet(name, name, *rasPtr));
                //auto rrv = new ROOT::TThreadedObject<RooRealVar>(TString(rrvName), TString(title), xLow, xUp, TString(unit));
                auto name = "TThreadedDutPixTbRDS_"+std::to_string(counter++);
                std::string title = "Title";
                m_mt_dut_pix_data_set.push_back(
                        std::unique_ptr<ROOT::TThreadedObject<TbRooDataSet>>(
                                new ROOT::TThreadedObject<TbRooDataSet>(name.c_str(),title.c_str(),ras) ) );
            }
        }
    }
    std::cout << "[INFO]:: MTRooFitSvc::CreateTThreadedRooDataSetCollection: DONE" << std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void MTRooFitSvc::MTRooDataSetMerge(){
    std::cout << "[INFO]:: MTRooFitSvc:: Merging TbRooDataSet collection..." << std::endl;

    /// Utility for https://root.cern/doc/v608/TThreadedObject_8hxx_source.html
    auto mergeFunc = [](std::shared_ptr<TbRooDataSet> target, std::vector<std::shared_ptr<TbRooDataSet>>& objs){
        if (!target) return;
        auto targetRdsPtr = target->GetRooDataSetPtr();
        auto name = static_cast<std::string>(targetRdsPtr->GetName());
        name = name.substr(0,name.find_last_of('_'))+"_MERGED";
        targetRdsPtr->SetName(name.c_str());
        for (const auto& obj : objs) {
            if (obj && obj != target){
                auto rdsPtr = obj->GetRooDataSetPtr();
                //std::cout<<"[DEBUG]:: MTRooFitSvc: "<<rdsPtr->GetName()<<" sum of entries: " << rdsPtr->sumEntries() << std::endl;
                targetRdsPtr->append(*rdsPtr);
            }
        }
        std::cout<<"[INFO]:: MTRooFitSvc: "<< name << " sum of entries: " << targetRdsPtr->sumEntries() << std::endl;
    };


    if(m_mt_dut_data_set) {
        m_tbroodataset.reset();
        m_tbroodataset = m_mt_dut_data_set->SnapshotMerge(mergeFunc);
    }

    if(!m_mt_dut_pix_data_set.empty()) {
        
        // Obsolete code! m_tbroodataset_collection.reset(new TbRooDataSetCollection(ThisTbJobPtr()->DutPtr()));
        for (auto &tbRds : m_mt_dut_pix_data_set) {
            m_tbroodataset_collection->Add(tbRds->SnapshotMerge(mergeFunc));
        }
    }

}