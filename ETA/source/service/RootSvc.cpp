//
// Created by brachwal on 11.10.18.
//

#include "RootSvc.h"
#include "DutBinning.h"
#include "DutStripBinning.h"
#include "Dut.h"
#include "DutPix.h"
#include "TbGaudi.h"
#include "TbAnalysis.h"
#include "Globals.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TThread.h"

HistUnit RootSvc::m_hist_unit = HistUnit::DutPositioning;

////////////////////////////////////////////////////////////////////////////////////
///
RootSvc::~RootSvc() {
    if(!m_dut_binning_data_hist.empty() ||
       !m_dut_data_hist.empty() ||
       !m_dut_data_2dhist.empty()) {
        std::cout << "[INFO]:: RootSvc:: Release memory. You can consider"
                  << " to call ClearEventLoopCollectedData() within your TbJobs loop."<<std::endl;
        ClearAllDutHist();
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void RootSvc::DefineDutBinningHist(std::string name, unsigned nBinsX, double xLow, double xUp){
    if(!Includes<std::string>(m_dut_binning_data_hist,name)){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: RootSvc:: Defining new set of TH1D DUT binning data histograms: " << name << std::endl;
        m_dut_binning_data_hist[name] = std::vector<TH1D*>{};
        auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
        for (unsigned i = 0; i < nDutBins; ++i) {
            auto hname = "TH1D_"+name+"_Bin"+itos(i);
            auto htitle = name+" DUT #bin "+itos(i)+";"+name+";Events";
            TH1D* hist = new TH1D(TString(hname),TString(htitle),nBinsX,xLow,xUp);
            m_dut_binning_data_hist.at(name).push_back(hist);
            m_dut_binning_data_hist.at(name).back()->SetDirectory(0); // have to release this object!
            m_is_dumped[m_dut_binning_data_hist.at(name).back()->GetName()] = false;
        }
    }
    else{
        std::cout << "[ERROR]::RootSvc::DefineDutBinningDataHist: The specified set of histograms"
                     " already exists. Call ClearDutBinningDataHist("<<name<<") first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void RootSvc::DefineDutBinningPixHist(const std::string& name, unsigned nBinsX, double xLow, double xUp){
    if(!Dut::IntraPixStudy()){
        std::cout << "[ERROR]:: RootSvc::DefineDutBinningHist:: Trying to define histograms for not IntraPixel study."
                  <<"The current analysis type is "<< Dut::StudyType() << std::endl;
        std::cout << "[INFO]:: You should call Dut::IntraPixStudy(true) in you analysis code."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    if(!Includes<std::string>(m_dut_binning_data_hist,name)){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: RootSvc:: Defining new set of TH1D DUT binning (and pixel binned) data histograms: " << name << std::endl;
        m_dut_binning_data_hist[name] = std::vector<TH1D*>{};
        auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
        unsigned index(0);
        for (unsigned i = 0; i < nDutBins; ++i) {
            for (unsigned iPixX = 0; iPixX < DutPix::NBinsX(); ++iPixX) {
                for (unsigned iPixY = 0; iPixY < DutPix::NBinsY(); ++iPixY) {
                    auto hname = "TH1D_" + name + "_Bin" +itos(i)+"_"+itos(iPixX)+"_"+itos(iPixY);
                    auto htitle = name + " DUT #bin " + itos(i) + " inPix bin ("+itos(iPixX)+","+itos(iPixY)+");" + name + ";Events";
                    TH1D *hist = new TH1D(TString(hname), TString(htitle), nBinsX, xLow, xUp);
                    m_dut_binning_data_hist.at(name).push_back(hist);
                    m_dut_binning_data_hist.at(name).back()->SetDirectory(0); // have to release this object!
                    m_is_dumped[m_dut_binning_data_hist.at(name).back()->GetName()] = false;
                    //std::cout << "[DEBUG]:: creating hist with indexing (" << i <<", " << iPixX << " ," << iPixY << "): "
                    //<<index++ << "  " << m_dut_binning_data_hist.at(name).back()->GetName() << std::endl;
                }
            }
        }
    }
    else{
        std::cout << "[ERROR]::RootSvc::DefineDutBinningDataHist: The specified set of pixel binned histograms"
                     " already exists. Call ClearDutBinningDataHist("<<name<<") first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    //for (const auto& ihist : m_dut_binning_data_hist.at(name))
    //    std::cout<<"[DEBUG]:: RootSvc::DefineDutBinningHist:: hname: "<< ihist->GetName()<<std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
void RootSvc::DefineDutBinning2DHist(std::string name, unsigned nBinsX, double xLow, double xUp,
                                                          unsigned nBinsY, double yLow, double yUp){
    if(!Includes<std::string>(m_dut_binning_data_2dhist,name)){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: RootSvc:: Defining new set of TH2D DUT binning data histograms: " << name << std::endl;
        m_dut_binning_data_2dhist[name] = std::vector<TH2D*>{};
        auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
        for (unsigned i = 0; i < nDutBins; ++i) {
            auto hname = "TH2D_"+name+"_Bin"+itos(i);
            //auto htitle = name+" DUT #bin "+itos(i)+";"+name+";Events";
            auto htitle = name+" DUT #bin "+itos(i)+";x [#um];y [#um]";
            TH2D* hist = new TH2D(TString(hname),TString(htitle),nBinsX,xLow,xUp,nBinsY,yLow,yUp);
            m_dut_binning_data_2dhist.at(name).push_back(hist);
            m_dut_binning_data_2dhist.at(name).back()->SetDirectory(0); // have to release this object!
            m_is_dumped[m_dut_binning_data_2dhist.at(name).back()->GetName()] = false;
        }
    }
    else{
        std::cout << "[ERROR]::RootSvc::DefineDutBinning2DHist: The specified set of histograms"
                     " already exists. Call ClearDutBinningDataHist("<<name<<") first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

}

////////////////////////////////////////////////////////////////////////////////////
///
void RootSvc::ClearDutBinningHist(const std::string& name){
    if( !ROOT::IsImplicitMTEnabled()) {
        if (Includes<std::string>(m_dut_binning_data_hist, name)) {
            if (TbGaudi::Debug())
                std::cout << "[INFO]:: RootSvc:: Removing the set of TH1D DUT binning data histograms: " << name
                          << std::endl;
            for (auto &ihist:m_dut_binning_data_hist.at(name)) delete ihist;
            m_dut_binning_data_hist.erase(name);
        }

        if (Includes<std::string>(m_dut_binning_data_2dhist, name)) {
            if (TbGaudi::Debug())
                std::cout << "[INFO]:: RootSvc:: Removing the set of TH2D DUT binning data histograms: " << name
                          << std::endl;
            for (auto &ihist:m_dut_binning_data_2dhist.at(name)) delete ihist;
            m_dut_binning_data_2dhist.erase(name);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH1D* RootSvc::DutHistPtr(std::string name, unsigned dutBinNumber){
    auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
    if(nDutBins == 1){
        std::cout << "[WARNING]:: RootSvc::Trying to extract DUT bin histogram which do not exists; "
                     "The DUT is not binned (NBins=1). Trying to find single DutHist for"<< name << std::endl;
        return DutHistPtr(name);
    }
    if(dutBinNumber>nDutBins){
        std::cout << "[ERROR]:: RootSvc::Trying to extract DUT bin histogram which do not exists:"
                     " called bin "<< dutBinNumber << " > " << nDutBins << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    if(Includes<std::string>(m_dut_binning_data_hist,name)) {
        if(m_dut_binning_data_hist.at(name).size()>dutBinNumber) {
            return m_dut_binning_data_hist.at(name).at(dutBinNumber);
        }
        else{
            std::cout << "[ERROR]:: RootSvc::Trying to extract DUT bin histogram which do not exists:"
                         " Histogram set: "<< name << " #bin " << dutBinNumber << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
            return nullptr; // to make compiler happy
        }
    }
    else{
        std::cout << "[ERROR]:: RootSvc::Trying to extract DUT bin histogram which do not exists:"
                     " Histogram set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH1D* RootSvc::DutPixHistPtr(const std::string& name, double inPix_x, double inPix_y){
    std::cout << "[INFO]:: RootSvc::DutPixHistPtr:: implement me."<<std::endl;
}

////////////////////////////////////////////////////////////////////////////////////
///
TH1D* RootSvc::DutPixHistPtr(const std::string& name, unsigned dutBinNumber, double inPix_x, double inPix_y){
    //_____________________________________________________________________________
    if(!Dut::IntraPixStudy()){
        std::cout << "[ERROR]:: RootSvc::DutHistPtr:: Trying to get histogram for not IntraPixel study."
                  <<"The current analysis type is "<< Dut::StudyType() << std::endl;
        std::cout << "[INFO]:: You should call Dut::IntraPixStudy(true) in you analysis code."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
    //_____________________________________________________________________________
    if(nDutBins == 1){
        std::cout << "[WARNING]:: RootSvc::Trying to extract DUT bin histograms which do not exist; "
                     "The DUT is not binned (NBins=1). Trying to find histograms from not binned Dut for"<< name << std::endl;
        return DutPixHistPtr(name,inPix_x,inPix_y);
    }

    //_____________________________________________________________________________
    if(dutBinNumber>nDutBins){
        std::cout << "[ERROR]:: RootSvc::Trying to extract DUT bin histograms which do not exists:"
                     " called bin "<< dutBinNumber << " > " << nDutBins << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    //_____________________________________________________________________________
    if(Includes<std::string>(m_dut_binning_data_hist,name)) {
        if(m_dut_binning_data_hist.at(name).size()>dutBinNumber) {
            // Note: objects ptr vectorization: dutBinning x PixX x PixY
            // -> check PixX and PixY are not out of scope
            if(inPix_x > Dut::PitchSize() && inPix_y > Dut::PitchSize()){
                std::cout << "[ERROR]:: RootSvc::DutPixHistPtr in pixel coordinates"
                             << " x ("<<inPix_x << ") "
                             << " y ("<<inPix_y << ") "
                             << " are out of scope!" << std::endl;
                TbGaudi::RunTimeError("Error in analysis workflow.");
            }
            // -> construct actual index
            auto xNBins = DutPix::NBinsX();
            auto xBin = DutPix::BinX(inPix_x);
            auto yNBins = DutPix::NBinsY();
            auto yBin = DutPix::BinY(inPix_y);
            unsigned index = dutBinNumber *  xNBins * yNBins + xBin * yNBins + yBin ;
            if (index > m_dut_binning_data_hist.at(name).size() ){
                std::cout << "[FATAL]:: RootSvc::DutPixHistPtr actual index is greater that the number of histograms being created"
                          << " - it shouldn't happen.." << std::endl;
                std::cout << "[DEBUG]:: dutBinNumber: " << dutBinNumber << std::endl;
                std::cout << "[DEBUG]:: xBin: " << xBin << std::endl;
                std::cout << "[DEBUG]:: xNBins: " << xNBins << std::endl;
                std::cout << "[DEBUG]:: yBin: " << yBin << std::endl;
                std::cout << "[DEBUG]:: yNBins: " << yNBins << std::endl;
                std::cout << "[DEBUG]:: >> got global index: " << index << std::endl;
                std::cout << "[DEBUG]:: >> number of hists : " << m_dut_binning_data_hist.at(name).size() << std::endl;
                TbGaudi::RunTimeError("Error in analysis workflow.");
            }

            return m_dut_binning_data_hist.at(name).at(index);
        }
        else{
            std::cout << "[ERROR]:: RootSvc::Trying to extract DUT bin histograms which do not exists:"
                         " Histogram set: "<< name << " #bin " << dutBinNumber << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
            return nullptr; // to make compiler happy
        }
    }
    else{
        std::cout << "[ERROR]:: RootSvc::Trying to extract DUT bin histogram which do not exists:"
                     " Histogram set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
}


////////////////////////////////////////////////////////////////////////////////////
///
TH1D* RootSvc::DutHistPtr(const std::string& name){
    if(Includes<std::string>(m_dut_data_hist,name)) {
        return m_dut_data_hist.at(name);
    }
    else{
        std::cout << "[ERROR]:: RootSvc::Trying to extract DUT histogram which do not exists:"
                     " Histogram: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH1D* RootSvc::DutHistPtr(const std::string& name, double inPix_x, double inPix_y){
    //_____________________________________________________________________________
    if(!Dut::IntraPixStudy()){
        std::cout << "[ERROR]:: RootSvc::DutHistPtr:: Trying to get histogram for not IntraPixel study."
                  <<"The current analysis type is "<< Dut::StudyType() << std::endl;
        std::cout << "[INFO]:: You should call Dut::IntraPixStudy(true) in you analysis code."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    //_____________________________________________________________________________
    // Note: objects ptr vectorization: PixX x PixY -> construct actual index:
    // implement me.
    return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
///
TH2D* RootSvc::Dut2DHistPtr(std::string name, unsigned dutBinNumber){
    auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
    if(nDutBins == 1){
        std::cout << "[WARNING]:: RootSvc::Trying to extract DUT bin histogram which do not exists; "
                     "The DUT is not binned (NBins=1). Trying to find single DutHist for"<< name << std::endl;
        return Dut2DHistPtr(name);
    }
    if(dutBinNumber>nDutBins){
        std::cout << "[ERROR]:: RootSvc::Trying to extract DUT bin histogram which do not exists:"
                     " called bin "<< dutBinNumber << " > " << nDutBins << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    if (Includes<std::string>(m_dut_binning_data_2dhist, name)) {
        if (m_dut_binning_data_2dhist.at(name).size() > dutBinNumber) {
            //std::cout << "[DEBUG]:: RootSvc:: returning ptr for TH2D " << name << " bin: " << dutBinNumber << std::endl;
            return m_dut_binning_data_2dhist.at(name).at(dutBinNumber);
        }
        else {
            std::cout << "[ERROR]:: RootSvc::Trying to extract DUT bin 2D histogram which do not exists:"
                         " Histogram set: " << name << " #bin " << dutBinNumber << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
            return nullptr; // to make compiler happy
        }
    }
    std::cout << "[ERROR]:: RootSvc::Trying to extract DUT bin histogram which do not exists:"
                 " Histogram set: "<< name << std::endl;
    TbGaudi::RunTimeError("Error in analysis workflow.");
    return nullptr; // to make compiler happy
}

////////////////////////////////////////////////////////////////////////////////////
///
TH2D* RootSvc::Dut2DHistPtr(const std::string& name){
    if(Includes<std::string>(m_dut_data_2dhist,name)) {
        return m_dut_data_2dhist.at(name);
    }
    else{
        std::cout << "[ERROR]:: RootSvc::Trying to extract DUT 2D histogram which do not exists:"
                     " Histogram: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void RootSvc::DefineDutHist(std::string name, unsigned nBinsX, double xLow, double xUp){
    if(!Includes<std::string>(m_dut_data_hist,name)) {
        if (TbGaudi::Debug())
            std::cout << "[INFO]:: RootSvc:: Defining new TH1D DUT binning data histogram: " << name << std::endl;
        auto hname = "TH1D_" + name;
        auto htitle = name + " DUT ;" + name + ";Events";
        TH1D *hist = new TH1D(TString(hname), TString(htitle), nBinsX, xLow, xUp);
        m_dut_data_hist[name] = hist;
        m_dut_data_hist.at(name)->SetDirectory(0); // have to release this object!
        m_is_dumped[m_dut_data_hist.at(name)->GetName()] = false;
    }
    else{
        std::cout << "[ERROR]::RootSvc::DefineDutBinningDataHist: The specified set of histograms"
                     " already exists. Call ClearDutBinningDataHist("<<name<<") first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void RootSvc::DefineDut2DHist(std::string name,
                                unsigned nBinsX, double xLow, double xUp,
                                unsigned nBinsY, double yLow, double yUp){

}

////////////////////////////////////////////////////////////////////////////////////
///
void RootSvc::SetDutBinContent2DHist(std::string name, unsigned x, unsigned y, double value){

}

////////////////////////////////////////////////////////////////////////////////////
///
void RootSvc::SetDutBinContent2DHist(std::string name, unsigned dutBin, double value){

}

////////////////////////////////////////////////////////////////////////////////////
///
void RootSvc::ClearAllDutHist(){
    // Remove binning related histograms
    std::cout << "[INFO]:: RootSvc:: Number of DUT binning data histograms to be deleted "
              << m_dut_binning_data_hist.size() * m_dut_binning_data_hist.begin()->second.size() << std::endl;
    for (auto iduthist: m_dut_binning_data_hist)
        ClearDutBinningHist(iduthist.first);
    m_dut_binning_data_hist.clear();

    std::cout << "[INFO]:: RootSvc:: Number of DUT binning data 2D histograms to be deleted "
              << m_dut_binning_data_2dhist.size() * m_dut_binning_data_2dhist.begin()->second.size() << std::endl;
    for (auto iduthist: m_dut_binning_data_2dhist)
        ClearDutBinningHist(iduthist.first);
    m_dut_binning_data_2dhist.clear();

    // Remove general histograms
    std::cout << "[INFO]:: RootSvc:: Number of DUT data histograms (TH1D) to be deleted "
              << m_dut_data_hist.size() << std::endl;
    for (auto iduthist: m_dut_data_hist) {
        if (TbGaudi::Debug())
            std::cout << "[INFO]:: RootSvc:: Removing the TH1D DUT data histogram: " << iduthist.first
                      << std::endl;
        delete iduthist.second;
    }
    m_dut_data_hist.clear();
    std::cout << "[INFO]:: RootSvc:: Number of DUT data histograms (TH2D) to be deleted "
              << m_dut_data_2dhist.size() << std::endl;
    for (auto iduthist: m_dut_data_2dhist) {
        if (TbGaudi::Debug())
            std::cout << "[INFO]:: RootSvc:: Removing the TH2D DUT data histogram: " << iduthist.first
                      << std::endl;
        delete iduthist.second;
    }
    m_dut_data_2dhist.clear();
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<TH1D*>* RootSvc::DutBinningTH1VectorPtr(std::string name){
    if(Includes<std::string>(m_dut_binning_data_hist,name)) {
        return &m_dut_binning_data_hist.at(name);
    } else{
        std::cout << "[ERROR]:: RootSvc::Trying to get set of DUT TH1D which do not exists:"
                     " TH1D set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::string RootSvc::TH2AxesTitles() const{
    switch (RootSvc::GetHistUnit()){
        case HistUnit::DutPositioning:
            if(ThisTbJobPtr()->DutPtr()->IsStripBinning() && !DutStripBinning::Is2DimMapping())
                return ";Strip Number;a.u."; // LHCb UT like
            else
                return ";column;row"; // LHCb VeloPix like
        case HistUnit::PitchSize:
            return ";[#mum];[#mum]";
        case HistUnit::DutSize:
            return ";[mm];[mm]";
    }
}