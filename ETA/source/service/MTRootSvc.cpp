//
// Created by brachwal on 30.01.20.
//

#include "MTRootSvc.h"
#include "TThread.h"
#include "TH1D.h"
#include "TH2D.h"
#include "DutPix.h"
#include "Globals.h"
#include "TbGaudi.h"
#include "TbJob.h"

////////////////////////////////////////////////////////////////////////////////////
///
MTRootSvc::~MTRootSvc() {
        ClearAllDutHist();
}

////////////////////////////////////////////////////////////////////////////////////
///
void MTRootSvc::MTDefineDutBinning2DHist(std::string name, unsigned nBinsX, double xLow, double xUp,
                                                              unsigned nBinsY, double yLow, double yUp){
    if(!Includes<std::string>(m_mt_dut_binning_data_2dhist,name)){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: MTRootSvc:: Defining new set of DUT binning TThreadedObject<TH2>: " << name << std::endl;
        m_mt_dut_binning_data_2dhist[name] = std::vector<ROOT::TThreadedObject<TH2D>*>{};
        auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
        for (unsigned i = 0; i < nDutBins; ++i) {
            auto hname = "MTTH2D_"+name+"_Bin"+itos(i);
            auto htitle = name+" DUT #bin "+itos(i)+";x [#um];y [#um]";
            auto histPtr = new ROOT::TThreadedObject<TH2D>(TString(hname),TString(htitle),nBinsX,xLow,xUp,nBinsY,yLow,yUp);
            m_mt_dut_binning_data_2dhist.at(name).push_back(histPtr);
        }
    }
    else{
        std::cout << "[ERROR]::MTRootSvc::MTDefineDutBinning2DHist: The specified set of TThreadedObject<TH2>"
                     " already exists. Call ClearDutBinningDataHist("<<name<<") first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

}

////////////////////////////////////////////////////////////////////////////////////
///
void MTRootSvc::MTDefineDutBinningHist(const std::string& name, unsigned nBinsX, double xLow, double xUp){
    if(!Includes<std::string>(m_mt_dut_binning_data_hist,name)){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: MTRootSvc:: Defining new set of DUT binning TThreadedObject<TH1>: " << name << std::endl;
        m_mt_dut_binning_data_hist[name] = std::vector<ROOT::TThreadedObject<TH1D>*>{};
        auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
        for (unsigned i = 0; i < nDutBins; ++i) {
            auto hname = "MTTH1D_"+name+"_Bin"+itos(i);
            auto htitle = name+" DUT #bin "+itos(i)+";"+name+";Events";
            auto histPtr = new ROOT::TThreadedObject<TH1D>(TString(hname),TString(htitle),nBinsX,xLow,xUp);
            m_mt_dut_binning_data_hist.at(name).push_back(histPtr);
        }
    }
    else{
        std::cout << "[ERROR]::MTRootSvc::DefineDutBinningDataHist: The specified set of TThreadedObject<TH1>"
                     " already exists. Call ClearDutBinningDataHist("<<name<<") first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void MTRootSvc::MTDefineDutBinningPixHist(const std::string& name, unsigned nBinsX, double xLow, double xUp){
    if(!Dut::IntraPixStudy()){
        std::cout << "[ERROR]:: MTRootSvc::DefineDutBinningHist:: Trying to define histograms for not IntraPixel study."
                  <<"The current analysis type is "<< Dut::StudyType() << std::endl;
        std::cout << "[INFO]:: You should call Dut::IntraPixStudy(true) in you analysis code."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    if(!Includes<std::string>(m_mt_dut_binning_data_hist,name)){
        if(TbGaudi::Debug())
            std::cout << "[INFO]:: MTRootSvc:: Defining new set of DUT binning (and pixel binned) TThreadedObject<TH1>: " << name << std::endl;
        m_mt_dut_binning_data_hist[name] = std::vector<ROOT::TThreadedObject<TH1D>*>{};
        auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
        unsigned index(0);
        for (unsigned i = 0; i < nDutBins; ++i) {
            for (unsigned iPixX = 0; iPixX < DutPix::NBinsX(); ++iPixX) {
                for (unsigned iPixY = 0; iPixY < DutPix::NBinsY(); ++iPixY) {
                    auto hname = "MTTH1D_" + name + "_Bin" +itos(i)+"_"+itos(iPixX)+"_"+itos(iPixY);
                    auto htitle = name + " DUT #bin " + itos(i) + " inPix bin ("+itos(iPixX)+","+itos(iPixY)+");" + name + ";Events";
                    auto histPtr = new ROOT::TThreadedObject<TH1D>(TString(hname),TString(htitle), nBinsX, xLow, xUp);
                    m_mt_dut_binning_data_hist.at(name).push_back(histPtr);
                }
            }
        }
    }
    else{
        std::cout << "[ERROR]::MTRootSvc::DefineDutBinningDataHist: The specified set of pixel binned TThreadedObject<TH1>"
                     " already exists. Call ClearDutBinningDataHist("<<name<<") first."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void MTRootSvc::ClearAllDutHist(){
    if( ROOT::IsImplicitMTEnabled()) {
        std::cout << "[INFO]:: MTRootSvc:: Number of DUT binning TThreadedObject<TH1> to be deleted "
                  << m_mt_dut_binning_data_hist.size() * m_mt_dut_binning_data_hist.begin()->second.size() << std::endl;
        for (auto iduthist: m_mt_dut_binning_data_hist)
            ClearDutBinningHist(iduthist.first);
        m_mt_dut_binning_data_hist.clear();

        std::cout << "[INFO]:: MTRootSvc:: Number of DUT binning TThreadedObject<TH2> to be deleted "
                  << m_mt_dut_binning_data_2dhist.size() * m_mt_dut_binning_data_2dhist.begin()->second.size()
                  << std::endl;
        for (auto iduthist: m_mt_dut_binning_data_2dhist)
            ClearDutBinningHist(iduthist.first);
        m_mt_dut_binning_data_2dhist.clear();
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void MTRootSvc::ClearDutBinningHist(const std::string& name){
    if( ROOT::IsImplicitMTEnabled()) {
        //___ TH1 _____________________________________________________
        if (Includes<std::string>(m_mt_dut_binning_data_hist, name)) {
            if (TbGaudi::Debug())
                std::cout << "[INFO]:: MTRootSvc:: Removing the set of DUT binning TThreadedObject<TH1> objects: " << name
                          << std::endl;
            for (auto &ihist:m_mt_dut_binning_data_hist.at(name)) delete ihist;
            m_mt_dut_binning_data_hist.erase(name);
        }
        //___ TH2 _____________________________________________________
        if (Includes<std::string>(m_mt_dut_binning_data_2dhist, name)) {
            if (TbGaudi::Debug())
                std::cout << "[INFO]:: MTRootSvc:: Removing the set of DUT TThreadedObject<TH2> objects: " << name
                          << std::endl;
            for (auto &ihist:m_mt_dut_binning_data_2dhist.at(name)) delete ihist;
            m_mt_dut_binning_data_2dhist.erase(name);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH2D* MTRootSvc::MTDut2DHistPtr(const std::string& name, unsigned dutBinNumber){
    auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
    if(nDutBins == 1){
        std::cout << "[WARNING]:: MTRootSvc::Trying to extract DUT bin TThreadedObject<TH2> which do not exists; "
                     "The DUT is not binned (NBins=1). Trying to find single DutHist for"<< name << std::endl;
        return MTDut2DHistPtr(name);
    }
    if(dutBinNumber>nDutBins){
        std::cout << "[ERROR]:: MTRootSvc::Trying to extract DUT bin TThreadedObject<TH2> which do not exists:"
                     " called bin "<< dutBinNumber << " > " << nDutBins << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    if( ROOT::IsImplicitMTEnabled() ){
        if (Includes<std::string>(m_mt_dut_binning_data_2dhist, name)) {
            if (m_mt_dut_binning_data_2dhist.at(name).size() > dutBinNumber)
                // For performance reasons, a copy of the pointer associated to this thread on the stack is used:
                return m_mt_dut_binning_data_2dhist.at(name).at(dutBinNumber)->Get().get();
            else {
                std::cout << "[ERROR]:: MTRootSvc::Trying to extract DUT bin TThreadedObject<TH2> which do not exists:"
                             " Histogram set: " << name << " #bin " << dutBinNumber << std::endl;
                TbGaudi::RunTimeError("Error in analysis workflow.");
            }
        }
    }
    // In success it should come to these lines
    std::cout << "[ERROR]:: MTRootSvc:: Trying to extract DUT bin TThreadedObject<TH2> which do not exists:"
                    " Histogram set: "<< name << std::endl;
    TbGaudi::RunTimeError("Error in analysis workflow.");
    return nullptr; // make compiler not complaning
}

////////////////////////////////////////////////////////////////////////////////////
///
TH1D* MTRootSvc::MTDutHistPtr(const std::string& name, unsigned dutBinNumber){
    auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
    if(nDutBins == 1){
        std::cout << "[WARNING]:: MTRootSvc::Trying to extract DUT bin TThreadedObject<TH1> which do not exists; "
                     "The DUT is not binned (NBins=1). Trying to find single DutHist for"<< name << std::endl;
        return MTDutHistPtr(name);
    }
    if(dutBinNumber>nDutBins){
        std::cout << "[ERROR]:: MTRootSvc::Trying to extract DUT bin TThreadedObject<TH1> which do not exists:"
                     " called bin "<< dutBinNumber << " > " << nDutBins << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }
    if(Includes<std::string>(m_mt_dut_binning_data_hist,name)) {
        if(m_mt_dut_binning_data_hist.at(name).size()>dutBinNumber) {
            // For performance reasons, a copy of the pointer associated to this thread on the stack is used:
            return m_mt_dut_binning_data_hist.at(name).at(dutBinNumber)->Get().get();
        }
        else{
            std::cout << "[ERROR]:: MTRootSvc::Trying to extract DUT bin TThreadedObject<TH1> which do not exists:"
                         " Histogram set: "<< name << " #bin " << dutBinNumber << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
            return nullptr; // to make compiler happy
        }
    }
    else{
        std::cout << "[ERROR]:: MTRootSvc::Trying to extract DUT bin TThreadedObject<TH1> which do not exists:"
                     " Histogram set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH1D* MTRootSvc::MTDutHistPtr(const std::string& name){
    if(Includes<std::string>(m_mt_dut_data_hist,name)) {
        // Access the pointer corresponding to the current slot, and then wrapped object
        return m_mt_dut_data_hist.at(name)->Get().get();
    }
    else{
        std::cout << "[ERROR]:: MTRootSvc::Trying to extract DUT TThreadedObject<TH1> which do not exists:"
                     " Histogram: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH2D* MTRootSvc::MTDut2DHistPtr(const std::string& name){
    if(Includes<std::string>(m_mt_dut_data_2dhist,name)) {
        // Access the pointer corresponding to the current slot, and then wrapped object
        return m_mt_dut_data_2dhist.at(name)->Get().get();
    }
    else{
        std::cout << "[ERROR]:: MTRootSvc::Trying to extract DUT TThreadedObject<TH2> which do not exists:"
                     " Histogram: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH1D* MTRootSvc::MTDutPixHistPtr(const std::string& name, unsigned dutBinNumber, double inPix_x, double inPix_y){
    //_____________________________________________________________________________
    if(!Dut::IntraPixStudy()){
        std::cout << "[ERROR]:: MTRootSvc::MTDutHistPtr:: Trying to get TThreadedObject<TH1> for not IntraPixel study."
                  <<"The current analysis type is "<< Dut::StudyType() << std::endl;
        std::cout << "[INFO]:: You should call Dut::IntraPixStudy(true) in you analysis code."<< std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    auto nDutBins = ThisTbJobPtr()->DutPtr()->NBins();
    //_____________________________________________________________________________
    if(nDutBins == 1){
        std::cout << "[WARNING]:: MTRootSvc::Trying to extract DUT bin TThreadedObject<TH1> which do not exist; "
                     "The DUT is not binned (NBins=1). Trying to find TThreadedObject<TH1> from not binned Dut for"<< name << std::endl;
        return MTDutPixHistPtr(name,inPix_x,inPix_y);
    }

    //_____________________________________________________________________________
    if(dutBinNumber>nDutBins){
        std::cout << "[ERROR]:: MTRootSvc::Trying to extract DUT bin TThreadedObject<TH1> which do not exists:"
                     " called bin "<< dutBinNumber << " > " << nDutBins << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
    }

    //_____________________________________________________________________________
    if(Includes<std::string>(m_mt_dut_binning_data_hist,name)) {
        if(m_mt_dut_binning_data_hist.at(name).size()>dutBinNumber) {
            // Note: objects ptr vectorization: dutBinning x PixX x PixY
            // -> check PixX and PixY are not out of scope
            if(inPix_x > Dut::PitchSize() && inPix_y > Dut::PitchSize()){
                std::cout << "[ERROR]:: MTRootSvc::MTDutPixHistPtr in pixel coordinates"
                          << " x ("<<inPix_x << ") "
                          << " y ("<<inPix_y << ") "
                          << " are out of scope!" << std::endl;
                TbGaudi::RunTimeError("Error in analysis workflow.");
            }
            // -> construct actual index
            auto xNBins = DutPix::NBinsX(); auto xBin = DutPix::BinX(inPix_x);
            auto yNBins = DutPix::NBinsY(); auto yBin = DutPix::BinY(inPix_y);
            unsigned index = dutBinNumber *  xNBins * yNBins + xBin * yNBins + yBin ;
            if (index > m_mt_dut_binning_data_hist.at(name).size() ){
                std::cout << "[FATAL]:: MTRootSvc::MTDutPixHistPtr actual index is greater that the number of TThreadedObject<TH1> being created"
                          << " - it shouldn't happen.." << std::endl;
                std::cout << "[DEBUG]:: dutBinNumber: " << dutBinNumber << std::endl;
                std::cout << "[DEBUG]:: xBin: " << xBin << std::endl;
                std::cout << "[DEBUG]:: xNBins: " << xNBins << std::endl;
                std::cout << "[DEBUG]:: yBin: " << yBin << std::endl;
                std::cout << "[DEBUG]:: yNBins: " << yNBins << std::endl;
                std::cout << "[DEBUG]:: >> got global index: " << index << std::endl;
                std::cout << "[DEBUG]:: >> number of hists : " << m_mt_dut_binning_data_hist.at(name).size() << std::endl;
                TbGaudi::RunTimeError("Error in analysis workflow.");
            }
            // Access the pointer corresponding to the current slot, and then wrapped object
            // NOTE: Currently is inefficient - it implies a lookup in a mapping between the threadIDs and the slot indices.
            // NOTE: I do not know how to workaround this. Typically, once would copy the pointer onto the stack and proceed into
            //       the loop....
            return m_mt_dut_binning_data_hist.at(name).at(index)->Get().get();
        }
        else{
            std::cout << "[ERROR]:: MTRootSvc::Trying to extract DUT bin TThreadedObject<TH1> which do not exists:"
                         " TThreadedObject<TH1> set: "<< name << " #bin " << dutBinNumber << std::endl;
            TbGaudi::RunTimeError("Error in analysis workflow.");
            return nullptr; // to make compiler happy
        }
    }
    else{
        std::cout << "[ERROR]:: MTRootSvc::Trying to extract DUT bin TThreadedObject<TH1> which do not exists:"
                     " TThreadedObject<TH1> set: "<< name << std::endl;
        TbGaudi::RunTimeError("Error in analysis workflow.");
        return nullptr; // to make compiler happy
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH1D* MTRootSvc::MTDutPixHistPtr(const std::string& name, double inPix_x, double inPix_y){
    std::cout << "[INFO]:: MTRootSvc::MTDutPixHistPtr:: implement me."<<std::endl;
    TbGaudi::RunTimeError("NOT IMPLEMENTED METHOD");
    return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<TH1D*> MTRootSvc::MTThisSlotTH1DCollection(const std::string& name){
    if(Includes<std::string>(m_mt_dut_binning_data_hist,name)) {
        auto threadedCollection = m_mt_dut_binning_data_hist.at(name);
        auto collectionSize = m_mt_dut_binning_data_hist.at(name).size();
        std::vector < TH1D * > histCollection;
        // For performance reasons, a copy of the pointer associated to this thread on the stack is used:
        for (auto iThrObject : threadedCollection)
            histCollection.push_back(iThrObject->Get().get());
        return histCollection;
    }
    else {
        std::cout << "[INFO]:: MTRootSvc::MTThisSlotTH1DCollection:: not found collection: " << name <<std::endl;
        TbGaudi::RunTimeError("Error in analysis flow");
        return std::vector<TH1D*>();
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
TH1D* MTRootSvc::MTThisSlotTH1D(const std::string& name){
    if(Includes<std::string>(m_mt_dut_data_hist,name)) {
        return m_mt_dut_data_hist.at(name)->Get().get();
    }
    else {
        std::cout << "[INFO]:: MTRootSvc::MTThisSlotTH1D:: not found object: " << name <<std::endl;
        TbGaudi::RunTimeError("Error in analysis flow");
        return nullptr;
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
std::vector<TH2D*> MTRootSvc::MTThisSlotTH2DCollection(const std::string& name){
    if(Includes<std::string>(m_mt_dut_binning_data_2dhist,name)) {
        auto threadedCollection = m_mt_dut_binning_data_2dhist.at(name);
        auto collectionSize = m_mt_dut_binning_data_2dhist.at(name).size();
        std::vector < TH2D * > histCollection;
        // For performance reasons, a copy of the pointer associated to this thread on the stack is used:
        for (auto iThrObject : threadedCollection)
            histCollection.push_back(iThrObject->Get().get());
        return histCollection;
    }
    else {
        std::cout << "[INFO]:: MTRootSvc::MTThisSlotTH2DCollection:: not found collection: " << name <<std::endl;
        TbGaudi::RunTimeError("Error in analysis flow");
        return std::vector<TH2D*>();
    }
}

////////////////////////////////////////////////////////////////////////////////////
///
void MTRootSvc::MTHistMerge(){
    if( !ROOT::IsImplicitMTEnabled() )
        return;
    ROOT::DisableImplicitMT();
    // ____________________________________________________________________________
    // Merge and set data to TH1DVectorCollection
    if(!m_mt_dut_binning_data_hist.empty()) {
        std::cout << "[INFO]:: MTRootSvc:: Merging TH1D DUT binning data histograms: "<< std::endl;
        for (auto &vHistTTObj : m_mt_dut_binning_data_hist) {
            auto name = vHistTTObj.first;
            std::cout << "[INFO]:: " << name << std::endl;
            m_dut_binning_data_hist[name] = std::vector<TH1D*>();
            for (auto &histTTObj : vHistTTObj.second) {
                m_dut_binning_data_hist.at(name).push_back(static_cast<TH1D*>(histTTObj->SnapshotMerge().release()));
                m_dut_binning_data_hist.at(name).back()->SetDirectory(0);
            }
        }
    }
    // ____________________________________________________________________________
    // Merge and set data to TH2DVectorCollection
    if(!m_mt_dut_binning_data_2dhist.empty()) {
        std::cout << "[INFO]:: MTRootSvc:: Merging TH2D DUT binning data histograms: "<< std::endl;
        for (auto &vHistTTObj : m_mt_dut_binning_data_2dhist) {
            auto name = vHistTTObj.first;
            std::cout << "[INFO]:: " << name << std::endl;
            m_dut_binning_data_2dhist[name] = std::vector<TH2D*>();
            unsigned counter(0);
            for (auto &histTTObj : vHistTTObj.second) {
                //std::cout << "[DEBUG]:: " << ++counter << std::endl;
                m_dut_binning_data_2dhist.at(name).push_back(static_cast<TH2D*>(histTTObj->SnapshotMerge().release()));
                m_dut_binning_data_2dhist.at(name).back()->SetDirectory(0);
            }
        }
    }
    // ____________________________________________________________________________
    // Merge and set data to TH1DCollection
    if(!m_mt_dut_data_hist.empty()) {
        std::cout << "[INFO]:: MTRootSvc:: Merging TH1D DUT data histograms: "<< std::endl;
        for (auto &histTTObj : m_mt_dut_data_hist) {
            auto name = histTTObj.first;
            std::cout << "[INFO]:: " << name << std::endl;
            m_dut_data_hist[name] = static_cast<TH1D*>(histTTObj.second->SnapshotMerge().release());
            m_dut_data_hist.at(name)->SetDirectory(0);
        }
    }
    // ____________________________________________________________________________
    // Merge and set data to TH2DCollection
    if(!m_mt_dut_data_2dhist.empty()) {
        std::cout << "[INFO]:: MTRootSvc:: Merging TH2D DUT data histograms: "<< std::endl;
        for (auto &histTTObj : m_mt_dut_data_2dhist) {
            auto name = histTTObj.first;
            std::cout << "[INFO]:: " << name << std::endl;
            m_dut_data_2dhist[name] = static_cast<TH2D*>(histTTObj.second->SnapshotMerge().release());
            m_dut_data_2dhist.at(name)->SetDirectory(0);
        }
    }
}