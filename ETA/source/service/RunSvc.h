//
// Created by brachwal on 12.10.18.
//

#ifndef RUNSVC_H
#define RUNSVC_H

#include "RunNode.h"
#include "Run.h"


class RunSvc : public RunNode {

    public:
        ///
        RunSvc() = default;

        ///
        RunSvc(RunSharedPtr run_ptr):RunNode(run_ptr){}

        ///
        virtual ~RunSvc() = default;

};

#endif //RUNSVC_H
