//
// Created by brachwal on 11.10.18.
//

#ifndef DUTROOTSVC_H
#define DUTROOTSVC_H

#include "TbJobNode.h"
#include <vector>
#include <map>

class TH1D;
class TH2D;

enum class HistUnit{DutSize,         // mm
                    PitchSize,       // um
                    DutPositioning}; // col / row

using TH1DVectorCollection = std::map<std::string, std::vector<TH1D*>>;
using TH2DVectorCollection = std::map<std::string, std::vector<TH2D*>>;
using TH1DCollection = std::map<std::string, TH1D*>;
using TH2DCollection = std::map<std::string, TH2D*>;
using UPairCollection = std::map<std::string, std::pair<unsigned,unsigned >>;

class RootSvc : public virtual TbJobNode {

    protected:
        ///
        TH1DVectorCollection m_dut_binning_data_hist;

        ///
        TH2DVectorCollection m_dut_binning_data_2dhist;

        ///
        TH1DCollection m_dut_data_hist;

        ///
        TH2DCollection m_dut_data_2dhist;

        ///
        static HistUnit m_hist_unit;

        ///
        std::map<std::string, bool> m_is_dumped;

    public:
        ///
        RootSvc() = default;

        ///
        RootSvc(TbJobSharedPtr tbjob_ptr):TbJobNode(tbjob_ptr){}

        ///
        virtual ~RootSvc();

        /// \brief Define (add to the std::map) the number of TH1D histograms for a given TbJob.
        /// The number of TH1D corresponds to the number of Dut bins defined by the User.
        /// \param name The name of the set of histograms (used also in the TH1D title and name).
        /// \param nBinsX The number of bins in particular TH1D instance.
        /// \param xLow The lower range for the particular TH1D instance.
        /// \param xUp The upper range for the particular TH1D instance.
        void DefineDutBinningHist(std::string name, unsigned nBinsX, double xLow, double xUp);

        ///
        void DefineDutBinningPixHist(const std::string& name, unsigned nBinsX, double xLow, double xUp);

        /// same as above but TH2D collection
        void DefineDutBinning2DHist(std::string name, unsigned nBinsX, double xLow, double xUp,
                                                      unsigned nBinsY, double yLow, double yUp);

        /// \brief Define (add to the std::map) the single TH1D histogram for a given TbJob.
        /// \param name The name of the set of histograms (used also in the TH1D title and name).
        /// \param nBinsX The number of bins in particular TH1D instance.
        /// \param xLow The lower range for the particular TH1D instance.
        /// \param xUp The upper range for the particular TH1D instance.
        void DefineDutHist(std::string name, unsigned nBinsX, double xLow, double xUp);

        /// \brief Define (add to the std::map) the single TH2D histogram for a given TbJob.
        /// \param name The name of the set of histograms (used also in the TH2D title and name).
        /// \param nBinsX The number of bins in particular TH2D instance on x-axis.
        /// \param xLow The lower range for the particular TH2D instance on x-axis.
        /// \param xUp The upper range for the particular TH2D instance on x-axis.
        /// \param nBinsY The number of bins in particular TH2D instance on y-axis.
        /// \param yLow The lower range for the particular TH2D instance on y-axis.
        /// \param yUp The upper range for the particular TH2D instance on y-axis.
        void DefineDut2DHist(std::string name, unsigned nBinsX, double xLow, double xUp,
                                               unsigned nBinsY, double yLow, double yUp);

        /// \brief SetBinContent to the TH2D histogram using pixel coordinates
        /// \param name The name of the TH2D to be filled.
        /// \param x x-coordinate of the pixel
        /// \param y y-coordinate of the pixel
        /// \param value the actual value to be set in the TH2D
        void SetDutBinContent2DHist(std::string name, unsigned x, unsigned y, double value);

        /// \brief SetBinContent to the TH2D histogram using global Dut binning scheme
        /// \param name The name of the TH2D to be filled.
        /// \param dutBin The global DUT bin number
        /// \param value the actual value to be set in the TH2D
        void SetDutBinContent2DHist(std::string name, unsigned dutBin, double value);

        /// \brief Remove the set of histograms from the std::map in a given TbJob
        /// \param name The name of the set to be removed.
        virtual void ClearDutBinningHist(const std::string& name);

        /// \brief Remove all stored histograms;
        virtual void ClearAllDutHist();

        ///
        TH1DCollection& Dut1DHistDataCollection() {return m_dut_data_hist; }

        ///
        TH2DCollection& Dut2DHistDataCollection() {return m_dut_data_2dhist; }

        ///
        TH1DVectorCollection& DutBinned1DHistDataCollection() {return m_dut_binning_data_hist; }

        ///
        TH2DVectorCollection& DutBinned2DHistDataCollection() {return m_dut_binning_data_2dhist; }

        /// \brief Get pointer to the particular TH1D, specified by:
        /// \param name The name of set (in the std::map) it belongs to,
        /// \param dutBinNumber The DUT bin number it is being identified with.
        /// The container if interest is m_dut_binning_data_hist
        TH1D* DutHistPtr(std::string name, unsigned dutBinNumber);

        ///
        TH1D* DutPixHistPtr(const std::string& name, unsigned dutBinNumber, double inPix_x, double inPix_y);

        ///
        TH1D* DutPixHistPtr(const std::string& name, double inPix_x, double inPix_y);

        ///
        TH2D* Dut2DHistPtr(std::string name, unsigned dutBinNumber);

        /// \brief Get pointer to the particular TH1D, specified by:
        /// \param name The name of set (in the std::map) it belongs to
        /// The container if interest is m_dut_data_hist
        TH1D* DutHistPtr(const std::string& name);

        ///
        TH1D* DutHistPtr(const std::string& name, double inPix_x, double inPix_y);

        ///
        TH2D* Dut2DHistPtr(const std::string& name);

        /// \brief Get pointer to the whole container of TH1D, specified by:
        /// \param name The name of set (in the std::map) it belongs to,
        std::vector<TH1D*>* DutBinningTH1VectorPtr(std::string name);

        ///
        static HistUnit GetHistUnit() {return m_hist_unit;}

        ///
        static void SetHistUnit(HistUnit unit) {m_hist_unit =unit;}

        ///
        std::string TH2AxesTitles() const;
};

#endif //DUTROOTSVC_H
