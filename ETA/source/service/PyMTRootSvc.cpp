#include <pybind11/pybind11.h>
#include "MTRootSvc.h"

namespace py = pybind11;

void PyMTRootSvc(py::module &m) {
    py::class_<MTRootSvc, std::shared_ptr<MTRootSvc>, RootSvc>(m, "MTRootSvc")
    .def(py::init<>());
}