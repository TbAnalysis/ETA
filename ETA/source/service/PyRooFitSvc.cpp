#include <pybind11/pybind11.h>
#include "RooFitSvc.h"

namespace py = pybind11;

void PyRooFitSvc(py::module &m) {
    py::class_<RooFitSvc, std::shared_ptr<RooFitSvc>, TbJobNode>(m, "RooFitSvc")
    .def(py::init<>());
}