//
// Created by brachwal on 31.01.20.
//

#ifndef DUTMTROOFITSVC_H
#define DUTMTROOFITSVC_H

#include "ROOT/TTreeProcessorMT.hxx"
#include "RooFitSvc.h"
#include <thread>

using MTTbRDS = std::unique_ptr<ROOT::TThreadedObject<TbRooDataSet>>;
using MTTbRDSCollection = std::vector<MTTbRDS>;

class MTRooFitSvc : public RooFitSvc {

    private:

        ///
        MTTbRDS m_mt_dut_data_set;

        ///
        MTTbRDSCollection m_mt_dut_pix_data_set;

        ///
        void CreateTThreadedDutRooDataSet();

        ///
        void CreateTThreadedRooDataSetCollection();

    public:

        ///
        MTRooFitSvc() = default;

        ///
        virtual ~MTRooFitSvc();

        /// \brief Remove all the RooDataSets
        virtual void ClearAllRooDataSet() override;


        /// TODO:
        std::vector<TbRooDataSet*> ThisSlotRooDataSet();

        ///
        std::vector<TbRooDataSet*> ThisSlotRooDataSetCollection();

        ///
        void MTRooDataSetMerge();


};
#endif //DUTMTROOFITSVC_H
