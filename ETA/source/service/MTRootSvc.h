//
// Created by brachwal on 30.01.20.
//

#ifndef DUTMTROOTSVC_H
#define DUTMTROOTSVC_H

#include "RootSvc.h"
#include "ROOT/TTreeProcessorMT.hxx"

using MTTH1DVectorCollection = std::map<std::string, std::vector<ROOT::TThreadedObject<TH1D>*>>;
using MTTH2DVectorCollection = std::map<std::string, std::vector<ROOT::TThreadedObject<TH2D>*>>;
using MTTH1DCollection = std::map<std::string, ROOT::TThreadedObject<TH1D>*>;
using MTTH2DCollection = std::map<std::string, ROOT::TThreadedObject<TH2D>*>;

class MTRootSvc: public RootSvc {

    protected:
        ///
        MTTH1DVectorCollection m_mt_dut_binning_data_hist;

        ///
        MTTH2DVectorCollection m_mt_dut_binning_data_2dhist;

        ///
        MTTH1DCollection m_mt_dut_data_hist;

        ///
        MTTH2DCollection m_mt_dut_data_2dhist;

    public:
        ///
        MTRootSvc() = default;

        ///
        MTRootSvc(TbJobSharedPtr tbjob_ptr):RootSvc(tbjob_ptr){}

        ///
        virtual ~MTRootSvc();

        /// Multithreaded version of RootSvc::DefineDutBinning2DHist
        void MTDefineDutBinning2DHist(std::string name, unsigned nBinsX, double xLow, double xUp,
                                                        unsigned nBinsY, double yLow, double yUp);
        ///
        void MTDefineDutBinningHist(const std::string& name, unsigned nBinsX, double xLow, double xUp);

        ///
        void MTDefineDutBinningPixHist(const std::string& name, unsigned nBinsX, double xLow, double xUp);

        ///
        TH2D* MTDut2DHistPtr(const std::string& name, unsigned dutBinNumber);

        ///
        TH2D* MTDut2DHistPtr(const std::string& name);

        ///
        TH1D* MTDutHistPtr(const std::string& name, unsigned dutBinNumber);

        ///
        TH1D* MTDutHistPtr(const std::string& name);

        ///
        TH1D* MTDutPixHistPtr(const std::string& name, unsigned dutBinNumber, double inPix_x, double inPix_y);

        ///
        TH1D* MTDutPixHistPtr(const std::string& name, double inPix_x, double inPix_y);

        /// \brief Remove the set of histograms from the std::map in a given TbJob
        /// \param name The name of the set to be removed.
        void ClearDutBinningHist(const std::string& name) override;

        /// \brief Remove all stored histograms;
        void ClearAllDutHist() override;

        ///
        void MTHistMerge();

        ///
        std::vector<TH1D*> MTThisSlotTH1DCollection(const std::string& name);

        ///
        std::vector<TH2D*> MTThisSlotTH2DCollection(const std::string& name);

        ///
        TH1D* MTThisSlotTH1D(const std::string& name);

};
#endif //DUTMTROOTSVC_H
