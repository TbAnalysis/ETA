#include <pybind11/pybind11.h>
#include "RootSvc.h"

namespace py = pybind11;

void PyRootSvc(py::module &m) {
    py::class_<RootSvc, std::shared_ptr<RootSvc>, TbJobNode>(m, "RootSvc")
    .def(py::init<>());
}