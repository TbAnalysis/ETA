#include "Globals.h"
 #include <filesystem>
#include <time.h>
#include <math.h>

namespace fs = std::filesystem;

/////////////////////////////////////////////////////////////////////////////////////////
void PressEnterToQuit(){
  int c;
  printf( "Press ENTER to quit... " );
  fflush( stdout );
  do c = getchar(); while ((c != '\n') && (c != EOF));
}

/////////////////////////////////////////////////////////////////////////////////////////
bool exists (const std::string& m_Fname) {

    std::ifstream f(m_Fname.c_str());
    return f.good();
}

/////////////////////////////////////////////////////////////////////////////////////////
TTree* GetTTreeFromTDir(std::string m_filename, std::string m_dir, std::string m_tree){

	TFile* file;

  	std::ifstream f(m_filename.c_str());
  	if(f.good()){
	  std::cout<< "[INFO]:: Open file : "<< m_filename << std::endl;
	  std::cout<< "[INFO]:: TTree     : "<< m_dir << "/" << m_tree << std::endl;
	  file = new TFile(TString(m_filename.c_str()));
	  if (m_dir=="")
	  	return (TTree*)file->Get(TString(m_tree.c_str()));
	  else
	  	return (TTree*)file->Get( TString(m_dir.c_str())+"/"+TString(m_tree.c_str()) );
	}
	else{
	  	std::cout<< "[ERROR]:: Couldn't find NTuple: "<< m_filename << std::endl;
		exit(1);
	}
}
////////////////////////////////////////////////////////////////////////////////////////
std::string idtos(const std::thread::id& m_id){
	std::stringstream ss;
	ss << m_id;
	return ss.str();
}

////////////////////////////////////////////////////////////////////////////////////////
std::string dtos(double m_d, int m_precission) {
	std::stringstream stream;
	stream << std::fixed << std::setprecision(m_precission) << m_d;
	return stream.str();
}

////////////////////////////////////////////////////////////////////////////////////////
std::string sto_lower(std::string data){
	std::transform(data.begin(), data.end(), data.begin(),
    [](unsigned char c){ return std::tolower(c); });
	return data;
}

////////////////////////////////////////////////////////////////////////////////////////
const std::string btos(bool val){
	return val ? "true" : "false";
}

////////////////////////////////////////////////////////////////////////////////////////
bool stob(const std::string& val){
	bool b;
	std::istringstream(sto_lower(val)) >> std::boolalpha >> b;
	return b;
}

////////////////////////////////////////////////////////////////////////////////////////
std::string dyn_dtos(double m_d, int m_max_precission){
	if(m_max_precission==0){
		std::cout << "[WARNING]:: dyn_dtos() called with zero presition. The dtos() is called for this!"<<std::endl;
		return dtos(m_d,m_max_precission);
	}
	auto value = round( m_d * 10. * m_max_precission ) / (10.*m_max_precission);
	auto after_decimal = value - trunc(value);
	if(after_decimal!=0.)
		return dtos(m_d,m_max_precission);
	else 
		return dtos(m_d,0);
}

////////////////////////////////////////////////////////////////////////////////////////
// std::string style "itoa":
// Contributions from
// Stuart Lowe, Ray-Yuan Sheu, Rodrigo de Salvo Braz, Luc Gallant, John Maloney and Brian Hunt
std::string itos(int value) {

		int base =10;
		std::string buf;

		// check that the base if valid
		if (base < 2 || base > 16) return buf;

		enum { kMaxDigits = 35 };
		buf.reserve( kMaxDigits ); // Pre-allocate enough space.

		int quotient = value;

		// Translating number to string with base:
		do {
			buf += "0123456789abcdef"[ std::abs( quotient % base ) ];
			quotient /= base;
		} while ( quotient );

		// Append the negative sign
		if ( value < 0) buf += '-';

		std::reverse( buf.begin(), buf.end() );
		return buf;
	}


////////////////////////////////////////////////////////////////////////////////////////
unsigned stou(std::string m_s){
	return static_cast<unsigned>(std::stoul(m_s,nullptr,0));
}

////////////////////////////////////////////////////////////////////////////////////////
double GetTF2UniformBinIntegral(TF2* m_func,
							 std::pair<int,int> m_bin_min,
							 std::pair<int,int> m_bin_max,
							 double m_normalization){
	// per pixel step
	int nPix_col = m_bin_max.first - m_bin_min.first;
	int nPix_row = m_bin_max.second - m_bin_min.second;

	double func_Xmin, func_Ymin, func_Xmax, func_Ymax;
	m_func->GetRange(func_Xmin, func_Ymin, func_Xmax, func_Ymax);

	double bin_integral;
	int pix_counter = 0;
	double col,row;
	for (int icol=0; icol<nPix_col;icol++){
		for (int irow=0; irow<nPix_row;irow++){
			col = m_bin_min.first+icol;
			row = m_bin_min.second+irow;
			bin_integral+=m_func->Eval(col,row)*m_normalization;
			pix_counter++;
		}

	}
	return bin_integral/pix_counter;
}

////////////////////////////////////////////////////////////////////////////////////////
bool IsInsideTEllipse(TEllipse* el, double px, double py)
  {

  	double x0 = el->GetX1();
    double y0 = el->GetY1();

    double epsilon = 0.001;
    if(abs(x0-px)<epsilon) px+=px/100; // increase slightly if called for very close to centre
    if(abs(y0-py)<epsilon) py+=py/100; // increase slightly if called for very close to centre

  	double r1 = el->GetR1();
    double r2 = el->GetR2();

	  if (r1 == 0 || r2 == 0){
		  std::cout << "[ERROR]:: Globals:: You call for IsInsideTEllipse with zero-radius ellipses:"
					<< "r1("<<r1<<") "
					<< "r2("<<r2<<")"<< std::endl;
		  // TODO:: TbGaudi::RunTimeError("Error in analysis workflow.");
		  return false;
	  }

    double ct = TMath::Cos(TMath::Pi() * el->GetTheta()/180.0);
  	double st = TMath::Sin(TMath::Pi() * el->GetTheta()/180.0);

    // NOTE: non-centered ellipse
    double dxp = px - x0;
    double dyp = py - y0;

    double dxpp = dxp*ct + dyp*st;
    double dypp = -dxp*st + dyp*ct;

    double phi = TMath::ATan2(dypp,dxpp); // radians!
    double tana = dypp/dxpp;

    double dxe =   r1 * TMath::Cos(phi);
    double dye =   r2 * TMath::Sin(phi);

     double distp = TMath::Sqrt(dxpp*dxpp + dypp*dypp);
     // double distr = TMath::Sqrt(dxe*dxe + dye*dye );
     double distr = TMath::Sqrt((1+tana*tana)/(1.0/(r1*r1) + tana*tana/(r2*r2)));

     if (distr >= distp){
     	return true;
     } else{
     	return false;
     }

  }

////////////////////////////////////////////////////////////////////////////////////////
void WriteHistos(std::string path, std::string fileName, std::vector<TH1D*>& histograms){
	fs::path dp (path);
	if(!fs::exists (dp)) fs::create_directory(dp);
	TString myfile = TString(path+"/"+fileName+".root");
	TFile* outfile = new TFile(myfile, "RECREATE");
	TList* hList = new TList();      // list of histograms to store
	for (auto &ihist : histograms){
		hList->Add(ihist);
	}
	hList->Write();
	outfile->Close();
	delete hList;
	delete outfile;
}

////////////////////////////////////////////////////////////////////////////////////////
// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
std::string CurrentDateTime() {
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	// Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
	// for more information about date/time format
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
	return buf;
}

////////////////////////////////////////////////////////////////////////////////////////
// Get current date format as YYYY-MM-DD
std::string CurrentDate() {
    auto dateTime = CurrentDateTime();
	return dateTime.substr(0,dateTime.find_first_of('.'));
}