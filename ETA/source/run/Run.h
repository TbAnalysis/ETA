/*! \brief Run class declaration
*   \author Bartek Rachwal (brachwal@agh.edu.pl)
*   \date April-2018
*/

#ifndef RUNINFO_H
#define RUNINFO_H

// std libriaries
#include <iostream>
#include <string>
#include <map>
#include "TbJobNode.h"

////////////////////////////////////////////////////////////////////////////////////
///
class Run : public TbJobNode {

	private:
		///
		unsigned m_number = 0.;

		///
		float m_bias_voltage = 0.;

		///
		float m_temperature = 0.;

		///
		static int m_default_cluster_size;

	public:

		///
		Run() = default;

		///
		Run(unsigned number,float voltage):m_number(number),m_bias_voltage(voltage){};

		///
		virtual ~Run() = default;

		void Initialize(TbJobSharedPtr tbjob_ptr);

		///
		const std::string Label() const;

		///
		static void DefaultClusterSize(int size) {m_default_cluster_size = size;}

		///
		static int DefaultClusterSize() {return m_default_cluster_size;}

};
#endif	// RUNINFO_H
