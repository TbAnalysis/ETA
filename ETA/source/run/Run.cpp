#include "Run.h"
#include "Globals.h"
#include "TbJob.h"

int Run::m_default_cluster_size = -1;

////////////////////////////////////////////////////////////////////////////////////
///
void Run::Initialize(TbJobSharedPtr tbjob_ptr){
    ThisTbJobPtr(tbjob_ptr);

    // Verify if expected parameters have been defined by user's input
    // - if not, fill them with default values
    if(tbjob_ptr->GetParam("RunClusterSize")=="-999"){
        std::cout << "[INFO]:: Run::Init:: The RunClusterSize NOT Specified. Using the default value: " << Run::DefaultClusterSize() << std::endl;
        tbjob_ptr->SetParam("RunClusterSize", itos(Run::DefaultClusterSize()));
    }

    m_number = tbjob_ptr->GetParam<unsigned>("RunNumber");
    m_bias_voltage = tbjob_ptr->GetParam<float>("RunBiasVoltage");
}

////////////////////////////////////////////////////////////////////////////////////
///
const std::string Run::Label() const {
    return std::to_string(m_number)+"_"+dtos(m_bias_voltage,2)+"V";
}