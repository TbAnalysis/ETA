#----------------------------------------------------------------------------
# Setup the project
cmake_minimum_required(VERSION 3.1)
project(TbGaudi_External)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
#----------------------------------------------------------------------------
# Configure the components (subprojects)
# NOTE: The order matters! The most independent ones should go first.
add_subdirectory(cmd)
add_subdirectory(toml)
add_subdirectory(spdlog)
add_subdirectory(pybind11)
