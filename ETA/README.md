# The TbGaudi core framework code
* [CHEP'18 proceedings](https://cernbox.cern.ch/index.php/s/9YTAMPCTmTR800L)
* [Dev notes (codimd)](https://codimd.web.cern.ch/s/HJp-5E8JB)
* [JIRA board](https://its.cern.ch/jira/secure/RapidBoard.jspa?projectKey=TESTBEAMSOFT&rapidView=6446)

## External packages

#### Lightweight C++ command line option parser
* [checkout for v2.1.2 release (January 13, 2019)](https://github.com/jarro2783/cxxopts/releases/tag/v2.1.2)
