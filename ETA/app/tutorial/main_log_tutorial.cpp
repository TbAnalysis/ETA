#include <iostream>
#include "TbGaudi.h"
#include "TbStyle.h"
#include "TbLog.h"

int main(int argc, char **argv)
{
    TbGaudi::Severity("Debug");

    TbLog::Info("TbLog test, changing console messages level to warn");
    TbLog::SetLevel("WARN");
    TbLog::Info("Doesn't work, it should not be visible");
    TbLog::Warn("It works");
    TbLog::AddLogger("usr");

    TbLog::SetLevel("default", "TRACE");

    TbLog::Debug("usr", "User defined logger test");

    TbLog::Info("try", "Non-existing logger test");
    TbLog::Info("Another default logger message");
   
    TbLog::AddLogger("info_test", "info");
    TbLog::Debug("info_test", "This should not be visible");
    TbLog::Warn("info_test", "This should be visible");
    
    std::exit(EXIT_SUCCESS);
}

