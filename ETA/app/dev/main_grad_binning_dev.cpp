#include "TbGaudi.h"
#include "Dut.h"
#include "DutBinning.h"
#include "TbLog.h"
#include "TbJob.h"
#include "TbJobCollector.h"
#include "TbResults.h"
#include <TROOT.h>
#include <TApplication.h>

int main(int argc, char **argv){

    auto rootapp = std::make_unique<TApplication>("App", &argc, argv);

    TbLog::Info("Gradient binning dev app...");

    TbGaudi::Severity("Debug"); // TODO: Should care about TbLog::SetLevel("debug");
    TbLog::SetLevel("debug");

    Dut::DefaultIrradiationStatus(true);

    DutBinning::DefaultMethod("Uniform");
    //DutBinning::DefaultMethod("Gradient");

    auto job = std::make_shared<TbJob>();

    // Define obligatory job parameterization
    job->SetParam("DutName", "S8");
    job->SetParam("RunNumber", "12345");
    job->SetParam("RunBiasVoltage", "100");
    job->SetParam("DutNBins", "10");

    // Add the job to the main store..
    // Note: needed to use global tools from the framework (jobs are being picked up from the store).
    TbJobCollector::GetInstance()->AddTbJob(job);

    auto dut = job->DutPtr();
    dut->DutBinningPtr()->Draw();
    // TODO: flnProfile->Draw("DUT"); // Binning: DUT or Pixel

    //TbResults::ExportResults(); // export all types of produced results: pdf, csv, ntuple
    
    rootapp->Run(!gROOT->IsBatch());
    
    std::exit(EXIT_SUCCESS);
}

